source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.3'
gem 'bootstrap-sass', '~> 3.3.7'
gem 'font-awesome-rails', '4.7.0.1'

# Use sqlite3 as the database for Active Record
gem 'sqlite3'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0.4'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# BYEBUGGG!!!
gem 'byebug'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :development, :test do
  gem 'railroady'
end

group :production do
  gem 'passenger'
end

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.1.2'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# For register changes in models
gem 'audited'

# Use debugger
# gem 'debugger', group: [:development, :test]
#gem 'pg'
gem 'breadcrumbs_on_rails'

# Gems for external databases engines
gem 'mysql2', '~> 0.3.18'              # External Mysql connections - Aurelia (Old) , Micronauta
gem "tiny_tds"                         # External MSSQL connections - PXP

# For reset primary keys
gem 'activerecord-reset-pk-sequence'

# ADDONS

  # For cities-states-countries
  gem 'countries'
  gem 'city-state'

# COMFORTABLY
  gem 'colorize'
  gem 'awesome_print'

# USERS MANAGEMENT
  gem 'devise'
  gem 'devise_lastseenable'

# Features humanized
  gem 'rails-timeago'

# Statuses for all Models
  gem 'simplest_status'

# For translations of errors in ActiveRecords
  gem 'dynamic_form'

# Jquery Rails
  gem 'jquery-rails'

# Rolify for users
  gem "rolify"

# Authorization
  gem 'cancancan'

# WSDL
  gem 'savon'

# Paper clip
  gem "paperclip", "~> 5.0.0"

# Progress bar
  gem 'ruby-progressbar'

# For searchings
  gem 'ransack'

# For pagination
  gem 'kaminari'

# Delayed jobs
  gem 'delayed_job_active_record'

# Tasksssss
  gem 'daemons'

# Show progress
  gem 'delayed_job_progress'

# CSV utils
  gem 'rchardet'
  gem 'acsv'

# For parsing XLS
  gem 'spreadsheet'

# For creating crontab list
  gem "whenever", :require => false

# FOr graphs
  gem 'rails-erd', require: false, group: :development

# string-similarity
  gem 'string-similarity'

# for making pdfs
  gem 'wicked_pdf' # apt install wkhtmltopdf
  gem 'wkhtmltopdf-binary'


# Telegram
  gem 'telegram-bot-ruby'
