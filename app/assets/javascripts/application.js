// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
//
//= require jquery/jquery-3.1.1.min.js
//= require bootstrap-sprockets
//= require metisMenu/jquery.metisMenu.js
//= require pace/pace.min.js
//= require peity/jquery.peity.min.js
//= require slimscroll/jquery.slimscroll.min.js
//= require inspinia.js
//= require rails-timeago
//= require locales/jquery.timeago.es.js
//= require jquery_ujs

//= require sweetalert/sweetalert.min.js
//= require toastr/toastr.min.js
//= require select2/select2.full.min.js
//= require select2/select2.locale_es.js

// For set spanish language for timeago jQuery plugin
jQuery.timeago.settings.lang = 'es'
$.fn.select2.defaults.set('language', 'es');

// Just to show menu, when all is charged
$(document).ready(function() {
  $('#side-menu').css("display", "block");
  $('.modal').on('hidden.bs.modal', function(e) { $(this).removeData('bs.modal') }) ;
});
