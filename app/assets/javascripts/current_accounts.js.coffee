# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
//= require fullcalendar/moment.min.js
//= require fullcalendar/fullcalendar.min.js
//= require fullcalendar/locale/es.js

//= require iCheck/icheck.min.js

//= require datapicker/bootstrap-datepicker.js
//= require datapicker/locales/bootstrap-datepicker.es.js
//= require jasny/jasny-bootstrap.min.js


$ ->
  $('.dropdown-menu li a').click ->
    $('#current_account_kind').val( $(this).data("key") )
    $('#current_account_kind_dropdown').html( $(this).text() + ( " <span class='caret'></span>" ))
    $('#current_account_kind_dropdown').val( $(this).data("key") )
    return
  return
