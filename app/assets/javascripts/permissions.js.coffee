//= require iCheck/icheck.min.js

$ ->
  $('.i-checks').iCheck
    checkboxClass: 'icheckbox_square-green'
    radioClass: 'iradio_square-green'
  return
