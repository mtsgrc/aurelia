class AgenciesController < ApplicationController
  load_and_authorize_resource

  before_action :set_agency, only: [:show, :edit, :update, :destroy]

  def index
    prepare_index
  end

  def show
    render :edit
  end

  def new
    @agency = Agency.new
  end

  def edit
    redirect_to( agencies_path ) unless @agency.can_be_modified?
  end

  def create
    @agency = Agency.new(agency_params)
    respond_to do |format|
      if @agency.save
        format.html { redirect_to @agency, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@agency.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @agency }
      else
        format.html { render action: 'new' }
        format.json { render json: @agency.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
		redirect_to( agencies_path ) unless @agency.can_be_modified?
    respond_to do |format|
      if @agency.update(agency_params)
        format.html { redirect_to @agency, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@agency.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @agency.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @agency.status = Agency::DELETED
    @agency.save
    respond_to do |format|
      format.html { redirect_to agencies_url }
      format.json { head :no_content }
    end
  end

  # -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_agency
      @agency = Agency.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def agency_params
      params.require( :agency ).permit( :code , :name , :status )
    end

end
