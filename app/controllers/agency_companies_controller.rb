class AgencyCompaniesController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_agency_company, only: [:show, :edit, :update, :destroy]

  def index
    prepare_index( { :joins => { :agency => :parent } , :order => "agencies.parent_id asc" } )
  end

  def show
    render :edit
  end

  def new
    @agency_company = AgencyCompany.new
  end

  def edit
    redirect_to( agency_companies_path ) unless @agency_company.can_be_modified?
  end

  def create
    @agency_company = AgencyCompany.new(agency_company_params)
    respond_to do |format|
      if @agency_company.save
        format.html { redirect_to @agency_company, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@agency_company.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @agency_company }
      else
        format.html { render action: 'new' }
        format.json { render json: @agency_company.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
		redirect_to( agency_companies_path ) unless @agency_company.can_be_modified?
    respond_to do |format|
      if @agency_company.update(agency_company_params)
        format.html { redirect_to @agency_company, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@agency_company.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @agency_company.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @agency_company.status = AgencyCompany::DELETED
    @agency_company.save
    respond_to do |format|
      format.html { redirect_to agency_companies_url }
      format.json { head :no_content }
    end
  end

  def get_edit_credit_maximum
    @agency_company = AgencyCompany.find( params[ "agency_company_id" ] )

    session[:return_to] = request.referer
    render :layout => false
  end


  # Method finally to create current account movement
  def post_edit_credit_maximum

    @agency_company = AgencyCompany.find( params[ "agency_company_id" ] )
    credit_maximum = params[ "agency_company" ][ "credit_maximum" ]

    begin
      if @agency_company
        @agency_company.credit_maximum = credit_maximum
        if @agency_company.save
          notice_to_user( @agency_company , [ "Se modificó correctamente el crédito máximo de la agencia/empresa => #{@agency_company.agency.detail} // #{@agency_company.company.detail} " ] , :success )
        else
          notice_to_user( @agency_company , @agency_company.errors.full_messages , :errors )
        end
      end
    rescue Exception => except
      notice_to_user( @agency_company , @agency_company.errors.full_messages , :errors )
    end
    redirect_to session.delete( :return_to )

  end


  # -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------


  private

    # Use callbacks to share common setup or constraints between actions.
    def set_agency_company
      @agency_company = AgencyCompany.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def agency_company_params
      params.require( :agency_company ).permit( :credit_maximum )
    end

end
