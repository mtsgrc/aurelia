class ApplicationController < ActionController::Base

  # Check authorization
  check_authorization :unless => :devise_controller?, :except=>[ :filter_search , :get_homologate , :post_homologate ]

  # APP CONFIGURATION EXTRA

  # This is for translations of time ago... More of one year is the limit
  Rails::Timeago.default_options :limit => proc { 1.year.ago }, :nojs => true, :lang => :es

  # Draw certain views without menuses and all that
  layout :layout

  # Auth
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?

  # Prevent CSRF attacks
  protect_from_forgery with: :exception

  #####################################################################################################################################
  ############################################################ [ METHODS ] ############################################################

  def get_homologate

    hash_current_instance_id = params.except( *[ "controller" , "action" ] )

    if hash_current_instance_id.keys.include?( "field_name" )
      @attribute_name_ref       = "#{hash_current_instance_id[ 'field_name' ]}_id"
      @this_table_name_sing     = hash_current_instance_id.except( *[ "field_name" ] ).keys.first.gsub( "_id" , "" )
      @model_this               = @this_table_name_sing.pluralize.classify.constantize
      model_ref                 = hash_current_instance_id["field_name"].pluralize.classify.constantize
    else
      @attribute_name_ref       = "parent_id"
      @this_table_name_sing     = hash_current_instance_id.keys.first.gsub( "_id" , "" )
      @model_this               = @this_table_name_sing.pluralize.classify.constantize
      model_ref                 = @model_this # is the same model
    end


    this_instance_id        = hash_current_instance_id[ "#{@this_table_name_sing}_id" ]
    @model_local_instances  = model_ref.local
    @model_local_instance   = @model_this.find( this_instance_id )
    @attribute_ref_id       = eval( "@model_local_instance.#{@attribute_name_ref}" )

    session[:return_to] = request.referer
    render "layouts/get_homologate.erb", :layout => false

  end

  def post_homologate

    hash_current_instances    = params.except( *[ "controller" , "action" , "utf8" , "authenticity_token" ] )
    hash_current_instance_id  = hash_current_instances.except( *[ "homologation" ] )
    hash_ref_instance_id      = hash_current_instances.slice( *[ "homologation" ] )["homologation"]

    model_current             = hash_current_instance_id.keys.first.gsub("_id","").pluralize.classify.constantize
    model_ref                 = ( hash_ref_instance_id.keys.first.gsub("_id","") == "parent" ? hash_current_instance_id.keys.first.gsub("_id","").pluralize.classify.constantize : hash_ref_instance_id.keys.first.gsub("_id","").pluralize.classify.constantize)

    if model_ref

      model_current_instance_id = hash_current_instance_id.values.first
      model_current_instance    = model_current.find( model_current_instance_id )

      model_ref_instance_id = hash_ref_instance_id.values.first
      model_ref_instance = model_ref.find( model_ref_instance_id )

      if model_current_instance.present? and model_ref_instance.present?

        ret_update = model_current_instance.update( hash_ref_instance_id.keys.first.to_sym => model_ref_instance.id )

        if ret_update == true
          notice_to_user( model_current_instance , [ "Se asignó el recurso padre al recurso indicado" ] , :success )
        else
          notice_to_user( model_current_instance , model_current_instance.keys.map{ |instance| instance.errors.full_messages }.flatten , :errors )
        end

      end

    end
    redirect_to session.delete( :return_to )
  end

  # Method for return if the instance is persisted or is new... Instead of find_or_create, that returns the instance created always
  def find_or_initialize_by(attributes, &block)
    find_by(attributes) || new(attributes, &block)
  end

  def traverse(obj, &blk)
    case obj
    when Hash
      # Forget keys because I don't know what to do with them
      obj.each {|k,v| traverse(v, &blk) }
    when Array
      obj.each {|v| traverse(v, &blk) }
    else
      blk.call(obj)
    end
  end

  # Global set status controller method
  helper_method :set_status
  def set_status
    instance = controller_name.classify.constantize.find( params[:id] )
    instance.set_status( params[:status] )
    redirect_to action: :index
  end

  # To show sweet-alert with messages - if message starts with - , will be replaced by an fa-angle-right from font-awesome
  def notice_to_user instance , messages , type = :success
    flash[ :notice ] = { type => messages.map{ |msg| ( "<p>#{ "<i class='fa fa-angle-right fa-fw'></i> " if msg.starts_with?("-") }" + ( msg.starts_with?("-") ? msg.from(1) : msg ) + "</p>" ) }.join.html_safe , :instance => instance.detail }
  end


  rescue_from CanCan::AccessDenied do |exception|
    Rails.logger.debug "[PERMISO DENEGADO] #{exception.action} > #{exception.subject.inspect}"
    @action = exception.action
    if @class_instances
      @model = @class_instances
    else
      @model = controller_name.classify.constantize
    end
    render :file => "public/unauthorized.html.erb", :status => :unauthorized, :layout => false
  end


  def filter_search

    records = { :items => [] , :total => 0 }
#byebug
    if params[:q].present?

      param_attr        = params[ :q ][ :attr_name ]  if params[ :q ][ :attr_name ].present?
      param_term        = params[ :q ][ :term ]       if params[ :q ][ :term ].present?
      param_page        = params[ :q ][ :page ]       if params[ :q ][ :page ].present?
      param_page_limit  = params[ :q ][ :page_limit ] if params[ :q ][ :page_limit ].present?

      if param_attr.present? and param_term.present?

        model_ref = param_attr.gsub("_id","").pluralize.classify.constantize

        if model_ref.present?

          model_attr_names = model_ref.attribute_names

          attrs_where_to_search = model_attr_names.map{ | attribute_name | attribute_name if attribute_name =~ /name/ }.compact

          arr_like_search = []

          param_term = param_term.gsub( ' ' , '%' )

          attrs_where_to_search.each do |attr_where_to_search|
            arr_like_search << "#{attr_where_to_search} LIKE '%#{param_term}%'"
          end

          if model_attr_names.include?( "system_id" )
            instances = model_ref.local.where( arr_like_search.join( ' OR ' ) ).select( ( [ "id"] + attrs_where_to_search ) )
          else
            instances = model_ref.where( arr_like_search.join( ' OR ' ) ).select( ( [ "id"] + attrs_where_to_search ) )
          end

          instances = instances.map{ |instance| { :id => instance[:id] , :text => attrs_where_to_search.map{ |attr| instance[attr] }.join(" ") } }

          records = { :items => instances , :total => instances.size }

        end

      end

    end

    render json: records.to_json, callback: params[ :callback ]

  end



  private # ==========================================================================================================================


    def prepare_index( options = {} , instances_ref = nil )

      # los filtros que apuntan directamente sobre las instancias
      q_param             = params[ :q ]

      if options[ :not_paginate ].present? and options[ :not_paginate ] == true

        @per_page = nil

      else

        @per_page = Kaminari.config.default_per_page
        if q_param
          @page = q_param[ :page ]
          if q_param[ :per_page ]
            if q_param[ :per_page ] == "*"
              @per_page = nil
            else
              @per_page = q_param[ :per_page ]
            end
          end
        end

      end

      @filters = clean_params

      # Si esta definido class_instances filtramos las instances de dicha clase, de lo contrario si se existe @instances, filtramos esos elementos, sino las instances del controlador
      if @class_instances
        @q = @class_instances.ransack clean_params
      else
        if instances_ref.present?
          if instances_ref
            @q = instances_ref.sample.class.where( :id => instances_ref.map(&:id) ).ransack clean_params
            # @q = instances_ref.ransack clean_params
          else
            @q = controller_name.classify.constantize.ransack clean_params
          end
        else
          if @instances.present?
            @q = @instances.ransack clean_params
          else
            clean_params_mod = ( clean_params.map do | key , value |
              if key.ends_with?( "_id_parent_eq" )
                attr_ref = key.gsub( "_id_parent_eq" , "_id_in" )
                model_ref = key.gsub( "_id_parent_eq" , "" ).pluralize
                { attr_ref => model_ref.classify.constantize.where( :parent_id => value ).map(&:id) }
              else
                { key => value }
              end
            end )
            @q = controller_name.classify.constantize.ransack clean_params_mod.reduce({},:merge)
          end
        end
      end

      # if @page.to_i > @q.result.page( @page ).total_pages
      if options[ :not_paginate ].present? and options[ :not_paginate ] == true

        @page = nil

      else

        if @page.to_i > @q.result.page( @page ).per( @per_page ).total_pages
          @page = @q.result.page( @page ).total_pages
          q_param[ :page ] = @page if q_param[ :page ]
        end

      end
# byebug
      if options.include?( :order )
        if options.include?( :joins )
          if instances_ref.present?
            instances_ref = @q.result.joins( options[ :joins ] ).order( options[ :order ] ).page( @page ).per( @per_page )
            return instances_ref
          else
            if options[ :not_paginate ].present? and options[ :not_paginate ] == true
              @instances = @q.result.joins( options[ :joins ] ).order( options[ :order ] )
            else
              @instances = @q.result.joins( options[ :joins ] ).order( options[ :order ] ).page( @page ).per( @per_page )
            end
          end
        else
          if instances_ref.present?
            instances_ref = @q.result.order( options[ :order ] ).page( @page ).per( @per_page )
            return instances_ref
          else
            if options[ :not_paginate ].present? and options[ :not_paginate ] == true
              @instances = @q.result.joins( options[ :joins ] ).order( options[ :order ] )
            else
              @instances = @q.result.order( options[ :order ] ).page( @page ).per( @per_page )
            end
          end
        end
      else
        if instances_ref.present?
          instances_ref = @q.result.page( @page ).per( @per_page )
          return instances_ref
        else
          if options[ :not_paginate ].present? and options[ :not_paginate ] == true
            @instances = @q.result.joins( options[ :joins ] ).order( options[ :order ] )
          else
            @instances = @q.result.page( @page ).per( @per_page )
          end
        end
      end

    end

    # Show filters? if any params is passed
    def clean_params
      if params[:q]
        allparams = params[:q]
        allparams = allparams.except("utf8")        if allparams["utf8"]
        allparams = allparams.except("controller")  if allparams["controller"]
        allparams = allparams.except("action")      if allparams["action"]
        allparams = allparams.except("page")        if allparams["page"]
        allparams = allparams.except("per_page")    if allparams["per_page"]

        # This is for convert to array a param value that is an array
        allparams = ( allparams.map do |cond_name,values|
          if cond_name.ends_with?( *["_in","_all","_any"] )
            [cond_name,values.split(',')]
          else
            [cond_name,values]
          end
        end ).to_h

      else
        {}
      end
    end

    # For specific controllers we will show distinct view
    def layout
      # is_a?(Devise::SessionsController) or is_a?(Users::PasswordsController) ? false : "application"
      if is_a?( Devise::SessionsController )
        false
      elsif is_a?( Users::PasswordsController )
        false
      else
        "application"
      end
    end

    def helper
      @helper ||= Class.new do
        include ActionView::Helpers::NumberHelper
      end.new
    end

  protected # ==========================================================================================================================

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_in, keys: [:username])
    end

    # Derive the model name from the controller. egs UsersController will return User
    def self.permission
      return name = controller_name.classify.constantize
    end

end
