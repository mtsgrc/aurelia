class BankAccountCardsController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_bank_account_card, only: [:show, :edit, :update, :destroy]

  def index
    prepare_index
  end

  def show
    render :edit
  end

  def new
    @bank_account_card = BankAccountCard.new
  end

  def edit
    redirect_to( bank_account_cards_path ) unless @bank_account_card.can_be_modified?
  end

  def create
    @bank_account_card = BankAccountCard.new(bank_account_card_params)
    respond_to do |format|
      if @bank_account_card.save
        format.html { redirect_to @bank_account_card, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@bank_account_card.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @bank_account_card }
      else
        format.html { render action: 'new' }
        format.json { render json: @bank_account_card.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
		redirect_to( bank_account_cards_path ) unless @bank_account_card.can_be_modified?
    respond_to do |format|
      if @bank_account_card.update(bank_account_card_params)
        format.html { redirect_to @bank_account_card, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@bank_account_card.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @bank_account_card.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @bank_account_card.status = BankAccountCard::DELETED
    @bank_account_card.save
    respond_to do |format|
      format.html { redirect_to bank_account_cards_url }
      format.json { head :no_content }
    end
  end

  # -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_bank_account_card
      @bank_account_card = BankAccountCard.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bank_account_card_params
      params.require( :bank_account_card ).permit( :code , :name , :status , :bank_id , :agency_id )
    end

end
