class BankAccountMovementsController < ApplicationController
  load_and_authorize_resource
  
  	respond_to :html, :json

  	before_action :set_bank_account_movement, only: [:show, :edit, :update, :destroy]

    def index
      @class_instances = BankAccountMovement
      prepare_index( { :order => { :created_at => :desc } } )
    end

  	def modal_show
  	end

    def show
  		render :edit
    end

    def new
      @bank_account_movement = BankAccountMovement.new
    end

    def edit
      redirect_to( bank_account_movements_path ) unless @bank_account_movement.can_be_modified?
    end

    def get_new_bank_account_card_from_bank_account_movement_detail
      @bank_account_movement = BankAccountMovement.find( params[ "bank_account_movement_id" ] )
      @agencies = Agency.local

      session[:return_to] = request.referer
      render :layout => false

    end

    def post_new_bank_account_card_from_bank_account_movement_detail
      @agencies = Agency.local

      @bank_account_movement  = BankAccountMovement.find( params[ "bank_account_movement_id" ] )
      @agency                 = Agency.find( params[ "bank_account_card" ][ "agency_id" ] )

      if @agency

        bank_account_card = BankAccountCard.new

        bank_account_card.code = "#{@bank_account_movement.bank_account.bank.code}@#{@bank_account_movement.detail}"
        bank_account_card.name = "#{@bank_account_movement.detail}"
        bank_account_card.number = "#{@bank_account_movement.detail}"
        bank_account_card.id_number = "#{@bank_account_movement.detail}"
        bank_account_card.bank_account_id = "#{@bank_account_movement.bank_account.id}"
        bank_account_card.agency_id = "#{@agency.id}"

        return_generation_of_bank_account_card = bank_account_card.save
        if return_generation_of_bank_account_card
          notice_to_user( bank_account_card , [ "Se creó correctamente la identificación #{@bank_account_movement.detail} asociada a #{@agency.detail}" ] , :success )
          @bank_account_movement.bank_account_card_id = bank_account_card.id
          if @bank_account_movement.save
            bank_account_movements_identical_detail = BankAccountMovement.where( :detail => @bank_account_movement.detail )
            if bank_account_movements_identical_detail.present?
              bank_account_movements_identical_detail.each do |another_bank_account_movement_with_identical_detail|
                another_bank_account_movement_with_identical_detail.search_bank_account_card_by_detail_and_assign
              end
            end
          end
        else
          notice_to_user( bank_account_card , bank_account_card.errors.full_messages , :errors )
        end
      end

      redirect_to ( session[ :return_to ] ||= request.referer )

    end

    # Request view for choose which current account will be affected_totally with the new movement
    def get_new_current_account_movement_from_bank_account_movement
      @bank_account_movement = BankAccountMovement.find( params[ "bank_account_movement_id" ] )
      @current_accounts = CurrentAccount.all
      session[:return_to] = request.referer
      render :layout => false
    end

    # Method finally to create current account movement
    def post_new_current_account_movement_from_bank_account_movement
      @current_accounts       = CurrentAccount.all
      @bank_account_movement  = BankAccountMovement.find( params[ "bank_account_movement_id" ] )

      @current_account        = CurrentAccount.find( params[ "current_account_movement" ][ "current_account_id" ] )
      begin
        if @current_account
          return_generation_of_movement = @current_account.generate_movement( @bank_account_movement )
          if return_generation_of_movement
            @bank_account_movement.status = BankAccountMovement.statuses[ :affected_totally ]
            if @bank_account_movement.save
              notice_to_user( @bank_account_movement , [ "Se creó correctamente el movimiento en la cuenta corriente #{@current_account.detail}" ] , :success )
            else
              notice_to_user( @bank_account_movement , @bank_account_movement.errors.full_messages , :errors )
            end
          else
            notice_to_user( @current_account , @current_account.errors.full_messages , :errors )
          end
        end
      rescue Exception => except
        notice_to_user( @current_account , @current_account.errors.full_messages , :errors )
      end
      redirect_to session.delete( :return_to )

    end

    # Request view for choose which current account will be affected with the new movement
    def get_new_current_account_movements_from_bank_account_movement
      @bank_account_movement = BankAccountMovement.find( params[ "bank_account_movement_id" ] )
      @current_accounts = CurrentAccount.all

      session[:return_to] = request.referer
      render :layout => false

    end

    # Method finally to create current account movement
    def post_new_current_account_movements_from_bank_account_movement

      ActiveRecord::Base.transaction do

        @bank_account_movement      = BankAccountMovement.find( params[ "bank_account_movement_id" ] )
        current_account_movements   = params[ "current_accounts_movements" ]

        @hash_current_account_movements = current_account_movements.map{ |current_account_movement| { CurrentAccount.find( current_account_movement[ "current_account_id" ] ) => current_account_movement[ "amount" ].to_f } }.reduce( {} , :merge )
        @ret_current_account_movements  = @hash_current_account_movements.dup.map{ | current_account , amount | { current_account => nil } }.reduce( {} , :merge )

        if @hash_current_account_movements.values.sum.to_f.round(4) <= @bank_account_movement.remaining_amount.to_f.round(4)

          @hash_current_account_movements.each do | current_account , amount |
            @ret_current_account_movements[ current_account ] = current_account.generate_movement( @bank_account_movement , amount )
          end

          if @ret_current_account_movements.values.uniq.first == true
            if @bank_account_movement.remaining_amount == 0
              @bank_account_movement.status = BankAccountMovement.statuses[ :affected_totally ]
            elsif ( @bank_account_movement.remaining_amount > 0 ) and ( @bank_account_movement.remaining_amount < @bank_account_movement.amount )
              @bank_account_movement.status = BankAccountMovement.statuses[ :affected_partial ]
            end
            if @bank_account_movement.save
              notice_to_user( @bank_account_movement , [ "Se creó correctamente el movimiento para cada cuenta corriente" ] + @hash_current_account_movements.map{ |current_account,amount| "-<strong>#{ helper.number_to_currency( amount ) }</strong> para <strong>#{current_account.detail}</strong>".html_safe } , :success )
            else
              notice_to_user( @bank_account_movement , @bank_account_movement.errors.full_messages , :errors )
            end
          else
            notice_to_user( @bank_account_movement , @ret_current_account_movements.keys.map{ |cam| cam.errors.full_messages }.flatten , :errors )
          end

        else
          notice_to_user( @bank_account_movement , [ "La suma de los importes ( #{ helper.number_to_currency( @hash_current_account_movements.values.sum ) } ) debe ser menor o igual al monto disponible para distribuirse ( #{ helper.number_to_currency( @bank_account_movement.remaining_amount.to_f.round(4) ) } )" , "<br />".html_safe ] + @hash_current_account_movements.map{ |current_account,amount| "-<strong>#{ helper.number_to_currency( amount ) }</strong> para <strong>#{current_account.detail}</strong>".html_safe } , :errors )
        end

      end

      redirect_to session.delete( :return_to )

    end

  	# ---------------------------- CREATE (POST) | UPDATE (PUT) | DESTROY (DELETE) ----------------------------

    def create
      @bank_account_movement = BankAccountMovement.new(bank_account_movement_params)
      respond_to do |format|
        if @bank_account_movement.save
          format.html { redirect_to @bank_account_movement, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@bank_account_movement.detail}</strong>".html_safe }
          format.json { render action: 'show', status: :created, location: @bank_account_movement }
        else
          format.html { render action: 'new' }
          format.json { render json: @bank_account_movement.errors, status: :unprocessable_entity }
        end
      end
    end

    # def update
    #   redirect_to( bank_account_movements_path ) unless @bank_account_movement.can_be_modified?
    #   respond_to do |format|
    #     if @bank_account_movement.update(bank_account_movement_params)
    #       format.html { redirect_to @bank_account_movement, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@bank_account_movement.detail}</strong>".html_safe }
    #       format.json { head :no_content }
    #     else
    #       format.html { render action: 'edit' }
    #       format.json { render json: @bank_account_movement.errors, status: :unprocessable_entity }
    #     end
    #   end
    # end

    def destroy
  		@bank_account_movement.status = BankAccountMovement::DELETED
  		@bank_account_movement.save
      respond_to do |format|
        format.html { redirect_to bank_account_movements_url }
        format.json { head :no_content }
      end
    end

  	# -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  	private

      # Use callbacks to share common setup or constraints between actions.
      def set_bank_account_movement
        @bank_account_movement = BankAccountMovement.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
  		def bank_account_movement_params
  	    params.require(:bank_account_movement).permit( :bank_account_id , :kind, :status )
  	  end

end
