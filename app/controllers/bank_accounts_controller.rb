class BankAccountsController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_bank_account, only: [:show, :edit, :update, :destroy]

  def index
    prepare_index
  end

  def show
    render :edit
  end

  def new
    @bank_account = BankAccount.new
  end

  def edit
    redirect_to( bank_accounts_path ) unless @bank_account.can_be_modified?
  end

  def create
    @bank_account = BankAccount.new(bank_account_params)
    respond_to do |format|
      if @bank_account.save
        format.html { redirect_to @bank_account, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@bank_account.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @bank_account }
      else
        format.html { render action: 'new' }
        format.json { render json: @bank_account.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
		redirect_to( bank_accounts_path ) unless @bank_account.can_be_modified?
    respond_to do |format|
      if @bank_account.update(bank_account_params)
        format.html { redirect_to @bank_account, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@bank_account.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @bank_account.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @bank_account.status = BankAccount::DELETED
    @bank_account.save
    respond_to do |format|
      format.html { redirect_to bank_accounts_url }
      format.json { head :no_content }
    end
  end

  # -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_bank_account
      @bank_account = BankAccount.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bank_account_params
      params.require( :bank_account ).permit( :code , :name , :status , :bank_id)
    end

end
