class BankBranchesController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_bank_branch, only: [:show, :edit, :update, :destroy]

  def index
    prepare_index
  end

  def show
    render :edit
  end

  def new
    @bank_branch = BankBranch.new
  end

  def edit
    redirect_to( bank_branches_path ) unless @bank_branch.can_be_modified?
  end

  def create
    @bank_branch = BankBranch.new(bank_branch_params)
    respond_to do |format|
      if @bank_branch.save
        format.html { redirect_to @bank_branch, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@bank_branch.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @bank_branch }
      else
        format.html { render action: 'new' }
        format.json { render json: @bank_branch.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
		redirect_to( bank_branches_path ) unless @bank_branch.can_be_modified?
    respond_to do |format|
      if @bank_branch.update(bank_branch_params)
        format.html { redirect_to @bank_branch, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@bank_branch.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @bank_branch.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @bank_branch.status = BankBranch::DELETED
    @bank_branch.save
    respond_to do |format|
      format.html { redirect_to bank_branches_url }
      format.json { head :no_content }
    end
  end

  # -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_bank_branch
      @bank_branch = BankBranch.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bank_branch_params
      params.require( :bank_branch ).permit( :code , :name , :status , :bank_id)
    end

end
