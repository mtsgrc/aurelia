class BanksController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_bank, only: [:show, :edit, :update, :destroy]

  def index
    prepare_index
  end

  def show
    render :edit
  end

  def new
    @bank = Bank.new
  end

  def edit
    redirect_to( banks_path ) unless @bank.can_be_modified?
  end

  def create
    @bank = Bank.new(bank_params)
    respond_to do |format|
      if @bank.save
        format.html { redirect_to @bank, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@bank.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @bank }
      else
        format.html { render action: 'new' }
        format.json { render json: @bank.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
		redirect_to( banks_path ) unless @bank.can_be_modified?
    respond_to do |format|
      if @bank.update(bank_params)
        format.html { redirect_to @bank, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@bank.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @bank.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @bank.status = Bank::DELETED
    @bank.save
    respond_to do |format|
      format.html { redirect_to banks_url }
      format.json { head :no_content }
    end
  end

  # -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_bank
      @bank = Bank.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bank_params
      params.require( :bank ).permit( :code , :name , :status )
    end

end
