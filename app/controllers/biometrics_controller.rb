class BiometricsController < ApplicationController
  load_and_authorize_resource
  before_action :set_biometric, only: [:show, :edit, :update, :destroy]

  def index
    # @biometrics = Biometric::statuses.map{ |v,k| { v.name => Biometric.where(:status => v.value).order(created_at: :desc) } }.reduce(:merge)
    # @biometrics = Biometric.all.order(created_at: :asc)
    prepare_index
  end

  def show
    render :edit
  end

  def new
    @biometric = Biometric.new
    @agencies = Agency.local
  end

  def edit
    redirect_to( biometrics_path ) unless @biometric.can_be_modified?
    @agencies = Agency.local
  end

  def create
    @biometric = Biometric.new( biometric_params )
    @biometric.user = current_user

    @agencies = Agency.local

    respond_to do |format|
      if @biometric.save
        format.html { redirect_to @biometric, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@biometric.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @biometric }
      else
        format.html { render action: 'new' }
        format.json { render json: @biometric.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( biometrics_path ) unless @biometric.can_be_modified?
    @agencies = Agency.local
    respond_to do |format|
      if @biometric.update(biometric_params)
        format.html { redirect_to @biometric, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@biometric.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @biometric.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @biometric.status = Biometric::DELETED
    @biometric.save
    respond_to do |format|
      format.html { redirect_to biometrics_url }
      format.json { head :no_content }
    end
  end

  # ------------------------------ SPECIAL ACTIONS ------------------------------

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_biometric
      @biometric = Biometric.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def biometric_params
      params.require(:biometric).permit( :code , :name , :agency_id )
    end
end
