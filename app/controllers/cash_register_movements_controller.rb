class CashRegisterMovementsController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_cash_register_movement, only: [:show, :edit, :update, :destroy]

  def index
    cash_register_id = params[ :cash_register_id ]
    # @instances = CashRegisterMovement.where( :cash_register_id => cash_register_id )
    @cash_register = CashRegister.find( cash_register_id )
    order_by = { :id => :desc }
    @instances = @cash_register.movements.order( order_by )
    prepare_index
  end

  def show
    render :edit
  end

  def new
    @cash_register_movement = CashRegisterMovement.new()
  end

  def edit
    redirect_to( cash_register_movements_path ) unless @cash_register_movement.can_be_modified?
  end

  def create
    @cash_register_movement = CashRegisterMovement.new(cash_register_movement_params)
    respond_to do |format|
      if @cash_register_movement.save
        format.html { redirect_to @cash_register_movement, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@cash_register_movement.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @cash_register_movement }
      else
        format.html { render action: 'new' }
        format.json { render json: @cash_register_movement.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
		redirect_to( cash_register_movements_path ) unless @cash_register_movement.can_be_modified?
    respond_to do |format|
      if @cash_register_movement.update(cash_register_movement_params)
        format.html { redirect_to @cash_register_movement, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@cash_register_movement.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @cash_register_movement.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @cash_register_movement.status = CashRegisterMovement::DELETED
    @cash_register_movement.save
    respond_to do |format|
      format.html { redirect_to cash_register_movements_url }
      format.json { head :no_content }
    end
  end

  # Request view for choose which current account will be affected_totally with the new movement
  def get_new_current_account_movement_from_cash_register_movement
    @cash_register_movement = CashRegisterMovement.find( params[ "cash_register_movement_id" ] )
    @current_accounts = CurrentAccount.all
    session[:return_to] = request.referer
    render :layout => false
  end

  # Method finally to create current account movement
  def post_new_current_account_movement_from_cash_register_movement
    @current_accounts       = CurrentAccount.all
    @cash_register_movement  = CashRegisterMovement.find( params[ "cash_register_movement_id" ] )

    @current_account        = CurrentAccount.find( params[ "current_account_movement" ][ "current_account_id" ] )
    begin
      if @current_account
        return_generation_of_movement = @current_account.generate_movement( @cash_register_movement )
        if return_generation_of_movement
          @cash_register_movement.status = CashRegisterMovement.statuses[ :affected_totally ]
          if @cash_register_movement.save
            notice_to_user( @cash_register_movement , [ "Se creó correctamente el movimiento en la cuenta corriente #{@current_account.detail}" ] , :success )
          else
            notice_to_user( @cash_register_movement , @cash_register_movement.errors.full_messages , :errors )
          end
        else
          notice_to_user( @current_account , @current_account.errors.full_messages , :errors )
        end
      end
    rescue Exception => except
      notice_to_user( @current_account , @current_account.errors.full_messages , :errors )
    end
    redirect_to session.delete( :return_to )

  end

  # Request view for choose which current account will be affected with the new movement
  def get_new_current_account_movements_from_cash_register_movement
    @cash_register_movement = CashRegisterMovement.find( params[ "cash_register_movement_id" ] )
    @current_accounts = CurrentAccount.all

    session[:return_to] = request.referer
    render :layout => false

  end

  # Method finally to create current account movement
  def post_new_current_account_movements_from_cash_register_movement

    ActiveRecord::Base.transaction do

      @cash_register_movement      = CashRegisterMovement.find( params[ "cash_register_movement_id" ] )
      current_account_movements   = params[ "current_accounts_movements" ]

      @hash_current_account_movements = current_account_movements.map{ |current_account_movement| { CurrentAccount.find( current_account_movement[ "current_account_id" ] ) => current_account_movement[ "amount" ].to_f } }.reduce( {} , :merge )
      @ret_current_account_movements  = @hash_current_account_movements.dup.map{ | current_account , amount | { current_account => nil } }.reduce( {} , :merge )

      if @hash_current_account_movements.values.sum.to_f.round(4) <= @cash_register_movement.remaining_ingress.to_f.round(4)

        @hash_current_account_movements.each do | current_account , amount |
          @ret_current_account_movements[ current_account ] = current_account.generate_movement( @cash_register_movement , amount )
        end

        if @ret_current_account_movements.values.uniq.first == true
          remaining_ingress = @cash_register_movement.remaining_ingress
          if remaining_ingress == 0
            @cash_register_movement.status = CashRegisterMovement.statuses[ :affected_totally ]
          elsif ( remaining_ingress > 0 ) and ( remaining_ingress < @cash_register_movement.amount )
            @cash_register_movement.status = CashRegisterMovement.statuses[ :affected_partial ]
          end
          if @cash_register_movement.save
            notice_to_user( @cash_register_movement , [ "Se creó correctamente el movimiento para cada cuenta corriente" ] + @hash_current_account_movements.map{ |current_account,amount| "-<strong>#{ helper.number_to_currency( amount ) }</strong> para <strong>#{current_account.detail}</strong>".html_safe } , :success )
          else
            notice_to_user( @cash_register_movement , @cash_register_movement.errors.full_messages , :errors )
          end
        else
          notice_to_user( @cash_register_movement , @ret_current_account_movements.keys.map{ |cam| cam.errors.full_messages }.flatten , :errors )
        end

      else
        notice_to_user( @cash_register_movement , [ "La suma de los importes ( #{ helper.number_to_currency( @hash_current_account_movements.values.sum ) } ) debe ser menor o igual al monto disponible para distribuirse ( #{ helper.number_to_currency( @cash_register_movement.remaining_ingress.to_f.round(4) ) } )" , "<br />".html_safe ] + @hash_current_account_movements.map{ |current_account,amount| "-<strong>#{ helper.number_to_currency( amount ) }</strong> para <strong>#{current_account.detail}</strong>".html_safe } , :errors )
      end

    end

    redirect_to session.delete( :return_to )

  end

  # -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_cash_register_movement
      @cash_register_movement = CashRegisterMovement.find( params[:id] )
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cash_register_movement_params
      params.require( :cash_register_movement ).permit( :cash_register_id, :amount, :detail, :kind , :status )
    end

end
