class CashRegistersController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_cash_register, only: [:show, :edit, :update, :destroy]

  def index
    prepare_index( { :order => { :created_at => :desc } } )
  end

  def show
    render :edit
  end

  def new
    @cash_register = CashRegister.new( :initial_datetime => Time.now , :user_id => current_user.id )
    respond_to do |format|
      if @cash_register.save
        notice_to_user( @cash_register , [ "Se abrió caja correctamente" ] , :success )
        format.html { redirect_to cash_registers_url }
        # format.json { render action: 'show', status: :created, location: @cash_register }
      else
        notice_to_user( @cash_register , @cash_register.errors.full_messages , :errors )
        format.html { redirect_to cash_registers_url }
        # format.json { render json: @cash_register.errors, status: :unprocessable_entity }
      end
    end

  end

  def edit
    redirect_to( cash_registers_path ) unless @cash_register.can_be_modified?
  end

  def destroy
    @cash_register.status = CashRegister::DELETED
    @cash_register.save
    respond_to do |format|
      format.html { redirect_to cash_registers_url }
      format.json { head :no_content }
    end
  end

  # def show_cash_register_movements
  #   cash_register_id = params[ :cash_register_id ]
  #   @instances = CashRegisterMovement.where( :cash_register_id => cash_register_id )
  #   prepare_index
  # end


  # Request view for make new cash register movement from cash register
  def get_new_cash_register_ingress_movement_from_cash_register
    @cash_register = CashRegister.find( params[ "cash_register_id" ] )
    session[:return_to] = request.referer
    render :layout => false
  end

  # Method finally to create movement to a cash register
  def post_new_cash_register_ingress_movement_from_cash_register

    cash_register = CashRegister.find( params[ "cash_register_id" ] )
    cash_register_movement_params = params[ :cash_register_movement ].deep_symbolize_keys
    cash_register_movement_params[ :cash_register_id ] = cash_register.id
    cash_register_movement_params[ :kind ] = CashRegisterMovement.kinds[ :ingress ]
    cash_register_movement_params[ :status ] = CashRegisterMovement.statuses[ :active_not_affected ]
    begin
      if cash_register.present?
        @cash_register_movement = CashRegisterMovement.new( cash_register_movement_params )
        if @cash_register_movement.save
          notice_to_user( @cash_register_movement , [ "Se creó movimiento de ingreso de dinero en caja \"#{cash_register.detail}\"" ] , :success )
        else
          notice_to_user( @cash_register_movement , @cash_register_movement.errors.full_messages , :errors )
        end
      end
    rescue Exception => except
      notice_to_user( @cash_register_movement , @cash_register_movement.errors.full_messages , :errors )
    end
    redirect_to session.delete( :return_to )
  end

  # Request view for make new cash register movement from cash register
  def get_new_cash_register_egress_movement_from_cash_register
    @cash_register = CashRegister.find( params[ "cash_register_id" ] )
    session[:return_to] = request.referer
    render :layout => false
  end

  # Method finally to create movement to a cash register
  def post_new_cash_register_egress_movement_from_cash_register

    cash_register = CashRegister.find( params[ "cash_register_id" ] )
    cash_register_movement_params = params[ :cash_register_movement ].deep_symbolize_keys
    cash_register_movement_params[ :cash_register_id ] = cash_register.id
    cash_register_movement_params[ :kind ] = CashRegisterMovement.kinds[ :egress ]
    cash_register_movement_params[ :status ] = CashRegisterMovement.statuses[ :active_not_affected ]
    begin
      if cash_register.present?
        @cash_register_movement = CashRegisterMovement.new( cash_register_movement_params )
        if @cash_register_movement.save
          notice_to_user( @cash_register_movement , [ "Se creó movimiento de egreso de dinero en caja \"#{cash_register.detail}\"" ] , :success )
        else
          notice_to_user( @cash_register_movement , @cash_register_movement.errors.full_messages , :errors )
        end
      end
    rescue Exception => except
      notice_to_user( @cash_register_movement , @cash_register_movement.errors.full_messages , :errors )
    end
    redirect_to session.delete( :return_to )
  end

  # -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_cash_register
      @cash_register = CashRegister.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cash_register_params
      params.require( :cash_register ).permit( :initial_datetime, :final_datetime, :number, :status )
    end

end
