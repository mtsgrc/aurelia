class CompaniesController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_company, only: [:show, :edit, :update, :destroy]

  def index
    prepare_index
  end

  def show
    render :edit
  end

  def new
    @company = Company.new
  end

  def edit
    redirect_to( companies_path ) unless @company.can_be_modified?
  end

  def create
    @company = Company.new(company_params)
    respond_to do |format|
      if @company.save
        format.html { redirect_to @company, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@company.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @company }
      else
        format.html { render action: 'new' }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( companies_path ) unless @company.can_be_modified?
    respond_to do |format|
      if @company.update(company_params)
        format.html { redirect_to @company, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@company.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @company.status = Company::DELETED
    @company.save
    respond_to do |format|
      format.html { redirect_to companies_url }
      format.json { head :no_content }
    end
  end

  # -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require( :company ).permit( :code , :name , :status , :logo )
    end

end
