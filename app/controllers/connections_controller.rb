class ConnectionsController < ApplicationController
  load_and_authorize_resource

  before_action :set_connection, only: [:show, :edit, :update, :destroy]

  def index
    # @connections = Connection.all.sort{ | a , b | a.detail <=> b.detail }
    prepare_index
  end

  def show
    render :edit
  end

  def new
    @connection = Connection.new
  end

  def edit
    redirect_to( connections_path ) unless @connection.can_be_modified?
  end

  def create
    @connection = Connection.new(connection_params)
    respond_to do |format|
      if @connection.save
        format.html { redirect_to @connection, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@connection.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @connection }
      else
        format.html { render action: 'new' }
        format.json { render json: @connection.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( connections_path ) unless @connection.can_be_modified?
    respond_to do |format|
      if @connection.update(connection_params)
        format.html { redirect_to @connection, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@connection.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @connection.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
		@connection.status = Connection::DELETED
		@connection.save
    respond_to do |format|
      format.html { redirect_to connections_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_connection
      @connection = Connection.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def connection_params
      params.require(:connection).permit( :system_id, :kind, :status, :host, :port, :username, :password, :uri, :kind_action )
    end
end
