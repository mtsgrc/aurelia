class CostCentersController < ApplicationController
	load_and_authorize_resource
	
  	respond_to :html, :json

  	before_action :set_cost_center, only: [:show, :edit, :update, :destroy]

    def index
      # @cost_centers = CostCenter::statuses.map{ |v,k| { v.name => CostCenter.where(:status => v.value).order(created_at: :desc) } }.reduce(:merge)
      # @cost_centers = CostCenter.all.order(created_at: :asc)
      prepare_index
    end

    def show
  		render :edit
    end

    def new
      @cost_center = CostCenter.new
    end

    def edit
      redirect_to( cost_centers_path ) unless @cost_center.can_be_modified?
    end


  	# ---------------------------- CREATE (POST) | UPDATE (PUT) | DESTROY (DELETE) ----------------------------

    def create
      @cost_center = CostCenter.new(cost_center_params)
      respond_to do |format|
        if @cost_center.save
          format.html { redirect_to @cost_center, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@cost_center.detail}</strong>".html_safe }
          format.json { render action: 'show', status: :created, location: @cost_center }
        else
          format.html { render action: 'new' }
          format.json { render json: @cost_center.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      redirect_to( cost_centers_path ) unless @cost_center.can_be_modified?
      respond_to do |format|
        if @cost_center.update(cost_center_params)
          format.html { redirect_to @cost_center, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@cost_center.detail}</strong>".html_safe }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @cost_center.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
  		@cost_center.status = CostCenter::DELETED
  		@cost_center.save
      respond_to do |format|
        format.html { redirect_to cost_centers_url }
        format.json { head :no_content }
      end
    end

  	# -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------


  	private

      # Use callbacks to share common setup or constraints between actions.
      def set_cost_center
        @cost_center = CostCenter.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
  		def cost_center_params
  	    params.require(:cost_center).permit( :name , :status )
  	  end

end
