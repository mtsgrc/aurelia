class CreditNotesController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_credit_note, only: [:show, :edit, :update, :destroy]

  def index
    prepare_index( { :order => { :created_at => :desc } } )
  end

  def show
    render :edit
  end

  def new
    @credit_note  = CreditNote.new
    @currencies   = Currency.all
  end

  def edit
    redirect_to( credit_notes_path ) unless @credit_note.can_be_modified?
    @currencies = Currency.all
  end

  def create
    @credit_note = CreditNote.new(credit_note_params)
    @currencies = Currency.all

    @credit_note.user = current_user
    respond_to do | format |
      if @credit_note.save
        format.html { redirect_to @credit_note, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@credit_note.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @credit_note }
      else
        format.html { render action: 'new' }
        format.json { render json: @credit_note.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( credit_notes_path ) unless @credit_note.can_be_modified?
    @currencies = Currency.all
    respond_to do |format|
      if @credit_note.update( credit_note_params )
        format.html { redirect_to @credit_note, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@credit_note.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @credit_note.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @credit_note.status = CreditNote::DELETED
    @credit_note.save
    respond_to do |format|
      format.html { redirect_to credit_notes_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_credit_note
      @credit_note = CreditNote.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def credit_note_params
      params.require(:credit_note).permit( :current_account_id , :detail, :amount, :user_id, :status)
    end
end
