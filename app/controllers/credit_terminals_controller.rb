class CreditTerminalsController < ApplicationController
  load_and_authorize_resource

  before_action :set_credit_terminal, only: [:show, :edit, :update, :destroy]

  def index
    # @credit_terminals = CreditTerminal::statuses.map{ |v,k| { v.name => CreditTerminal.where(:status => v.value).order(created_at: :desc) } }.reduce(:merge)
    # @credit_terminals = CreditTerminal.all.order(created_at: :asc)
    # @class_instances = SystemCreditTerminal
    prepare_index
  end

  def calendar

  end

  def show
    render :edit
  end

  def new
    @credit_terminal = CreditTerminal.new
    @agencies = Agency.local
  end

  def edit
    redirect_to( credit_terminals_path ) unless @credit_terminal.can_be_modified?
    @agencies = Agency.local
  end

  def create
    @credit_terminal = CreditTerminal.new( credit_terminal_params )
    @credit_terminal.user = current_user

    @agencies = Agency.local

    respond_to do |format|
      if @credit_terminal.save
        format.html { redirect_to @credit_terminal, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@credit_terminal.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @credit_terminal }
      else
        format.html { render action: 'new' }
        format.json { render json: @credit_terminal.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( credit_terminals_path ) unless @credit_terminal.can_be_modified?
    @agencies = Agency.local
    respond_to do |format|
      if @credit_terminal.update(credit_terminal_params)
        format.html { redirect_to @credit_terminal, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@credit_terminal.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @credit_terminal.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @credit_terminal.status = CreditTerminal::DELETED
    @credit_terminal.save
    respond_to do |format|
      format.html { redirect_to credit_terminals_url }
      format.json { head :no_content }
    end
  end

  # ------------------------------ SPECIAL ACTIONS ------------------------------

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_credit_terminal
      @credit_terminal = CreditTerminal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def credit_terminal_params
      params.require(:credit_terminal).permit( :code , :name , :agency_id )
    end
end
