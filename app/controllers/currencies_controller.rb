class CurrenciesController < ApplicationController
  load_and_authorize_resource
  
  	before_action :set_currency, only: [:show, :edit, :update, :destroy]

    def index
#      @currencies = Currency::statuses.map{ |v,k| { v.name => Currency.where(:status => v.value).order(created_at: :desc) } }.reduce(:merge)
      # @currencies = Currency.all.order(created_at: :asc)
      prepare_index
    end

  	def modal_show
  	end

    def show
  		render :edit
    end

    def new
      @currency = Currency.new
    end

    def edit
      redirect_to( currencies_path ) unless @currency.can_be_modified?
    end


  	# ---------------------------- CREATE (POST) | UPDATE (PUT) | DESTROY (DELETE) ----------------------------

    def create
      @currency = Currency.new(currency_params)
      respond_to do |format|
        if @currency.save
          format.html { redirect_to @currency, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@currency.detail}</strong>".html_safe }
          format.json { render action: 'show', status: :created, location: @currency }
        else
          format.html { render action: 'new' }
          format.json { render json: @currency.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      redirect_to( currencies_path ) unless @currency.can_be_modified?
      respond_to do |format|
        if @currency.update(currency_params)
          format.html { redirect_to @currency, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@currency.detail}</strong>".html_safe }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @currency.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      #@currency.destroy	# The information is not deleted, just anulled
  		@currency.status = Currency::DELETED
  		@currency.save
      respond_to do |format|
        format.html { redirect_to currencies_url }
        format.json { head :no_content }
      end
    end

  	# -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  	private

      # Use callbacks to share common setup or constraints between actions.
      def set_currency
        @currency = Currency.find( params[:id] )
      end

      # Never trust parameters from the scary internet, only allow the white list through.
  		def currency_params
  	    params.require(:currency).permit( :name , :symbol , :status )
  	  end

end
