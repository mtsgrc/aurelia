class CurrentAccountMovementsController < ApplicationController
  load_and_authorize_resource

  	respond_to :html, :json

  	before_action :set_current_account_movement, only: [:show, :edit, :update, :destroy]

    def index
      prepare_index( { :order => { :created_at => :desc } } )
    end

    def show
  		render :edit
    end

    def new
      @current_account_movement = CurrentAccountMovement.new
    end

    def edit
      redirect_to( current_account_movements_path ) unless @current_account_movement.can_be_modified?
    end


  	# ---------------------------- CREATE (POST) | UPDATE (PUT) | DESTROY (DELETE) ----------------------------

    def create
      @current_account_movement = CurrentAccountMovement.new(current_account_movement_params)
      respond_to do |format|
        if @current_account_movement.save
          format.html { redirect_to @current_account_movement, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@current_account_movement.detail}</strong>".html_safe }
          format.json { render action: 'show', status: :created, location: @current_account_movement }
        else
          format.html { render action: 'new' }
          format.json { render json: @current_account_movement.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
  		@current_account_movement.status = CurrentAccountMovement::DELETED
  		@current_account_movement.save
      respond_to do |format|
        format.html { redirect_to current_account_movements_url }
        format.json { head :no_content }
      end
    end


    # Request view for choose which current account will be affected with the new movement
    def get_new_movement_affectations_from_current_account_movement
      @current_account_movement = CurrentAccountMovement.find( params[ "current_account_movement_id" ] )
      @current_account_movements = @current_account_movement.current_account.movements.debit

      session[:return_to] = request.referer
      render :layout => false

    end

    # Method finally to create current account movement
    def post_new_movement_affectations_from_current_account_movement

      ActiveRecord::Base.transaction do

        @current_account_movement = CurrentAccountMovement.find( params[ "current_account_movement_id" ] )
        movement_affectations     = params[ "movement_affectations" ]

        @hash_movement_affectations = movement_affectations.map{ | movement_affectation | { CurrentAccountMovement.find( movement_affectation[ "current_account_movement_id" ] ) => movement_affectation[ "amount" ].to_f } }.reduce( {} , :merge )

        @ret_movement_affectations  = @hash_movement_affectations.dup.map{ | current_account_movement , amount | { current_account_movement => MovementAffectation.new( :movement_from => @current_account_movement , :movement_to => current_account_movement, :amount => amount ) } }.reduce( {} , :merge )

        if @hash_movement_affectations.values.sum.to_f.round(4) <= @current_account_movement.remaining_amount.to_f.round(4)

          @ret_movement_affectations.each do | current_account_movement , movement_affectation |
            movement_affectation.save
          end

          if ( @ret_movement_affectations.map{ |current_account,movement_affectation| movement_affectation.persisted? }.uniq.count == 1 ) and ( @ret_movement_affectations.map{ |current_account,movement_affectation| movement_affectation.persisted? }.uniq.first == true )

            notice_to_user( @current_account_movement , [ "Se crearon las afectaciones correspondientes" ] + @hash_movement_affectations.map{ |current_account_movement,amount| "-<strong>#{ helper.number_to_currency( amount ) }</strong> para <strong>#{current_account_movement.detail}</strong>".html_safe } , :success )

          else

            notice_to_user( @current_account_movement , @ret_movement_affectations.values.map{ |movement_affectation| movement_affectation.errors.full_messages }.flatten , :errors )
            
          end

        else
          notice_to_user( @current_account_movement , [ "La suma de los importes ( #{ helper.number_to_currency( @hash_movement_affectations.values.sum ) } ) debe ser menor o igual al monto disponible para afectar ( #{ helper.number_to_currency( @current_account_movement.remaining_amount.to_f.round(4) ) } )" , "<br />".html_safe ] , :errors )
        end

      end

      redirect_to session.delete( :return_to )

    end

  	# -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  	private

      # Use callbacks to share common setup or constraints between actions.
      def set_current_account_movement
        @current_account_movement = CurrentAccountMovement.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
  		def current_account_movement_params
  	    params.require(:current_account_movement).permit( :current_account_id , :kind, :status , :resource_type, :resource_id )
  	  end

end
