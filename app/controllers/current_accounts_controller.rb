class CurrentAccountsController < ApplicationController
  load_and_authorize_resource

  	respond_to :html, :json

  	before_action :set_current_account, only: [:show, :edit, :update, :destroy]

    def index
      prepare_index
    end

    def show
  		render :edit
    end

    def new
      @current_account = CurrentAccount.new
    end

    def edit
      redirect_to( current_accounts_path ) unless @current_account.can_be_modified?
    end

    def get_main_view
      @current_account  = CurrentAccount.find( params[ "current_account_id"] )
      @agency_company_period_id = params[ "agency_company"]

      order_by =  { :id => :desc }

      if params[ "period"].present?
        @current_period = @current_account.periods.where( :order_number => params[ "period"] )
        if @current_period.present?

          @periods        = @current_period.limit(1)
          @current_period = @current_period.first

          if @agency_company_period_id.present?
            @period = @current_period.agency_company_periods.map{ |acp| acp if acp.agency_company_id == @agency_company_period_id.to_i }.compact.first
          else
            @period = @current_period
          end

        else
          @periods = @current_account.periods.reverse_order.limit(1)
        end
      else
        @periods = @current_account.periods.reverse_order.limit(1)
      end

      @periods = prepare_index( { } , @periods )

      unless @period.present?
        @period = @current_account.current_period
      end

      if @period.present?

        if @period.movements.class == Array
          array_of_ids = @period.movements.map{ |m| m.id if m.class == CurrentAccountMovement }.compact
          @period_movements = CurrentAccountMovement.where( :id => array_of_ids ).order( order_by )
        else
          # @period_movements = @period.movements.order( order_by )
          @period_movements = @period.movements.order( order_by )
        end

        @hash_group_by_date_and_type_of_resource = @period_movements.group_by(&:resource_date).map{ | date_ref, movements_in_date | { date_ref => movements_in_date } }.reduce( {},:merge)
        @instances = @period_movements

      else
        @instances = []
      end

      # @instances =  prepare_index( order_by )
      # prepare_index( { :id => :desc } )

    end

  	# ---------------------------- CREATE (POST) | UPDATE (PUT) | DESTROY (DELETE) ----------------------------

    def create
      @current_account = CurrentAccount.new(current_account_params)
      respond_to do |format|
        if @current_account.save
          format.html { redirect_to @current_account, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@company.detail}</strong>".html_safe }
          format.json { render action: 'show', status: :created, location: @current_account }
        else
          format.html { render action: 'new' }
          format.json { render json: @current_account.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      redirect_to( current_accounts_path ) unless @current_account.can_be_modified?
      respond_to do |format|
        if @current_account.update(current_account_params)
          format.html { redirect_to @current_account, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase}".html_safe }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @current_account.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
  		@current_account.status = CurrentAccount::DELETED
  		@current_account.save
      respond_to do |format|
        format.html { redirect_to current_accounts_url }
        format.json { head :no_content }
      end
  		return false
    end

    def get_assign_credits
      @current_account  = CurrentAccount.find( params[ "current_account_id"] )
      @external_agencies_companies = @current_account.owner.external_agencies_companies
      session[:return_to] = request.referer
      render :layout => false
    end

    def post_assign_credits

      ActiveRecord::Base.transaction do

        @current_account  = CurrentAccount.find( params[ "current_account_id"] )
        @external_agencies_companies = @current_account.owner.external_agencies_companies
        @external_agencies_companies_amounts = params[ "agencies_companies" ]

        # amounts sum equal or minor than maximum credit
        if @external_agencies_companies_amounts.map{ |aca| aca["amount"].to_f }.sum.to_f.round(4) <= @current_account.credit_maximum.to_f.round(4)

          @external_agencies_companies_amounts_saving = []

          @external_agencies_companies_amounts.each do | agency_company_amount |

            agency_company_id     = agency_company_amount["agency_company_id"]
            agency_company_amount = agency_company_amount["amount"]

            agency_company = @current_account.owner.external_agencies_companies.map{ |ac| ac if ac.id==agency_company_id.to_i }.compact.first

            if agency_company.present?
              agency_company.credit_maximum = agency_company_amount.to_f
              if agency_company.save
                @external_agencies_companies_amounts_saving = { agency_company => true  }
              else
                @external_agencies_companies_amounts_saving = { agency_company => false }
              end
            end

          end

          if @external_agencies_companies_amounts_saving.values.uniq.first == true
            notice_to_user( @current_account , [ "Se modificaron los créditos para las agencias/compañias indicados" ] , :success )
          else
            notice_to_user( @current_account , @external_agencies_companies_amounts_saving.keys.map{ |cam| cam.errors.full_messages }.flatten , :errors )
          end

        else
          notice_to_user( @current_account , [ "La suma de los créditos ( #{ helper.number_to_currency( @external_agencies_companies_amounts.map{ |aca| aca["amount"].to_f }.sum.to_f.round(4) ) } ) debe ser menor o igual al crédito máximo asignado a la cuenta corriente para distribuirse ( #{ helper.number_to_currency( @current_account.credit_maximum.to_f.round(4) ) } )" , "<br />".html_safe ] , :errors )
        end

      end

      redirect_to session.delete( :return_to )

    end

  	# -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  	private

      # Use callbacks to share common setup or constraints between actions.
      def set_current_account
        @current_account = CurrentAccount.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
  		def current_account_params
  	    params.require( :current_account ).permit( :credit_maximum , :kind , :status , :resource_id )
  	  end

end
