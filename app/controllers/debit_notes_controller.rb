class DebitNotesController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_debit_note, only: [:show, :edit, :update, :destroy]

  def index
    prepare_index( { :order => { :created_at => :desc } } )
  end

  def show
    render :edit
  end

  def new
    @debit_note  = DebitNote.new
    @currencies   = Currency.all
  end

  def edit
    redirect_to( debit_notes_path ) unless @debit_note.can_be_modified?
    @currencies = Currency.all
  end

  def create
    @debit_note = DebitNote.new(debit_note_params)
    @currencies = Currency.all

    @debit_note.user = current_user
    respond_to do |format|
      if @debit_note.save
        format.html { redirect_to @debit_note, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@debit_note.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @debit_note }
      else
        format.html { render action: 'new' }
        format.json { render json: @debit_note.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( debit_notes_path ) unless @debit_note.can_be_modified?
    @currencies = Currency.all
    respond_to do |format|
      if @debit_note.update( debit_note_params )
        format.html { redirect_to @debit_note, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@debit_note.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @debit_note.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @debit_note.status = DebitNote::DELETED
    @debit_note.save
    respond_to do |format|
      format.html { redirect_to debit_notes_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_debit_note
      @debit_note = DebitNote.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def debit_note_params
      params.require(:debit_note).permit( :current_account_id , :detail, :amount, :user_id, :status)
    end
end
