class ExpensesController < ApplicationController
  load_and_authorize_resource

  before_action :set_expense, only: [:show, :edit, :update, :destroy]

  def index
    prepare_index( { :order => { :created_at => :desc } } )
  end

  def show
    render :edit
  end

  def new
    @expense    = Expense.new
    @currencies = Currency.all
  end

  def edit
    redirect_to( expenses_path ) unless @expense.can_be_modified?
    @currencies = Currency.all
  end

  def create
    @expense = Expense.new(expense_params)
    @currencies = Currency.all

    @expense.user = current_user
    respond_to do |format|
      if @expense.save
        format.html { redirect_to @expense, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@expense.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @expense }
      else
        format.html { render action: 'new' }
        format.json { render json: @expense.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( expenses_path ) unless @expense.can_be_modified?
    @currencies = Currency.all
    respond_to do |format|
      if @expense.update( expense_params )
        format.html { redirect_to @expense, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@expense.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @expense.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @expense.status = Expense::DELETED
    @expense.save
    respond_to do |format|
      format.html { redirect_to expenses_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_expense
      @expense = Expense.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def expense_params
      params.require(:expense).permit(:commerce_number, :receipt_number, :receipt_date, :detail, :receipt_amount, :currency_id, :agency_id, :status)
    end
end
