class GobbiSchemasController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_gobbi_schema, only: [:show, :edit, :update, :destroy]

  def index
    # @gobbi_schemas = GobbiSchema.all.order(created_at: :asc)
    prepare_index
  end

  def show
    render :edit
  end

  def new
    @gobbi_schema = GobbiSchema.new
  end

  def edit
    redirect_to( gobbi_schemas_path ) unless @gobbi_schema.can_be_modified?
  end

  def create
    @gobbi_schema = GobbiSchema.new(gobbi_schema_params)
    respond_to do |format|
      if @gobbi_schema.save
        format.html { redirect_to @gobbi_schema, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@gobbi_schema.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @gobbi_schema }
      else
        format.html { render action: 'new' }
        format.json { render json: @gobbi_schema.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( gobbi_schemas_path ) unless @gobbi_schema.can_be_modified?
    respond_to do |format|
      if @gobbi_schema.update(gobbi_schemas_params)
        format.html { redirect_to @gobbi_schema, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@gobbi_schema.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @gobbi_schema.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @gobbi_schema.status = GobbiSchema::DELETED
    @gobbi_schema.save
    respond_to do |format|
      format.html { redirect_to gobbi_schemas_url }
      format.json { head :no_content }
    end
  end

  # -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_gobbi_schema
      @gobbi_schema = GobbiSchema.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gobbi_schema_params
      params.require( :gobbi_schema ).permit( :name , :status )
    end

end
