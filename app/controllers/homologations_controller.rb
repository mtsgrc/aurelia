class HomologationsController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_homologation, only: [:show, :edit, :update, :destroy]

  def index
    # @homologationss = Homologation.all.sort{ |a,b| a.detail <=> b.detail }
    prepare_index
  end

  def show
    render :edit
  end

  def new
    @homologation = Homologation.new
  end

  def edit
    redirect_to( homologations_path ) unless @homologation.can_be_modified?
  end

  def create
    @homologation = Homologation.new(homologation_params)

    respond_to do |format|
      if @homologation.save
        format.html { redirect_to @homologation, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@homologation.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @homologation }
      else
        format.html { render action: 'new' }
        format.json { render json: @homologation.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( homologations_path ) unless @homologation.can_be_modified?
    respond_to do |format|
      if @homologation.update(homologation_params)
        format.html { redirect_to @homologation, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@homologation.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @homologation.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
		@homologation.status = Homologation::DELETED
		@homologation.save
    respond_to do |format|
      format.html { redirect_to homologations_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_homologation
      @homologation = Homologation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def homologation_params
      params.require(:homologation).permit( :kind , :child_table_name , :child_value , :parent_table_name , :parent_value )
    end
end
