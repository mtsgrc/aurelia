class InspectorsController < ApplicationController
  load_and_authorize_resource

  before_action :set_inspector, only: [ :show, :edit, :update, :destroy ]

  def index
    prepare_index( { :order => { :created_at => :desc } } )
  end

  def show
    render :edit
  end

  def new
    @inspector = Inspector.new
  end

  def edit
    redirect_to( inspectors_path ) unless @inspector.can_be_modified?
  end

  def create
    @inspector = Inspector.new( inspector_params )
    respond_to do |format|
      if @inspector.save
        format.html { redirect_to @inspector, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@inspector.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @inspector }
      else
        format.html { render action: 'new' }
        format.json { render json: @inspector.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( inspector_path ) unless @inspector.can_be_modified?
    respond_to do |format|
      if @inspector.update(inspector_params)
        format.html { redirect_to @inspector, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@inspector.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @inspector.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @inspector.status = Inspector::DELETED
    @inspector.save
    respond_to do |format|
      format.html { redirect_to inspector_url }
      format.json { head :no_content }
    end
  end

  def get_inspector_trigger
    inspector_id = params[ :inspector_id ] if params[ :inspector_id ].present?
    inspector = Inspector.find( inspector_id ) if inspector_id.present?

    if inspector
      require 'inspectors/' + inspector.symbol_name
      inspector_module = "Inspectors::#{inspector.symbol_name.camelize}".constantize

      return_triggering = inspector_module.trigger

      inspect = Inspect.new

      inspect.inspector_id      = inspector_id
      inspect.run_at            = Time.now
      inspect.response_status   = return_triggering[ :response_status ]
      inspect.response_message  = return_triggering[ :messages ].join('n')
      inspect.user_id           = current_user.id

      inspect.save!

      redirect_to( inspectors_path )
    end


  end

  # -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_inspector
      @inspector = Inspector.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inspector_params
      params.require( :inspector ).permit( :code , :name , :status , :logo )
    end

end
