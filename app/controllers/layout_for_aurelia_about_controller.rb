class LayoutForAureliaAboutController < ApplicationController
	load_and_authorize_resource

	def index
		@yml_version_structure = YAML.load_file( Rails.root.to_s + "/app/versions.yml" )[ "versions" ]
	end

end
