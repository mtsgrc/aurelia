class LayoutForInspiniaController < ApplicationController
	skip_authorization_check

	def index
		redirect_to( "#{request.protocol}#{request.host}:3001" )
	end

end
