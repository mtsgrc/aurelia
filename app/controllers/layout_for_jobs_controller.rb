class LayoutForJobsController < ApplicationController
  load_and_authorize_resource

  CONST_ARRAY_OF_JOBS = { :liquidations_importer_job                    => { :code_system_ref => :select2 , :date => :date } ,
                          :lot_closures_importer_job                    => { :code_system_ref => :select2 , :date => :date } ,
                          :agencies_importer_job                        => { :code_system_ref => :select2 , :file_ref => :file } ,
                          :companies_importer_job                       => { :code_system_ref => :select2 , :file_ref => :file } ,
                          :lot_closure_transactions_importer_job        => { :code_system_ref => :select2 , :file_ref => :file } ,
                          :bank_account_movements_importer_job          => { :code_system_ref => :select2 , :file_ref => :file } ,
                          :bank_account_movements_affecter_job          => { :date => :date } ,
                          :liquidations_affecter_job                    => { :date => :date } ,
                          :lot_closures_affecter_job                    => { :date => :date } ,
                          :current_account_periods_starter_job          => { :date => :date } ,
                          :current_account_period_starter_job           => { :id_current_account_ref => :select2 , :date => :date } ,
                          :person_checks_importer_job                   => { :date => :date } ,
                          :people_importer_job                          => { } ,
                          :lot_closure_transactions_csv_downloader_job  => { :code_system_ref => :select2 , :from_date => :date , :to_date => :date } ,
                          # :current_account_period_finalizer_job       => { :id_current_account_ref => :select2 , :date => :date } ,
                        }

  CONST_GROUP_OF_JOBS = {

  }

  CONST_MAX_SIZE_BYTES = 10485760 # 10 Mb in bytes

  CONST_VALID_MIMETYPES = { :csv => "text/csv" ,
                            :xls => "application/vnd.ms-excel" ,
                            :nsv => "text/plain" ,
                          }

  def get_new

    @ref_data = {}
    @ref_url  = params[ "ref_url" ]  if params[ "ref_url" ].present?

    @ref_data = params.except( *["controller", "filter_jobs", "action"] ).deep_symbolize_keys

    @filter_jobs = params[ "filter_jobs" ] if params[ "filter_jobs" ].present?


    @job = Delayed::Job.new
    @class_instances = Delayed::Job
    if @filter_jobs.present?
      if CONST_ARRAY_OF_JOBS.keys.include?( @filter_jobs.to_sym )
        @tasks = CONST_ARRAY_OF_JOBS.select{ |job| job.class if job == @filter_jobs.to_sym }.keys.map{ |o| [ o.to_s , o.to_s.camelize.constantize.description ] }.to_h
      end
    else
      @tasks = CONST_ARRAY_OF_JOBS.keys.map{ |o| [ o.to_s , o.to_s.camelize.constantize.description ] }.to_h
    end

    render :layout => false

  end

  def post_new
    if params.include?( "job" )
      params_job  = params["job"]
      task_name   = params_job["task_name"]                                                             if params_job.include?( "task_name" )
      task_params = params_job.except("task_name").inject({}){ |memo,(k,v)| memo[k.to_sym] = v; memo }  if params_job.include?( "task_name" )

      task_params = task_params.except( :file_ref ).inject({}){ |memo,(k,v)| memo[k.to_sym] = v; memo }   if task_params.include?( :file_ref )

      file_ref    = params_job[ "file_ref" ]

      if file_ref.present? and task_params[ :code_system_ref ]

        # Variables needed for do the job
        file_name     = task_name.gsub( "_importer_job" , "" )
        class_ref     = file_name.singularize.camelize.constantize
        synchronizer  = class_ref.get_synchronizer_from( task_params[ :code_system_ref ] )

        # Synchronizer exists ?
        if synchronizer.present?

          # Get extensions seted through synchronizer
          file_extension = synchronizer.connection.kind

          # Check mimetype acceptance and check if the extension according to the mimetype matches with the extension of synchronizer
          if ( CONST_VALID_MIMETYPES.key( file_ref.content_type ).present? ) and ( CONST_VALID_MIMETYPES.key( file_ref.content_type ).to_s == file_extension )

            # Check limit of files
            if CONST_MAX_SIZE_BYTES >= file_ref.size

              # Create file in tmp folder under external system lib folder
              ret_writing = File.open( Rails.root.join( 'lib', 'external_systems', task_params[ :code_system_ref ] , 'tmp' , "#{file_name}.#{file_extension}"), 'wb') do | file |
                file.write( file_ref.read )
              end
              task_params[ :file_ref ] = file_ref.original_filename

            end

          end

        end

      end

    end

    run_at = Time.now + 5.second
    task_name.camelize.constantize.set( wait_until: run_at ).perform_later( task_params )

    # @class_instances = Delayed::Job
    # @tasks = CONST_ARRAY_OF_JOBS.keys.map{ |o| [ o.to_s , o.to_s.camelize.constantize.description ] }.to_h
    redirect_to :action => "index"
  end

  def index
    @class_instances = Delayed::Job
    prepare_index( { :order => { :run_at => :desc } } )
  end

  def get_view_log

    @content_log_file       = nil
    @content_sys_log_file   = []
    job_id                  = params[:job_id] if params[:job_id]
    path_log_file           = File.join( Rails.root.join('log' , 'job' ) , "#{job_id}.log" )

    if File.exists?( path_log_file )
      @content_log_file = File.read( path_log_file )
      @date_log_file    = File.mtime( path_log_file )
      @content_log_file.each_line do |line|
        if line.strip.include?("Path LOG nativo:")
          path_sys_log_path = File.join( Rails.root , line.strip.split("Path LOG nativo:").second.strip )
          if File.exists?( path_sys_log_path )
            File.read( path_sys_log_path ).each_line do |file_line|
              @content_sys_log_file << file_line.gsub("JOB_ID##{job_id} |","") if file_line.include?("JOB_ID##{job_id} |")
            end
          else
            @content_sys_log_file = nil
          end
          break
        end
      end
    else
      redirect_to url_for( :controller => "layout_for_jobs" , :action => "index" )
    end
  end

  def get_retry_job

    job_id                  = params[:job_id] if params[:job_id]
    dj = Delayed::Job.find( job_id )

    if dj.present? and dj.error_message != nil
      run_at = Time.now + 10.second
      dj.error_message = nil
      dj.failed_at = nil
      dj.locked_at = nil
      dj.completed_at = nil
      dj.progress_current = 0
      dj.last_error = nil
      dj.run_at = run_at
      dj.save
    end

    redirect_to url_for( :controller => "layout_for_jobs" , :action => "index" )

  end

  def get_raw_log

    @content_log_file = []
    job_id            = params[:job_id] if params[:job_id]
    path_log_file     = File.join( Rails.root.join('log' , 'job' ) , "#{job_id}.log" )

    if File.exists?( path_log_file )
      File.read( path_log_file ).each_line do |file_line|
        @content_log_file << file_line.split("INFO -- :").second if file_line.include?("INFO -- :")
      end
      @date_log_file    = File.mtime( path_log_file )
      render :layout => false
    else
      redirect_to url_for( :controller => "layout_for_jobs" , :action => "index" )
    end

  end

  private
    def job_params
      params.require(:job).permit( :name_task, :params )
    end
end
