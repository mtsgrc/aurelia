class LiquidationsController < ApplicationController
  load_and_authorize_resource

  before_action :set_liquidation, only: [:show, :edit, :update, :destroy]

  def index
    prepare_index
  end

  def calendar

  end

  def show
    render :edit
  end

  def new
    @liquidation = Liquidation.new
    @agencies = Agency.all
    @companies = Company.all
  end

  def edit
    redirect_to( liquidations_path ) unless @liquidation.can_be_modified?
    @agencies = Agency.all
    @companies = Company.all
  end

  def create
    @liquidation = Liquidation.new( liquidation_params )
    @liquidation.user = current_user

    @agencies = Agency.all
    @companies = Company.all

    respond_to do |format|
      if @liquidation.save
        format.html { redirect_to @liquidation, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@liquidation.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @liquidation }
      else
        format.html { render action: 'new' }
        format.json { render json: @liquidation.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( liquidations_path ) unless @liquidation.can_be_modified?
    @agencies = Agency.all
    @companies = Company.all
    respond_to do |format|
      if @liquidation.update(liquidation_params)
        format.html { redirect_to @liquidation, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@liquidation.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @liquidation.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @liquidation.status = Liquidation::DELETED
    @liquidation.save
    respond_to do |format|
      format.html { redirect_to liquidations_url }
      format.json { head :no_content }
    end
  end

  # ------------------------------ SPECIAL ACTIONS ------------------------------

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_liquidation
      @liquidation = Liquidation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def liquidation_params
      params.require(:liquidation).permit( :company_id , :agency_id, :agency_number, :unique_number, :liquidated_at, :total_liquidated, :total_effective, :total_trade, :total_pass, :total_voucher, :total_purse , :user_id )
    end
end
