class LotClosureTransactionsController < ApplicationController
  load_and_authorize_resource

  	respond_to :html, :json

  	before_action :set_lot_closure_transaction, only: [:show, :edit, :update, :destroy]

    def index
      @class_instances = LotClosureTransaction
      prepare_index( { :order => { :created_at => :desc } } )
    end

    def show
  		render :edit
    end

    def new
      @lot_closure_transaction = LotClosureTransaction.new
    end

    def edit
      redirect_to( lot_closure_transactions_path ) unless @lot_closure_transaction.can_be_modified?
    end

  	# ---------------------------- CREATE (POST) | UPDATE (PUT) | DESTROY (DELETE) ----------------------------

    def create
      @lot_closure_transaction = LotClosureTransaction.new(lot_closure_transaction_params)
      respond_to do |format|
        if @lot_closure_transaction.save
          format.html { redirect_to @lot_closure_transaction, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@lot_closure_transaction.detail}</strong>".html_safe }
          format.json { render action: 'show', status: :created, location: @lot_closure_transaction }
        else
          format.html { render action: 'new' }
          format.json { render json: @lot_closure_transaction.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      redirect_to( lot_closure_transactions_path ) unless @lot_closure_transaction.can_be_modified?
      respond_to do |format|
        if @lot_closure_transaction.update(lot_closure_transaction_params)
          format.html { redirect_to @lot_closure_transaction, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@lot_closure_transaction.detail}</strong>".html_safe }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @lot_closure_transaction.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
  		@lot_closure_transaction.status = LotClosureTransaction::DELETED
  		@lot_closure_transaction.save
      respond_to do |format|
        format.html { redirect_to lot_closure_transactions_url }
        format.json { head :no_content }
      end
    end

  	# -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  	private

      # Use callbacks to share common setup or constraints between actions.
      def set_lot_closure_transaction
        @lot_closure_transaction = LotClosureTransaction.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
  		def lot_closure_transaction_params
  	    params.require( :lot_closure_transaction ).permit( )
  	  end

end
