class LotClosuresController < ApplicationController
  load_and_authorize_resource

  	respond_to :html, :json

  	before_action :set_lot_closure, only: [:show, :edit, :update, :destroy]

    def index
      @class_instances = LotClosure
      prepare_index( { :order => { :created_at => :desc } } )
    end

    def show
  		render :edit
    end

    def new
      @lot_closure = LotClosure.new
    end

    def edit
      redirect_to( lot_closures_path ) unless @lot_closure.can_be_modified?
    end

  	# ---------------------------- CREATE (POST) | UPDATE (PUT) | DESTROY (DELETE) ----------------------------

    def create
      @lot_closure = LotClosure.new(lot_closure_params)
      respond_to do |format|
        if @lot_closure.save
          format.html { redirect_to @lot_closure, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@lot_closure.detail}</strong>".html_safe }
          format.json { render action: 'show', status: :created, location: @lot_closure }
        else
          format.html { render action: 'new' }
          format.json { render json: @lot_closure.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      redirect_to( lot_closures_path ) unless @lot_closure.can_be_modified?
      respond_to do |format|
        if @lot_closure.update(lot_closure_params)
          format.html { redirect_to @lot_closure, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@lot_closure.detail}</strong>".html_safe }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @lot_closure.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
  		@lot_closure.status = LotClosure::DELETED
  		@lot_closure.save
      respond_to do |format|
        format.html { redirect_to lot_closures_url }
        format.json { head :no_content }
      end
    end

  	# -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  	private

      # Use callbacks to share common setup or constraints between actions.
      def set_lot_closure
        @lot_closure = LotClosure.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
  		def lot_closure_params
  	    params.require( :lot_closure ).permit( )
  	  end

end
