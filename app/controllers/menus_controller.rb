class MenusController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_menu, only: [:show, :edit, :update, :destroy]

  def index
    # @menus = Menu.all.sort{ |a,b| a.detail <=> b.detail }
    prepare_index
  end

  def show
    render :edit
  end

  def new
    @menu = Menu.new
  end

  def edit
    redirect_to( menus_path ) unless @menu.can_be_modified?
  end

  def create
    @menu = Menu.new(menu_params)

    respond_to do |format|
      if @menu.save
        format.html { redirect_to @menu, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@menu.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @menu }
      else
        format.html { render action: 'new' }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( menus_path ) unless @menu.can_be_modified?
    respond_to do |format|
      if @menu.update(menu_params)
        format.html { redirect_to @menu, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@menu.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @menu.status = Menu::DELETED
    @menu.save
    respond_to do |format|
      format.html { redirect_to menus_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_menu
      @menu = Menu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def menu_params
      params.require(:menu).permit(:name, :symbol, :parent_id, :status, :kind, :faicon )
    end
end
