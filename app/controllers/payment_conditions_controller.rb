class PaymentConditionsController < ApplicationController
  load_and_authorize_resource

  	respond_to :html, :json

  	before_action :set_payment_condition, only: [:show, :edit, :update, :destroy]

    def index
      #@payment_conditions = PaymentCondition::statuses.map{ |v,k| { v.name => PaymentCondition.where(:status => v.value).order(created_at: :desc) } }.reduce(:merge)
      # @payment_conditions = PaymentCondition.all.order( created_at: :asc )
      prepare_index
    end

    def show
  		render :edit
    end

    def new
      @payment_condition = PaymentCondition.new
    end

    def edit
      redirect_to( payment_conditions_path ) unless @payment_condition.can_be_modified?
    end


  	# ---------------------------- CREATE (POST) | UPDATE (PUT) | DESTROY (DELETE) ----------------------------

    def create
      @payment_condition = PaymentCondition.new(payment_condition_params)
      respond_to do |format|
        if @payment_condition.save
          format.html { redirect_to @payment_condition, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@payment_condition.detail}</strong>".html_safe }
          format.json { render action: 'show', status: :created, location: @payment_condition }
        else
          format.html { render action: 'new' }
          format.json { render json: @payment_condition.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      redirect_to( payment_conditions_path ) unless @payment_condition.can_be_modified?
      respond_to do |format|
        if @payment_condition.update(payment_condition_params)
          format.html { redirect_to @payment_condition, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@payment_condition.detail}</strong>".html_safe }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @payment_condition.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
  		@payment_condition.status = PaymentCondition::DELETED
  		@payment_condition.save
      respond_to do |format|
        format.html { redirect_to payment_conditions_url }
        format.json { head :no_content }
      end
    end

  	# -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

  	private

      # Use callbacks to share common setup or constraints between actions.
      def set_payment_condition
        @payment_condition = PaymentCondition.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
  		def payment_condition_params
  	    params.require(:payment_condition).permit( :code , :name , :status )
  	  end

end
