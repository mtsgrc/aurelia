class PeopleController < ApplicationController
	load_and_authorize_resource

	before_action :set_person, only: [ :show, :edit, :update, :destroy ]

  def index
		# @people = Person.all
		prepare_index( { :order => { :system_id => :asc } } )
  end

  def show
		render :edit
  end

  def new
    @person = Person.new
  end

  def edit
		redirect_to( people_path ) unless @person.can_be_modified?
  end

	def create
    @person = Person.new(person_params)
    respond_to do |format|
    	if @person.save
        format.html { redirect_to @person, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@person.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @person }
      else
        format.html { render action: 'new' }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
		redirect_to( people_path ) unless @person.can_be_modified?
    respond_to do |format|
      if @person.update(person_params)
        format.html { redirect_to @person, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@person.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
		@person.status = Person::DELETED
		@person.save
    respond_to do |format|
      format.html { redirect_to people_url }
      format.json { head :no_content }
    end
  end


  def get_new_user_from_native_person
    @person = Person.find( params[ "person_id" ] )
		if @person.native?
			session[:return_to] = request.referer
			render :layout => false
		end

  end

  def post_new_user_from_native_person

    @person = Person.find( params[ "person_id" ] )

    if @person.present?

			if @person.native?

				unless @person.user.present?

					if @person.status == 0

						token = SecureRandom.urlsafe_base64

			      @new_user = User.new
			      @new_user.username 								= "#{@person.dni}"
			      @new_user.person_id								= @person.id
			      @new_user.status									= 0
			      @new_user.password								= ":blank:"
			      @new_user.password_confirmation		= ":blank:"
			      @new_user.reset_password_token		= ":blank:#{token}:"
			      @new_user.reset_password_sent_at	= DateTime.now

			      return_generation_of_new_user = @new_user.save
			      if return_generation_of_new_user

			        notice_to_user( @new_user , [ "Se creó correctamente el usuario #{@new_user.detail}" ] , :success )
			      else
			        notice_to_user( @new_user , @new_user.errors.full_messages , :errors )
			      end

		      else
		        notice_to_user( @person , [ "Sólo se puede crear un usuario de personas activas" ] , :errors )
		      end

	      else
	        notice_to_user( @person , [ "La persona ya posee un usuario de accesso" ] , :errors )
	      end

      else
        notice_to_user( @person , [ "Sólo se puede crear un usuario de una persona nativa" ] , :errors )
      end

		else
			notice_to_user( @person , [ "No existe la persona de la cuál desea crear un usuario" ] , :errors )
		end

    redirect_to ( session[ :return_to ] ||= request.referer )

  end


	private

    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
		def person_params
	    params.require( :person ).permit( :id_person , :first_name , :last_name, :role_id , :status )
	  end
end
