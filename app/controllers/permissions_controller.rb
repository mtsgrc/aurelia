class PermissionsController < ApplicationController
  load_and_authorize_resource

	before_action :set_permission, only: [:show, :edit, :update, :destroy]

  def index
    prepare_index
  end

  def show
		render :edit
  end

  def new
    @permission = Permission.new
  end

  def edit
    redirect_to( permissions_path ) unless @permission.can_be_modified?
  end

	# ---------------------------- CREATE (POST) | UPDATE (PUT) | DESTROY (DELETE) ----------------------------

  def create
    @permission = Permission.new(permission_params)
    respond_to do |format|
      if @permission.save
        format.html { redirect_to @permission, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@permission.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @permission }
      else
        format.html { render action: 'new' }
        format.json { render json: @permission.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( permissions_path ) unless @permission.can_be_modified?
    respond_to do |format|
      if @permission.update(permission_params)
        format.html { redirect_to @permission, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@permission.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @permission.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    #@permission.destroy	# The information is not deleted, just anulled
		@permission.status = Permission::DELETED
		@permission.save
    respond_to do |format|
      format.html { redirect_to permissions_url }
      format.json { head :no_content }
    end
  end

	# -------------------------------------------------------- PRIVATE METHODS --------------------------------------------------------

	private

    # Use callbacks to share common setup or constraints between actions.
    def set_permission
      @permission = Permission.find( params[:id] )
    end

    # Never trust parameters from the scary internet, only allow the white list through.
		def permission_params
	    params.require(:permission).permit( :role_id , :resource_class , :action )
	  end

end
