class PersonChecksController < ApplicationController
  load_and_authorize_resource

  before_action :set_person_check, only: [:show, :edit, :update, :destroy]

  def index

    if params["_action"].present?

      prepare_index( { :order => { :check_at => :desc } , :not_paginate => true , :limit => 1000 } )

      if @instances.present?

        # ordered by check_at DESC (newest down)
        @date_from = @instances.first.check_at.to_date
        @date_to   = @instances.last.check_at.to_date

        @year_from  = @date_from.year
        @month_from = @date_from.month
        @cweek_from = @date_from.cweek

        @year_to   = @date_to.year
        @month_to  = @date_to.month
        @cweek_to  = @date_to.cweek

      end

      if params["_action"] == "legal"

        render(
          :template => "person_checks/legal_export.html.erb",
          :layout   => false
        )
      end

      # if params["_action"] == "legal_pdf"
      #
      #   send_data(
      #     WickedPdf.new.pdf_from_string(
      #       render_to_string(
      #         :template => "person_checks/legal_export.html.erb",
      #         :layout   => false
      #       )
      #     ) ,
      #     :filename => DateTime.now.strftime("%Y%m%dT%H%M") + ".pdf",
      #     :disposition => 'attachment'
      #   )
      #
      # end

    else

      prepare_index( )

    end

  end

  def show
    render :edit
  end

  def new
    @person_check = PersonCheck.new
  end

  def edit
    redirect_to( person_checks_path ) unless @person_check.can_be_modified?
  end

  def swap_kind
    #byebug
    person_check = PersonCheck.find(params[:id].to_i)
    if PersonCheck.kinds[ person_check.kind.to_sym ] == PersonCheck.kinds[ :input ]
      person_check.kind = PersonCheck.kinds[ :output ]
    else
      person_check.kind = PersonCheck.kinds[ :input ]
    end

    if person_check.save
      # respond_to do |format|
        # format.json { head :ok , json: "{'as' => 's'}"}
        render json: {
          success: person_check.kind,
          status: 200
        }, status: 200
      # end
    else
      # respond_to do |format|
        # format.json { render json: "{'as' => 's'}",head :error }
        render json: {
          error: "Can't swap person_check kind",
          status: 400
        }, status: 400
      # end
    end

  end

  def create
    @person_check = PersonCheck.new(person_check_params)

    respond_to do |format|
      if @person_check.save
        format.html { redirect_to @person_check, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@person_check.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @person_check }
      else
        format.html { render action: 'new' }
        format.json { render json: @person_check.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( person_checks_path ) unless @person_check.can_be_modified?
    respond_to do |format|
      if @person_check.update(person_check_params)
        format.html { redirect_to @person_check, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@person_check.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @person_check.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
		@person_check.status = PersonCheck::DELETED
		@person_check.save
    respond_to do |format|
      format.html { redirect_to person_checks_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person_check
      @person_check = PersonCheck.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def person_check_params
      params.require(:person_check).permit( )
    end
end
