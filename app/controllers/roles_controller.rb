class RolesController < ApplicationController
	load_and_authorize_resource

	before_action :set_role, only: [ :show, :edit, :update, :destroy ]

  def index
		# @roles = Role.all
		prepare_index
  end

  def show
		render :edit
  end

  def new
    @role = Role.new
  end

  def edit
		redirect_to( roles_path ) unless @roles.can_be_modified?
  end


	def create
    @role = Role.new(role_params)
    respond_to do |format|
    	if @role.save
        format.html { redirect_to @role, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@role.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @role }
      else
        format.html { render action: 'new' }
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
		redirect_to( roles_path ) unless @roles.can_be_modified?
    respond_to do |format|
      if @role.update(role_params)
        format.html { redirect_to @role, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@role.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
		@role.status = Role::DELETED
		@role.save
    respond_to do |format|
      format.html { redirect_to roles_url }
      format.json { head :no_content }
    end
  end


	def post_assign_permissions

    this_role = @role = Role.find( params[ "role_id"] )

    if this_role.present?

      all_perms = this_role.permissions
      new_perms = []
      perm_to_delete_count = 0

      if params["permission"].present?

        if params["permission"]["ctrllrs"].present?

          controllers = params["permission"]["ctrllrs"]
          controllers.map do | controller_name , actions | controller_name

            this_permission_class = controller_name.classify.constantize
            actions.each do | action_name , action_status |

              perm = Permission.find_or_initialize_by( :role_id => this_role.id , :resource_class => this_permission_class.to_s , :action => action_name )

              unless perm.persisted?
                new_perms << perm if perm.save!
              else
                all_perms = all_perms - [ perm ]
              end

            end

          end

        end

        if ( all_perms - new_perms ).present?

          ( all_perms - new_perms ).each do | perm_to_delete |
            perm_to_delete_count = (perm_to_delete_count + 1) if perm_to_delete.delete
          end
        end
      end

    end

    respond_to do |format|
      # format.html { redirect_to get_permissions_main_view_path }
      format.html { redirect_to get_assign_permissions_path }
    end

  end

  # def get_main_view
  def get_assign_permissions

    if params[ "role_id"].present?
      @role = Role.find( params[ "role_id"] )
      @role_permissions = @role.permissions
    else
      @role = nil
      @role_permissions = nil
    end

    @roles 		= Role.all

    order_by = { :id => :desc }

    if @role.present?

      @controllers_constantized = []
      action_avoided = [ "set_status" , "notice_to_user" , "traverse" , "find_or_initialize_by" ]
      controllers = Dir.new("#{Rails.root}/app/controllers").entries
      controllers.each do |controller|

        if controller =~ /_controller/

          obj_controller = controller.camelize.gsub(".rb","").constantize.new

          if obj_controller.class != ApplicationController
            model = obj_controller.controller_name.classify.constantize unless obj_controller.controller_name.starts_with?('layout_')
            layout = obj_controller if obj_controller.controller_name.starts_with?('layout_')

            if model.present?
              translate_controller = model.model_name.human( :count => 2 )
            elsif layout.present?
              translate_controller = t("layouts.#{obj_controller.controller_name}")
            end

            # actions_for_controller = { "index" => [] , "new" => [] , "view" => [] , "edit" => [] , "delete" => [] , "special" => [] }
            actions_for_controller = { "basic" => { "index" => [] , "new" => [] , "show" => [] , "edit" => [] , "delete" => [] } , "special" => [] }

            controllers_for_show = ( obj_controller.action_methods - action_avoided )

            controllers_for_show.each do | action |
              if action == "index"
                actions_for_controller[ "basic" ][ "index" ] << action
              elsif [ "view", "show" ].include?(action)
                actions_for_controller[ "basic" ][ "show" ] << action
              elsif [ "new", "create" ].include?(action)
                actions_for_controller[ "basic" ][ "new" ] << action
              elsif [ "edit", "update" ].include?(action)
                actions_for_controller[ "basic" ][ "edit" ] << action
              elsif [ "delete", "destroy" ].include?(action)
                actions_for_controller[ "basic" ][ "delete" ] << action
              else
                actions_for_controller[ "special" ] << action
              end
            end

            actions_for_controller = ( actions_for_controller.map do |action_key,actions|
              if actions.present?
                if action_key == "basic"
                  r = { action_key => ( actions.map do | action_parent , action_child |
                    if action_parent == "index"
                      { action_parent => action_child.sort } # Making sort to index (get) => IS UNIQUE
                    elsif action_parent == "show"
                      { action_parent => action_child.sort.reverse } # Making sort to view (get)
                    elsif action_parent == "new"
                      { action_parent => action_child.sort.reverse } # Making sort to new (get), create (post)
                    elsif action_parent == "edit"
                      { action_parent => action_child.sort } # Making sort to edit (get), update (post)
                    elsif action_parent == "delete"
                      { action_parent => action_child.sort } # Making sort to delete (get), destroy (post)
                    end
                  end ).reduce({},:merge) }
                else
                  # This make :special => { "parent"  => [asdjhlads,asldkjlqad,asdljkasd]}
                  { action_key => ( actions.map do |action|
                    { action.gsub( /(get_|post_)/ , '') => [action] }
                  end ).inject{|memo, el| memo.merge( el ){|k, old_v, new_v| old_v + new_v}} }
                end
              end
            end ).compact.reduce({},:merge)

            # Borrar la action sin actions , como por ejemplo { ... , ... , :special => [] }
            actions_for_controller = ( actions_for_controller.map do |action_key,actions|
              if actions.present?
                { action_key => ( actions.map do |k,v|
                  if v.present?
                    {k=>v}
                  end
                end ).compact.reduce({},:merge) }
              end
            end ).compact.reduce({},:merge)

            @controllers_constantized << { translate_controller => { :actions => actions_for_controller , :controller_name => obj_controller.controller_name , :grouped => true } }

          end

        end

      end

      @controllers_constantized = @controllers_constantized.sort_by{ |key, val| key.to_s }

      if @role
        @instances = @role.permissions
      else
        @instances = []
      end

    else

      @instances = []

    end

    prepare_index( order_by )
  end

	private

    # Use callbacks to share common setup or constraints between actions.
    def set_role
      @role = Role.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
		def role_params
	    params.require( :role ).permit( :sector_id , :name , :status )
	  end
end
