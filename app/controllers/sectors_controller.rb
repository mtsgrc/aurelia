class SectorsController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_sector, only: [:show, :edit, :update, :destroy]

  def index
    # @sectors = Sector.all.sort{ |a,b| a.detail <=> b.detail }
    prepare_index
  end

  def show
    render :edit
  end

  def new
    @sector = Sector.new
  end

  def edit
    redirect_to( sectors_path ) unless @sector.can_be_modified?
  end

  def create
    @sector = Sector.new(sector_params)

    respond_to do |format|
      if @sector.save
        format.html { redirect_to @sector, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@sector.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @sector }
      else
        format.html { render action: 'new' }
        format.json { render json: @sector.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( sectors_path ) unless @sector.can_be_modified?
    respond_to do |format|
      if @sector.update(sector_params)
        format.html { redirect_to @sector, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@sector.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @sector.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @sector.status = Sector::DELETED
    @sector.save
    respond_to do |format|
      format.html { redirect_to sectors_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sector
      @sector = Sector.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sector_params
      params.require( :sector ).permit( :company_id , :parent_id , :status , :name )
    end
end
