class SynchronizersController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_synchronizer, only: [:show, :edit, :update, :destroy]

  def index
    # @synchronizers = Synchronizer.all.sort{ |a,b| a.detail <=> b.detail }
    prepare_index
  end

  def show
    render :edit
  end

  def new
    @synchronizer = Synchronizer.new
  end

  def edit
    redirect_to( synchronizers_path ) unless @synchronizer.can_be_modified?
  end

  def create
    @synchronizer = Synchronizer.new(synchronizer_params)

    respond_to do |format|
      if @synchronizer.save
        format.html { redirect_to @synchronizer, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@synchronizer.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @synchronizer }
      else
        format.html { render action: 'new' }
        format.json { render json: @synchronizer.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( synchronizers_path ) unless @synchronizer.can_be_modified?
    respond_to do |format|
      if @synchronizer.update(synchronizer_params)
        format.html { redirect_to @synchronizer, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@synchronizer.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @synchronizer.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
		@synchronizer.status = Synchronizer::DELETED
		@synchronizer.save
    respond_to do |format|
      format.html { redirect_to synchronizers_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_synchronizer
      @synchronizer = Synchronizer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def synchronizer_params
      params.require(:synchronizer).permit( :connection_id, :detail, :action, :table_name, :status, :conditions_default, :job_start_hour, :job_frecuency_seconds, :schemas_default)
    end
end
