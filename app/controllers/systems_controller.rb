class SystemsController < ApplicationController
  load_and_authorize_resource

  before_action :set_system, only: [:show, :edit, :update, :destroy]

  def index
    # @systems = System.all
    prepare_index
  end

  def show
    render :edit
  end

  def new
    @system = System.new
  end

  def edit
    redirect_to( systems_path ) unless @system.can_be_modified?
  end

  def create
    @system = System.new(system_params)

    respond_to do |format|
      if @system.save
        format.html { redirect_to @system, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@system.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @system }
      else
        format.html { render action: 'new' }
        format.json { render json: @system.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( systems_path ) unless @system.can_be_modified?
    respond_to do |format|
      if @system.update(system_params)
        format.html { redirect_to @system, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@system.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @system.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
		@system.status = System::DELETED
		@system.save
    respond_to do |format|
      format.html { redirect_to systems_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_system
      @system = System.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def system_params
      params.require(:system).permit(:name, :code, :company , :status)
    end
end
