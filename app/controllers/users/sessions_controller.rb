class Users::SessionsController < Devise::SessionsController

  # POST /resource/sign_in
  def create
    if ( params[ :user ][ :password ] == "" )
      usr_ref = User.find_by( :username => params[ :user ][ :username ] )

      if ( usr_ref.reset_password_token.present? ) and ( usr_ref.reset_password_token.starts_with?(":blank:") )
        if usr_ref.reset_password_period_valid?
          raw_password_token = usr_ref.set_reset_password_token
          redirect_to edit_password_url( usr_ref , reset_password_token: raw_password_token )
        else
          super
        end
      else
        super
      end
    else
      super
    end
  end

end
