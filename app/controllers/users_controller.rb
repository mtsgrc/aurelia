class UsersController < ApplicationController
	load_and_authorize_resource

	before_action :set_user, only: [ :show, :edit, :update, :destroy ]

  def index
		# @users = User.all
		prepare_index
  end

  def show
		render :edit
  end

  def new
    @user = User.new
  end

  def edit
		redirect_to( users_path ) unless @user.can_be_modified?
  end


	def create
    @user = User.new(user_params)
    respond_to do |format|
    	if @user.save
        format.html { redirect_to @user, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@user.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
		redirect_to( users_path ) unless @user.can_be_modified?
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@user.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
		@user.status = User::DELETED unless @user.username=='root'
		@user.save
    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

	# ____--_-_--__-


  def get_reset_password

    @user = User.find( params[ "user_id" ] )

		if @user.active?
			if @user.person.native?
				session[:return_to] = request.referer
				render :layout => false
			end
		end

  end

  def post_reset_password

    @user = User.find( params[ "user_id" ] )

    if @user.active?

			if @user.person.present?

				if @user.person.native?

					if @user.person.active?

						if @user.email.present?

							token = @user.send_reset_password_instructions

							notice_to_user( @user , [ "Se envió un correo al mail asociado al usuario con el link para que pueda generar una nueva contraseña" , "El token del usuario es <strong>#{token}<strong>".html_safe , "<br />".html_safe , "<br />Link: <a href='#{edit_password_url(@user, reset_password_token: (Devise::VERSION.start_with?('3.', '4.') ? token : @user.reset_password_token))}'> Enlace para nueva contraseña</a>".html_safe] , :success )

			      else
			        notice_to_user( @person , [ "No existe correo electrónico dónde enviar el token generado" ] , :errors )
			      end

		      else
		        notice_to_user( @person , [ "Sólo se puede crear un usuario de una persona activa" ] , :errors )
		      end

	      else
	        notice_to_user( @person , [ "Sólo se puede crear un usuario de una persona nativa" ] , :errors )
	      end

      else
        notice_to_user( @person , [ "El usuario no tiene persona asociada" ] , :errors )
      end

		else
			notice_to_user( @person , [ "No existe la persona de la cuál desea crear un usuario" ] , :errors )
		end

    redirect_to ( session[ :return_to ] ||= request.referer )

  end

	private

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
		def user_params
	    params.require(:user).permit( :username , :name, :email, :password, :password_confirmation, :status, :new_password, :new_password_confirmation )
	  end
end
