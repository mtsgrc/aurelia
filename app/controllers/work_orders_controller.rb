class WorkOrdersController < ApplicationController
  load_and_authorize_resource
  
  before_action :set_work_order, only: [:show, :edit, :update, :destroy]

  def index
    # @work_orders = WorkOrder.all
    prepare_index
  end

  def show
  end

  def new
    @work_order = WorkOrder.new
  end

  def edit
    redirect_to( work_orders_path ) unless @work_order.can_be_modified?
  end

  def create
    @work_order = WorkOrder.new(work_order_params)

    respond_to do |format|
      if @work_order.save
        format.html { redirect_to @work_order, notice: "Se creó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@user.detail}</strong>".html_safe }
        format.json { render action: 'show', status: :created, location: @work_order }
      else
        format.html { render action: 'new' }
        format.json { render json: @work_order.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to( work_orders_path ) unless @work_order.can_be_modified?
    respond_to do |format|
      if @work_order.update(work_order_params)
        format.html { redirect_to @work_order, notice: "Se modificó #{controller_name.classify.constantize.model_name.human.downcase} <strong>#{@user.detail}</strong>".html_safe }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @work_order.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    # @work_order.destroy
    @work_order = WordOrder::DELETED
    @work_order.save
    respond_to do |format|
      format.html { redirect_to work_orders_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_work_order
      @work_order = WorkOrder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def work_order_params
      params.require( :work_order ).permit(:gobbi_number)
    end
end
