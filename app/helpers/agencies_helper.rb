module AgenciesHelper
	def print_quantity_child_homologated instance
		quantity = instance.children.count
		content_tag( :span , :class => "tooltip-demo" ) do
			content_tag( :button , :type => "button" , :class => "btn btn-link" , :data => { :toggle => 'tooltip' , :placement => 'right' , :html => 'true' } , :title => "<span class='text-left'>#{instance.child_arr_descriptions.join('<br /><br />')}</span>".html_safe ) do
				"<strong>#{quantity}</strong> #{ (quantity == 1 ? ' agencia asociada' : ' agencias asociadas' ) }".html_safe
			end
		end
	end

	def print_external_or_local_box instance
		if instance.native?
			content_tag( :span , :class => "label label-success" , :style => "font-size: 1em;") do
				content_tag( :strong ) do "Local" end
			end
		elsif instance.external?
			content_tag( :span , :class => "label" , :style => "font-size: 1em;" ) do
				content_tag( :strong ) do "Externo" end
			end
		end
	end

	def print_homologation_detail instance
		if instance.native?
			print_quantity_child_homologated instance
		elsif instance.external?
			if instance.parent.present?
				content_tag( :span ) do
					content_tag( :i , :class => "fa fa-check-circle fa-fw text-info" ) do end +
					content_tag( :strong ) do
						instance.parent.detail.html_safe
					end
				end
			else
				content_tag( :span , :class => "text-muted" ) do
					content_tag( :i , :class => "fa fa-exclamation-triangle fa-fw text-danger" ) do end + "Sin Homologar".html_safe
				end
			end
		end
	end

end
