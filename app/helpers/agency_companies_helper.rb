module AgencyCompaniesHelper

	def print_system_agency_code instance
		# content_tag( :div , :class => "row" ) do
		# 	content_tag( :div , :class => "col col-xs-4" ) do
		# 		"#{ instance.system.code }".html_safe
		# 	end +
		# 	content_tag( :i , :class => "fa fa-arrow-right fa-fw" ) do end +
		# 	content_tag( :div , :class => "col col-xs-4" ) do
		# 		"#{ instance.agency.code_nosys }".html_safe
		# 	end +
		# 	content_tag( :i , :class => "fa fa-arrow-right fa-fw" ) do end +
		# 	content_tag( :div , :class => "col col-xs-4" ) do
		# 		"#{ instance.company.code_nosys }".html_safe
		# 	end
		# end
		"#{ instance.system.code }".html_safe +
		content_tag( :i , :class => "fa fa-arrow-right fa-fw" ) do end +
		"#{ instance.agency.code_nosys }".html_safe +
		content_tag( :i , :class => "fa fa-arrow-right fa-fw" ) do end +
		"#{ instance.company.code_nosys }".html_safe
	end

	def print_input_param_agency_company param_name, param_type , param_values = []

		content_tag( :div , :class => "row p-xxs form-horizontal" ) do

			content_tag( :div , :class => "col-md-2 control-label text-right" ) do

				if ( param_name == :credit_maximum )
					"Crédito máximo"
				else
					""
				end

			end +

			content_tag( :div , :class => "col-md-10" ) do

				if ( param_type == :text_field )

					content_tag( :input , :class => "form-control" , :placeholder => "Ingrese un valor para #{AgencyCompany.human_attribute_name( param_name )}" , :style => "width: 100%" , :name => "agency_company[#{param_name}]" ) do end

				end

			end
		end

	end

end
