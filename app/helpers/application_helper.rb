module ApplicationHelper

  # For deactivate breadcrumbs in certain view that explodes at loading
  ARR_CONTROLLERS_WITHOUT_BREADCRUMB = [ 'welcome' , '' , 'passwords' ]

  # CONSTANTS
  CONST_BORDERED_CLASSES = "font-bold bg-muted w-100 p-w-xs b-r-sm border-top border-bottom border-left border-right text-center h4 m-xxs"

  # CONSTANTS OF CONDITIONS
  CONST_CONDITIONS = {  "Único valor" => [
                          [ "*_eq"              , "Igual a"             ] ,
                          [ "*_parent_eq"       , "Homologación igual a"] ,
                          [ "*_not_eq"          , "Distinto a"          ] ,
                          [ "*_lt"              , "Menor que"           ] ,
                          [ "*_lteq"            , "Menor o igual que"   ] ,
                          [ "*_gt"              , "Mayor que"           ] ,
                          [ "*_gteq"            , "Mayor o igual que"   ] ,
                          [ "*_start"           , "Comience con"        ] ,
                          [ "*_not_start"       , "No comience con"     ] ,
                          [ "*_end"             , "Termine con"         ] ,
                          [ "*_not_end"         , "No termine con"      ] ,
                          [ "*_cont"            , "Contenga"            ] ,
                          [ "*_not_cont"        , "No contenga"         ] ,
                          [ "*_matches"         , "(REGEX) Contenga"    ] ,
                          [ "*_does_not_match"  , "(REGEX) No contenga" ] ,
                      ] ,
                      "Varios valores" => [
                          [ "*_in"                  , "[ Arreglo ] Igual a"                                 ] ,
                          [ "*_not_in"              , "[ Arreglo ] Distinto a"                              ] ,
                          [ "*_start_any"           , "[ Arreglo ] Comience con (algunos valores)"          ] ,
                          [ "*_start_all"           , "[ Arreglo ] Comience con (todos los valores)"        ] ,
                          [ "*_not_start_any"       , "[ Arreglo ] No comience con (algunos valores)"       ] ,
                          [ "*_not_start_all"       , "[ Arreglo ] No comience con (todos los valores)"     ] ,
                          [ "*_end_any"             , "[ Arreglo ] Termine con (algunos valores)"           ] ,
                          [ "*_end_all"             , "[ Arreglo ] Termine con (todos los valores)"         ] ,
                          [ "*_not_end_any"         , "[ Arreglo ] No termine con (algunos valores)"        ] ,
                          [ "*_not_end_all"         , "[ Arreglo ] No termine con (todos los valores)"      ] ,
                          [ "*_cont_any"            , "[ Arreglo ] Contenga (algunos valores)"              ] ,
                          [ "*_cont_all"            , "[ Arreglo ] Contenga (todos los valores)"            ] ,
                          [ "*_not_cont_any"        , "[ Arreglo ] No contenga (algunos valores)"           ] ,
                          [ "*_not_cont_all"        , "[ Arreglo ] No contenga (todos los valores)"         ] ,
                          [ "*_matches_any"         , "[ Arreglo ] (REGEX) Contenga (algunos valores)"      ] ,
                          [ "*_matches_all"         , "[ Arreglo ] (REGEX) Contenga (todos los valores)"    ] ,
                          [ "*_does_not_match_any"  , "[ Arreglo ] (REGEX) No contenga (algunos valores)"   ] ,
                          [ "*_does_not_match_all"  , "[ Arreglo ] (REGEX) No contenga (todos los valores)" ]
                      ] ,
                      "Otros" => [
                          [ "*_present" , "Con valor"     ] ,
                          [ "*_blank"   , "Sin valor"     ] ,
                          [ "*_null"    , "Nulo"          ] ,
                          [ "*_true"    , "Sea verdadero" ] ,
                          [ "*_false"   , "Sea falso"     ]
                      ]

                    }

  # HASH FOR APPEARANCES IN LINK TYPES
  HASH_LINKS_APPEARANCES = {
                              :in_form => {

                                            :view => {
                                                        :type => 'button',
                                                        :class => {
                                                                    :type      => 'btn',
                                                                    :size      => 'btn-md',
                                                                    :color_bg  => 'btn-primary',
                                                        },
                                                        :icon => 'fa-search',
                                                        :text => 'Ver'
                                            } ,

                                            :special => {
                                                        :type => 'button',
                                                        :class => {
                                                                    :type      => 'btn',
                                                                    :size      => 'btn-md',
                                                                    :color_bg  => 'btn-link',
                                                                    :color_txt => 'text-muted',
                                                        },
                                                        :icon => 'fa-hand-o-right',
                                                        :text => ''
                                            } ,

                                            :index => {
                                                        :type => 'button',
                                                        :class => {
                                                                    :type      => 'btn',
                                                                    :size      => 'btn-md',
                                                                    :color_bg  => 'btn-link',
                                                                    :color_txt => 'text-muted',
                                                        },
                                                        :icon => 'fa-chevron-left',
                                                        :text => 'Volver'
                                            } ,

                                            :new => {
                                                        :type => 'button',
                                                        :class => {
                                                                    :type      => 'btn',
                                                                    :size      => 'btn-md',
                                                                    :color_bg  => 'btn-info',
                                                        },
                                                        :icon => 'fa-plus',
                                                        :text => ''
                                            } ,

                                            :edit => {
                                                        :type => 'button',
                                                        :class => {
                                                                    :type       => 'btn',
                                                                    :size       => 'btn-md',
                                                                    :color_bg   => 'btn-success',
                                                        },
                                                        :icon => 'fa-pencil',
                                                        :text => 'Editar'
                                            } ,

                                            :delete => {
                                                        :type => 'button',
                                                        :class => {
                                                                    :type       => 'btn',
                                                                    :size       => 'btn-md',
                                                                    :color_bg   => 'btn-danger',
                                                        },
                                                        :icon => 'fa-times-circle',
                                                        :text => 'Eliminar'
                                            } ,
                                            :submit => {
                                                        :type => nil,
                                                        :class => {
                                                                    :type       => 'btn',
                                                                    :size       => 'btn-md',
                                                                    :color_bg   => 'btn-primary',
                                                        },
                                                        :icon => 'fa-check',
                                                        :text => 'Guardar'
                                            } ,

                              } ,

                              :in_dropdown => {

                                          :special => {
                                            :type => 'button',
                                            :class => {
                                              :type       => 'btn',
                                              :size       => 'btn-md',
                                              :color_bg   => 'btn-link',
                                              :color_txt  => 'text-muted',
                                            },
                                            :icon => 'fa-hand-o-right',
                                            :text => ''
                                            } ,

                                            :view => {
                                                        :type => 'button',
                                                        :class => {
                                                                    :type       => 'btn',
                                                                    :size       => 'btn-md',
                                                                    :color_bg   => 'btn-link',
                                                                    :color_txt  => 'text-muted',
                                                        },
                                                        :icon => 'fa-eye',
                                                        :text => 'Ver'
                                            } ,

                                            :edit => {
                                                        :type => 'button',
                                                        :class => {
                                                                    :type       => 'btn',
                                                                    :size       => 'btn-md',
                                                                    :color_bg   => 'btn-link',
                                                                    :color_txt  => 'text-success',
                                                        },
                                                        :icon => 'fa-pencil',
                                                        :text => 'Editar'
                                            } ,

                                            :delete => {
                                                        :type => 'button',
                                                        :class => {
                                                                    :type       => 'btn',
                                                                    :size       => 'btn-md',
                                                                    :color_bg   => 'btn-link',
                                                                    :color_txt  => 'text-danger',
                                                        },
                                                        :icon => 'fa-times-circle',
                                                        :text => 'Eliminar'
                                            } ,


                              } ,

                              :just_text => {

                                            :special => {
                                                        :type => 'span',
                                                        :text => '' ,
                                                        :class => {}
                                            } ,

                                            :view => {
                                                        :type => 'span',
                                                        :text => 'Ver' ,
                                                        :class => {}
                                            } ,
                                            :edit => {
                                                        :type => 'span',
                                                        :text => 'Ver' ,
                                                        :class => {}
                                            } ,
                                            :delete => {
                                                        :type => 'span',
                                                        :text => 'Ver' ,
                                                        :class => {}
                                            } ,
                              } ,

                              :menu => {
                                :index => {
                                  :type => 'link',
                                } ,
                                :pure_link => {
                                  :type => 'link',
                                } ,
                              }

                           }

   HASH_FORM_INPUTS = {
                        :class => 'form-control',
                        :placeholder => 'Ingrese un valor para',
                        :style => 'width: 100%'
    }

   HASH_FORMAT_INPUTS = {
                        :position => :in_form ,
                        :with_label => true
    }

    MODELS_WITH_NO_HOMOLOGATION = [ Liquidation ]

  # For make in navigation left menu parent item focused
  def is_active_controller(controller_name, class_name = nil)
    if params[:controller] == controller_name
      class_name == nil ? "active" : class_name
    else
      nil
    end
  end

  # For make in navigation left menu item focused
  def is_active_action(action_name)
    params[:action] == action_name ? "active" : nil
  end

# ------------------------------------------------ [ HEADERS ] ------------------------------------------------

  def print_form_input( form , action , input_type , attribute , format_input={}, opts={} )

    # Merging default options with passed by user
    options       = HASH_FORM_INPUTS.merge( opts )
    input_format  = HASH_FORMAT_INPUTS.merge( format_input )

    #if form.object.class.
    str_human_attribute = form.object.class.human_attribute_name( attribute )
    str_class = form.object.class.to_s.underscore
    value_of_attribute = form.object.read_attribute( attribute )

    options[ :placeholder ] = options[ :placeholder ] + " " + str_human_attribute unless opts[ :placeholder ]
    arr_static_syms = { :p => [ :text_field, :email_field , :select_field , :date_field, :text_area , :number_field , :float_field , :image_field , :mask_field ] }

    # If is a view of :show we show data in <p></p> tag
    if action.to_sym == :show
      if ( attribute != :password && attribute != :password_confirmation && attribute != :password_new )
        if ( input_type == :select_field )
          if ( attribute == :status )
            str_input = content_tag :p, :class => "form-control-static" do t( "statuses.#{str_class}.single.#{str_class.classify.safe_constantize.statuses.status_for(value_of_attribute).symbol}" , :default => "statuses.#{str_class}.single.#{str_class.classify.safe_constantize.statuses.status_for(value_of_attribute).symbol}" ) end if arr_static_syms[ :p ].include?( input_type )
          elsif ( attribute == :kind )
            str_input = content_tag :p, :class => "form-control-static" do t( "kinds.#{str_class}.single.#{form.object.kind}" ) end if arr_static_syms[ :p ].include?( input_type )
          elsif ( attribute == :kind_action )
            str_input = content_tag :p, :class => "form-control-static" do t( "kind_actions.#{str_class}.#{form.object.kind_action}" ) end if arr_static_syms[ :p ].include?( input_type )
          elsif ( attribute == :parent_id )
            str_input = content_tag :p, :class => "form-control-static" do form.object.parent.detail end if arr_static_syms[ :p ].include?( input_type ) and form.object.parent_id?
          elsif ( attribute == :faicon )
            str_input = content_tag :p, :class => "form-control-static" do "<i class='fa #{form.object.faicon} fa-fw'></i>".html_safe end if arr_static_syms[ :p ].include?( input_type )
          elsif ( attribute == :external_id )
            str_input = content_tag :p, :class => "form-control-static" do value_of_attribute.to_s end if arr_static_syms[ :p ].include?( input_type )
          elsif ( attribute == :resource_id )
            str_input = content_tag :p, :class => "form-control-static" do "#{ ( !( Object.const_get form.object.kind.capitalize ).where( :id => form.object.resource_id).empty? ? t( "kinds.#{str_class}.single.#{form.object.kind}" ) + " " + ( Object.const_get form.object.kind.capitalize).find( form.object.resource_id) : 'No se encuentra resurso' ) } " end if arr_static_syms[ :p ].include?( input_type )
          elsif ( attribute.to_s.ends_with?("_id") )
            str_input = content_tag :p, :class => "form-control-static" do "#{ ( !( Object.const_get attribute.to_s.gsub("_id","").classify ).where( :id => form.object.read_attribute(attribute) ).empty? ? ( Object.const_get attribute.to_s.gsub("_id","").classify ).where( :id => form.object.read_attribute(attribute) ).first.detail : 'No se encuentra resurso' ) } " end if arr_static_syms[ :p ].include?( input_type )
          else
            str_input = content_tag :p, :class => "form-control-static" do options[ :all_values ].collect { |k,v| [v,k] }.to_h[value_of_attribute].html_safe end if arr_static_syms[ :p ].include?( input_type )
          end
        elsif ( input_type == :image_field )
          if eval( "form.object.#{attribute}.exists?" )
            str_input = content_tag :p, :class => "form-control-static" do image_tag eval( "form.object.#{attribute}" ) end if arr_static_syms[ :p ].include?( input_type )
          else
            str_input = content_tag :p, :class => "form-control-static" do "**  Sin imágen **" end if arr_static_syms[ :p ].include?( input_type )
          end
        elsif ( attribute == :external_jsondata )
          str_input = print_hash( value_of_attribute.to_s )
        else
          if arr_static_syms[ :p ].include?( input_type )
            str_input = content_tag :p, :class => "form-control-static" do value_of_attribute.to_s end
          end
        end
      else
        str_input = content_tag :p, :class => "form-control-static" do "<i>** Contraseña encriptada **</i>".html_safe end
      end
    end

    # If is a view of :edit, :new or :create, where we have text_boxes, then we'll insert them
    if [ :edit , :new , :create , :update ].include?(action.to_sym)

      str_input = form.text_field attribute, :class => options[ :class ] , :placeholder => options[ :placeholder ] , :style => options[ :style ]                                                                                    if input_type == :text_field
      str_input = form.date_field attribute, :class => options[ :class ] , :placeholder => options[ :placeholder ] , :style => options[ :style ] , :min => options[ :min ] , :max => options[ :max ] , :value => options[ :value ]  if input_type == :date_field
      str_input = form.text_area attribute, :class => options[ :class ] , :placeholder => options[ :placeholder ] , :style => options[ :style ]                                                                                     if input_type == :text_area
      str_input = form.password_field attribute, :class => options[ :class ] , :placeholder => options[ :placeholder ] , :style => options[ :style ]                                                                                if input_type == :password_field
      str_input = form.email_field attribute, :class => options[ :class ] , :placeholder => options[ :placeholder ] , :style => options[ :style ]                                                                                   if input_type == :email_field
      str_input = form.number_field attribute, :class => options[ :class ] , :placeholder => options[ :placeholder ] , :style => options[ :style ]                                                                                  if input_type == :number_field
      str_input = form.number_field attribute, :class => options[ :class ] , :placeholder => options[ :placeholder ] , :style => options[ :style ] , :step => :any                                                                  if input_type == :float_field
      str_input = form.file_field attribute, :class => options[ :class ] , :placeholder => options[ :placeholder ] , :style => options[ :style ]                                                                                    if input_type == :image_field

      if input_type == :select_field
        if attribute == :status
          str_input = form.select attribute , options[ :all_values ].collect { |v| [ t( "statuses.#{str_class}.single.#{v.name}" , :default => "statuses.#{str_class}.single.#{v.name}" ) , v.value ] } , { include_blank: options[ :include_blank ] }, { :class => options[ :class ] , :style => options[ :style ] , :selected => form.object }
        elsif attribute == :kind
          str_input = form.select attribute , options[ :all_values ].collect { |k,v| [ t( "kinds.#{str_class}.single.#{k}") , k ] } , { include_blank: options[ :include_blank ] }, { :class => options[ :class ] , :style => options[ :style ] , :selected => form.object }
        elsif attribute == :kind_action
          str_input = form.select attribute , options[ :all_values ].collect { |k,v| [ t( "kind_actions.#{str_class}.#{k}") , k ] } , { include_blank: options[ :include_blank ] }, { :class => options[ :class ] , :style => options[ :style ] , :selected => form.object }
        elsif attribute == :resource_id
          str_input = ( content_tag( :div , :class => "input-group m-b" ) do
            content_tag( :div , :class => "input-group-btn" ) do
              hidden_field( str_class.to_sym , :kind , :id => "#{str_class}_kind" ) +
              ( button_tag( :data => { :toggle => "dropdown" } , :class => "btn btn-primary dropdown-toggle" , :type => "button" , :aria => { :expanded => "false" } , :id => "#{str_class}_kind_dropdown" ) do
                ( "Recurso " + content_tag( :span , :class => "caret") do end ).html_safe
              end ).html_safe +
              ( content_tag( :ul , :class => "dropdown-menu" ) do
                arr_options = []
                options[ :all_values ].each_with_index do | ( option_key , option_value ) ,index|
                  arr_options << content_tag( :li ) do
                    content_tag( :a , :href => "#" , :class => "dropdown-item" , :data => { :key => option_key , :value => option_value } ) do "#{ t( "kinds.#{str_class}.single.#{option_key}") }" end
                  end
                end
                arr_options.join.html_safe
              end ).html_safe
            end +
            form.text_field( attribute, :class => options[ :class ] , :placeholder => options[ :placeholder ] , :style => options[ :style ] )
            # ( content_tag( :input , :type => "text" , :class => "form-control" ) ).html_safe
          end ).html_safe
        elsif ( attribute.to_s.ends_with?("_id") )
          str_input = form.select attribute , options[ :all_values ].collect { |k,v| [k.detail,k.id] } , { include_blank: options[ :include_blank ] }, { :class => options[ :class ] , :style => options[ :style ] , :selected => form.object }
        else
          values_for_options = {}
          values_for_options = options[ :all_values ] unless options[ :all_values ].nil?
          str_input = form.select attribute , values_for_options.collect { |k,v| [k,v] } , { include_blank: options[ :include_blank ] }, { :class => options[ :class ] , :style => options[ :style ] , :selected => form.object }
        end
      elsif input_type == :mask_field
        str_input = form.text_field attribute, :class => options[ :class ] , :placeholder => options[ :placeholder ] , :style => options[ :style ] , :data => options[ :data ]
      end

    end

    str_to_render = ""

    # If text_field
    # form.text_field attribute, class: options[ :class ] , placeholder: options[ :placeholder ] , style: options[ :style ]  if input_type == :text_field
    #content_tag :p, :class => "form-control-static" do child_form.object.name end
    if input_format[ :position ] == :in_form
      if input_format[ :with_label ] == true
        str_to_render << "<div class='form-group'>"
        if attribute != :external_jsondata
          str_to_render << form.label( attribute  , str_human_attribute , class: 'col-xs-3 control-label' )
        end
          if attribute == :external_jsondata
            str_to_render << content_tag( :div , :class => "row" ) do
              form.label( attribute , content_tag( :i , :class => "fa fa-angle-double-right fa-fw" ) do end + content_tag( :u ) do str_human_attribute end + content_tag( :i , :class => "fa fa-angle-double-left fa-fw") do end  , class: 'col-xs-6 text-center' )
            end
          end
          str_to_render << ( attribute != :external_jsondata ? "" : "<div class='row'>" )
            str_to_render << ( attribute != :external_jsondata ? "<div class='col-xs-9'>" : "<div class='col-xs-6'>" )
              str_to_render << str_input.to_s
              unless form.object.valid_attribute?( attribute )
                str_to_render << "<p class=\"text-danger text-left\">"
                str_to_render << form.object.errors[attribute].first.capitalize
                str_to_render << "</p>"
              end
            str_to_render << "</div>"
          str_to_render << ( attribute != :external_jsondata ? "" : "</div>" )
        str_to_render << "</div>"
      else
        str_to_render << "<div class='form-group'>"
          str_to_render << "<div class='col-xs-12'>"
            str_to_render << str_input
          str_to_render << "</div>"
        str_to_render << "</div>"
      end
    end

    return str_to_render.html_safe

  end

  # ------------------------------------------------ [ LINKS ] ------------------------------------------------

  # Super method for make links (buttons or non-buttons)
  def super_link_to( action , resource, type_link, opts = {} , modal = nil , special_action = false , str_args = nil )

    # Merging default options with passed by user
    if HASH_LINKS_APPEARANCES[ type_link ][ action ]
      options = HASH_LINKS_APPEARANCES[ type_link ][ action ].merge(opts)
    else
      options = HASH_LINKS_APPEARANCES[ type_link ][ :special ].merge(opts)
    end

    # string for icon in content concatenated with any text like Eliminar, Modificar, Agregar, ... if is setted
    str_content_link = [ ( options[ :icon ] ? "<i class='fa #{options[ :icon ]} fa-fw'></i>" : nil ) , options[ :text ] ].join(' ').strip

    # This make a string like 'btn btn-link btn-md text-muted', joining values of keys inside :class
    str_class_link = options[ :class ].map{|k,v| "#{v}"}.join(' ') if !options[ :class ].nil?

    # Index if resource is a Class,
    if resource.class == Class
      if modal.present?
        return ( link_to self.send( "#{resource.name.underscore.pluralize}_path" ) , :data => { :toggle => "modal" , :target => "##{modal}" } do str_content_link.html_safe end ) if action == :index
      else
        return ( link_to self.send( "#{resource.name.underscore.pluralize}_path" ) do str_content_link.html_safe end ) if action == :index
      end

      # Next is for DelayedJob that not exists new_..._path
      begin
        if modal.present?
          if modal.class == String
            # return ( link_to self.send( "#{action.to_s}#{ ( special_action == false ? "_" + resource.class.to_s.underscore : "" ) }_path", resource ) , :class => str_class_link , :data => { :toggle => "modal" , :target => "##{modal}" } do str_content_link.html_safe end )
            if resource == Delayed::Backend::ActiveRecord::Job
              return ( link_to self.send( "get_new_#{resource.name.underscore.split('/').last}_path" ) + "#{str_args}" , :class => str_class_link , :data => { :toggle => "modal" , :target => "##{modal}" } do str_content_link.html_safe end ) if action == :new
            else
              return ( link_to self.send( "new_#{resource.name.underscore.split('/').last}_path" ) + "#{str_args}" , :class => str_class_link , :data => { :toggle => "modal" , :target => "##{modal}" } do str_content_link.html_safe end ) if action == :new
            end
          elsif modal.class == TrueClass
            if resource == Delayed::Backend::ActiveRecord::Job
              return ( link_to self.send( "get_new_#{resource.name.underscore.split('/').last}_path" ) + "#{str_args}"  , :class => str_class_link , :data => { :toggle => "modal" , :target => "#modal" } do str_content_link.html_safe end ) if action == :new
            else
              return ( link_to self.send( "new_#{resource.name.underscore.split('/').last}_path" ) + "#{str_args}" , :class => str_class_link , :data => { :toggle => "modal" , :target => "##{modal}" } do str_content_link.html_safe end ) if action == :new
            end
          end
        else
          return ( link_to self.send( "new_#{resource.name.underscore.split('/').last}_path" ) + "#{str_args}" , :class => str_class_link do str_content_link.html_safe end ) if action == :new
        end
      rescue
        return "".html_safe
      end

    elsif resource.class == String and action != :pure_link
      if modal.present?
        return ( link_to self.send( "#{resource}" ) , :data => { :toggle => "modal" , :target => "##{modal}" }  do str_content_link.html_safe end )
      else
        return ( link_to self.send( "#{resource}" ) do str_content_link.html_safe end )
      end
    elsif resource.class == String and action == :pure_link
      if modal.present?
        return ( link_to "#{resource}" , :data => { :toggle => "modal" , :target => "##{modal}" } do str_content_link.html_safe end )
      else
        if str_class_link.present?
          if request.path == resource
            # return ( link_to "#" , :class => str_class_link  do str_content_link.html_safe end )
            str_class_link = str_class_link
            return content_tag( :span , :class => str_class_link.gsub("btn-default", "btn-success"), :style => "cursor: default;" ) do str_content_link.html_safe end
          else
            return ( link_to "#{resource}" , :class => str_class_link  do str_content_link.html_safe end )
          end
        else
          if request.path == resource
            return content_tag( :a , :href => "#" ) do str_content_link.html_safe end
          else
            return ( link_to "#{resource}" do str_content_link.html_safe end )
          end
        end
      end
    else

      if action == :pure_link
        if modal.present?
          return ( link_to self.send( "#{resource.class.to_s.underscore.pluralize}_path" ) , :class => str_class_link , :data => { :toggle => "modal" , :target => "##{modal}" } do str_content_link.html_safe end )
        else
          return ( link_to self.send( "#{resource.class.to_s.underscore.pluralize}_path" ) , :class => str_class_link do str_content_link.html_safe end )
        end
      end
      if action == :index
        if modal.present?
          return ( link_to self.send( "#{resource.class.to_s.underscore.pluralize}_path" , :data => { :toggle => "modal" , :target => "##{modal}" } ) , :class => str_class_link do str_content_link.html_safe end )
        else
          return ( link_to self.send( "#{resource.class.to_s.underscore.pluralize}_path" ) , :class => str_class_link do str_content_link.html_safe end )
        end
      end
      if action == :view
        if modal.present?
          return ( link_to self.send( "#{resource.class.to_s.underscore}_path", resource ) , :class => str_class_link , :data => { :toggle => "modal" , :target => "##{modal}" } do str_content_link.html_safe end )
        else
          begin
            return ( link_to self.send( "#{resource.class.to_s.underscore}_path", resource ) , :class => str_class_link do str_content_link.html_safe end )
          rescue Exception => exc
            return ( link_to eval( "#{resource.class.to_s.underscore}_path( :id => resource.id )" ) , :class => str_class_link do str_content_link.html_safe end )
          end
        end
      end
      if action == :edit
        if modal.present?
          return ( link_to self.send( "edit_#{resource.class.to_s.underscore}_path", resource ) , :class => str_class_link , :data => { :toggle => "modal" , :target => "##{modal}" } do str_content_link.html_safe end )
        else
          return ( link_to self.send( "edit_#{resource.class.to_s.underscore}_path", resource ) , :class => str_class_link do str_content_link.html_safe end )
        end
      end

      if action == :delete
        return ( link_to self.send( "#{resource.class.to_s.underscore}_path", resource ), method: :delete, data: { confirm: 'Está seguro que desea borrar el elemento seleccionado?' } , :class => str_class_link do str_content_link.html_safe end )
      end

      if action == :submit
        if modal.present?
          return ( content_tag :button, :type => :submit , :class => str_class_link , :data => { :toggle => "modal" , :target => "##{modal}" } do str_content_link.html_safe end )
        else
          return ( content_tag :button, :type => :submit , :class => str_class_link do str_content_link.html_safe end )
        end
      end

      # Special actions
      if action.class == Symbol
        if modal.present?
          if modal.class == String
            return ( link_to self.send( "#{action.to_s}#{ ( special_action == false ? "_" + resource.class.to_s.underscore : "" ) }_path", resource ) , :class => str_class_link , :data => { :toggle => "modal" , :target => "##{modal}" } do str_content_link.html_safe end )
          elsif modal.class == TrueClass
            # return ( link_to self.send( "#{action.to_s}#{ ( special_action == false ? "_" + resource.class.to_s.underscore : "" ) }_path", resource ) , :id => resource , :class => str_class_link , :data => { :toggle => "modal" , :target => "#modal" } do str_content_link.html_safe end )
            link = "#{action.to_s}#{ ( special_action == false ? "_" + resource.class.to_s.underscore : "" ) }_path( :#{resource.class.to_s.underscore}_id => #{resource.id} )"
            return ( link_to eval( link ) + "#{str_args}" , :class => str_class_link , :data => { :toggle => "modal" , :target => "#modal" } do str_content_link.html_safe end )
          end
        else
          return ( link_to self.send( "#{action.to_s}#{ ( special_action == false ? "_" + resource.class.to_s.underscore : "" ) }_path", resource ) , :class => str_class_link do str_content_link.html_safe end )
        end
      else
        if modal.present?
          if modal.class == String
            return ( link_to [ self.send( "#{resource.class.to_s.underscore}_path", resource ) , action ].join('/') , :id => resource, :class => str_class_link , :data => { :toggle => "modal" , :target => "##{modal}" } do str_content_link.html_safe end )
          elsif modal.class == TrueClass
            return ( link_to [ self.send( "#{resource.class.to_s.underscore}_path", resource ) , action ].join('/') , :id => resource, :class => str_class_link , :data => { :toggle => "modal" , :target => "#modal" } do str_content_link.html_safe end )
          end
        else
          return ( link_to [ self.send( "#{resource.class.to_s.underscore}_path", resource ) , action ].join('/') , :id => resource, :class => str_class_link do str_content_link.html_safe end )
        end
      end

    end

  end

  # ------------------------------------------------ [ BREADNAV ] ------------------------------------------------
  def render_header arr_breadcrumb_inner_nodes = []

    if !ARR_CONTROLLERS_WITHOUT_BREADCRUMB.include?(controller_name) && !controller_name.starts_with?("layout_")

      str_controller_humanized_single  = controller_name.classify.constantize.model_name.human( :count => 1 )
      str_controller_humanized_plural  = controller_name.classify.constantize.model_name.human( :count => 2 )

      str_action_humanized      = I18n.t( "actions.#{controller_name}.#{action_name}" , :default => I18n.t( "actions.default.#{action_name}" ) ) unless action_name == 'index'

      if action_name == 'index'
        str_controller_action   = str_controller_humanized_plural
      else
        str_controller_action   = str_action_humanized + " " + str_controller_humanized_single.downcase
      end

      # ------------------------------------------------------------ Add Breadcrumbs
        # Add Home Link to Breadcrumbs
        add_breadcrumb("Inicio", root_path)

        # Add inner Controllers Indexes if exists
        arr_breadcrumb_inner_nodes.each do |n|
          unless current_page?( n[:link] )
            add_breadcrumb( n[ :text ], n[:link] )
          end
        end

        # Add Parent Controller
        if action_name.to_sym != :index
          add_breadcrumb( str_controller_humanized_plural , url_for(:action => :index))
        end

        # Finally add action
        add_breadcrumb(str_controller_action)

      # ------------------------------------------------------------ Render Title & Breadcrumbs
        # Make HTML
        content_tag(:div, class: 'row wrapper border-bottom white-bg page-heading') do
          content_tag(:div, class: 'col-lg-10') do
            content_tag(:h2) do
              content_tag(:i, nil, class: 'fa fa-angle-double-right fa-fw') + " " + str_controller_action
            end +
            content_tag(:ol, class: 'breadcrumb') do
              render_breadcrumbs(tag: "li", separator: "" )
            end
          end +
          content_tag(:div, class: 'col-lg-2') { "" }
        end +
        tag(:br)

    end
  end

  # Just call to the translation throught yml file
	def translate_status( instance , count = :single )
    if instance.kind_of?(Hash)
      model_name = instance[:class].model_name.to_s.underscore
      return t( "statuses.#{model_name}.single.#{instance[:status]}" , :default => "statuses.#{model_name}.single.#{instance[:status]}" ) if count == :single
      return t( "statuses.#{model_name}.plural.#{instance[:status]}" , :default => "statuses.#{model_name}.plural.#{instance[:status]}" ) if count == :plural
    else
      model_name = instance.class.to_s.underscore
    	return t( "statuses.#{model_name}.single.#{instance.class.statuses.key( instance.status ).to_s}" , :default => "statuses.#{model_name}.single.#{instance.class.statuses.key( instance.status ).to_s}" ) if count == :single
    	return t( "statuses.#{model_name}.plural.#{instance.class.statuses.key( instance.status ).to_s}" , :default => "statuses.#{model_name}.plural.#{instance.class.statuses.key( instance.status ).to_s}" ) if count == :plural
    end
	end

  # Just call to the translation throught yml file
	def translate_kind( instance , count = :single )
    if instance.kind_of?(Hash)
      return t( "kinds.#{instance[:class].model_name.to_s.underscore}.single.#{instance[:kind]}" ) if count == :single
      return t( "kinds.#{instance[:class].model_name.to_s.underscore}.plural.#{instance[:kind]}" ) if count == :plural
    else
    	return t( "kinds.#{instance.class.to_s.underscore}.single.#{instance.kind}" ) if count == :single
    	return t( "kinds.#{instance.class.to_s.underscore}.plural.#{instance.kind}" ) if count == :plural
    end
	end

  # Just call to the translation throught yml file
	def translate_action ( model , action )
    if action.class == Symbol
      # if action.to_s.starts_with?('get_') or action.to_s.starts_with?('post_')
      #   action_to_translate = action.to_s.gsub(/^get_|^post_/,'')
      #   return t( "actions.#{ model.to_s }.#{ action_to_translate }" , :default => t( "actions.#{model.to_s}.#{ action_to_translate }" , :default => t( "actions.default.#{ action_to_translate }") ) )
      # else
        return t( "actions.#{ model.to_s }.#{ action.to_s }" , :default => t( "actions.#{model.to_s}.#{ action.to_s }" , :default => t( "actions.default.#{ action.to_s }") ) )
      # end
    else
      return t( "actions.#{ model.to_s }.#{ action }" , :default => t( "actions.#{model.to_s}.#{ action }" , :default => t( "actions.default.#{ action }")  )  )
    end
	end

  # Print global form
  def render_form_entire form

    # We open file that contains all form structure of attributes models's grouping
    yml_attrs_form_structure = YAML.load_file( Rails.root.to_s + "/lib/structures.yml" )

    # This are specials settings
    content               = []
    content_tabs          = []
    content_tabs_content  = []

    if yml_attrs_form_structure[ form.object.class.to_s.underscore ]

      # We get da structure of the attributes grouping
      form_structure = yml_attrs_form_structure[ form.object.class.to_s.underscore ]["form"]

      # Adding status information
      form_structure[ "status" ] = {"transl"=>"Estado actual", "icon"=>"fa-toggle-on", "attrs"=>{"status"=>{"type"=>"icon"}}}

      if ( ( form.object.class.column_names & [ "system_id" ] ).present? )
        if ( form.object.system_id != 0 )
          if ( form.object.class.column_names & [ "system_id", "synchronizer_id", "external_id", "external_jsondata" , "gobbi_schema_id" ] )
            form_structure[ "external" ] = {"transl"=>"Datos externos", "icon"=>"fa-external-link", "attrs"=>{"system_id"=>{"type"=>"select_field"},"synchronizer_id"=>{"type"=>"select_field"},"external_id"=>{"type"=>"text_field"},"external_jsondata"=>{"type"=>"select_field"},"gobbi_schema_id"=>{"type"=>"select_field"}}}
          end
          if ( form.object.class.column_names.include?("parent_id") )
            form_structure[ "homologation" ] = {"transl"=>"Homologación", "icon"=>"fa-sitemap", "attrs"=>{ "parent_id"=>{ "type"=>"select_field" } } }
          end
        end
      end
      form_structure[ "audits" ] = {"transl"=>"Auditoría", "icon"=>"fa-clock-o", "attrs"=>{ "audits"=> {} }}


      # --------------------------------------------- HEADER ---------------------------------------------
      content << content_tag( :div , :class => "ibox float-e-margins" ) do

        content_tag( :div , :class => "ibox-title" , :style => "padding-bottom: 0px;") do
          # content_tag( :div , :class => "row form_header" ) do
          content_tag( :div, :class => "row form_header" ) do
            content_tag( :div, :class => "col-lg-12 col-md-12 col-sm-12 col-xs-12" ) do
              content_tag( :div, :class => "pull-left m-sm" ) do
                super_link_to( :index, form.object , :in_form )
              end +
              content_tag( :div, :class => "pull-right m-sm") do
                super_link_to( :submit, form.object , :in_form ) if params[ :action ].to_sym != :show and form.object.can_be_modified?
              end
            end
          end
        end +
        content_tag( :div , :class => "ibox-content" , :style => "padding-top: 0px;") do
          # --------------------------------------------- TABS ---------------------------------------------
          content_tag( :div, :class => "tabs-container" ) do
            content_tag( :div, :class => "tabs-left" ) do

              content_tag( :ul, :class => "nav nav-tabs" ) do
                form_structure.each_with_index do | ( attrs_key ,attrs_opts ) , index |
                  content_tabs << content_tag( :li , :class => ( index == 0 ? "active" : "" ) ) do
                    if ( ! [ :new , :create , :edit , :update ].include?(params[ :action ].to_sym) || ! [ :status , :audits ].include?( attrs_key.to_sym ) )
                      content_tag( :a , :data => { :toggle => "tab" } , :href => "#tab-#{attrs_key}" ) do
                        content_tag( :i , :class => "fa #{attrs_opts['icon']} fa-fw" ) do "" end + " #{attrs_opts['transl']}" +
                        ( !( form.object.errors.messages.keys & attrs_opts["attrs"].keys.map{ |n| n.to_sym } ).empty? ? content_tag( :i , :class => "fa fa-exclamation-circle fa-fw text-danger pull-right" ) do end : "" )
                      end
                    end
                  end
                end
                content_tabs.join.html_safe
              end +

              # --------------------------------------------- TAB CONTENT ---------------------------------------------
              content_tag( :div, :class => "tab-content" ) do
                form_structure.each_with_index do | ( attrs_key ,attrs_opts ) , index |
                  content_tabs_content << content_tag( :div , :id => "tab-#{attrs_key}" , :class => "tab-pane #{( index == 0 ? "active" : "" )}" ) do
                    if ( ! [ :new , :create , :edit , :update ].include?( params[:action].to_sym ) || ! [ :status , :audits ].include?( attrs_key.to_sym ) )
                      content_tag( :div , :class => "panel-body" ) do
                        content_tag( :div , :class => "form-horizontal col-xs-12 #{ ( attrs_key.to_sym == :status ? "text-center" : "" ) }" ) do
                          ( attrs_opts[ "attrs" ].map do | attribute_name , attribute_opts |

                            attribute_symbol = attribute_name.to_sym

                            if attribute_symbol.to_s.starts_with?("separator")
                              content_tag( :div , :class => "hr-line-dashed" ) do end
                            elsif attribute_symbol == :status
                              content_tag( :h1 ) do
                                print_status( form.object , :icon )
                              end
                            elsif attribute_symbol == :audits
                              print_audits( form.object )
                            else

                              attribute_type          = attribute_opts[ "type" ].to_sym if attribute_opts["type"]
                              attribute_select_scope  = attribute_opts[ "select_scope" ].to_sym if attribute_opts["select_scope"]

                              if attribute_opts["params"]
                                input_params = ( attribute_opts[ "params" ].map do | param , value |
                                  if ( value.class == String ) && ( value.strip.starts_with?( "(" ) && value.strip.ends_with?(")") )
                                    { param => "#{eval(value)}" }
                                  else
                                    { param => value }
                                  end
                                end ).reduce(:merge).deep_symbolize_keys
                              else
                                input_params = {}
                              end

                              if attribute_type == :mask_field
                                print_form_input( form , ( attrs_key.to_sym == :external ? :show : params[:action] ) , attribute_type , attribute_symbol , {} , input_params )
                              elsif attribute_type == :select_field

                                if form.object.class.columns.find{ |c| c.name == attribute_symbol.to_s }.present?
                                  can_be_null = form.object.class.columns.find{ |c| c.name == attribute_symbol.to_s }.null
                                else
                                  can_be_null = false
                                end

                                if attribute_symbol == :kind
                                  print_form_input( form , ( attrs_key.to_sym == :external ? :show : params[:action] ) , :select_field , :kind , {} , { :all_values => form.object.class.kinds , :include_blank => can_be_null } )
                                elsif attribute_symbol.to_s.starts_with?("kind_")
                                  types_of_resources = eval( "form.object.class.#{attribute_symbol.to_s.pluralize}" )
                                  print_form_input( form , ( attrs_key.to_sym == :external ? :show : params[:action] ) , :select_field , attribute_symbol.to_sym , {} , { :all_values => types_of_resources , :include_blank => can_be_null } )
                                elsif attribute_symbol == :resource_id
                                  types_of_resources = form.object.class.kinds
                                  print_form_input( form , ( attrs_key.to_sym == :external ? :show : params[:action] ) , :select_field , :kind , {} , { :all_values => types_of_resources , :include_blank => can_be_null } )
                                elsif ( attribute_symbol.to_s.ends_with?("_id") )
                                  if ( attribute_symbol == :parent_id )
                                    if ( form.object.class.column_names.include?("system_id") )
                                      if ( form.object.system_id != 0 )
                                        options_for_select = form.object.class.where.not( :id => form.object.id )
                                      else
                                        options_for_select = form.object.class.where.not( :id => form.object.id )
                                      end
                                    else
                                      options_for_select = form.object.class.where.not( :id => form.object.id )
                                    end

                                    # options_for_select = eval( "form.object.class.local.where.not( :id => form.object.id )" )
                                  else
                                    if attribute_select_scope
                                      options_for_select = eval( "attribute_symbol.to_s.gsub('_id','').classify.constantize.#{ attribute_select_scope }" )
                                    else
                                      options_for_select = attribute_symbol.to_s.gsub("_id","").classify.constantize.all
                                    end
                                  end
                                  print_form_input( form , ( attrs_key.to_sym == :external ? :show : params[:action] ) , :select_field , attribute_symbol , {} , { :all_values => options_for_select , :include_blank => can_be_null } )
                                end
                              else
                                print_form_input( form , ( attrs_key.to_sym == :external ? :show : params[:action] ) , attribute_type , attribute_symbol , { :include_blank => can_be_null } , input_params )
                              end

                            end
                          end ).join.html_safe
                        end
                      end
                    end
                  end
                end
                content_tabs_content.join.html_safe
              end

            end
          end
        end
      end

      # --------------------------------------------- JAVASCRIPT FOR ERRORS ---------------------------------------------
      unless form.error_messages.empty?
        content << content_tag( :script , :type => "text/javascript" ) do
          "$( document ).ready(function() {
            swal( {
                title: \"Se encontraron errores\",
                text: \"#{ form.object.errors.full_messages.map{ |msg| ( "<p><i class='fa fa-chevron-right fa-fw'></i> " + msg + "</p>" ) }.join.html_safe }\",
                type: \"error\",
                html: true
            } );
          });".html_safe
        end
      end

    else
      content << content_tag( :span , :class => "text-center" ) do
        content_tag( :h1 , :class => "text-danger" ) do
          content_tag( :i , :class => "fa fa-exclamation-triangle fa-fw ") do end
        end +
        content_tag( :h2 , :class => "text-danger" ) do
          "No se encuentra <strong>structures &gt; <u>#{form.object.class.to_s.underscore}</u> &gt; form</strong>".html_safe
        end
      end
    end

    content.map{ |c| c.html_safe }.join.html_safe

  end

  def render_filter_to_add_in_search_by_filters q_attr = nil , q_cond = nil , q_values = nil

    # This is for cases: "uninitialized constant..."
    begin

      if @class_instances
        model = @class_instances
      else
        model = controller_name.classify.constantize
      end
      attrs = model.attribute_names.collect{ |a| a }

      content_tag( :div , :class => "row no-margins no-paddings filter_line" , :id => "filter" ) do

        content_tag( :div , :class => "col-md-1 no-paddings no-margins" ) do
          content_tag( :a , :href => "#" , :class => "btn btn-link btn-sm remove_field" ) do
            content_tag( :span , :class => "h4" ) do content_tag( :i , :class => "fa fa-times-circle" ) do end end
          end
        end +

        content_tag( :div , :class => "col-md-3 no-paddings no-margins" ) do
          content_tag( :select , :class => "select2_demo_1 form-control select2_attribute" ) do

            arr_group_cond = []

            attrs_native    = ( attrs - [ "system_id" , "synchronizer_id" , "external_id" , "external_jsondata" , "gobbi_schema_id" , "parent_id" ] )
            attrs_external  = ( attrs & [ "system_id" , "synchronizer_id" , "external_id" , "external_jsondata" , "gobbi_schema_id" , "parent_id" ] )

            if attrs_native.present?
              arr_group_cond << content_tag( :optgroup , :label => "Nativos" ) do
                arr_opts = []
                attrs_native.each do | attribute |
                  str_human_attribute = ( attribute.ends_with?("_id") ? "<i class='fa fa-arrow-right fa-fw'></i>" : "<i class='fa fa-blank fa-fw'></i>" ) + model.human_attribute_name( attribute )
                  arr_opts << content_tag( :option , :value => "#{attribute}" , :selected => ( q_attr == attribute ? "selected" : nil ) ) do "#{str_human_attribute}" end
                end
                arr_opts.join.html_safe
              end
            end

            if attrs_external.present?
              arr_group_cond << content_tag( :optgroup , :label => "Externos" ) do
                arr_opts = []
                attrs_external.each do | attribute |
                  str_human_attribute = ( attribute.ends_with?("_id") ? "<i class='fa fa-arrow-circle-right fa-fw'></i>" : "<i class='fa fa-blank fa-fw'></i>" ) + model.human_attribute_name( attribute )
                  arr_opts << content_tag( :option , :value => "#{attribute}" , :selected => ( q_attr == attribute ? "selected" : nil ) ) do "#{str_human_attribute}" end
                end
                arr_opts.join.html_safe
              end
            end

            arr_group_cond.join.html_safe

          end
        end +

        content_tag( :div , :class => "col-md-3 no-paddings no-margins" ) do
          content_tag( :select , :class => "select2_demo_1 form-control select2_condition" ) do
            arr_group_cond = []
            CONST_CONDITIONS.each do | conds_group_name , conds_list |
              arr_group_cond << content_tag( :optgroup , :label => conds_group_name ) do
                arr_cond = []
                conds_list.to_h.each do | cond_name , cond_trans |
                  arr_cond << content_tag( :option , :value => cond_name , :selected => ( q_cond == cond_name ? "selected" : nil ) ) do cond_trans end
                end
                arr_cond.join.html_safe
              end
            end
            arr_group_cond.join.html_safe
          end
        end +
        content_tag( :div , :class => "col-md-5 no-paddings no-margins" ) do
          content_tag( :select , :class => "select2_demo_1 form-control select2_values" , :multiple => ( CONST_CONDITIONS.collect{ | key , hash | key if hash.to_h.keys.include?(q_cond) }.compact.first == "Varios valores" ? "multiple" : nil ) ) do
            arr_values = []

            if q_values

              true_or_false_values = CONST_CONDITIONS.map{|key, array| key if array.map{ |a| a.first}.include?(q_cond)}.compact.first
              if q_attr == "parent_id"
                model_ref = model
              elsif q_attr.ends_with?( "_id" )
                model_ref = q_attr.gsub( "_id" , "" ).pluralize.classify.constantize
              end


              # If q_cond is a condition that comprends many values, like array, we trust that like that
              if CONST_CONDITIONS.collect{ | key , hash | key if hash.to_h.keys.include?(q_cond) }.compact.first == "Varios valores"
                q_values.split( "," ).each do | value |
                  arr_values << content_tag( :option , :value => value , :selected => "selected" ) do
                    if model_ref.present?
                      model_ref.find(value).detail
                    else
                      value
                    end
                  end
                end
              elsif CONST_CONDITIONS.collect{ | key , hash | key if hash.to_h.keys.include?(q_cond) }.compact.first == "Otros"
                arr_values << content_tag( :option , :value => q_values , :selected => "selected" ) do
                  if q_values == "true"
                    "Verdadero"
                  else
                    "Falso"
                  end
                end
              else
                arr_values << content_tag( :option , :value => q_values , :selected => "selected" ) do
                  if model_ref.present?
                    model_ref.find(q_values ).detail
                  else
                    q_values
                  end
                end
              end

            end
            arr_values.join.html_safe
          end
        end +

        tag( :input , :id => "" , :name => ( q_attr != nil && q_cond != nil ? "q[#{q_attr}#{q_cond.gsub('*','')}]"  : "" ) , :class => "hidden attribute_condition" , :value => ( q_values ? q_values : "" ) )

      end
    rescue NameError => exception
      if exception.to_s.starts_with?( "uninitialized constant" )
        return ""
      end
    end
  end

  def translate_controller( controller , action = nil )
    if controller.class == String
      model = controller.classify.constantize unless controller.starts_with?( *['layout_','Layout'] )
      layout = controller if controller.starts_with?( *['layout_','Layout'] )
    else
      model = controller.controller_name.classify.constantize unless controller.controller_name.starts_with?( *['layout_','Layout'] )
      layout = controller if controller.controller_name.starts_with?( *['layout_','Layout'] )
    end

    # unless controller_name.starts_with?( "layout_" )
    if model.present?
      if action
        translation = translate_action( model.to_s.underscore.to_sym , action ) + " " + model.model_name.human
      else
        translation = model.model_name.human( :count => 2 )
      end
    end
    if layout.present?
      translation = t("layouts.#{layout.underscore}")
    end
    return translation
  end

  # Draw header actions instances
  def render_instances_header_actions
    str_to_render = "".html_safe

    if @class_instances
      model = @class_instances
    else
      model = controller_name.classify.constantize
    end

    unless controller_name.starts_with?( "layout_" )
      if model.respond_to?( :actions )
        model.actions.each do |class_action|
          if class_action.to_s == "new"
            str_to_render += super_link_to( :new , model , :in_form , { :text => translate_action( model.to_s.underscore.to_sym , :new ) + " " + model.model_name.human.downcase } )
          end
          if class_action.to_s.end_with?("_job_inmodal")
            current_action_model = model::GLOBAL_ACTIONS.map{ | action_hash | action_hash if action_hash.keys.include?(class_action) }.compact.first
            if current_action_model.present?
              str_args = '?' + ( current_action_model[ :args ].to_a.map do | attr_val |
                attr_val.join('=')
              end ).join('&')
            end
            str_to_render += super_link_to( :new , Delayed::Job , :in_dropdown , { :text => translate_action( model.table_name.to_sym , class_action.to_s.gsub( "_job_inmodal" , "" ).to_sym ) } , true , true , str_args )
          end

        end
      end
      if str_to_render.present?
        return content_tag( :div , :class => "p-xs" ) do str_to_render end
      else
        return str_to_render
      end

    end

  end

  def render_instances_search_modal

    if @class_instances
      model = @class_instances
    else
      model = controller_name.classify.constantize
    end

    content_tag( :div , :class => "modal inmodal" , :id => "search_modal" , :role => "dialog" , :aria => { :hidden => "true" } ) do

      content_tag( :div , :class => "modal-dialog modal-lg" ) do

        search_form_for( @q , url: request.env['PATH_INFO'] , html: { method: :get } ) do |f|

          content_tag( :div , :class => "modal-content" ) do

            content_tag( :div , :class => "modal-header" ) do
              content_tag( :button , :type => "button" , :class => "close" , :data => { :dismiss => "modal" } ) do
                content_tag( :span , :aria => { :hidden => "true" } ) do "×".html_safe end + content_tag( :span , :class => "sr-only" ) do "Cerrar" end
              end +
              content_tag( :h1 ) do "Filtros de búsqueda".html_safe end +
              content_tag( :small ) do "Aplique filtros para buscar registros específicos".html_safe end
            end +

            content_tag( :div , :class => "modal-body" ) do

              content_tag( :div , :class => "panel-body p-lg" ) do

                content_tag( :div , :id => "filtering_group" , :class => "form-horizontal" ) do

                  arr_filters = []
                  if @filters.present?

                    attrs = model.attribute_names.collect{ |a| a }
                    conds = CONST_CONDITIONS.collect{ | k , values | values.collect{ | c | c.first } }.flatten

                    @filters.each do | nameof_attribute_and_condition , values |
                      attrs.each do | attribute_name |
                        # search by attribute name
                        if nameof_attribute_and_condition.starts_with?( attribute_name )

                          # Replace attribute_name to "" in nameof_attribute_and_condition
                          name_of_condition = nameof_attribute_and_condition.gsub( attribute_name , '')

                          # search by cond that contains the actual condition
                          if conds.include?( "*" + name_of_condition )

                            # We replace name_of_condition to ""
                            name_of_attribute = nameof_attribute_and_condition.gsub( name_of_condition , '')

                            # If values is an Array we join with commas
                            values = values.join(',') if values.class == Array

                            # Get the html rendered through name of attribute, name of condition and values, and we append to filters added yet
                            arr_filters << render_filter_to_add_in_search_by_filters( name_of_attribute , "*" + name_of_condition , values.to_s )

                            break

                          end
                        end
                      end
                    end
                    arr_filters.join.html_safe
                  end
                end +
                content_tag( :div , :class => "col-md-12 text-center") do
                  content_tag( :button , :id => "filter_add" , :class => "btn btn-outline btn-link text-success" ) do content_tag( :i , :class => "fa fa-plus fa-fw" ) do end + " Agregar filtro" end
                end
              end +
              if @filters.present?
                content_tag( :div , :class => "panel-body p-xxs" ) do
                  params_to_pass = { :q => request.query_parameters[:q].select{ |p,v| [p,v] if ["per_page","page"].include?(p) }.to_h }
                  content_tag( :div , :class => "col-md-12 text-center p-xxs") do
                    link_to( params_to_pass ) do
                      content_tag( :button , :type => "button" , :class => "btn btn-xs btn-link text-muted" ) do
                        content_tag( :i , :class => "fa fa-refresh" ) do " Borrar filtros" end
                      end
                    end
                  end
                end
              end +
              if params[:q].present?
                if params[:q][:per_page].present?
                  tag( :input , :name => "q[per_page]" , :value => params[:q][:per_page] , :class => "hidden" )
                else
                  "".html_safe
                end +
                if params[:q][:page].present?
                  tag( :input , :name => "q[page]" , :value => params[:q][:page] , :class => "hidden" )
                else
                  "".html_safe
                end
              else
                "".html_safe
              end

            end +

            content_tag( :div , :class => "modal-footer" ) do

              content_tag( :button , :type => "button" , :class => "btn btn-white" , :data => { :dismiss => "modal" } ) do "Cerrar".html_safe end +
              content_tag( :button , :type => "submit" , :class => "btn btn-primary" ) do "Aplicar filtros".html_safe end

            end

          end

        end

      end

    end




  end

  def render_pagination instances , position

    content = ""

    if instances.present?

      if @class_instances
        str_controller_humanized_plural = @class_instances.model_name.human( :count => 2 )
      else
        if @instances.present?
          str_controller_humanized_plural = @instances.sample.class.model_name.human( :count => @instances.count )
        else
          str_controller_humanized_plural = controller_name.classify.constantize.model_name.human( :count => 2 )
        end
      end

      # Render pages and options for results
      content << content_tag( :div , :class => "row form-horizontal row-eq-height p-xs" , :style => "background-color: #f1f1f1 !important;") do
        content_tag( :div , :class => "col-lg-4 col-md-12 col-sm-12 no-borders") do
          content_tag( :div , :class => "btn-group" ) do
            content_tag( :button , :data => { :toggle => "dropdown" } , :class => "btn btn-default dropdown-toggle" , :aria => { :expanded => "false" } ) do
              ( @per_page.to_s + " resultados por página &nbsp; " ).html_safe + content_tag( :span , :class => "caret" ) do end
            end +
            content_tag( :ul , :class => "dropdown-menu" ) do
              contents_tags_for_results = []
              params_to_pass = {}
              [ "10" , "25" , "50" , "100", "200" , "500" , "1000" , "*" ].each do | result |

                contents_tags_for_results << content_tag( :li ) do

                  # Merge with params that exists yet ( IF PARAMS HAS BEEN PASSED )
                  if request.query_parameters[:q]
                    params_to_pass = { :q => request.query_parameters[:q].merge( { :per_page => result } ) }
                  else
                    params_to_pass = request.query_parameters.merge( { :q => { :per_page => result } } )
                  end

                  link_to( params_to_pass ) do
                    ( @per_page.to_s == result ? "<strong>" : "" ).html_safe +
                    ( @per_page.to_s == result ? content_tag( :i , :class => "fa fa-check fa-fw" ) do end : content_tag( :i , :class => "fa fa-fw" ) do end ) +
                    if result == "*"
                      " * Todos los resultados * "
                    else
                      "#{result} resultados por página"
                    end +
                    ( @per_page.to_s == result ? "</strong>" : "" ).html_safe
                  end

                end

              end
              contents_tags_for_results.join.html_safe
            end
          end
        end +
        content_tag( :div , :class => "col-lg-4 col-md-12 col-sm-12 text-center no-borders" ) do
          content_tag( :div , :class => "p-xs" ) do page_entries_info( instances, entry_name: str_controller_humanized_plural.downcase ) end
        end +
        content_tag( :div , :class => "col-lg-4 col-md-12 col-sm-12 no-borders text-right") do
          paginate( instances )
        end
      end
    end

    return content

  end

  # Draw index for basic model
  def render_instances_list( options = {} )

    # Get the class of this model/controller
    if options[ :instances ].present?
      instances = options[ :instances ]
      instances_class = instances.sample.class
    elsif @instances.present?
      instances = @instances
      instances_class = instances.sample.class
    end

    if @class_instances.present?
      instances_class = @class_instances
    else
      if @instances.present?
        instances_class = @instances.sample.class
      else
        instances_class = controller_name.classify.constantize
      end
    end

    if instances_class == Delayed::Backend::ActiveRecord::Job
      instances_controller = instances_class
    else
      instances_controller = controller_name.classify.constantize
    end

    # We open file that contains all index and form structure of attributes models's grouping
    yml_attrs_list_structure = YAML.load_file( Rails.root.to_s + "/lib/structures.yml" )

    # Clean vars
    content       = []
    content_head  = []
    content_th    = []
    content_body  = []

    # Structure exists in .yml ?
    if yml_attrs_list_structure[ instances_controller.to_s.underscore ]

      # We get da structure of the attributes grouping
      if options[ :structure ].present?
        list_structure = yml_attrs_list_structure[ options[ :structure ][ :instances_controller ] ][ options[ :structure ][ :action_name ] ]
      else
        list_structure = yml_attrs_list_structure[ instances_controller.to_s.underscore ][ action_name ]
      end

      if list_structure

        unless action_name == "index"
          if options[ :header ].present? and options[ :header ][ :return_link ].present?
            link_to_returnback  = options[ :header ][ :return_link ]
          else
            link_to_returnback  = "#{controller_name}_path"
          end
        end

        title_action     = translate_action( controller_name , action_name )
        title_controller = translate_controller( controller_name )

        if options[ :header ].present?
          title_action     = options[ :header ][ :title ][ :top ]      if options[ :header ][ :title ].present? and options[ :header ][ :title ][ :top ].present?
          title_controller  = options[ :header ][ :title ][ :bottom ]   if options[ :header ][ :title ].present? and options[ :header ][ :title ][ :bottom ].present?
          actions = options[ :header ][ :actions ]
        end

        content << content_tag( :div , :class => "ibox float-e-margins" ) do

          if options[ :only_results ].present? and options[ :only_results ] == true
            "".html_safe
          else

            content_tag( :div , :class => "ibox-title" , :style => "padding-bottom: 0px;") do

              content_tag( :div , :class => "row form_header" ) do

                unless action_name == "index"
                  content_tag( :div , :class => "pull-left p-xs border-right" ) do
                    super_link_to( :pure_link , eval( link_to_returnback ) , :in_form , { :text => "" , :icon => 'fa-arrow-left' } )
                  end
                else
                  content_tag( :div , :class => "pull-left p-xs" ) do content_tag( :button , :class => "btn btn-link" , :style => "cursor:initial !important;" ) do content_tag( :i , :class => "fa fa-blank fa-fw" ) do end end end
                end +

                content_tag( :div , :class => "pull-left p-xs" ) do
                  if params["#{controller_name.singularize}_id"].present?
                    param_instance_ref = params["#{controller_name.singularize}_id"]
                    instance_ref = controller_name.classify.constantize.find( param_instance_ref )
                  end
                  content_tag( :h2 ) do
                    "#{title_action}".html_safe +
                    if instance_ref.present?
                      content_tag( :i , :class => "fa fa-caret-right fa-fw" ) do end +
                      content_tag( :strong ) do
                        "#{instance_ref.detail} ".html_safe
                      end
                    else
                      "".html_safe
                    end
                  end
                end +

                content_tag( :div , :class => "pull-right ibox-tools p-xs border-left" ) do


                  # unless options[ :actions ].present? == false
                  unless controller_name.starts_with?( "layout_" )

                    if @class_instances
                      model = @class_instances
                    else
                      model = controller_name.classify.constantize
                    end

                    str_to_render_actions = "".html_safe

                    unless model.respond_to?( :actions )

                      # content_tag( :a , :class => " btn btn-md btn-link text-muted"  , :data => { :toggle => "dropdown" } , :href => "#" ) do content_tag( :i , :class => "fa fa-ellipsis-v fa-fw" ) do end end
                      content_tag( :div , :class => "pull-left p-xxs" ) do content_tag( :button , :class => "btn btn-link text-muted" , :style => "cursor:initial !important;" ) do content_tag( :i , :class => "fa fa-ellipsis-v fa-fw" ) do end end end

                    else


                      content_tag( :a , :class => " btn btn-md btn-link text-muted"  , :data => { :toggle => "dropdown" } , :href => "#" ) do content_tag( :i , :class => "fa fa-ellipsis-v fa-fw" ) do end end +

                        content_tag( :ul , :class => "dropdown-menu dropdown-user" ) do

                        # content_tag( :li ) do content_tag( :a , :href => "#" ) do "Link 1" end end


                        model.actions.each do |class_action|
                          if class_action.to_s == "new"
                            # str_to_render_actions += super_link_to( :new , model , :in_dropdown , { :text => translate_action( model.to_s.underscore.to_sym , :new ) + " " + model.model_name.human.downcase } )
                            # str_to_render_actions += super_link_to( :new , model , :just_text )
                            str_to_render_actions += content_tag( :li ) do
                              # content_tag( :a , :href => "#{super_link_to( :new , model , :just_text )}" ) do "#{translate_action( model.to_s.underscore.to_sym , :new ) + " " + model.model_name.human.downcase}" end
                              super_link_to( :new , model , :just_text , :text => "#{translate_action( model.to_s.underscore.to_sym , :new ) + " " + model.model_name.human.downcase}" )
                            end
                          end
                          if class_action.to_s.end_with?( "_job_inmodal" )
                            current_action_model = model::GLOBAL_ACTIONS.map{ | action_hash | action_hash if action_hash.keys.include?(class_action) }.compact.first
                            if current_action_model.present?
                              str_args = '?' + ( current_action_model[ :args ].to_a.map do | attr_val |
                                attr_val.join('=')
                              end ).join('&')
                            end
                            str_to_render_actions += super_link_to( :new , Delayed::Job , :in_dropdown , { :text =>  translate_action( model.table_name.to_sym , class_action.to_s.gsub( "_job_inmodal" , "" ).to_sym ) } , true , true , str_args )
                          end

                        end
                        if str_to_render_actions.present?
                          content_tag( :div , :class => "p-xs" ) do str_to_render_actions end
                          else
                            str_to_render_actions
                          end
                      end

                    end
                    #  +
                    #
                    # content_tag( :i , :class => "fa fa-blank fa-fw" ) do end

                  end
                  # end +
                  # if options[ :header ].present? and options[ :header ][ :hideable ].present? and options[ :header ][ :hideable ] == true
                  #   content_tag( :a , :class => "collapse-link" ) do content_tag( :i , :class => "fa fa-chevron-up fa-fw" ) do end end
                  # else
                  #   "".html_safe
                  # end +
                  # if options[ :header ].present? and options[ :header ][ :closeable ].present? and options[ :header ][ :closeable ] == true
                  #   content_tag( :a , :class => "close-link" ) do content_tag( :i , :class => "fa fa-times fa-fw" ) do end end
                  # else
                  #   "".html_safe
                  # end
                end +

                # content_tag( :a , :class => "dropdown-toggle" , :data => { :toggle => "collapse" , :parent => "#accordion" } , :href => "#collapseOne" , :aria => { :expanded => "true" } ) do content_tag( :i , :class => "fa fa-search fa-fw" ) do end end +
                # unless options[ :show_searching_link ].present? == false
                if action_name == "index"
                  content_tag( :div , :class => "pull-right ibox-tools p-xs" ) do
                    if @filters.present?
                      content_tag( :a , :class => "dropdown-toggle" , :data => { :toggle => "modal" , :target => "#search_modal" } ) do
                        content_tag( :span , :class => "text-info" ) do
                          "(Filtros aplicados) ".html_safe + content_tag( :i , :class => "fa fa-search m-sm" ) do end
                        end
                      end
                    else
                      content_tag( :a , :class => "dropdown-toggle" , :data => { :toggle => "modal" , :target => "#search_modal" } ) do content_tag( :i , :class => "fa fa-search fa-fw m-sm" ) do end end
                    end
                  end
                else
                  "".html_safe
                end

              end

            end

          end +

          # Add to content
          content_tag( :div , :class => "ibox-content p-w-md" ) do

            if !( options[ :only_results ].present? and options[ :only_results ] == true )
              ( options[ :show_pagination ] == false ?  "" : render_pagination( instances , :top ) + tag( :br ) ).html_safe
            else
              "".html_safe
            end +

            content_tag( :table , :class => "table table-striped table-hover border-size-xs") do

              content_tag( :thead ) do

                list_structure.collect{ |c| c.first }.each do | attr_key |

                  # if not is footable_data => this is for hidden rows below actual instance
                  unless [ "_footable_data_" , "_footable_data_extra_"].include?( attr_key )

                    content_th << content_tag( :th ) do
                      if attr_key == "actions"
                        "Acciones"
                      elsif attr_key == "detail"
                        "Descripción"
                      else
                        if list_structure[ attr_key ]["display"].present?
                          "#{list_structure[ attr_key ]["display"]}"
                        else
                          "#{instances_class.human_attribute_name( attr_key )}"
                        end
                      end
                    end

                  end

                end
                content_tag( :tr ) do
                  content_th.join.html_safe
                end
              end +

              content_tag( :tbody ) do

                if instances.present?

                  instances.each do | instance |

                    is_footable       = list_structure.include?( "_footable_data_" )
                    is_footable_extra = list_structure.include?( "_footable_data_extra_" )

                    if is_footable
                      footable_data = list_structure[ "_footable_data_" ]
                      footable_data_instances = footable_data[ "instances" ]
                      footable_data_instances_id = footable_data[ "id" ] if footable_data[ "id" ].present?
                      footable_data_structure = footable_data[ "structure" ]

                      child_instances = eval( footable_data_instances )

                    end


                    if is_footable_extra

                      footable_data_extra = list_structure[ "_footable_data_extra_" ]
                      footable_data_extra_instances = footable_data_extra[ "instances" ]
                      footable_data_extra_structure = footable_data_extra[ "structure" ]

                      extra_child_instances = eval( footable_data_extra_instances )

                    end
                    # content_body << content_tag( :tr , :class => ( is_footable ? "clickable" : nil ) , :data => { :toggle => ( is_footable ? 'collapse' : nil ) , :target => ( is_footable ? ".row#{ instance.id }" : nil ) } , :id => ( is_footable ? "row#{instance.id}" : nil ) ) do
                    # , :class => ( is_footable ? "clickable" : nil ) , :data => { :toggle => ( is_footable ? 'collapse' : nil ) , :target => ( is_footable ? ".row#{ instance.id }" : nil ) } , :id => ( is_footable ? "row#{instance.id}" : nil )

                    # footable_data
                    if instance.class == Hash
                      hash_footable_data = { :instance_id => eval(footable_data_instances_id) , :is_footable => is_footable , :count => ( child_instances.present? ? child_instances.count : 0 ) }
                    elsif instance.class.column_names.include?( 'id' )
                      hash_footable_data = { :instance_id => instance.id , :is_footable => is_footable , :count => ( child_instances.present? ? child_instances.count : 0 ) }
                    end

                    content_body << content_tag( :tr ) do

                      content_td = []
                      list_structure.deep_symbolize_keys.each_with_index do | ( attr_key , attrs_opts ) , index |

                        attr_attribute_name   = attr_key
                        attr_type             = attrs_opts[ :type ]
                        attr_style            = attrs_opts[ :style ]
                        attr_class            = attrs_opts[ :class ]
                        attr_content          = attrs_opts[ :content ]
                        attr_helper_function  = attrs_opts[ :helper_function ]
                        attr_helper_params    = attrs_opts[ :helper_params ]
                        attr_img_size         = attrs_opts[ :img_size ]

                        unless attr_attribute_name == :_footable_data_ or attr_attribute_name == :_footable_data_extra_

                          content_td << content_tag( :td , :class => "#{ ( !attr_class.nil? ? attr_class : "") } #{( instance.class != Hash and instance.class.column_names.include?("status") ? ( instance.deleted? ? " nulled" : "") : "" )}" , :style => "vertical-align: middle;" ) do
                            if attr_attribute_name == :actions
                              if instance.class == Hash
                                print_dropdown_menu_record( instance , [] , hash_footable_data )
                              else
                                print_dropdown_menu_record( instance , instance.actions , hash_footable_data )
                              end
                            elsif attr_attribute_name == :kind
                              translate_kind( instance )
                            else
                              content_tag( :div  , :class => ( attr_style == "bordered" ? CONST_BORDERED_CLASSES : "" ) ) do
                                if attr_type == "image_tag" && eval( "instance.#{attr_attribute_name}?" )
                                  image_tag eval( "instance." + attr_attribute_name + ".url(:#{attr_img_size})" )
                                elsif attr_type == "currency"
                                  number_to_currency( eval( "instance.#{attr_attribute_name}" ) )
                                elsif attr_type == "helper"
                                  eval( "#{attr_helper_function}(#{attr_helper_params})" )
                                else
                                  if attr_content
                                    begin
                                      eval( "instance.#{attr_content}" )
                                    rescue => e
                                      if e.message.ends_with?("for nil:NilClass")
                                        "".html_safe if attr_content.include?('.')
                                      end
                                    end
                                  else
                                    eval( "instance.#{attr_attribute_name}" )
                                  end
                                end
                              end
                            end
                          end
                        end

                      end
                      content_td.join.html_safe
                    end

                    # render each sub instance if is_footable data
                    if child_instances.present?

                      child_instances.each do | child_instance |
                        if instance.class == Hash
                          instance_id = eval( footable_data_instances_id )
                          child_instance = Hash[ *child_instance ] if child_instance.class == Array
                        else
                          instance_id = instance.id
                        end

                        content_body << content_tag( :tr , :class => "collapse row#{instance_id} child_row_clickable_highlight" ) do

                          content_child_td = []
                          footable_data_structure.deep_symbolize_keys.each_with_index do | ( attr_key , attrs_opts ) , index |

                            attr_attribute_name   = attr_key
                            attr_type             = attrs_opts[ :type ]
                            attr_style            = attrs_opts[ :style ]
                            attr_class            = attrs_opts[ :class ]
                            attr_content          = attrs_opts[ :content ]
                            attr_helper_function  = attrs_opts[ :helper_function ]
                            attr_helper_params    = attrs_opts[ :helper_params ]
                            attr_img_size         = attrs_opts[ :img_size ]
                            attr_colspan          = attrs_opts[ :colspan ]


                            content_child_td << content_tag( :td , :class => "#{ ( !attr_class.nil? ? attr_class : "") } #{( child_instance.class != Hash and child_instance.class.column_names.include?("status") ? ( child_instance.deleted? ? " nulled" : "") : "" )} ", :colspan => ( attr_colspan.present? ? attr_colspan : nil ) ) do

                              if attr_attribute_name == :actions
                                print_dropdown_menu_record( child_instance , child_instance.actions )
                              elsif attr_attribute_name == :kind
                                translate_kind( child_instance )
                              else
                                content_tag( :div  , :class => ( attr_style == "bordered" ? CONST_BORDERED_CLASSES : "" ) ) do
                                  if attr_type == "image_tag" && eval( "child_instance.#{attr_attribute_name}?" )
                                    image_tag eval( "child_instance." + attr_attribute_name + ".url(:#{attr_img_size})" )
                                  elsif attr_type == "currency"
                                    number_to_currency( eval( "child_instance.#{attr_attribute_name}" ) )
                                  elsif attr_type == "helper"
                                    eval( "#{attr_helper_function}(#{attr_helper_params})" )
                                  else
                                    if attr_content
                                      begin
                                        eval( "child_instance.#{attr_content}" )
                                      rescue => e
                                        if e.message.ends_with?("for nil:NilClass")
                                          "".html_safe if attr_content.include?('.')
                                        end
                                      end
                                    else
                                      eval( "child_instance.#{attr_attribute_name}" )
                                    end
                                  end
                                end

                              end
                            end

                          end
                          content_child_td.join.html_safe
                        end

                      end

                    end

                    # render each sub instance if is_footable data
                    if extra_child_instances.present?

                      extra_child_instances.each do | extra_child_instance |
                        content_body << content_tag( :tr , :class => "collapse row#{instance.id} child_row_clickable_highlight" ) do

                          content_child_td = []
                          footable_data_extra_structure.deep_symbolize_keys.each_with_index do | ( attr_key , attrs_opts ) , index |

                            attr_attribute_name   = attr_key
                            attr_type             = attrs_opts[ :type ]
                            attr_style            = attrs_opts[ :style ]
                            attr_class            = attrs_opts[ :class ]
                            attr_content          = attrs_opts[ :content ]
                            attr_helper_function  = attrs_opts[ :helper_function ]
                            attr_helper_params    = attrs_opts[ :helper_params ]
                            attr_img_size         = attrs_opts[ :img_size ]
                            attr_colspan          = attrs_opts[ :colspan ]


                            content_child_td << content_tag( :td , :class => "#{ ( !attr_class.nil? ? attr_class : "") }", :colspan => ( attr_colspan.present? ? attr_colspan : nil ) ) do

                              content_tag( :div  , :class => ( attr_style == "bordered" ? CONST_BORDERED_CLASSES : "" ) ) do
                                if attr_type == "image_tag" && eval( "extra_child_instance.#{attr_attribute_name}?" )
                                  image_tag eval( "extra_child_instance." + attr_attribute_name + ".url(:#{attr_img_size})" )
                                elsif attr_type == "currency"
                                  number_to_currency( eval( "extra_child_instance.#{attr_attribute_name}" ) )
                                elsif attr_type == "helper"
                                  eval( "#{attr_helper_function}(#{attr_helper_params})" )
                                else
                                  if attr_content
                                    begin
                                      eval( "extra_child_instance.#{attr_content}" )
                                    rescue => e
                                      if e.message.ends_with?("for nil:NilClass")
                                        "".html_safe if attr_content.include?('.')
                                      end
                                    end
                                  else
                                    eval( "extra_child_instance.#{attr_attribute_name}" )
                                  end
                                end

                              end

                            end

                          end
                          content_child_td.join.html_safe
                        end

                      end

                    end

                  end
                  content_body.join.html_safe
                else
                  content_body << content_tag( :tr ) do
                    content_tag( :td , :colspan => list_structure.deep_symbolize_keys.count.to_s , :class => "text-center" ) do
                      content_tag( :h1 , :class => "text-warning" ) do
                        content_tag( :i , :class => "fa fa-exclamation-circle fa-fw ") do end
                      end +
                      content_tag( :h2 ) do
                        if @filters
                          "No hay resultados para los filtros seleccionados"
                        else
                          "No hay " + "#{instances_class.model_name.to_s.underscore.pluralize}".classify.constantize.model_name.human( :count => 2 ).downcase + " de sistemas externos" + " en el sistema"
                        end
                      end
                    end
                  end
                  content_body.join.html_safe
                end

              end +
              content_tag( :tfoot ) do
                content_th = []
                list_structure.collect{ |c| c.first }.each do | attr_key |

                  unless [ "_footable_data_" , "_footable_data_extra_"].include?( attr_key )
                    content_th << content_tag( :th ) do

                      if attr_key == "actions"
                        "Acciones"
                      elsif attr_key == "detail"
                        "Descripción"
                      else
                        if list_structure[ attr_key ]["display"].present?
                          "#{list_structure[ attr_key ]["display"]}"
                        else
                          "#{instances_class.human_attribute_name( attr_key )}"
                        end
                      end
                    end

                  end

                end

                if !( options[ :only_results ].present? and options[ :only_results ] == true )
                  content_tag( :tr ) do
                    content_th.join.html_safe
                  end
                else
                  "".html_safe
                end


              end

            end +

            if !( options[ :only_results ].present? and options[ :only_results ] == true )
              ( options[ :show_pagination ] == false ?  "" : render_pagination( instances , :bottom ) ).html_safe
            else
              "".html_safe
            end

          end

        end

      else

        content << content_tag( :span , :class => "text-center" ) do
          content_tag( :h1 , :class => "text-danger" ) do
            content_tag( :i , :class => "fa fa-exclamation-triangle fa-fw ") do end
          end +
          content_tag( :h2 , :class => "text-danger" ) do
            "No se encuentra <strong>structures &gt; <u>#{instances_class.to_s.underscore}</u> &gt; #{action_name}</strong>".html_safe
          end
        end

      end

    else

      content << content_tag( :span , :class => "text-center" ) do
        content_tag( :h1 , :class => "text-danger" ) do
          content_tag( :i , :class => "fa fa-exclamation-triangle fa-fw ") do end
        end +
        content_tag( :h2 , :class => "text-danger" ) do
          "No se encuentra <strong>structures &gt; <u>#{instances_class.to_s.underscore}</u> &gt; index</strong>".html_safe
        end
      end

    end

    # Return
    content.join.html_safe

  end

  # For printing hash in templates
  def print_hash( arg_hash , print_type = :div , model_ref = nil )
    content_td = []
    h_obj = JSON.parse(arg_hash)
    if print_type == :text
      truncate( h_obj.to_s , :length => 100 )
    else
      content_tag( :div , :class => "form-group") do
        h_obj.each do | x_key , x_val |
          x_key = eval( "#{model_ref}.human_attribute_name( :#{x_key} )" ) if model_ref
          content_td << content_tag( :div , :class => "row" ) do
            content_tag( ( print_type == :inline ? :span : :div ) , :class => ( print_type == :inline ? "" : "col-md-6 text-success text-right p-xxs" ) ) do
              content_tag( :strong ) do x_key end + content_tag( :i , :class => "fa fa-arrow-right fa-fw") do end
            end +
            content_tag( ( print_type == :inline ? :span : :div ) , :class => ( print_type == :inline ? "" : "col-md-6 text-success text-left p-xxs" ) ) do
              x_val.to_s
            end
          end
        end
        content_td.join.html_safe
      end
    end
  end

  # For printing hash in templates
  def print_if_is_homologated( instance_of_resource )
    unless ApplicationHelper::MODELS_WITH_NO_HOMOLOGATION.include?( instance_of_resource.class )
      if instance_of_resource.resource_id?
        return ( '<span class="text-info"><i class="fa fa-bookmark fa-fw font-size-bigger"></i> Homologado</span>' ).html_safe
      else
        return ( '<span class="text-danger"><i class="fa fa-bookmark-o fa-fw font-size-bigger"></i> Sin Homologar</span>' ).html_safe
      end
    else
      return content_tag( :div , :icon ) do ( !instance_of_resource.active? ? print_status( instance_of_resource ) : "" ) end
    end
  end

  # For print label of status
  def print_audits( instance , print_type = :label , print_size = 'p-xs')
    arr_tags = []
    actions = { "create" => "crea" , "update" => "modifica" , "delete" => "borra" }
    instance.audits.each do |audit|
      arr_tags << content_tag( :p ) do
        content_tag( :strong , :class => "h4" ) do
          content_tag( :i , :class => "fa fa-angle-double-right fa-fw") do end +
            if audit.user
              "VERSION N°#{audit.version} - #{ l( audit.created_at , :format => :dd_mm_yyyy_hh_mm ) } - #{ audit.user.detail } #{ actions[ audit.action ] } #{ print_model_human_name( audit.auditable_type.constantize.table_name.singularize ).downcase }"
            else
              "VERSION N°#{audit.version} - #{ l( audit.created_at , :format => :dd_mm_yyyy_hh_mm ) } - [Sistema] #{ actions[ audit.action ] } #{ print_model_human_name( audit.auditable_type.constantize.table_name.singularize ).downcase }"
            end
        end +
        # content_tag( :p ) do
        #   "[ #{ audit.remote_address } ] (Auditoría N##{audit.id})".html_safe
        # end +
        content_tag( :p ) do
          print_hash( audit.audited_changes.to_json , :div , instance.class )
        end
      end
      arr_tags << content_tag( :div , :class => "hr-line-solid" ) do end if instance.audits.count > 1
    end
    arr_tags.join.html_safe
  end

  # For print label of status
  def print_status( instance , print_type = :label , print_size = 'p-xs')

    # if ( instance.kind_of?(Hash) and instance[:class].attribute_names.include?("status") ) or ( !instance.kind_of?(Hash) and instance.class.attribute_names.include?("status") )

      if instance.kind_of?(Hash)
        instance_class = instance[:class]
        instance_status = instance[:status]
      else
        if instance.class.attribute_names.include?("status")
          instance_class = instance.class
          instance_status = instance_class.statuses.key(instance.status).to_s
        else
          instance_class = nil
          instance_status = nil
        end
      end

      if print_type == :icon
        return ( '<span class="text-primary"><i class="fa fa-check fa-fw"></i><br />' + translate_status( instance ) + '</span>' ).html_safe if instance_status == "active"
    		return ( '<span class="text-danger"><i class="fa fa-close fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe  if instance_status == "inactive"
        return ( '<span class="text-danger"><i class="fa fa-trash-o fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe  if instance_status == "deleted"
        return ( '<span class="text-primary"><i class="fa fa-eye fa-fw"></i><br />' + translate_status( instance ) + '</span>' ).html_safe if instance_status == "visible"
    		return ( '<span class="text-muted"><i class="fa fa-eye-slash fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "hidden"
        return ( '<span class="text-success"><i class="fa fa-refresh fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "testing"
        return ( '<span class="text-muted"><i class="fa fa-clock-o fa-fw"></i><br />' + translate_status( instance ) + '</span>' ).html_safe if instance_status == "waiting"
    		return ( '<span class="text-info"><i class="fa fa-check fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "approved"
    		return ( '<span class="text-info"><i class="fa fa-check fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "processed"
        return ( '<span class="text-danger"><i class="fa fa-close fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "rejected"
        return ( '<span class="text-muted"><i class="fa fa-close fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "nulled"
        return ( '<span class="text-info"><i class="fa fa-check-circle fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "affected"
        return ( '<span class="text-muted"><i class="fa fa-check fa-fw"></i><br />' + translate_status( instance ) + '</span>' ).html_safe if instance_status == "active_not_affected"
        return ( '<span class="text-info"><i class="fa fa-check-circle fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "affected_totally"
        return ( '<span class="text-warning"><i class="fa fa-circle-o fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "affected_partial"
        return ( '<span class="text-info"><i class="fa fa-toggle-on fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "current"
        return ( '<span class="text-muted"><i class="fa fa-toggle-off fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "finished"
        return ( '<span class="text-info"><i class="fa fa-toggle-on fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "opened"
        return ( '<span class="text-muted"><i class="fa fa-toggle-off fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "closed"
        return ( '<span class="text-info"><i class="fa fa-check-circle fa-fw"></i><br />'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "approved_and_affected_completely"
      elsif print_type == :only_icon
        return ( '<span class="text-primary font-size-bigger "><i class="fa fa-eye fa-fw"></i></span>' ).html_safe if instance_status == "visible"
        return ( '<span class="text-muted font-size-bigger"><i class="fa fa-eye-slash fa-fw"></i></span>' ).html_safe if instance_status == "hidden"
        return ( '<span class="text-success font-size-bigger"><i class="fa fa-refresh fa-fw"></i></span>' ).html_safe if instance_status == "testing"
        return ( '<span class="text-info font-size-bigger"><i class="fa fa-check fa-fw"></i></span>' ).html_safe if instance_status == "active"
        return ( '<span class="text-muted font-size-bigger"><i class="fa fa-close fa-fw"></i></span>' ).html_safe if instance_status == "inactive"
        return ( '<span class="text-muted font-size-bigger"><i class="fa fa-trash fa-fw"></i></span>' ).html_safe if instance_status == "deleted"
        return ( '<span class="text-info font-size-bigger"><i class="fa fa-check fa-fw"></i></span>' ).html_safe if instance_status == "approved"
        return ( '<span class="text-info font-size-bigger"><i class="fa fa-check fa-fw"></i></span>' ).html_safe if instance_status == "processed"
        return ( '<span class="text-muted font-size-bigger"><i class="fa fa-close fa-fw"></i></span>' ).html_safe if instance_status == "nulled"
        return ( '<span class="text-muted font-size-bigger"><i class="fa fa-clock-o fa-fw"></i></span>' ).html_safe if instance_status == "waiting"
        return ( '<span class="text-danger font-size-bigger"><i class="fa fa-close fa-fw"></i></span>' ).html_safe if instance_status == "rejected"
        # return ( '<span class="fa-stack fa-xs text-success"><i class="fa fa-check fa-stack-1x" style="margin-left:4px"></i><i class="fa fa-check fa-inverse fa-stack-1x" style="margin-left:-3px;"></i><i class="fa fa-check  fa-stack-1x" style="margin-left:-4px"></i></span>' ).html_safe if instance_status == "affected" and instance_class == BankAccountMovement
        # return ( '<span class="text-info font-size-bigger"><i class="fa fa-check-circle fa-fw"></i></span>' ).html_safe if instance_status == "affected" and instance_class == BankAccountMovement
        return ( '<span class="text-info font-size-bigger"><i class="fa fa-check-circle fa-fw"></i></span>' ).html_safe if instance_status == "affected"
        return ( '<span class="text-muted font-size-bigger"><i class="fa fa-check fa-fw"></i></span>' ).html_safe if instance_status == "active_not_affected"
        return ( '<span class="text-info font-size-bigger"><i class="fa fa-check-circle fa-fw"></i></span>' ).html_safe if instance_status == "affected_totally"
        return ( '<span class="text-warning font-size-bigger"><i class="fa fa-circle-o fa-fw"></i></span>' ).html_safe if instance_status == "affected_partial"
        return ( '<span class="text-info font-size-bigger"><i class="fa fa-toggle-on fa-fw"></i></span>' ).html_safe if instance_status == "current"
        return ( '<span class="text-muted font-size-bigger"><i class="fa fa-toggle-off fa-fw"></i></span>' ).html_safe if instance_status == "finished"
        return ( '<span class="text-info font-size-bigger"><i class="fa fa-toggle-on fa-fw"></i></span>' ).html_safe if instance_status == "opened"
        return ( '<span class="text-muted font-size-bigger"><i class="fa fa-toggle-off fa-fw"></i></span>' ).html_safe if instance_status == "closed"
        return ( '<span class="text-info font-size-bigger"><i class="fa fa-check-circle fa-fw"></i></span>' ).html_safe if instance_status == "approved_and_affected_completely"
      elsif print_type == :label
        return ( '<span class="label ' + print_size + ' label-primary label-full-width text-uppercase text-bold">' + translate_status( instance ) + '</span>' ).html_safe if instance_status == "active"
        return ( '<span class="label ' + print_size + ' label-danger label-full-width text-uppercase text-bold">'  + translate_status( instance ) + '</span>' ).html_safe  if instance_status == "inactive"
        return ( '<span class="label ' + print_size + ' label-danger label-full-width text-uppercase text-bold">'  + translate_status( instance ) + '</span>' ).html_safe  if instance_status == "deleted"
        return ( '<span class="label ' + print_size + ' label-warning label-full-width text-uppercase text-bold">' + translate_status( instance ) + '</span>' ).html_safe if instance_status == "waiting"
        return ( '<span class="label ' + print_size + ' label-primary label-full-width text-uppercase text-bold">' + translate_status( instance ) + '</span>' ).html_safe if instance_status == "approved"
        return ( '<span class="label ' + print_size + ' label-primary label-full-width text-uppercase text-bold">' + translate_status( instance ) + '</span>' ).html_safe if instance_status == "processed"
        return ( '<span class="label ' + print_size + ' label-danger label-full-width text-uppercase text-bold">'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "rejected"
        return ( '<span class="label ' + print_size + ' label-muted label-full-width text-uppercase text-bold">'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "nulled"
        return ( '<span class="label ' + print_size + ' label-primary label-full-width text-uppercase text-bold">'  + translate_status( instance ) + '</span>' ).html_safe if instance_status == "affected"
      end
    # end
  end

  # draw_menu
  def draw_menu

    html_menu = ""

    if Menu.any?
      # First add the Index at the top of the side-menu
      index_menu = Menu.get_modules.where(status: [Menu::VISIBLE, Menu::TESTING]).find_by( :symbol => 'root' )
      html_menu << content_tag( :li ,super_link_to( :pure_link, "/", :menu , {:text => index_menu.name , :icon => index_menu.faicon } ) , :class => "#{is_active_controller(index_menu.symbol)}" )

      # Then we add every module sorted by abc
      Menu.get_modules.where(status: [Menu::VISIBLE, Menu::TESTING]).where.not( :symbol => 'root' ).order(name: :asc).each do |m|
        html_menu << content_tag( :li ,
          (
            content_tag(:a, ( content_tag( :i, nil, :class => "fa #{m.faicon} fa-fw" ) + content_tag( :span, m.name, :class => "nav-label" ) + ( m.symbol != "welcome" ? content_tag(:span, nil, :class => "fa arrow") : "" ) ) , :href => "#" ) +
            ( m.get_menus_and_resources.blank? != true ?
              content_tag( :ul, (
                m.get_menus_and_resources.where(status: [Menu::VISIBLE, Menu::TESTING]).collect{ |s|
                  content_tag( :li , ( s.menu? ?
                    content_tag( :a, ( content_tag(:i, nil, :class => "fa #{s.faicon} fa-fw") + s.name ) , :href => "#" ) +
                    ( s.get_resources.blank? != true ?
                      content_tag(:ul,
                        s.get_resources.where(status: [Menu::VISIBLE, Menu::TESTING]).collect{ |r|
                          content_tag( :li, super_link_to( :pure_link, r.get_full(:symbol), :menu , {:text => r.name , :icon => r.faicon }) , :class => "#{is_active_action(r.symbol)}" )
                        }.join.html_safe , :class => "nav nav-third-level"
                      ) : ""
                    ) : content_tag( :li, super_link_to( :pure_link, s.get_full(:symbol), :menu , {:text => s.name , :icon => s.faicon }) , :class => "#{is_active_action(s.symbol)}" )
                  ) )
                }.join.html_safe
              ) , :class => "nav nav-second-level" ) : ""
            )
          ) , :class => "#{is_active_controller(m.symbol)}" )

      end
    end

    html_menu.html_safe

  end

  # This method is for all recors to permit view, edit, destroy all special functions
  def print_dropdown_menu_record( object , arr_actions = [ ] , hash_footable_data = nil )
    content_tag( :div , :class => "row p-w-xs" ) do
      if hash_footable_data.present?

        is_footable = hash_footable_data[ :is_footable ]
        count_child = hash_footable_data[ :count ]
        instance_id = hash_footable_data[ :instance_id ]

        content_tag( :div , :class => "col-xs-4 text-center no-padding" ) do
          if is_footable and count_child > 0
            content_tag( :span , :class =>"", :style => "font-size: 110%; cursor: pointer;" ) do
              content_tag( :i , :class => "fa fa-plus-square-o fa-fw clickable" , :data => { :toggle => ( is_footable ? 'collapse' : nil ) , :target => ( is_footable ? ".row#{ instance_id }" : nil ) } , :id => ( is_footable ? "row#{ instance_id }" : nil ) ) do end
            end
          else
            content_tag( :span , :class =>"font-size-bigger") do
              content_tag( :i , :class => "fa fa-plus-square-o fa-fw" , :style => "color: #CCC;") do end
            end
          end
        end
      end +
      if arr_actions.present?
        content_tag( :div , :class => "col-xs-4 text-center" , :style => "padding-left:0px;padding-right:0px;" ) do
          content_tag( :div , :class => "btn-group" ) do
            "<button data-toggle='dropdown' type='button' class='btn btn-default btn-md btn-block dropdown-toggle p-w-xxs' aria-expanded='false'><i class='fa fa-gear fa-fw'></i><span class='caret'></span></button>".html_safe +
            content_tag( :ul , :class => "dropdown-menu" ) do
              (
                (
                  arr_actions.chunk{ |x| x }.map(&:first).map{ |action|
                    content_tag( :li , :class => ( action == :divider ? "divider" : "" ) ) do
                      unless action == :divider
                        if action == :edit && object.can_be_modified?
                          super_link_to( :edit, object, :in_dropdown )
                        elsif action == :delete && object.can_be_deleted?
                          super_link_to( :delete, object, :in_dropdown, { :text => translate_action( object.class.table_name.to_sym , action ) } )
                        elsif action == :view
                          super_link_to( :view, object, :in_dropdown, { :text => translate_action( object.class.table_name.to_sym , action ) } )
                        elsif action.to_s.starts_with?( "set/" )
                          super_link_to( action, object, :in_dropdown, { :text => translate_action( object.class.table_name.to_sym , action ) } )
                        else
                          if action.to_s.end_with?("_job_inmodal")

                            current_action_model = object.class::ACTIONS.map{ | action_hash | action_hash if action_hash.keys.include?(action) }.compact.first
                            if current_action_model.present?
                              str_args = '?' + ( current_action_model[ :args ].to_a.map do | attr_val |
                                if attr_val.second  == :self # IS THE VALUE
                                  "#{attr_val.first}=#{object.id}"
                                else
                                  attr_val.join('=')
                                end
                              end ).join('&')
                            end
                            super_link_to( :new , Delayed::Job , :in_dropdown , { :text => translate_action( object.class.table_name.to_sym , action.to_s.gsub( "_job_inmodal" , "" ).to_sym ) } , true , true , str_args )
                          elsif action.to_s.end_with?( "_inmodal" )
                            if object.class.constants.include?( :HOMO_FIELDS )
                              if object.class.const_get( :HOMO_FIELDS ).class == Array
                                arr_actions_for_homologate_fields = []
                                object.class.const_get( :HOMO_FIELDS ).each do |field_name|
                                  if field_name == :parent
                                    text_of_link = "Homologar #{print_model_human_name( object.class.to_s ).downcase}"
                                  else
                                    text_of_link = "Homologar #{print_model_human_name( field_name.to_s ).downcase}"
                                  end
                                  arr_actions_for_homologate_fields << super_link_to( action.to_s.gsub( "_inmodal" , "" ).to_sym , object, :in_dropdown, { :text => text_of_link } , true , true , (field_name == :parent ? "" : "?field_name=#{field_name}" ) )
                                end
                                arr_actions_for_homologate_fields.join.html_safe
                              end
                            else
                              super_link_to( action.to_s.gsub( "_inmodal" , "" ).to_sym, object, :in_dropdown, { :text => translate_action( object.class.table_name.to_sym , action.to_s.gsub( "_inmodal" , "" ).to_sym ) } , true , true )
                            end
                          else
                            super_link_to( action, object, :in_dropdown, { :text => translate_action( object.class.table_name.to_sym , action ) } , nil , true )
                          end
                        end
                      end
                    end
                  }.join.html_safe
                )
              ).html_safe
            end
          end
        end
      end +
      content_tag( :div , :class => "col-xs-4 text-center" , :style => "padding-left:0px;padding-right:0px;" ) do
        print_status( object , :only_icon )
      end

    end

  end

  # Concat all values method
  def concat_all(*strings)
    strings.join
  end

  def print_dated_at instance, date_info, show_diff_time = false, formated = nil

    dated_at          = eval( "instance.#{date_info}" )

    if dated_at.present?
      if dated_at.class == DateTime
        if formated.present?
          formated_dated_at = l( dated_at , :format => formated )
        else
          formated_dated_at = l( dated_at , :format => :dd_mm_yyyy_hh_mm_ss )
        end
      elsif dated_at.class == ActiveSupport::TimeWithZone
        if formated.present?
          formated_dated_at = l( dated_at , :format => formated )
        else
          formated_dated_at = l( dated_at , :format => :dd_mm_yyyy )
        end
      elsif dated_at.class == Date
        if formated.present?
          formated_dated_at = l( dated_at , :format => formated )
        else
          formated_dated_at = l( dated_at , :format => :dd_mm_yyyy )
        end
      end

      content_tag( :span ) do
        if show_diff_time
          content_tag( :strong ) do
            formated_dated_at.html_safe
          end + tag( :br ) + content_tag( :span , :class => "text-muted" ) do
            timeago_tag( dated_at )
          end
        else
          formated_dated_at.html_safe
        end
      end
    else
      ""
    end

	end

  def print_created_at instance, show_diff_time = false
    return ( print_dated_at instance , :created_at , show_diff_time ).html_safe
  end

  def print_updated_at instance, show_diff_time = false
    return ( print_dated_at instance , :updated_at , show_diff_time  ).html_safe
  end

  def print_run_at instance, show_diff_time = false
    return ( print_dated_at instance , :run_at , show_diff_time  ).html_safe
  end

  def print_completed_at instance, show_diff_time = false
    return ( print_dated_at instance , :completed_at , show_diff_time  ).html_safe
  end

  def print_model_human_name table_name, plural = false
    return table_name.camelize.constantize.model_name.human( :count => ( plural ? 2 : 1))
  end

  def time_ago(from_date_ordatetime, to_date_ordatetime = nil)
    hash_ret = nil
    #if to_date is present?
    if to_date_ordatetime.present?
      delta = to_date_ordatetime.to_time.to_i - from_date_ordatetime.to_time.to_i
    else
      delta = Time.now.to_i - from_date_ordatetime.to_time.to_i
    end

    if delta >= 0
      case delta
      when 0..59        then hash_ret = { :cant => "menos de un" , :unit => ( from_date_ordatetime.class == Date and to_date_ordatetime.class == Date ? "día" : "minuto" )  }
        when 60           then hash_ret = { :cant => "1" , :unit => "minuto"}
        when 61..119      then hash_ret = { :cant => "alrededor de un" , :unit => "minuto"}
        when 120..3599    then hash_ret = { :cant => "#{delta / 60}" , :unit => "minutos"}
        when 3600         then hash_ret = { :cant => "1" , :unit => "hora"}
        when 3601..86399  then hash_ret = { :cant => "#{(delta / 3600).round}" , :unit => "horas"}
        when 86400        then hash_ret = { :cant => "1" ,:unit => "día"}
        else hash_ret = { :cant => "#{(delta / 86400).round}" , :unit => "días"}
      end
    else
      return"!ERR-time_ago!"
    end
    return hash_ret.values.join(' ')
  end

  def print_homologation_param_select param_name, param_type , param_values = [], id_selected = nil

		content_tag( :div , :class => "row p-xxs form-horizontal" ) do

			content_tag( :div , :class => "col-md-2 control-label text-right" ) do "Recurso padre" end +

			content_tag( :div , :class => "col-md-10" ) do

				content_tag( :select , :class => "select2_demo_1 form-control select2_homologation" , :name => "homologation[#{param_name}]" ) do
					arr_opts = []
					param_values.each_with_index do | instance , index_of_value |
            arr_opts << content_tag( :option ) do "" end
						if instance.id == id_selected
							arr_opts << content_tag( :option , :value => "#{instance.id}" , :selected => "selected" ) do "#{instance.detail}" end
						else
							arr_opts << content_tag( :option , :value => "#{instance.id}" ) do "#{instance.detail}" end
						end
					end
					arr_opts.join.html_safe
				end

  		end

  	end

	end


end
