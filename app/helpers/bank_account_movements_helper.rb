module BankAccountMovementsHelper
	def print_amount column_kind, instance
		if column_kind == :credit
			if instance.credit?
				return content_tag( :span, :class => 'text-info'  ) do number_to_currency( instance.amount ) end if instance.amount > 0
				return content_tag( :span, :class => 'text-muted' ) do number_to_currency( 0 ) end if instance.amount == 0
			else
				return content_tag( :span, :class => 'text-muted' ) do number_to_currency( 0 ) end
			end
		end

		if column_kind == :debit
			if instance.debit?
				return content_tag( :span, :class => 'text-info'  ) do number_to_currency( instance.amount ) end if instance.amount > 0
				return content_tag( :span, :class => 'text-muted' ) do number_to_currency( 0 ) end if instance.amount == 0
			else
				return content_tag( :span, :class => 'text-muted' ) do number_to_currency( 0 ) end
			end
		end

		if column_kind == :total
			return content_tag( :span, :class => 'text-info font-bold'  ) do number_to_currency( instance ) end if instance > 0
			return content_tag( :span, :class => 'text-muted font-bold' ) do number_to_currency( instance ) end if instance == 0
			return content_tag( :span, :class => 'text-danger font-bold' ) do number_to_currency( instance ) end if instance < 0
		end

	end

	def bank_account_movement_print_if_kind_or_zero kind_of_movement, movement, movement_attr = :amount
		movement_value = eval( "movement.#{movement_attr}" )
		if movement.kind.to_sym == kind_of_movement
			if movement_value == 0
				style = "color: #888;"
			else
				if kind_of_movement == :credit
					style = "color: forestgreen;"
				elsif kind_of_movement == :debit
					style = "color: firebrick;"
				end
			end
			return content_tag( :span , :style => style ) do number_to_currency( movement_value ).to_s end
		else
			return content_tag( :span , :style => "color: #bbb;" ) do number_to_currency( 0 ).to_s end
		end
	end

	def bank_account_movement_print_if_credit_or_zero_and_available movement
		remaining_amount = movement.remaining_amount
		total_credit = movement.amount
		style_striked = "text-decoration: line-through" if remaining_amount>0.00 and total_credit != remaining_amount
		return content_tag( :span , :style => "#{style_striked}" ) do
			bank_account_movement_print_if_kind_or_zero( :credit , movement )
		end +
		if ( remaining_amount.present? and movement.kind.to_sym == :credit ) and ( remaining_amount>0.00 and total_credit != remaining_amount )
			content_tag( :span ) do
				( " (Disponible: " + bank_account_movement_print_if_kind_or_zero( :credit , movement , :remaining_amount ) + ")" ).html_safe
			end
		end
	end

	def bank_account_movement_print_if_debit_or_zero movement
		return bank_account_movement_print_if_kind_or_zero( :debit , movement )
	end

	def print_super_detail movement, format_created_at = :just_date
		content_tag( :div , :class => "row" ) do
			content_tag( :div , :class => "col col-xs-2" ) do
				if movement.date.class == Date
					"<strong>#{l( movement.date , :format => :dd_mm_yyyy )}</strong>".html_safe
				else
					"<strong>#{l( movement.date , :format => :dd_mm_yyyy_hh_mm )}</strong>".html_safe
				end
			end +
			content_tag( :div , :class => "col col-xs-2" ) do
				"##{movement.number}"
			end +
			content_tag( :div , :class => "col col-xs-8" ) do
				( "#{ (movement.current_account_movement_not_created_yet_with_bank_account_card? ? '<i class=\'fa fa-exclamation-triangle fa-fw text-warning\'></i>' : '' )} #{movement.detail}" + "#{ '<i class=\'fa fa-angle-right fa-fw\'></i>' + movement.other_detail if movement.other_detail.present? }" ).html_safe
			end
		end
	end

	def print_input_param_current_account_movement param_name, param_type , param_values = []

		content_tag( :div , :class => "row p-xxs form-horizontal" ) do

			content_tag( :div , :class => "col-md-2 control-label text-right" ) do

				if ( param_name == :agency_id )
					"Agencia"
				elsif ( param_name == :current_account_id )
					"Cuenta corriente"
				else
					""
				end

			end +

			content_tag( :div , :class => "col-md-10" ) do

				if ( param_type == :select2 )

					if ( param_name == :current_account_id )

						content_tag( :select , :class => "select2_demo_1 form-control select2_current_account_id" , :name => "current_account_movement[#{param_name}]" ) do
							arr_opts = []
							param_values.each_with_index do | instance , index_of_value |
								if index_of_value == 0
									arr_opts << content_tag( :option , :value => "#{instance.id}" , :selected => "selected" ) do "#{instance.detail}" end
								else
									arr_opts << content_tag( :option , :value => "#{instance.id}" ) do "#{instance.detail}" end
								end
								# arr_opts << content_tag( :option , :value => "#{value_key}" , :selected => ( q_attr == attribute ? "selected" : nil ) ) do "#{value_description}" end
							end
							arr_opts.join.html_safe
						end

					elsif ( param_name == :code_system_ref )

						content_tag( :select , :class => "select2_demo_1 form-control select2_job_name" , :name => "job[#{param_name}]" ) do
							arr_opts = []
							if param_values.nil?
								param_values = System.external.map{ |s| [ s.code , s.detail ] }.to_h
							else
								param_values = param_values.map{ |s| [ s.code , s.detail ] }.to_h
							end
							param_values.each do | value_key , value_description |
								arr_opts << content_tag( :option , :value => "#{value_key}" ) do "#{value_description}" end
								# arr_opts << content_tag( :option , :value => "#{value_key}" , :selected => ( q_attr == attribute ? "selected" : nil ) ) do "#{value_description}" end
							end
							arr_opts.join.html_safe
						end

					end

				elsif ( param_type == :date )

					if [ :from_date , :to_date ].include?( param_name )
						content_tag( :div , :class => "input-group date" , :id => "input_job_#{param_name}") do
							content_tag( :span , :class => "input-group-addon" ) do
								content_tag( :i , :class => "fa fa-calendar" ) do end
							end + tag( :input , :type => "text" , :class => "form-control" , :name => "job[#{param_name}]" )
						end
					end

				elsif ( param_type == :file )

					if ( param_name == :file_ref )

						content_tag( :div , :class => "fileinput fileinput-new input-group" , :data => { :provides => "fileinput" } , :id => "input_job_#{param_name}" ) do
							content_tag( :div , :class => "form-control" , :data => { :trigger => "fileinput" } ) do
								content_tag( :i , :class => "glyphicon glyphicon-file fileinput-exists" ) do end +
									content_tag( :span , :class => "fileinput-filename" ) do end
							end +
							content_tag( :span , :class => "input-group-addon btn btn-default btn-file" ) do
								content_tag( :span ,  :class => "fileinput-new" ) do "Seleccione archivo".html_safe end +
								content_tag( :span , :class => "fileinput-exists" ) do "Cambiar archivo".html_safe end +
								tag( :input , :type => "file" , :name => "job[#{param_name}]" )
							end +
							content_tag( :a , :href => "#" , :class => "input-group-addon btn btn-default fileinput-exists" , :data => { :dismiss => "fileinput" } ) do "Borrar".html_safe end
						end

					end

				end

			end
		end

	end

	def print_input_param_bank_account_card param_name, param_type , param_values = []

		content_tag( :div , :class => "row p-xxs form-horizontal" ) do

			content_tag( :div , :class => "col-md-2 control-label text-right" ) do

				if ( param_name == :agency_id )
					"Agencia"
				else
					""
				end

			end +

			content_tag( :div , :class => "col-md-10" ) do

				if ( param_type == :select2 )

					if ( param_name == :agency_id )

						content_tag( :select , :class => "select2_demo_1 form-control select2_agency_id" , :name => "bank_account_card[#{param_name}]" ) do
							arr_opts = []
							param_values.each_with_index do | instance , index_of_value |
								if index_of_value == 0
									arr_opts << content_tag( :option , :value => "#{instance.id}" , :selected => "selected" ) do "#{instance.detail}" end
								else
									arr_opts << content_tag( :option , :value => "#{instance.id}" ) do "#{instance.detail}" end
								end
								# arr_opts << content_tag( :option , :value => "#{value_key}" , :selected => ( q_attr == attribute ? "selected" : nil ) ) do "#{value_description}" end
							end
							arr_opts.join.html_safe
						end
					end

				end

			end
		end

	end

	def render_to_add_current_account q_current_account_id = nil , q_amount = nil, disabled = nil

		content_tag( :div , :class => "row form-horizontal current_account_line" , :id => "current_account" ) do

			content_tag( :div , :class => "col-md-1 text-right" ) do
				unless disabled
					content_tag( :a , :href => "#" , :class => "btn btn-link btn-sm remove_field" ) do
						content_tag( :span , :class => "h4" ) do content_tag( :i , :class => "fa fa-times-circle fa-fw" ) do end
						end
					end
				else
					content_tag( :a , :href => "#" , :class => "btn btn-link btn-sm" , :style => "cursor: default;") do
						content_tag( :span , :class => "h4 text-info" ) do
							content_tag( :i , :class => "fa fa-check fa-fw" ) do end
						end
					end
				end
			end +

			content_tag( :div , :class => "col-md-2 text-right control-label" ) do
				content_tag( :i , :class => "fa fa-exclamation-triangle fa-fw text-warning warning_current_account" , :style => "display: none;" ) do end +
				"Cuenta corriente".html_safe
			end +

			content_tag( :div , :class => "col-md-5" ) do
				content_tag( :select , :class => "select2_demo_1 form-control select2_current_account" , :name => ( disabled ? nil : "current_accounts_movements[][current_account_id]" ) , :disabled => disabled ) do
					arr_opts = []
					CurrentAccount.all.each do | current_account |
						arr_opts << content_tag( :option , :value => "#{ current_account.id }" , :selected => ( q_current_account_id == current_account.id ? "selected" : nil ) ) do "#{current_account.detail}" end
					end
					arr_opts.join.html_safe
				end
			end +

			content_tag( :div , :class => "col-md-1 text-right control-label" ) do
				"Importe".html_safe
			end +

			content_tag( :div , :class => "col-md-3" ) do
				tag( :input , :type => "number" , :class => "form-control input_amount" , :name => ( disabled ? nil : "current_accounts_movements[][amount]" ) , :placeholder => "0.00" , :step => ".01" , :value => q_amount , :disabled => disabled )
			end

		end

	end

end
