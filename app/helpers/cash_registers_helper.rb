module CashRegistersHelper

	def cash_register_movement_print_if_kind_or_zero kind_of_movement, movement, movement_attr = :amount, style_force = nil
		if movement.class == CashRegisterMovement
			movement_value = eval( "movement.#{movement_attr}" )
			if movement.kind.to_sym == kind_of_movement
				if movement_value == 0
					style = "color: #888;"
				else
					if kind_of_movement == :ingress
						style = "color: forestgreen;"
					elsif kind_of_movement == :egress
						style = "color: firebrick;"
					end
				end
				return content_tag( :span , :style => style ) do number_to_currency( movement_value ).to_s end
			else
				return content_tag( :span , :style => "color: #bbb;" ) do number_to_currency( 0 ).to_s end
			end
		elsif [BigDecimal,Fixnum].include?(movement.class)
			unless style_force.present?
				if movement == 0.00
					style = "color: #888;"
				elsif movement < 0.00
					style = "color: firebrick;"
				else
					style = "color: forestgreen;"
				end
			else
				style = style_force
			end
			return content_tag( :span , :style => style ) do number_to_currency( movement ).to_s end
		end
	end

	def print_cash_register_amount_accumulated instance, attribute
		return print_if_kind_or_zero( :ingress 	, instance.calc_ingress 	) if attribute == :calc_ingress
		return print_if_kind_or_zero( :egress 	, instance.calc_egress 		)	if attribute == :calc_egress
		return print_if_kind_or_zero( :balance 	, instance.calc_balance 	)	if attribute == :calc_balance
	end


	def print_input_param_cash_register_movement param_name, param_type , param_values = []

		content_tag( :div , :class => "row p-xxs form-horizontal" ) do

			content_tag( :div , :class => "col-md-2 control-label text-right" ) do

				if ( param_name == :amount )
					"Importe"
				elsif ( param_name == :detail )
					"Detalle"
				else
					""
				end

			end +

			content_tag( :div , :class => "col-md-10" ) do

				if ( param_type == :text_field )

					content_tag( :input , :class => "form-control" , :placeholder => "Ingrese un valor para #{CashRegisterMovement.human_attribute_name( param_name )}" , :style => "width: 100%" , :name => "cash_register_movement[#{param_name}]" ) do end

				end

			end
		end

	end

	def print_cash_register_movement_if_ingress_or_zero movement
		# return cash_register_movement_print_if_kind_or_zero( :ingress , movement )
		remaining_ingress = movement.remaining_ingress
		total_ingress = movement.amount
		style_striked = "text-decoration: line-through" if remaining_ingress>0.00 and total_ingress != remaining_ingress
		return content_tag( :span , :style => "#{style_striked}" ) do
			cash_register_movement_print_if_kind_or_zero( :ingress , movement )
		end +
		if ( remaining_ingress.present? and movement.kind.to_sym == :ingress ) and ( remaining_ingress>0.00 and total_ingress != remaining_ingress )
			content_tag( :span ) do
				( " (Disponible: " + cash_register_movement_print_if_kind_or_zero( :ingress , movement , :remaining_ingress ) + ")" ).html_safe
			end
		end
	end

	def print_cash_register_movement_if_egress_or_zero movement
		return cash_register_movement_print_if_kind_or_zero( :egress , movement )
	end

	def print_cash_register_number_and_link instance
		# return "CAJA".html_safe + print_number( instance.number )
		return super_link_to( :pure_link , cash_register_movements_path( :cash_register_id => instance[ :id ] ) , :just_text , { :text => "Caja ##{instance.number}" } )
	end

	def print_cash_register_movement_balance_to_movement movement_ref
		amounts = []
		@instances.reverse_order.map do |movement|
			amounts << movement.signed_amount
			break if movement == movement_ref
		end
		return cash_register_movement_print_if_kind_or_zero( :balance , amounts.sum )
	end

end
