module CurrentAccountMovementsHelper

	def print_if_kind_or_zero kind_of_movement, movement, style_force = nil
		if movement.class == CurrentAccountMovement
			if movement.kind.to_sym == kind_of_movement
				unless style_force.present?
					if style_force == :muted
						return content_tag( :span , :style => "color: #bbb;" ) do number_to_currency( movement.amount ).to_s end
					else
						if movement.amount == 0
							style = "color: #888;"
						else
							if kind_of_movement == :credit
								style = "color: forestgreen;"
							elsif kind_of_movement == :debit
								style = "color: firebrick;"
							end
						end
					end
				end
				return content_tag( :span , :style => ( style_force.present? ? style_force : style ) ) do number_to_currency( movement.amount ).to_s end
			else
				return content_tag( :span , :style => ( style_force.present? ? style_force : "color: #bbb;" ) ) do number_to_currency( 0.00 ).to_s end
			end
		elsif [ BigDecimal , Fixnum , Float ].include?( movement.class )
			unless style_force.present?
				if style_force == :muted
					return content_tag( :span , :style => "color: #bbb;" ) do number_to_currency( movement ).to_s end
				else
					if kind_of_movement == :remaining_credit
						style = "color: forestgreen;"
					elsif kind_of_movement == :remaining_debit
						style = "color: firebrick;"
					else
						if movement == 0.00
							style = "color: #888;"
						elsif movement < 0.00
							style = "color: firebrick;"
						else
							style = "color: forestgreen;"
						end
					end
				end
				return content_tag( :span , :style => ( style_force.present? ? style_force : style ) ) do number_to_currency( movement ).to_s end
			else
				style = style_force
				return content_tag( :span , :style => ( style_force.present? ? style_force : style ) ) do number_to_currency( movement ).to_s end
			end
		end
	end

	def print_if_credit_or_zero movement
		return print_if_kind_or_zero( :credit , movement )
	end

	def print_if_debit_or_zero movement
		return print_if_kind_or_zero( :debit , movement )
	end

	def print_movement_child_detail affectation, parent_kind
		if parent_kind == "credit"
			movement_to_print = affectation.movement_to
		elsif parent_kind == "debit"
			movement_to_print = affectation.movement_from
		end

		content_tag( :i , :class => "fa fa-blank fa-fw ") do end +
		if parent_kind == "credit"
			content_tag( :i , :class => "fa fa-caret-right fa-fw text-muted") do end
		elsif parent_kind == "debit"
			content_tag( :i , :class => "fa fa-caret-up fa-fw text-muted") do end
		end +
		content_tag( :i , :class => "fa fa-blank fa-fw ") do end +
		"<strong>#{ l( movement_to_print.resource_date , :format => :dd_mm_yyyy ) }#{ ( movement_to_print.resource_time ? ' ' + l( movement_to_print.resource_time , :format => :hh_mm ) : "" ) }</strong> | ".html_safe +
		if ( movement_to_print.remaining_amount > 0 )
			"<strong>#{movement_to_print.resource_type.classify.constantize.model_name.human}</strong> <i class='fa fa-angle-double-right fa-fw'></i><span class='text-muted'>#{movement_to_print.detail}</span><i class='fa fa-angle-double-right fa-fw'></i><strong>#{ number_to_currency( movement_to_print.amount ) }</strong> <span class='text-warning'><i class='fa fa-clock-o fa-fw'></i>RESTA #{number_to_currency( movement_to_print.remaining_amount )}</span>".html_safe
		else
			"<strong>#{movement_to_print.resource_type.classify.constantize.model_name.human}</strong> <i class='fa fa-angle-double-right fa-fw'></i><span class='text-muted'>#{movement_to_print.detail}</span><i class='fa fa-angle-double-right fa-fw'></i><strong>#{ number_to_currency( movement_to_print.amount ) }</strong> <span class='text-info'><i class='fa fa-check fa-fw'></i>COMPLETO</span>".html_safe
		end

	end

	def print_if_debit_or_zero_child affectation
		if affectation.movement_from.debit?
			return print_if_kind_or_zero( :debit , affectation.amount )
		else
			return print_if_kind_or_zero( :credit , 0.00 )
		end
	end

	def print_if_credit_or_zero_child affectation
		if affectation.movement_from.debit?
			return print_if_kind_or_zero( :debit , 0.00 )
		else
			return print_if_kind_or_zero( :credit , affectation.amount )
		end
	end

	def print_if_credit_or_zero_and_remaining movement

		remaining_amount = movement.remaining_amount
		total_credit = movement.amount
		style_striked = "text-decoration: line-through;" if remaining_amount>0.00 and total_credit != remaining_amount and movement.kind.to_sym == :credit

		return content_tag( :span , :style => "#{style_striked}" ) do
			print_if_kind_or_zero( :credit , movement , ( style_striked.present? ? "color: #888!important;" : ( remaining_amount==0.00 ? "color: #888!important;" : nil ) ) )
		end +
		if ( remaining_amount.present? and movement.kind.to_sym == :credit ) and ( remaining_amount>0.00 and total_credit != remaining_amount )
			content_tag( :span ) do
				( " " + print_if_kind_or_zero( :remaining_credit , remaining_amount ) ).html_safe
			end
		end

	end

	def print_if_debit_or_zero_and_remaining movement

		remaining_amount = movement.remaining_amount
		total_debit = movement.amount
		style_striked = "text-decoration: line-through" if remaining_amount>0.00 and total_debit != remaining_amount and movement.kind.to_sym == :debit

		return content_tag( :span , :style => "#{style_striked}" ) do
			print_if_kind_or_zero( :debit , movement , ( style_striked.present? ? "color: #888!important;" : ( remaining_amount==0.00 ? "color: #888!important;" : nil ) ) )
		end +
		if ( remaining_amount.present? and movement.kind.to_sym == :debit ) and ( remaining_amount>0.00 and total_debit != remaining_amount )
			content_tag( :span ) do
				( " " + print_if_kind_or_zero( :remaining_debit , remaining_amount ) ).html_safe
			end
		end

	end


	def print_balance_of_current_period_to_movement movement_ref
		amounts = []
		@period_movements.reverse_order.map do | movement |
			# amounts << movement.signed_amount
			amounts << movement.signed_remaining_amount
			break if movement == movement_ref
		end
		return print_if_kind_or_zero( :balance , amounts.sum )
	end

	def print_detail movement, format_created_at = :just_date
		content_tag( :div , :class => "row" ) do
			content_tag( :div , :class => "col col-md-4" ) do
				"<strong>#{ l( movement.resource_date , :format => :dd_mm_yyyy ) }#{ ( movement.resource_time ? ' ' + l( movement.resource_time , :format => :hh_mm ) : "" ) }</strong>".html_safe
			end +
			content_tag( :div , :class => "col col-md-8" ) do
				"<strong>#{movement.resource_type.classify.constantize.model_name.human}</strong> <i class='fa fa-angle-double-right fa-fw'></i><span class='text-muted'>#{movement.detail}</span>".html_safe
			end
		end
	end

	# --------------------------------------------------

	def print_period_info period_ref
		if period_ref.present?
			content_tag( :div , :class => "row" ) do
				content_tag( :div , :class => "col-sm-12 border-left-right border-top-bottom p-xs b-r-md #{ ( period_ref.final_date.present? ? "bg-danger" : "bg-info" ) }" ) do
					content_tag( :center ) do "Usted está viendo".html_safe end + tag( :br ) +
					"- Período: ".html_safe + content_tag( :strong ) do "N°#{period_ref.order_number}" end + tag( :br ) +
					"- Desde: ".html_safe + content_tag( :strong ) do  "#{l(period_ref.initial_date)}" end + " (Hace #{time_ago(period_ref.initial_date)})".html_safe + tag( :br ) +
					if period_ref.final_date.present?
						"- Hasta: ".html_safe + content_tag( :strong ) do "#{l(period_ref.final_date)}" end + " (Hace #{time_ago(period_ref.final_date)})" + tag( :br ) +
						"- Duración: ".html_safe + content_tag( :strong ) do "#{time_ago(period_ref.initial_date, (period_ref.final_date + 1.day ) )}" end  + tag( :br )  + tag( :br ) +
						content_tag( :center ) do content_tag( :span , :class => "h4") do "El período se encuentra cerrado".html_safe end end
					else
						"- No ha finalizado".html_safe + tag( :br ) + tag( :br ) +
						content_tag( :center ) do content_tag( :span , :class => "h4") do "El período se encuentra vigente".html_safe end end
					end
				end
			end
		end
	end

	def print_period_info_about kind , period_ref

		if period_ref.present?

			class_div 	= { :debits => nil , :credits => nil , :balance => "border-left-right border-top-bottom" }
			style_div 	= { :debits => nil , :credits => nil , :balance => "background-color: #eee;" }
			h1_style 		= { :debits => "text-danger" 	, :credits => "text-info" , :balance => "#{( @hash_current_account_info[ kind ][ :in_period ] < 0 ? "text-danger" : "text-info" )} font-bold" 	}
			translation = { :debits => "Débitos" 			, :credits => "Créditos" 	, :balance => "Balance" }

			content_tag( :div , :class => "row" ) do
				content_tag( :div , :class => "col-xs-12 #{ class_div[ kind ] }" , :style => "#{style_div[ kind ]}" ) do
					content_tag( :h3 , :class => "text-center" ) do translation[ kind ].html_safe end +
					content_tag( :h1 , :class => "m-b-xs #{ h1_style[ kind ] }" ) do "#{number_to_currency( @hash_current_account_info[ kind ][ :in_period ] )}" end +
					content_tag( :div , :id => "sparkline1" ) do end +
					content_tag( :div , :class => "row" )	do
						content_tag( :div , :class => "col-xs-6" ) do
							content_tag( :small , :class => "stats-label" ) do "Inicial" end + content_tag( :h4 ) do number_to_currency( @hash_current_account_info[ kind ][ :initial ] ) end
						end +
						content_tag( :div , :class => "col-xs-6" ) do
							content_tag( :small , :class => "stats-label" ) do "Acumulado" end + content_tag( :h4 ) do number_to_currency( @hash_current_account_info[ kind ][ :accumulated ] ) end
						end
					end
				end
			end

		end

	end

	def print_periods_view_currency instance, attribute
		return print_if_kind_or_zero( :credit , instance.initial_credit ) if attribute == :initial_credit
		return print_if_kind_or_zero( :debit , instance.initial_debit ) 	if attribute == :initial_debit
		if attribute == :final_credit
			unless instance.final_date.present?
				return content_tag( :span , :class => "text-muted" ) do
					content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :credit , instance.final_credit , :muted )
				end
			else
				return print_if_kind_or_zero( :credit , instance.final_credit )
			end
		end
		if attribute == :final_debit
			unless instance.final_date.present?
				return content_tag( :span , :class => "text-muted" ) do
					content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :debit , instance.final_debit , "color: #AAA !important;" )
				end
			else
				return print_if_kind_or_zero( :debit , instance.final_debit )
			end
		end
		if attribute == :final_balance
			unless instance.final_date.present?
				return content_tag( :span , :class => "text-muted" ) do
					content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :balance , instance.final_balance , "color: #AAA !important;" )
				end
			else
				return print_if_kind_or_zero( :balance , instance.final_balance )
			end
		end
		# if attribute == :total_balance
		# 	unless instance.final_date.present?
		# 		return content_tag( :span , :class => "text-muted" ) do
		# 			content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :credit , instance.total_balance , "color: #AAA !important;" )
		# 		end
		# 	else
		# 		return print_if_kind_or_zero( :credit , instance.total_balance )
		# 	end
		# end

	end

	def print_current_account_remaining_amount instance
		remaining_amount = instance.current_account.credit_maximum + instance.final_balance
		unless instance.final_date.present?
			return content_tag( :span , :class => "text-muted" ) do
				content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :balance , remaining_amount , "color: #AAA !important;" )
				end
		else
			return print_if_kind_or_zero( :balance , remaining_amount )
		end
	end

	def print_agency_company_remaining_amount instance
		remaining_amount = instance.agency_company.credit_maximum + instance.final_balance
		unless instance.current_account_period.final_date.present?
			return content_tag( :span , :class => "text-muted" ) do
				content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :balance , remaining_amount , "color: #AAA !important;" )
			end
		else
			return print_if_kind_or_zero( :balance , remaining_amount )
		end
	end

	def print_current_account_credit_maximum instance
		content_tag( :span , :class => "h5" ) do
			number_to_currency( instance.current_account.credit_maximum ).html_safe
		end
	end

	def print_agency_company_credit_maximum instance
		content_tag( :span , :class => "h5" ) do
			number_to_currency( instance.agency_company.credit_maximum ).html_safe
		end
	end

	def print_agency_company_detail instance
		content_tag( :span , :class => "h4" ) do
			"#{instance.agency_company.detail}".html_safe + content_tag( :i , :class => "fa fa-arrow-right fa-fw" ) do end
		end + tag( :br ) + tag( :br ) +
		content_tag( :span , :class => "h5" ) do
			if @period.class == AgencyCompanyPeriod and @period == instance
				super_link_to( :pure_link , "#table_of_movements" 	, :in_dropdown , { :text => "Usted está viendo estos movimientos" , :icon => "fa-angle-double-right", :class => { :type => "btn btn-outline" , :size => "btn-sm" , :color_bg => "btn-default" } } )
			else
				super_link_to( :pure_link , get_current_account_main_view_path( :period => instance.current_account_period.order_number , :agency_company => instance.agency_company.id) 	, :in_dropdown , { :text => "Ver movimientos" , :icon => "fa-angle-double-right", :class => { :type => "btn" , :size => "btn-sm" , :color_bg => "btn-success" } } )
			end
		end
	end

	def print_not_affected_amount_detail instance
		content_tag( :span , :class => "h4" ) do
			if instance[ :id ].present?
				super_link_to( :pure_link , get_current_account_main_view_path( :period => instance[ :order_number ] , :agency_company => instance[ :id ] ) 	, :just_text , { :text => "#{instance[ :detail ]}" } ) + content_tag( :i , :class => "fa fa-arrow-right fa-fw" ) do end
			else
				"#{instance[ :detail ]} ".html_safe + content_tag( :i , :class => "fa fa-arrow-right fa-fw" ) do end
			end
		end
	end

	def print_agency_company_periods_view_currency instance, attribute
		return print_if_kind_or_zero( :credit , instance.initial_credit ) if attribute == :initial_credit
		return print_if_kind_or_zero( :debit , instance.initial_debit ) 	if attribute == :initial_debit
		if attribute == :final_credit
			unless instance.current_account_period.final_date.present?
				return content_tag( :span , :class => "text-muted" ) do
					content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :credit , instance.final_credit , "color: #AAA !important;" )
				end
			else
				return print_if_kind_or_zero( :credit , instance.final_credit )
			end
		end
		if attribute == :final_debit
			unless instance.current_account_period.final_date.present?
				return content_tag( :span , :class => "text-muted" ) do
					content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :debit , instance.final_debit , "color: #AAA !important;" )
				end
			else
				return print_if_kind_or_zero( :debit , instance.final_debit )
			end
		end
		if attribute == :final_balance
			unless instance.current_account_period.final_date.present?
				return content_tag( :span , :class => "text-muted" ) do
					content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :balance , instance.final_balance , "color: #AAA !important;" )
				end
			else
				return print_if_kind_or_zero( :balance , instance.final_balance )
			end
		end
		# if attribute == :total_balance
		# 	unless instance.current_account_period.final_date.present?
		# 		return content_tag( :span , :class => "text-muted" ) do
		# 			content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :balance , instance.total_balance , "color: #AAA !important;" )
		# 		end
		# 	else
		# 		return print_if_kind_or_zero( :balance , instance.total_balance )
		# 	end
		# end

	end

	def print_not_affected_amount_view_currency instance, attribute
		if attribute == :credit
			return content_tag( :span , :class => "text-muted h4" ) do
				content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :credit , instance[ :credit ] , "color: #AAA !important;" )
			end
		end
		if attribute == :debit
			return content_tag( :span , :class => "text-muted h4" ) do
				content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :debit , instance[ :debit ] , "color: #AAA !important;" )
			end
		end
	end

	def print_periods_view_final_date instance
		if instance.final_date.present?
			print_dated_at( instance , :final_date , true )
		else
			content_tag( :span , :class => "text-muted" ) do content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + "Vigente".html_safe end
		end
	end

	def print_number_of_order_period instance
		"Período ".html_safe + print_number( instance.order_number )
	end

	def print_initial_and_final_data instance, data

		final_date = instance.final_date 												if instance.class == CurrentAccountPeriod
		final_date = instance.current_account_period.final_date if instance.class == AgencyCompanyPeriod

		content_tag( :div , :class => "row text-center" ) do

			content_tag( :div , :class => "col-md-12" ) do

				title_description = "Días acumulados" if data == :dates
				title_description = "Débito acumulado" if data == :debits
				title_description = "Crédito acumulado" if data == :credits
				title_description = "Balance período" if data == :balances

				content_tag( :small , :class => "stats-label text-muted" ) do "#{title_description}" end +

				content_tag( :h3 ) do

					if data == :dates
						# print_dated_at( instance , :initial_date , true )
						time_ago( instance.initial_date , ( final_date.present? ? final_date : Date.today ) )
					elsif data == :credits
						unless final_date.present?
							content_tag( :span , :class => "text-muted" ) do
								content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :credit , ( instance.final_credit - instance.initial_credit ) , "color: #AAA !important;" )
							end
						else
							print_if_kind_or_zero( :credit , ( instance.final_credit - instance.initial_credit ) )
						end
					elsif data == :debits
						unless final_date.present?
							content_tag( :span , :class => "text-muted" ) do
								content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :debit , ( instance.final_debit - instance.initial_debit ) , "color: #AAA !important;" )
							end
						else
							print_if_kind_or_zero( :debit , ( instance.final_debit - instance.initial_debit ) )
						end
					elsif data == :balances
						unless final_date.present?
							content_tag( :span , :class => "text-muted" ) do
								content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :balance , ( instance.final_balance - instance.initial_balance ) , "color: #AAA !important;" )
							end
						else
							print_if_kind_or_zero( :balance , ( instance.final_balance - instance.initial_balance ) )
						end
					end

				end

			end

		end +
		content_tag( :div , :class => "row" ) do

			title_description = "Fecha inicial" if data == :dates
			title_description = "Débito inicial" if data == :debits
			title_description = "Crédito inicial" if data == :credits
			title_description = "Balance inicial" if data == :balances

			content_tag( :div , :class => "col-md-6 verticalcenter text-center p-xxs" ) do
				content_tag( :small , :class => "stats-label text-muted" ) do "#{title_description}" end + tag( :br ) + content_tag( :strong ) do
					if data == :dates
						print_dated_at( instance , :initial_date )
					elsif data == :credits
						print_periods_view_currency( instance, :initial_credit )
					elsif data == :debits
						print_periods_view_currency( instance, :initial_debit )
					elsif data == :balances
						print_if_kind_or_zero( :balance , instance.initial_balance )
					end
				end
			end +
			content_tag( :div , :class => "col-md-6 verticalcenter text-center p-xxs" ) do

				title_description = "Fecha final" if data == :dates
				title_description = "Débito final" if data == :debits
				title_description = "Crédito final" if data == :credits
				title_description = "Balance final" if data == :balances

				content_tag( :small , :class => "stats-label text-muted" ) do "#{title_description}" end + tag( :br ) + content_tag( :strong ) do
					if data == :dates
						if final_date.present?
							print_dated_at( instance , :final_date )
						else
							content_tag( :span , :class => "text-muted" ) do content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + "Vigente".html_safe end
						end
					elsif data == :credits
						print_periods_view_currency( instance, :final_credit )
					elsif data == :debits
						print_periods_view_currency( instance, :final_debit )
					elsif data == :balances
						unless final_date.present?
							content_tag( :span , :class => "text-muted" ) do
								content_tag( :i , :class => "fa fa-clock-o fa-fw" ) do end + print_if_kind_or_zero( :balance , instance.final_balance , "color: #AAA !important;" )
							end
						else
							print_if_kind_or_zero( :balance , instance.final_balance )
						end
					end
				end
			end

		end

	end

	def render_to_add_affectation q_current_account_movement_id = nil , q_amount = nil, disabled = nil

		content_tag( :div , :class => "row form-horizontal current_account_movement_line" , :id => "current_account_movement" ) do

			content_tag( :div , :class => "col-md-1 text-right" ) do
				unless disabled
					content_tag( :a , :href => "#" , :class => "btn btn-link btn-sm remove_field" ) do
						content_tag( :span , :class => "h4" ) do content_tag( :i , :class => "fa fa-times-circle fa-fw" ) do end
						end
					end
				else
					content_tag( :a , :href => "#" , :class => "btn btn-link btn-sm" , :style => "cursor: default;") do
						content_tag( :span , :class => "h4 text-info" ) do
							content_tag( :i , :class => "fa fa-check fa-fw" ) do end
						end
					end
				end
			end +

			content_tag( :div , :class => "col-md-2 text-right control-label" ) do
				content_tag( :i , :class => "fa fa-exclamation-triangle fa-fw text-warning warning_current_account_movement" , :style => "display: none;" ) do end +
				"Movimiento".html_safe
			end +

			content_tag( :div , :class => "col-md-6" ) do
				content_tag( :select , :class => "select2_demo_1 form-control select2_current_account_movement" , :name => ( disabled ? nil : "movement_affectations[][current_account_movement_id]" ) , :disabled => disabled ) do
					arr_opts = []
					if q_current_account_movement_id.present?
						@current_account_movements.where( 'id = ?' , q_current_account_movement_id ).each do | current_account_movement |
							if current_account_movement.remaining_amount == 0
								arr_opts << content_tag( :option , :value => "#{ current_account_movement.id }" , :selected => "selected" ) do "#{current_account_movement.detail} | #{number_to_currency( current_account_movement.amount )}#{( current_account_movement.remaining_amount != current_account_movement.amount ? " ( CANCELADO )" : "" ) }" end
							else
								arr_opts << content_tag( :option , :value => "#{ current_account_movement.id }" , :selected => "selected" ) do "#{current_account_movement.detail} | #{number_to_currency( current_account_movement.amount )}#{( current_account_movement.remaining_amount != current_account_movement.amount ? " ( Restante #{number_to_currency( current_account_movement.remaining_amount ) } )" : "" ) }" end
							end
						end
					else
						@current_account_movements.where( 'remaining_amount > ? ' , 0 ).each do | current_account_movement |
							arr_opts << content_tag( :option , :value => "#{ current_account_movement.id }" , :selected => ( q_current_account_movement_id == current_account_movement.id ? "selected" : nil ) ) do "#{current_account_movement.detail} | #{number_to_currency( current_account_movement.amount )}#{( current_account_movement.remaining_amount != current_account_movement.amount ? " ( Restante #{number_to_currency( current_account_movement.remaining_amount )} )" : "" ) }" end
						end
					end
					arr_opts.join.html_safe
				end
			end +

			content_tag( :div , :class => "col-md-1 text-right control-label" ) do
				"Importe".html_safe
			end +

			content_tag( :div , :class => "col-md-2" ) do
				tag( :input , :type => "number" , :class => "form-control input_amount" , :name => ( disabled ? nil : "movement_affectations[][amount]" ) , :placeholder => "0.00" , :step => ".01" , :value => q_amount , :disabled => disabled )
			end

		end

	end

end
