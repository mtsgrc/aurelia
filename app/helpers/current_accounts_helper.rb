module CurrentAccountsHelper
	def print_remaining_amount current_account
		str_class = "text-danger" if current_account.remaining_amount < 0
		str_class = "text-muted" if current_account.remaining_amount == 0
		str_class = "text-info" if current_account.remaining_amount > 0
		return content_tag(:span, :class => str_class ) do
			number_to_currency(current_account.remaining_amount)
		end
	end

	def detail_of_current_account_more_link_to_movements current_account
		return super_link_to( :pure_link , get_current_account_main_view_path( :current_account_id => current_account.id ) , :just_text , { :text => "#{current_account.detail}" } )
		# return link_to( "#{current_account.detail}" , current_account_movements_path( :q => { :current_account_id_eq => current_account.id } ) )
	end

	def print_period_current_detail current_account

		content_tag( :div , :class => "row" ) do
			if current_account.current_period.present?
				content_tag( :div , :class => "col-md-1" ) do
					content_tag(:span, :class => "text-info" ) do
						"<i class='fa fa-check'></i>".html_safe
					end
				end +
				content_tag( :div , :class => "col-md-6" ) do
					"<strong>\##{current_account.periods.count}</strong> - <strong>#{l(current_account.current_period.initial_date)}</strong>".html_safe
				end
			elsif current_account.last_period.present?

			else

			end

		end

	end

	def print_period_total_amounts current_account, company_ref = nil

		content_tag( :div , :class => "row" ) do
			if current_account.current_period.present?
				content_tag( :div , :class => "col-md-4" ) do
					content_tag(:span, :class => ( current_account.total_amounts[ :credits ] > 0.00 ? "text-info" : "text-muted" ) ) do
						"#{number_to_currency(current_account.total_amounts[ :credits ])}".html_safe
					end
				end +
				content_tag( :div , :class => "col-md-4" ) do
					content_tag(:span, :class => ( current_account.total_amounts[ :debits ] < 0.00 ? "text-danger" : "text-muted" ) ) do
						"#{number_to_currency(current_account.total_amounts[ :debits ])}".html_safe
					end
				end
			elsif current_account.last_period.present?
			else

			end

		end

	end

	def print_period_total_amounts_by_agency_company current_account, company_ref

		return print_period_total_amounts current_account, company_ref

	end

	def render_external_agency_company q_agency_company_id = nil , q_amount = nil, disabled = nil

		content_tag( :div , :class => "row form-horizontal agency_company_line p-xxs" , :id => "agency_company" ) do

			content_tag( :div , :class => "col-md-4 text-right control-label" ) do
				content_tag( :i , :class => "fa fa-exclamation-triangle fa-fw text-warning warning_agency_company" , :style => "display: none;" ) do end +
				content_tag( :strong ) do
					"Sistema <i class=\"fa fa-angle-right fa-fw\"></i> Agencia <i class=\"fa fa-angle-right fa-fw\"></i> Empresa:".html_safe
				end
			end +
			tag( :input , :type => "number" , :class => "form-control hidden" , :name => "agencies_companies[][agency_company_id]" , :value => q_agency_company_id , :disabled => disabled , :readonly => "readonly" ) +
			content_tag( :div , :class => "col-md-3 control-label text-left" ) do
				unless q_agency_company_id.present?
					content_tag( :select , :class => "select2_demo_1 form-control select2_agency_company" , :name => ( disabled ? nil : "agencies_companies[][agency_company_id]" ) , :disabled => disabled ) do
						arr_opts = []
						@external_agencies_companies.each do | agency_company |
							arr_opts << content_tag( :option , :value => "#{ agency_company.id }" , :selected => ( q_agency_company_id == agency_company.id ? "selected" : nil ) ) do "#{agency_company.detail}" end
						end
						arr_opts.join.html_safe
					end
				else
					arr_opts = []
					@external_agencies_companies.each do | agency_company |
						if q_agency_company_id == agency_company.id
							arr_opts << content_tag( :span ,:class => "h4 text-muted" ) do "#{agency_company.detail}" end
						end
					end
					arr_opts.join.html_safe
				end
			end +

			content_tag( :div , :class => "col-md-2 text-right control-label" ) do
				"Crédito máximo".html_safe
			end +

			content_tag( :div , :class => "col-md-3" ) do
				tag( :input , :type => "number" , :class => "form-control input_amount" , :min => 0 , :name => ( disabled ? nil : "agencies_companies[][amount]" ) , :placeholder => "0.00" , :step => ".01" , :value => q_amount , :disabled => disabled )
			end

		end

	end

end
