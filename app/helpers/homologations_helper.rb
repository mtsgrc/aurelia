module HomologationsHelper

	def print_models_relationship ( homologation )

		child_model_human_name 	= print_model_human_name( homologation.child_table_name )
		parent_model_human_name = print_model_human_name( homologation.parent_table_name )

		content_tag( :span ) do
			( child_model_human_name + content_tag( :i , :class => "fa fa-angle-right fa-fw" ) do end + parent_model_human_name ).html_safe
		end

	end

	def print_identification_parent_value instance

		class_table_name = instance.parent_table_name
		class_value = instance.parent_value

		if class_table_name

			class_ref = class_table_name.camelize.constantize
			instances = []

			class_ref.all.select do |r|
				if r.homologation_identity == class_value
					instances << r
					break
				end
			end

			if instances.count == 1
				return instances.first.detail
			end

		end

	end

	def print_identification_child_value instance
		return instance.child_value
	end

end
