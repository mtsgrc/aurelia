module InspectorsHelper

	def print_frecuency inspector
		if inspector.frecuency_amount == 1
			t = "minuto" 	if inspector.frecuency_unit  == "minutes"
			t = "hora"	 	if inspector.frecuency_unit  == "hours"
			t = "día" 		if inspector.frecuency_unit  == "days"
		else
			t = "minutos" if inspector.frecuency_unit  == "minutes"
			t = "horas" 	if inspector.frecuency_unit  == "hours"
			t = "días" 		if inspector.frecuency_unit  == "days"
		end

		content_tag( :span ) do
			"Cada #{inspector.frecuency_amount} #{t}"
		end

	end

	def print_on_error inspector

	end

	def print_last_status inspector
		if inspector.inspects
			if inspector.inspects.present?

				inspect_since_seconds = ( Time.now - inspector.inspects.last.run_at )

				if inspect_since_seconds.present? and ( inspect_since_seconds > 60 )
					inspect_since_minutes = ( inspect_since_seconds / 60 )
				end
				if inspect_since_minutes.present? and ( inspect_since_minutes > 60 )
					inspect_since_hours 	= ( inspect_since_minutes / 60 )
				end
				if inspect_since_hours.present? and ( inspect_since_hours > 24 )
					inspect_since_days 		= ( inspect_since_hours / 24 )
				end

				if inspector.inspects.last.response_status == true
					if ( inspect_since_seconds / 60 ) < 5
						content_tag( :span , :class => "text-info" ) do
							content_tag( :i , :class => "fa fa-circle fa-fw") do end
						end
					else
						content_tag( :span , :class => "text-warning" ) do
							content_tag( :i , :class => "fa fa-circle fa-fw") do end
						end
					end
				else
					content_tag( :span , :class => "text-danger" ) do
						content_tag( :i , :class => "fa fa-circle fa-fw") do end
					end
				end
			else
				content_tag( :span , :class => "text-muted" ) do
					content_tag( :i , :class => "fa fa-circle-o fa-fw") do end
				end
			end +
			if inspect_since_days.present?
				"Ejecutado hace #{inspect_since_days.to_i} días"
			elsif inspect_since_hours.present?
				"Ejecutado hace #{inspect_since_hours.to_i} horas"
			elsif inspect_since_minutes.present?
				"Ejecutado hace #{inspect_since_minutes.to_i} minutos"
			else
				"Ejecutado hace #{inspect_since_seconds.to_i} segundos"
			end
		else
			content_tag( :span , :class => "text-muted" ) do
				content_tag( :i , :class => "fa fa-circle-o fa-fw") do end + "No se ha ejecutado nunca"
			end
		end
	end

end
