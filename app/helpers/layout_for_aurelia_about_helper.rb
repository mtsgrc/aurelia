module LayoutForAureliaAboutHelper

	def print_version hash

		content_tag( :li , :style => "padding-bottom: 20px;") do

			content_tag( :h4 ) do
				content_tag( :strong , :class => "text-success") do
					content_tag( :i , :class => "fa fa-angle-double-right fa-fw" ) do end + "Versión ".html_safe +
						content_tag( :span , :class => "badge badge-success" ) do "v.#{hash['id']}" end + ":".html_safe
				end
			end +

			( hash["details"].map do | detail |

				content_tag( :ul , :class => "unstyled" ) do

					content_tag( :h5 ) do
						content_tag( :strong ) do
							content_tag( :i , :class => "fa fa-caret-right fa-fw" ) do end + "#{detail[ "title" ].html_safe}:".html_safe
						end + tag( :br ) + content_tag( :span , :class => "text-muted" , :style => "font-weight: normal !important;") do "#{detail[ 'datetime' ].present? ? ' (Fecha de actualización: ' + l( detail[ "datetime" ] , :format => :dd_mm_yyyy_hh_mm_ss ) + ')' : '' }" end
					end +
					content_tag( :ul , :class => "unstyled" ) do
						( detail[ "changes" ].map do | change |
							content_tag( :li ) do
								content_tag( :i , :class => "fa fa-angle-right fa-fw" ) do end +
								if change[ 'type' ] == "Nuevo"
									content_tag( :span , :class => "label label-primary" ) do "#{change[ 'type' ].upcase}" end
								elsif change[ 'type' ] == "General"
									content_tag( :span , :class => "label" ) do "#{change[ 'type' ].upcase}" end
								elsif change[ 'type' ] == "FIX"
									content_tag( :span , :class => "label label-warning" ) do "#{change[ 'type' ].upcase}"end
								else
									content_tag( :span , :class => "label" ) do "#{change[ 'type' ].upcase}" end
								end + " #{ change[ 'detail' ] }".html_safe
							end
						end ).join.html_safe
					end

				end

			end ).join.html_safe

		end

	end

end
