module LayoutForJobsHelper

	def print_description_of_job job, data
		handler = YAML.parse(job.handler).to_ruby

		if handler.class == ActiveJob::QueueAdapters::DelayedJobAdapter::JobWrapper
			params 				= handler.job_data["arguments"]
		end

 		if data == :action
			return handler.job_data["job_class"].constantize.description
		end

		if data == :params
			return deserialize_params(params)
		end

	end

	def deserialize_params parameters
		content_parameters = []

		if parameters.class == Array

			parameters.each do |param_value|

				param_value.each do |param,value|
					unless [ "_aj_globalid" , "_aj_symbol_keys" , "_aj_hash_with_indifferent_access"].include?(param)
						content_parameters << content_tag( :div , :class => "row" ) do
							content_tag( :div , :class => "col-md-3 text-right") do
								if param == "from_date"
									"<strong>Desde</strong>".html_safe
								elsif param == "to_date"
									"<strong>Hasta</strong>".html_safe
								elsif param == "date"
									"<strong>Fecha</strong>".html_safe
								elsif param == "code_system_ref"
									"<strong>Sistema</strong>".html_safe
								elsif param == "id_current_account_ref"
									"<strong>Cta. Cte.</strong>".html_safe
								elsif param == "file_ref"
									"<strong>Archivo</strong>".html_safe
								else
									"<strong>#{param}</strong>".html_safe
								end
							end +
							content_tag( :div , :class => "col-md-9 text-left") do
								if param == "from_date" or param == "to_date" or param == "date"
									if /^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}[-|+][0-9]{2}:[0-9]{2}$/.match( value )
										l( DateTime.strptime(value , "%Y-%m-%dT%H:%M:%S%:z" ) , :format => :dd_mm_yyyy_hh_mm_ss ).html_safe
									elsif /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/.match( value )
										l( DateTime.strptime(value , "%Y-%m-%d" ) , :format => :dd_mm_yyyy_hh_mm_ss ).html_safe
									else
										value.html_safe
									end
								elsif param == "code_system_ref"
									begin
										System.find_by( :code => value ).detail.html_safe
									rescue
										value.html_safe
									end
								elsif param == "id_current_account_ref"
									begin
										CurrentAccount.find_by( :id => value ).detail.html_safe
									rescue
										value.html_safe
									end
								else
									value.to_s.html_safe
								end
							end
						end
					end
				end

			end

		end

		return content_parameters.join.html_safe

	end

	def link_to_job_log job

		handler = YAML.parse(job.handler).to_ruby

		unless handler.class == Delayed::PerformableMethod
			if handler.job_data
				job_id  = handler.job_data["job_id"]
				path_log_file = File.join( Rails.root.join('log' , 'job' ) , "#{job_id}.log" )
				if File.exists?( path_log_file ) and job.progress_current == 100
					return link_to( { :controller => "layout_for_jobs" , :action => "get_view_log" , :job_id => job_id } ) do
						content_tag( :span , :class => "h2" ) do
							content_tag( :i , :class => "fa fa-file-text-o") do end
						end
					end + " &nbsp; ".html_safe + link_to( { :controller => "layout_for_jobs" , :action => "get_raw_log" , :job_id => job_id } , :data => { :toggle => "modal" , :target => "#log_modal" }) do
						content_tag( :span , :class => "h2" ) do
							content_tag( :i , :class => "fa fa-window-maximize") do end
						end
					end
				else
					return content_tag( :span , :class => "h2 text-muted" ) do
						content_tag( :i , :class => "fa fa-file-o") do end
					end
				end
			end
		end
	end

	def print_description_of_action_job job
		return print_description_of_job job, :action
	end

	def print_description_of_params_job job
		return print_description_of_job job, :params
	end

	def print_check_and_completed_at job, show_diff_time
		handler = YAML.parse(job.handler).to_ruby
		content_tag( :div , :class => "row") do
			content_tag( :div , :class => "col-md-2" ) do
				if job.error_message.present?
					content_tag( :span , :class => "text-danger h2" ) do
						content_tag( :i , :class => "fa fa-times-circle fa-fw") do end
					end
				else
					if job.completed_at
						content_tag( :span , :class => "text-info h2" ) do
							content_tag( :i , :class => "fa fa-check fa-fw") do end
						end
					else
						if handler.job_data
							job_id  = handler.job_data["job_id"]
							path_log_file = File.join( Rails.root.join('log' , 'job' ) , "#{job_id}.log" )
						end
						if File.exists?( path_log_file ) and job.progress_current < 100
							content_tag( :span , :class => "text-warning h2" ) do
								content_tag( :i , :class => "fa fa-clock-o fa-fw") do end
							end
						else
							content_tag( :span , :class => "text-muted h2" ) do
								content_tag( :i , :class => "fa fa-clock-o fa-fw") do end
							end
						end
					end
				end
			end +
			content_tag( :div , :class => "col-md-10" ) do
				# job_id = handler.job_data["job_id"]
				if job.error_message.present?
					content_tag( :strong ) do
						"Se encontraron errores"
					end + tag( :br ) + content_tag( :span , :class => "text-muted" ) do
						job.error_message.html_safe
					end + tag( :br ) + link_to( { :controller => "layout_for_jobs" , :action => "get_retry_job" , :job_id => job.id } ) do
						content_tag( :button , :class => "btn btn-default btn-xs" ) do
							content_tag( :i , :class => "fa fa-refresh") do end + " Reintentar".html_safe
						end
					end
				else
					if handler.job_data
						job_id  = handler.job_data["job_id"]
						path_log_file = File.join( Rails.root.join('log' , 'job' ) , "#{job_id}.log" )
					end
					if File.exists?( path_log_file ) and job.progress_current < 100
						content_tag( :strong ) do
							"En progreso... (#{job.progress_current}%)"
						end + tag( :br ) + content_tag( :span , :class => "text-muted" ) do
							"Esta tarea se está ejecutando..."
						end
					else
						print_completed_at( job, show_diff_time )
					end
				end
			end
		end
	end

	def print_link_to_get_new_job_modal
		content_tag( :p ) do
			super_link_to( :new , @class_instances , :in_form , { :text => translate_action(@class_instances.to_s.underscore.to_sym,:new) + " " + @class_instances.model_name.human.downcase } , "new_modal" )
		end
	end

	def print_params_for_task name_of_job, params_selected = {}
		params_job = LayoutForJobsController::CONST_ARRAY_OF_JOBS[ name_of_job.to_sym ]
		# This indicates that we have to strict the range of dates to a single day
		( params_job.map do | param_name , param_type |
				if ( name_of_job.ends_with?( "_importer_job" ) or name_of_job.ends_with?( "_csv_downloader_job" ) ) and param_name == :code_system_ref
					systems_ref = Synchronizer.where( :table_name => name_of_job.gsub( "_importer_job" , "" ).gsub( "_csv_downloader_job" , "" ) ).map(&:system)
					print_input_param_task( param_name , param_type , systems_ref , params_selected )
				else
					print_input_param_task( param_name , param_type , [] , params_selected )
				end
			end
		).join.html_safe
	end

	def print_input_param_task param_name, param_type , param_values = [] , params_selected = {}

		content_tag( :div , :class => "row p-xxs form-horizontal" ) do

			content_tag( :div , :class => "col-md-2 control-label text-right" ) do

				if ( param_name == :task_name )
					"Tarea"
				elsif ( param_name == :from_date )
					"Desde"
				elsif ( param_name == :to_date )
					"Hasta"
				elsif ( param_name == :date )
					"Fecha"
				elsif ( param_name == :code_system_ref )
					"Sistema"
				elsif ( param_name == :id_current_account_ref )
					"Cta. Cte."
				elsif ( param_name == :file_ref )
					"Archivo"
				else
					""
				end

			end +

			content_tag( :div , :class => "col-md-10" ) do

				if ( param_type == :select2 )

					if ( param_name == :task_name )

						content_tag( :select , :class => "select2_demo_1 form-control select2_job_name" , :name => "job[#{param_name}]" ) do
							arr_opts = []
							param_values.each_with_index do | ( value_key , value_description ) , index_of_value |
								if index_of_value == 0
									arr_opts << content_tag( :option , :value => "#{value_key}" , :selected => "selected" ) do "#{value_description}" end
								else
									arr_opts << content_tag( :option , :value => "#{value_key}" ) do "#{value_description}" end
								end
								# arr_opts << content_tag( :option , :value => "#{value_key}" , :selected => ( q_attr == attribute ? "selected" : nil ) ) do "#{value_description}" end
							end
							arr_opts.join.html_safe
						end

					elsif ( param_name == :code_system_ref )

						content_tag( :select , :class => "select2_demo_1 form-control select2_job_name" , :name => "job[#{param_name}]" ) do
							arr_opts = []
							unless param_values.present?
								param_values = System.external.map{ |s| [ s.code , s.detail ] }.to_h
							else
								param_values = param_values.map{ |s| [ s.code , s.detail ] }.to_h
							end
							param_values.each do | value_key , value_description |

								arr_opts << content_tag( :option , :value => "#{value_key}" ) do "#{value_description}" end
								# arr_opts << content_tag( :option , :value => "#{value_key}" , :selected => ( q_attr == attribute ? "selected" : nil ) ) do "#{value_description}" end
							end
							arr_opts.join.html_safe
						end

					elsif ( param_name == :id_current_account_ref )

						content_tag( :select , :class => "select2_demo_1 form-control select2_job_name" , :name => "job[#{param_name}]" ) do
							arr_opts = []
							unless param_values.present?
								if @ref_data[ param_name ].present?
									if param_name.to_s.starts_with?("id_")
										param_values = CurrentAccount.where( :id => @ref_data[ param_name ].to_i ).map{ |s| [ s.id , s.detail ] }.to_h
									end
								else
									param_values = CurrentAccount.active.map{ |s| [ s.id , s.detail ] }.to_h
								end
							else
								param_values = param_values.map{ |s| [ s.id , s.detail ] }.to_h
							end
							param_values.each do | value_key , value_description |
								arr_opts << content_tag( :option , :value => "#{value_key}" ) do "#{value_description}" end
							end
							arr_opts.join.html_safe
						end

					end

				elsif ( param_type == :date )

					if [ :from_date , :to_date , :date ].include?( param_name )
						content_tag( :div , :class => "input-group date" , :id => "input_job_#{param_name}") do
							content_tag( :span , :class => "input-group-addon" ) do
								content_tag( :i , :class => "fa fa-calendar" ) do end
							end + tag( :input , :type => "text" , :class => "form-control" , :name => "job[#{param_name}]" )
						end
					end

				elsif ( param_type == :file )

					if ( param_name == :file_ref )

						content_tag( :div , :class => "fileinput fileinput-new input-group" , :data => { :provides => "fileinput" } , :id => "input_job_#{param_name}" ) do
							content_tag( :div , :class => "form-control" , :data => { :trigger => "fileinput" } ) do
								content_tag( :i , :class => "glyphicon glyphicon-file fileinput-exists" ) do end +
									content_tag( :span , :class => "fileinput-filename" ) do end
							end +
							content_tag( :span , :class => "input-group-addon btn btn-default btn-file" ) do
								content_tag( :span ,  :class => "fileinput-new" ) do "Seleccione archivo".html_safe end +
								content_tag( :span , :class => "fileinput-exists" ) do "Cambiar archivo".html_safe end +
								tag( :input , :type => "file" , :name => "job[#{param_name}]" )
							end +
							content_tag( :a , :href => "#" , :class => "input-group-addon btn btn-default fileinput-exists" , :data => { :dismiss => "fileinput" } ) do "Borrar".html_safe end
						end

					end

				end

			end
		end

	end

end
