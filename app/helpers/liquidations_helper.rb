module LiquidationsHelper

	def print_total( amount )
		content_tag( :h2 ) do number_to_currency(amount) end
	end

	def print_number( number )
		content_tag( :h3 ) do
			content_tag( :i , :class => "fa fa-hashtag fa-fw" ) do
			end + " " + number.to_s
		end
	end

	def print_date( date_ref )
		content_tag( :h3 ) do
			l( date_ref , :format => :dd_mm_yy_hh_mm )
		end
	end

end
