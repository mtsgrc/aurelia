module MenusHelper

	def print_font_awesome_icon( faicon_name )
		content_tag( :i , :class => "fa #{faicon_name} fa-fw") do "" end
	end

	def print_detail_with_font_awesome_icon( instance )
		content_tag( :span ) do
			(
				content_tag( :i , :class => "fa #{instance.faicon} fa-fw") do "" end + " " + instance.detail + " " + print_link_menu( instance )
			).html_safe
		end
	end

	def print_link_menu( instance )
		link_to instance.get_full( :symbol ) do
			content_tag( :i , :class => "fa fa-arrow-circle-right fa-fw") do "" end
		end
	end

end
