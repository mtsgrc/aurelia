module PeopleHelper

	def print_id_person instance
		content_tag( :h3 ) do
			content_tag( :i , :class => "fa fa-hashtag fa-fw" ) do end + " #{instance.id_person}"
		end
	end

	def print_dni instance
		content_tag( :h3 ) do
			if instance.user.present?
				content_tag( :i , :class => "fa fa-id-card fa-fw" ) do end + " " + super_link_to( :pure_link , user_path( instance.user.id ) , :just_text , { :text => " #{number_with_delimiter(instance.dni, delimiter: ".")}" } )
			else
				content_tag( :i , :class => "fa fa-id-card fa-fw" ) do end + " #{number_with_delimiter(instance.dni, delimiter: ".")}"
			end
		end
	end

	def print_if_is_external_or_local_instance instance
		if instance.native?
			content_tag( :span , :class => "label label-primary" ) do "NATIVO" end
		else
			content_tag( :span , :class => "label w-150" ) do "EXTERNO" end
		end

	end

	# def print_user_ref ( user )
	# 	if user
	# 		link_to self.send( "users_path", user ) do
	# 			user.username
	# 		end
	# 	end
	# end


end
