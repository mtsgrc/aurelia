module PermissionsHelper

	def print_name_of_controller controller
		permissions_count = @role_permissions.map{ |permission| permission.action if permission.resource_class == controller.values.first[ :controller_name ].classify }.compact.count
		if permissions_count > 0
			content_tag( :span , :class => "font-bold" ) do "#{controller.first.first}".html_safe end +
			if permissions_count == 1
				content_tag( :span , :class => "text-info" ) do " (#{permissions_count} permiso)".html_safe end
			else
				content_tag( :span , :class => "text-info" ) do " (#{permissions_count} permisos)".html_safe end
			end
		else
			content_tag( :span ) do "#{controller.first.first}".html_safe end
		end
	end

	def print_company_sector_role_and_link_of_permission permission
		content_tag( :span ) do
			content_tag( :strong ) do
				"#{permission.role.sector.company.detail}"
			end
		end +
		content_tag( :i , :class => "fa fa-angle-double-right fa-fw") do end +
		content_tag( :span ) do
			"#{permission.role.sector.name}"
		end +
		content_tag( :i , :class => "fa fa-angle-right fa-fw") do end + "#{permission.role.name}".html_safe
	end

	def print_resource_class_translation permission

		model = permission.resource_class.constantize unless permission.resource_class.starts_with?('Layout')
		layout = permission.resource_class if permission.resource_class.starts_with?('Layout')

		if model.present?
			translate_controller = model.model_name.human
		elsif layout.present?
			translate_controller = t("layouts.#{obj_controller.controller_name}")
		end

		content_tag( :span ) do
			"#{translate_controller}"
		end

	end

	def print_action_translation permission
		if ( [  "show" , "view", "new" , "edit" , "delete" ].include?(permission.action) ) or ( permission.action.starts_with?( *[ 'get_' , 'new_' , 'edit_' ] ) )
			content_tag( :span ) do
				"#{ translate_action( permission.resource_class.underscore.pluralize.to_sym , permission.action ) } (Solicitar)"
			end
		elsif ( [ "create" , "update" , "destroy" ].include?(permission.action) ) or ( permission.action.starts_with?( *[ 'post_' , 'create_' , 'update_' ] ) )
			content_tag( :span ) do
				"#{ translate_action( permission.resource_class.underscore.pluralize.to_sym , permission.action.gsub(/(^post_|^create_|^update_)/, 'get_') ) } (Realizar)"
			end
		else
			content_tag( :span ) do
				"#{ translate_action( permission.resource_class.underscore.pluralize.to_sym , permission.action ) } (Solicitar)"
			end
		end

	end

	def print_action_of_controller controller_hash, action

		if action.values.first.keys.first.present?

			actions_to_show = []

			action.values.first.each do | action_parent , action_childs |

				actions_to_show << content_tag( :div , :class => "row" ) do

					content_tag( :div , :class => "col-md-3 text-right p-xxs" ) do

						if action.keys.first == 'special'
							content_tag( :span ) do "#{ translate_action( controller_hash.values.first[ :controller_name ].to_sym , 'get_' + action_parent ) }".html_safe end
						else
							content_tag( :span ) do "#{ translate_action( controller_hash.values.first[ :controller_name ].to_sym , action_parent ) }".html_safe end
						end +
						content_tag( :i , :class => "fa fa-angle-right fa-fw" ) do end

					end +

					content_tag( :div , :class => "col-md-9" ) do

						sub_actions_to_show = []

						action_childs.each do | action_ref |

							content_tag( :span ) do

								sub_actions_to_show << content_tag( :label )  do

									begin
										# permission_exists = @role_permissions.find_by( :resource_class => "#{controller_hash.values.first[ :controller_name ].classify.constantize.to_s}" , :action => "#{action_ref}")
										permission_exists = @role_permissions.map{ |permission| { :resource_class => permission.resource_class , :action => permission.action } }.include?( { :resource_class => "#{controller_hash.values.first[ :controller_name ].classify.constantize.to_s}" , :action => "#{action_ref}" } )
									rescue
										permission_exists = nil
									end

									if ( [ "show" , "view", "new" , "edit" , "delete" ].include?(action_ref) ) or ( action_ref.starts_with?( *[ 'get_' , 'new_' , 'edit_' ] ) )
										tag( :input , :type => "checkbox" , :class => "i-checks" , :name => "permission[ctrllrs][#{ controller_hash.values.first[ :controller_name ] }]#{action_ref}" , :checked => ( permission_exists.present? ? "checked" : nil ) ) +
										content_tag( :span , :class => "h5 p-w-xxs" , :style => "cursor: pointer;") do "Solicitar" end
									elsif ( [ "create" , "update" , "destroy" ].include?(action_ref) ) or ( action_ref.starts_with?( *[ 'post_' , 'create_' , 'update_' ] ) )
										tag( :input , :type => "checkbox" , :class => "i-checks" , :name => "permission[ctrllrs][#{ controller_hash.values.first[ :controller_name ] }]#{action_ref}" , :checked => ( permission_exists.present? ? "checked" : nil ) ) +
										content_tag( :span , :class => "h5 p-w-xxs" , :style => "cursor: pointer;" ) do "Realizar" end
									else
										tag( :input , :type => "checkbox" , :class => "i-checks" , :name => "permission[ctrllrs][#{ controller_hash.values.first[ :controller_name ] }]#{action_ref}" , :checked => ( permission_exists.present? ? "checked" : nil ) ) +
										content_tag( :span , :class => "h5 p-w-xxs" , :style => "cursor: pointer;" ) do "Solicitar" end
									end + content_tag( :i , :class => "fa fa-blank fa-fw" ) do end
								end

							end

						end

						content_tag( :div , :class => "p-xxs" ) do
							sub_actions_to_show.join.html_safe
						end

					end

				end

			end

			actions_to_show.join.html_safe

		end

	end

end
