module PersonChecksHelper

	def print_link_to_printeable
		params_to_pass = request.query_parameters.merge( { :_action => "legal" } )
		link_to( params_to_pass ) do content_tag( :button , :class => "btn btn-primary" ) do content_tag( :i , :class => "fa fa-print" ) do end + " Versión imprimible" end end
	end

	def input_or_output_clickeable instance
		content_tag( :span, :class => "divio" , :data => { :id => instance.id },  :style => "cursor: pointer;") do
			# if instance.input_or_output == PersonCheck.kinds[ :input ]
			if instance.input_or_output == "input"
				"Entrada".html_safe
			# elsif instance.input_or_output == PersonCheck.kinds[ :output ]
		elsif instance.input_or_output == "output"
				"Salida".html_safe
			end
		end
	end

	def render_person_checks( options = {} )

		instances = @instances.reverse
		instances_class = controller_name.classify.constantize
		instances_controller = controller_name.classify.constantize

		structure = YAML.load( """
    check_at_date:
      class: col-md-1 text-right text-capitalize
      class_header: text-right
      type: helper
      helper_function: print_dated_at
      helper_params: instance, :check_at , false, :a_dd
    check_at_time:
      class: col-md-1
      type: helper
      helper_function: print_dated_at
      helper_params: instance, :check_at , false, :hh_mm_ss
    biometric_id:
      class: col-md-2
      content: biometric.detail_pdf
    # person_idperson:
    #   class: col-md-1
    #   content: person.id_person.to_s
    # person_name:
    #   class: col-md-4
    #   content: person.full_name
    check_at_turn:
      class: col-md-1
    input_or_output_clickeable:
      class: col-md-1
      type: helper
      helper_function: input_or_output_clickeable
      helper_params: instance
    """ )

		# Clean vars
		content       = []
		content_head  = []
		content_th    = []
		content_body  = []

		title_action     = translate_action( controller_name , action_name )
		title_controller = translate_controller( controller_name )

		content << content_tag( :div , :class => "ibox float-e-margins" ) do

			# Add to content
			content_tag( :div , :class => "" ) do

				content_tag( :table , :class => "table " ) do

					content_tag( :thead  ) do

						structure.collect{ |c| c.first }.each do | attr_key |

							content_th << content_tag( :th , :style => "border-bottom: 1px solid #999; background-color: #eee;", :class => ( structure[ attr_key ]["class_header"].present? ? structure[ attr_key ]["class_header"] : "#{}" ) ) do
								if attr_key == "actions"
									"Acciones"
								elsif attr_key == "detail"
									"Descripción"
								else
									if structure[ attr_key ]["display"].present?
										"#{structure[ attr_key ]["display"]}"
									else
										"#{instances_class.human_attribute_name( attr_key )}"
									end
								end
							end

						end

						content_tag( :tr ) do
							content_th.join.html_safe
						end
					end +

					content_tag( :tbody ) do

						if instances.present?

							last_check_date = nil
							instances.each_with_index do | instance , index_row |

								if ( last_check_date != instance.check_at.to_date )
									# byebug
									if ( index_row > 0 )
										last_check_date_style= "border-top: 1px solid #ccc;"
									end
									last_check_date = instance.check_at.to_date
								else
									if ( index_row > 0 )
										last_check_date_style= ""
									end
								end

								content_body << content_tag( :tr ) do

									content_td = []
									structure.deep_symbolize_keys.each_with_index do | ( attr_key , attrs_opts ) , index |

										attr_attribute_name   = attr_key
										attr_type             = attrs_opts[ :type ]
										attr_style            = attrs_opts[ :style ]
										attr_class            = attrs_opts[ :class ]
										attr_content          = attrs_opts[ :content ]
										attr_helper_function  = attrs_opts[ :helper_function ]
										attr_helper_params    = attrs_opts[ :helper_params ]
										attr_img_size         = attrs_opts[ :img_size ]

										unless attr_attribute_name == :_footable_data_ or attr_attribute_name == :_footable_data_extra_

											content_td << content_tag( :td , :class => "#{ ( !attr_class.nil? ? attr_class : "") } #{( instance.class != Hash and instance.class.column_names.include?("status") ? ( instance.deleted? ? " nulled" : "") : "" )}" , :style => "vertical-align: middle;#{last_check_date_style}" ) do
												if attr_attribute_name == :actions
													if instance.class == Hash
														print_dropdown_menu_record( instance , [] )
													else
														print_dropdown_menu_record( instance , instance.actions )
													end
												elsif attr_attribute_name == :kind
													translate_kind( instance )
												else
													content_tag( :div  , :class => ( attr_style == "bordered" ? CONST_BORDERED_CLASSES : "" ) ) do
														if attr_type == "image_tag" && eval( "instance.#{attr_attribute_name}?" )
															image_tag eval( "instance." + attr_attribute_name + ".url(:#{attr_img_size})" )
														elsif attr_type == "currency"
															number_to_currency( eval( "instance.#{attr_attribute_name}" ) )
														elsif attr_type == "helper"
															eval( "#{attr_helper_function}(#{attr_helper_params})" )
														else
															if attr_content
																begin
																	eval( "instance.#{attr_content}" )
																rescue => e
																	if e.message.ends_with?("for nil:NilClass")
																		"".html_safe if attr_content.include?('.')
																	end
																end
															else
																eval( "instance.#{attr_attribute_name}" )
															end
														end
													end
												end
											end
										end

									end
									content_td.join.html_safe
								end

							end
							content_body.join.html_safe
						else

							# Aca hacer recursion para cada día
							content_body << content_tag( :tr ) do
								content_tag( :td , :colspan => structure.deep_symbolize_keys.count.to_s , :class => "text-center" ) do
									content_tag( :h1 , :class => "text-warning" ) do
										content_tag( :i , :class => "fa fa-exclamation-circle fa-fw ") do end
									end +
									content_tag( :h2 ) do
										"No hay registros"
									end
								end
							end
							content_body.join.html_safe

						end

					end

				end

			end

		end

		# Return
		content.join.html_safe

	end

end
