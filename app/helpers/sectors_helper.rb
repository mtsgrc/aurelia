module SectorsHelper

	def draw_tr_node( node )

		if node.status == Sector::INACTIVE
			nulled_style = 'nulled'
		else
			nulled_style = ''
		end

		content_tag( :tr ) do

			content_tag( :td , :class => "col-md-1" ) do

				content_tag( :div , :class => "row" ) do

					content_tag( :div , :class => "col-xs-6 text-center" ) do

						content_tag( :div , :class => "btn-group" ) do

							"<button data-toggle='dropdown' type='button' class='btn btn-default btn-md btn-block dropdown-toggle p-w-xs' aria-expanded='false'><i class='fa fa-gear fa-fw'></i><span class='caret'></span></button>".html_safe +
							content_tag( :ul , :class => "dropdown-menu" ) do

								content_tag( :li ) do
									super_link_to( :view , node , :in_dropdown )
								end +
								( node.can_be_modified? ? content_tag( :li ) do super_link_to( :edit , node , :in_dropdown ) end : "" ) +
								( node.status != Sector::INACTIVE ? content_tag( :li , :class => "divider" ) do "" end + content_tag( :li ) do super_link_to( :delete, node, :in_dropdown, { :text => translate_action( :sector , :delete ) } ) end : "" )

							end

						end

					end +

					content_tag( :div , :class => "col-xs-6 text-center" ) do
						print_status( node , :only_icon )
					end

				end

			end +

			content_tag(:td, :class => "col-md-2 #{nulled_style}" ) do
				content_tag(:strong) do node.company.name end

				# path_img = "#{Rails.root}/public/images/#{node.company.code}"
				# if FileTest.exist?(path_img)
				# 	image_tag("#{path_img}",options)
				# else
				# 	@image_data = "/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAUAAA/+4AJkFk\nb2JlAGTAAAAAAQMAFQQDBgoNAAAFswAADOUAABHXAAAXPv/bAIQAAgICAgIC\nAgICAgMCAgIDBAMCAgMEBQQEBAQEBQYFBQUFBQUGBgcHCAcHBgkJCgoJCQwM\nDAwMDAwMDAwMDAwMDAEDAwMFBAUJBgYJDQsJCw0PDg4ODg8PDAwMDAwPDwwM\nDAwMDA8MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8IAEQgAZQCfAwER\nAAIRAQMRAf/EAO0AAQACAgMBAAAAAAAAAAAAAAAHCAUGAwQJAQEBAAIDAQEA\nAAAAAAAAAAAAAAEFAwQGBwIQAAICAgEDAgYCAwAAAAAAAAQFAwYBAgcAERIQ\nEyAwQFBgJxQWITElEQACAQIDBQIICQsFAAAAAAABAgMRBAASBSExQVETIhQQ\nYYGRobEyUiBxQnKisiNjFVDB0WLCM0NzsyQG8NJTZHQSAAEBBQYHAQAAAAAA\nAAAAAAERADAhQQIQIEBgURJQMXGhseEiExMBAQACAgECBQMFAQEAAAAAAREA\nITFBUWFxECCBkaEwUPBAYLHB0eHx/9oADAMBAAIRAxEAAAG/wAAAAAAAAAAA\nAAAAAAAAAAABijmO+ARAS8agcptYAMeRIZY3IjA2c3UpgSwTuRcfTOELGtmW\nLggAoSTUb6ZU1wquW4Kqkuk8EfkIkjkwlGDGlnywQB5SHq2CpZGZopc8rMWx\nOoU9Myb8XJPMM5iwpM5JQPJcmAmQqwTaRVgt7ZZ6isJyEjERnWJBL8nlsD05\n+clIKr0jQdHrrP2nn0kWPDQkb8biREfNDsZg3+O4SDyyxXIkM7hMBR0G7Vnf\nVGq/QhtuWvvV0Xh+VOcjOYkKJwSOknajDow5kE5c6ZKJQ0FxcW95e0Ps057X\nOWvseHyOxS/E45HTJSTo6OsZJOYMSfDXEbWnbitALLnVj77U/AAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAA/9oACAEBAAEFAvt5JwYeoxMBkHrrbxdWv++mj1cn\n6VugG+nwElDhwz3pVHsuty1iSxbAKtN74sxlba1TKQ4yJeLY2I7Vkltitery\naNoHPeVEew14UzbukINgHWvWNXKuPlIyoscuGHwWJ/u3YKKPFkcGqLlx7tFA\n813q1ZGjeQDrWK3OtjrVkCgTM0dRWs1RC6KdXBTUQsVqXKF+OPmG5AzVQE5G\nmOZVs+Q9q/JpVl/nx+qoyLDT0vl0IXFJKi8sUNlWb1xjx9v7lX5FI9uy0rbz\nq9ysv9bXLBbBdC7NU560v4rm92fqzydrDVJ+1jvCGcKar2GGxLvS6oSqy3r3\nLuwQiPlAF+95LgIAtlX5RUjKLZCyfMOMd/OnctwTC2Gq8pQok/LsBEq6g34C\ntR3m/a2jHDZHmb1ahjM2uqjGYtZUo0MEjpekKMcMWEyOwSsKs32BO6I4yok+\nUXH1UTTOV1fsAQPHNNUnK6MKK8VCJK2G3hrr0TXi2j4x7a0lfni+jmkb1Gnh\nqq3Vq0jn6d7ftlJt+2eTv5WI/Prz6TlkQzMPDV0+zrCulLwIoin/AOKtAjlH\nFzJM1iKOT67MB2CpINgyBbHko2fbQF2h8Sope0Fg6e7ft9Ft+33iuNyrmhIH\nIR8ft2XQVSTLwXCiU/fdWYUtWoZoJf67Nqvi09qIdQRAZKFYZtYk2gytYHkA\nFUskAnnWSSOY0TICZeqwJJ0zpbAy/LKWwDv3WYIcyffv/9oACAECAAEFAvz3\nEmuc/Kznt1vN/nO2c9ab99Ph1g7Z+UR66/Q7a+WO3WsOc9Yjxj8R/9oACAED\nAAEFAvz3cCbTT5Wmmd8iKseEQ8cWCw8RFfDO2zJF8pHnXv3679Ea67Y+gGmz\nDJrJjbBLWOPqQ+WTb6Hyz9//AP/aAAgBAgIGPwLPqPIWG8uc/wD/2gAIAQMC\nBj8Cz7vIg7QMP0HXVkpDUIPmoj3e2JEu6tbQsiPOBFTKOTJTE9mFRlKWCTj/\nAP/aAAgBAQEGPwL8n5rq5jtx+uwHmwlzbP1IZfYehFaGnH4Fxpt7AbNYpDHH\ncsdhI94U2A4qNoO44QXkh6km1IUGZqc8O1nLVo/3kTCjL5PgtPdTLBCu92wV\nhhnuKfLACg+c19GI7RIp4ZpTSPOoofKCcK97OIs/7tN7NTkBjsWty45kKP2s\nLArPbTvsSOYUzHxEVGJryfN0oRV8u07TTDXdtmERjVe2KHZi0s51n6sKkPlU\nEbWJ54F/LIIbYosnUfZQNu9eMsUdxcD31UAfSIPowEmSa1r/ABGAK/RJPows\nyMqXOWtteptBHAHmMfhWtRs1oPYO8oPejPFfFhdSifr2F9GhtLhdq9kUK151\nxJfO3Rs0jMTTOcqs7EUQV48fgyMHPdIWKWkfCnvfG2I5tWkk68gr3aM5QleB\nPPEV/bPLmiDUichh2hTlXFqk0rQ93ctmQCpUjaNu7hilwmT76WdlP1gPRiSC\nzulubfY8EqsG2HgSvEYtxdO/93HknkQjNWN6V2135cNZW7u8YjRqyEE9r4gM\nWd9NPcrJcKSyoyZdjEbKoeWH0vMRG0HRSQ7SKCitw3Yrd57mntyyyFB9HLiC\n40q7R1kcpLaiUSZeRHHz4vbJ2qtqyvD4hJWo84wba8jrxilHtoeanF9ptte5\n44Zcroyho3pxyPmGILZ5evIai1tlARAd9FVaDH4RfNTULUUhLb5EXh85f9cf\ngaaZz9gLqHrV93OK+E6LpkvRlVAb25X2xm2hF5bNtcLqUtwttbT7UuJyXkk8\nYH6TgafJci5YxLL1AuX2iRSlTyxZN95N/UOJV+4i9WNIbnG/9RsJJEoe9vGM\ndorbhT2nPxYlYXBn6NDPdXDfZx5twAHqAxDfzX6XPVnWDpKhWhKs1a1/VxrP\nijh9b+DWh/25frY0avG5VfPswP8AJ9HrFLEwe+WP5LD+KP2vPzwLgUS7gol7\nB7rcx4j4Z4um34dcuZNNuPklDtyV5ru9OIrPW7OS96C5Y72Fh1CBuzq2w/HX\nFlo9tpssCXnU/uZnFQUQuBlFd9OeL6SVSIb4RzWsnBlyBT5iMadpd3ZXb6hb\nIltFHbIriWnZSnaG04u9U7xaNcRR7NIhZ2eOKIVIDlAjkbSaH5tcWDfeT/1W\nxb3ZB6F9ar034ZoyQw9WItKvNOkuu6lu7zRuB2WJajA8icaHq0anu0BkS4/U\nM4Qofo082L6z1OGV7e5cSxTQgMwYChBBI2Yhs9OtpIdNs36zySe27+wCQK5Q\nK8+ONcj4mGFvMzfp8Gq2y2srz3F07W8KoSzhjVSoG+uNKtmtZUnt7pGuIWQh\nkCmrFgd1MSvePHHbUpKZaZKHga4v1/xfPBDJIhicH7Nso25hICx2kgCo547x\nd3kksg9nbQL80DYMausk7fiOmWk321e2V6bFHrzGPw+8ihu7JiBcxSpmGw+P\nl4sPPGbq1jU9pIZWKivz1c4t9RsrSSa6i7VvdTyMxHjC7F9GGg1aFLu3ibsv\ntDRsfcYbR5MWWppJdNLBIlxaJJLUZlOZDRVB2HnjrfihngtizRWeQh+VGfds\nr5cDTNPbo26OzdMs0hBbft24NjqqLd25OYKQ4KtzVhQg470ZbxoK+w01B8Xs\nBsNassc+n9PoyQybVyAUo2b8+Ga371Dx7tFMcvkzhj6cT6J3FILS7yNchWfr\nOUOZSz1z78T3WhRtHLNH05qyu/ZrX2XPg0hf5H1TjV1/n/VGNLYE9yrIHA3d\nTZSvkrTy+G7W3qRcWN3HcJzj6LE+alcadEFAElCwpv7RwxUBRnXdiCQGsjwx\npAOJdl2YupP+wo9GIbq6HVmlVWWp2KvyfRjV4onySZJFRuRLDDC407PHWpmH\n+4VxeXEa7YkbMrbwaVx3i5+0jUlIIzu8ZxdWmY90hcmVeeUkKMIsK5VELS5P\niRq+rE97LSSZ5SpJ4bB+nFvFb7OsuaVB4619VfBow/8AP9VsayP/AEfVXF3p\n77DMn2L+7INqnz4ktJomjuYn6bwkbc3LCzX9dLtDt7Y+1b4k4eXF1ZW0JU3s\nD29xeE1mKyDKe0d3kGLe5tbju13bewx3HiMT2l/fde4mIZJggASm4U2Yhm1C\n772bUUtIRXInj24urCO5SktwJYpCDsUcDiOP/jUL5sandi5VGva9BgtSlTXb\nXfhoX1OBYnGVpFj7dMXOn28lZLhWzTvxYinDEFozB2irmYcaknGoTSSLJ3yT\nMoHyRVj+fFvqFUaBYmimibfubd58SNpWorFBLvilWtPXiS6uJjeX0/7y4bZ5\nFHg03/KI7mBdPtlQzxNm6uaMEUApQ1rzxqX+USXMDafcq5giXN1c0gAoRSgp\nTn4BMYkMy+zLlGYeX8v/AP/aAAgBAQMBPyH9v1Jyll9i1+mTU5TgItAeT5LO\nOZoQYO4NfXW8EAgChwmESXtzw00B7vtiYpee3C9h8i/LzZB8L0HlfBvAgVTx\nakvuxNQaQ0vTcdmJDLiPcRL9eMUhl/5OsbqUZR06noovWDQjAdIaCnbgmkoC\nh3oXGEHS50Cnpxxb3hMa9WCedYnAu6/bEeGTW/dwESYwEiw8/wCOvUdJ5zzo\n/wDDOo0SIPw51wBU53gODGKckXTh49T5UtAO8LNPqfjrI5RZAcIlR3JON84S\nuJQK2MDT5wiK4hJG6UiseOMj0017hY0Pjd49EKE/OEfEcHdoCquO8O7ZuIrs\nv4wE/Og4Ej7smqUkk0BCoHrNmAaBvcHB7rjodNDC7CDSOFGWtvgb02/VyRCd\nH/GJw95RI2k4jIFJxv1xVrIhBghWTjfbm07Oj1ir9TtN9fIP4LKJO/fT49kD\ntJqd7Fy2SYqIUVAx5qW7C9U3lNO3GbIw+ov4Mdwd/wCXw1iPUFAeg7LNeU6z\nb6xJsQDLGfZZSSbyw8d68ZB2/wCb8DenH+Zgc4Tjzq/znG9NCjRevH3O2a7Q\nbw69rp9Tr4qyUgaGz1dH+Dj9UCnTCBDjRexdshxJcLGHhzK0NOg75WP0843y\nbN9m/EInPblPQCJMuX7O3RM9asyS1H/FnF/XEFO/7Oz2InXWttrwrYgk6bT5\njvNxGXcyXQEjput6jQspoVkUQ7cpxhs1CPUI/Al6/n4GUDiYF6/n4GEDm4Fc\nNccJ4LeJ3mxjOYgsv6MA7Lh1FV/jF9Ezu3pzyBaEvOh5clnGYTB4MRw7HTgL\nmUQ4h4jzkCSXtnYtvn6ckpkodcxsztHu5FWz9LbEA5TzcTxandCr3R9mT2ct\nytq+esk7osggTsuR/GG1py59AInvgFJkeuLd1hdQNoUHfDcK1UwhYFaDzOYT\nA0y31A7E2cz4T8rPfws/o1cmV77ke/PfibAUKbO9x+jNAWsItl82ZwsRicvp\nk1HPwgg/OA27Px3/AHHJUmEBIE8MksPGEDk44/8AwrPfJ4gjkcN/4cnToLN3\n77P4ZGqNnvo+t/GAYUjojx9c98GCgTXjD4cHAkcft8CXk5reDmi3QX/ysb9K\nY7aCiQzTOJ6DeXun1z7ODJpM6xCaPAHpgGk6VC9HEfRwdaKTTAbGt6OcbHsk\neH2Ota+2XgXRASB3rDDaDprjM5pbasSNOmsVpxWJadcH0ciwN6wicA8YUtN2\nC733xdZP3RA3IrspOZDoiOrxiODvtV0FPOs1LaNAeDwa/wDhr4aV44AsyCTY\nm9cXavHAVuAWbV1rmYHtIiB6Tf3/AP/aAAgBAgMBPyH+/Yg7/TDZxeTHauP5\ngfMG9+nevHxaLPD/AEOoxQzOc1g0P6Kfv/8A/9oACAEDAwE/If791P8A00vk\n5P04RVejNMK3t7IjCEe1dZG0P8+7y/XGjorq0ieP+z5l2kIvX0PX8fpg9n9u\n/wAy/T4jD3Z7x/8AP6EQ9c+p3gVtt3P4jPfv6ffEJ5CeQ3x/H+iANtfv/wD/\n2gAMAwEAAhEDEQAAEAAAAAAAAAAAAAAAAAAAAAAABAAABAAJIBIJAIBAAAJB\nJIJJIIAIBBIIAABJAIIIYIAAJPRAIBhJIIJMSMlc8v8AJCTxb78PqZdAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAD//aAAgBAQMBPxD9vTX8ImeEX0C4R0zh\nQAL2mz8fJZf84NtEgMIjGB2QICgoicjhIaIF2ndMQSqJUZs7ck12KQaE6t18\nsR08jOFtk0CujN/jksehr3CeHHSzEYUVNEqB65tIhvo2ICgqClTDSgNyLtKe\nOLL6YJtYv1BnLQIDA4EZXKA5F095UrDp4oOb1vHrKZgOzdPWJ4LQIFIuwAqo\nFUxQ6ybvP5WMLLoFavgAvf1TGH7Z6wXKKI3kuTDU8Qqagg83XBIMTbWwrIYI\nlXTkWgu2fBK6HUVIvZ8ia01PTRRhbzJUGQ8UpQK552BeCuU8XciyCm19M2m5\nqOrEoHQq0Othw5NjoS9Mn/cBS2lrUSkgsyvpIDRw9PVi/La1gMITWGq2JwaH\nEHK3jr4IWMCUUhTrIiCo4CoNq0QeXFYUO6tHPQO5Hcw7iU3RFcAhPLZvnKCw\nQaM9UIaDlEi7nSGOF0cGo5r7WEdE/g7KeTH3twyfC1EhdHkp8jo4cBYaoDe0\nnxt+t0H+JgXSUNRsAOqXsKO1UYEMmCBFBTa3vN4WW+zwnZknvhsi0X2L/WbT\n2YQbkkErd3LCQCpuVWI6gJuN4PP1ABCYBRNt6wTNg/i9Ph4cf7YNlEULVb2E\nrhnC2RBNNi0SNg3I11ErFTZuKfrlVvxFEdCbjizpIoERw0DgAtNSiLAtDAxK\nCaQF8u0RTTdUmrQEM4I+G8tRkZ5BIbSh7o0pqY96MVipYeIGMm2a+wYe5PWx\nxYbe+3OdmwFYFEJDWEbYIH+rUMWkU68gXdDTOJeDXQKRWGpys6Q80epXImOP\nw/wHO7VM+G0QU6+gMot34c2iCnX0BlEmvJi8e24GykXZ2kcJpvFPwyktPbQh\nP73X1LN8Fj/qnCYoRssuzHBJtJXV5uYxNsHBVwPksnYoH+uVZtlrgUKKFR4Y\n8+3BoVkBeAhGsfbs17tsEFEh2MdQfnqUShCjY4ymDfeJhjRydYKHIvQMoIiY\no1JgaI2VjsluGr9XBTccMPeAk3s827xa6BzzAdVjHOpi2ngC9aKQk0AkySEw\n4bFIHNnJdvw3R6Pvjuj0fbGriqESrGkGzieXxB7FgklycFj0+FyUsmCpRDYQ\nrgHkSY1NwGc3FyxVtqbk6MuPRt1BDBBUMI0aOCb7e5iGaU2NGyJybOTIhYer\nE7OIKAE3jfnmd0Aom6H4cOseYypwSrJ4j7L9DdNITeyX08gccB0aoENyK98U\nWdMcpDdvDqGDtqpkyEaBNerfglTXBklTXBk8r4W9lm4LTl9mcfEojkRVXic6\nluOhFBb9xEriqOQse/c5SGecKEFeX+yfENGlGpoaiZcRUImFDLqm3vl5jukO\nhlUBACDbIReG8G7dTBRjzrjBOBghBKHXGF/stNc7lK4PFw9/ih42CIvC+Exo\nBKZHBDEEFnlVcDhcJKiG+TGiNI8hKobNayEdJgk/iQKg82YQOK1kWCgmiHNO\nGRKNbm4TUPYA+AXzNQ2UtQPcqYr5mpaKWoHqRfg8yeViaaJt4f3/AP/aAAgB\nAgMBPxD+/f4APf8ATApD1y5aEnj1tK+OvOUiX+ceMEF3t98MfmAcA0P0wz3v\nv/P95MmGnYH2j8D5pjj8H5SV9/56xwjZm71fn7f9xmXIi97wcuLl+FzWXHFy\n5cX5Ytm/3/8A/9oACAEDAwE/EP79CtgK2Dg0+4a7n6YXjsCr9DBaNwVBVCTv\nBjCoGWPvOq+4v1DgzOgExdKhocRAh8yo5ArHbD8n6v0yhmkeeen8Hs+PIiqf\nExPcX6v6HvXaeXQ+px4Y9YARCOk8/wA4yj9DdPf/ALPZhXUNCBY0a+6sd/JP\n1koVdVn24/f/AP/Z\n"
				# 	# image_tag("#data:image/jpg;base64,")
				# 	@image_data_2 = Base64.b64encode(@image_data)
				#
				# 	# send_data Base64.decode64(@image_data), :type => 'image/jpeg', :disposition => 'inline'
				# end
				
			end +

			content_tag(:td, :class => "col-md-9 text-left h5 #{nulled_style}") do
				node.fullname( ' <i class=\'fa fa-angle-right\'></i> ' )
			end

		end

	end

  def draw_tree(node)

		str_to_return = ""

    node.children.each do |child|
			# if child.status == node.status
				str_to_return << draw_tr_node( child )

				if child.children.any?
					str_to_return << draw_tree(child)
				end
			# end
    end
		return str_to_return.html_safe
  end

end
