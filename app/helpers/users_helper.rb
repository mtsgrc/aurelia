module UsersHelper

	# For show ONLINE / OFFLINE dotted
	def print_full_status_session ( user , show_only_dot = false )
		return ( '<span class="color-online"><i class="fa fa-circle fa-fw"></i> Online</span>' ).html_safe if user.online
		return ( '<span class="color-offline"><i class="fa fa-circle fa-fw"></i> Offline</span>' ).html_safe if !user.online
	end

	# For show ONLINE / OFFLINE dotted
	def print_dot_status_session ( user )
		return ( '<i class="fa fa-circle color-online fa-fw"></i>' ).html_safe if user.online
		return ('<i class="fa fa-circle color-offline fa-fw"></i>').html_safe if !user.online
	end

	def print_person ( user )
		if user.person_id?
			user.person.full_name
		end
	end

	def print_role ( user )
		if user.person_id?
			user.person.role.detail
		end
	end

	def print_sector ( user )
		if user.person_id?
			user.person.role.sector.detail
		end
	end


end
