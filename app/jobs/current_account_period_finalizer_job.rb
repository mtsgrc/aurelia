class CurrentAccountPeriodFinalizerJob < ActiveJob::Base

	queue_as :default
	CONST_MAX_DAYS_RANGE = 3
	CONST_PATH_LOGS	= File.join( Rails.root.join( 'log' , 'job' ) )

	def self.description
		"[ Cuentas Corrientes ] Finalizar período actual"
	end

	def perform( params )

		@LOGGER = Logger.new( File.join( CONST_PATH_LOGS , "#{self.job_id}.log" ) )

		if ( params[:from_date] and params[ :id_current_account_ref ] )

			from_date 							= params[ :from_date ]
			id_current_account_ref 	= params[ :id_current_account_ref ]

			begin

				date_from = Date.strptime( from_date  , "%d/%m/%Y" )

				datetime_from	= DateTime.new( date_from.year , date_from.month , date_from.day , 00 , 00 , 00 , Time.zone.formatted_offset )

				arr_data = {}

				# @job.message = "Buscando cierres de lote"
				# @job.save
	      if datetime_from.present? and datetime_to.present?
	        instances = LotClosure.between_dates( datetime_from , datetime_to )
	      elsif datetime_from.present? and !datetime_to.present?
	        instances = LotClosure.between_dates( datetime_from , datetime_from )
	      end

				arr_data[ :total ] 								= instances.count

				arr_data[ :affected_before ] 			= instances.select{ | instance | instance.is_movement_created? }
				arr_data[ :not_affected_before ] 	= instances.select{ | instance | !instance.is_movement_created? }

				# Foreach
				# @job.message = "Generando movimientos"
	      instances.each_with_index do | instance , index |
					instance.generate_movement
					# @job.progress_current = ( ( index.to_f / instances.count.to_f ) * 100.00 ).to_i
					# @job.save
				end

				arr_data[ :affected_after ] 		= instances.select{ | instance | instance.is_movement_created? }
				arr_data[ :not_affected_after ] = instances.select{ | instance | !instance.is_movement_created? }

				arr_data[ :new_affected ] 			= arr_data[ :affected_after ] - arr_data[ :affected_before ]


				@LOGGER.info( "*******************************************************************************" )
				@LOGGER.info( "" )
				@LOGGER.info( "  Los resultados del proceso de afectación son los siguientes " )
				@LOGGER.info( " ------------------------------------------------------------------------------ " )
				@LOGGER.info( "" )
				@LOGGER.info( "       Fecha desde (Param): #{from_date}" )
				@LOGGER.info( "       Cantidad de cierres de lote: #{arr_data[ :total ].to_s}" )
				@LOGGER.info( "" )
				@LOGGER.info( " ------------------------------------------------------------------------ " )
				@LOGGER.info( "                ANTES DEL PROCESO DE AFECTACIÓN" )
				@LOGGER.info( "" )
				@LOGGER.info( "      Cierres de lote ya afectados: #{arr_data[ :affected_before ].count}" )
				@LOGGER.info( "       Cierres de lote sin afectar: #{arr_data[ :not_affected_before ].count}" )
				@LOGGER.info( "" )
				@LOGGER.info( " ------------------------------------------------------------------------ " )
				@LOGGER.info( "                DESPUÉS DEL PROCESO DE AFECTACIÓN" )
				@LOGGER.info( "" )
				@LOGGER.info( "         Cierres de lote afectados: #{arr_data[ :affected_after ].count} ( #{ ( arr_data[ :affected_after ].count - arr_data[ :affected_before ].count ) } nuevas)" )
				@LOGGER.info( "       Cierres de lote sin afectar: #{arr_data[ :not_affected_after ].count}" )
				@LOGGER.info( "" )
				if arr_data[ :not_affected_after ].present?
					@LOGGER.info( " ------------------------------------------------------------------------ " )
					@LOGGER.info( "" )
					@LOGGER.info( "                SIN AFECTAR" )
					@LOGGER.info( "" )
					arr_data[ :not_affected_after ].each do | not_affected_instance |
						@LOGGER.info( "                >>> #{ not_affected_instance.detail }" )
						@LOGGER.info( "                 <span class=\"text-danger\">#{ not_affected_instance.get_problems_to_affect }</span>" )
						@LOGGER.info( "" )
					end
				end

			rescue ArgumentError => e
				@LOGGER.info( "Excepción ocurrida:" )
				@LOGGER.info( "                    #{e.to_s}" )
			end

		else
			puts "Por favor indique almenos una fecha ( desde ) y una fecha hasta(opcional) [ Formato YYYY-MM-DD ]"
			@LOGGER.info( "Faltan argumento (feca)" )
		end

		@LOGGER.info( "" )
		@LOGGER.info( " ------------------------------------------------------------------------------ " )

	end

end
