class CurrentAccountPeriodStarterJob < ActiveJob::Base

	queue_as :default
	CONST_MAX_DAYS_RANGE = 3
	CONST_PATH_LOGS	= File.join( Rails.root.join( 'log' , 'job' ) )

	def self.description
		"[ Cuentas Corrientes ] Iniciar nuevo período"
	end

	def perform( params )

		@LOGGER = Logger.new( File.join( CONST_PATH_LOGS , "#{self.job_id}.log" ) )

		if ( params[:date] and params[:id_current_account_ref] )

			date 										= params[:date]
			id_current_account_ref 	= params[:id_current_account_ref]

			begin

				date = Date.strptime( date  , "%d/%m/%Y" )

				arr_data = {}

				current_account = CurrentAccount.find( id_current_account_ref )

				if current_account.present?

					@LOGGER.info( "*********************************************************************************" )
					@LOGGER.info( "" )
					@LOGGER.info( "  Los resultados del proceso de inicio de nuevo período de cuenta corriente son: " )
					@LOGGER.info( " -------------------------------------------------------------------------------- " )
					@LOGGER.info( "" )
					@LOGGER.info( "             Cuenta corriente: #{current_account.detail}" )
					@LOGGER.info( "       Fecha de nuevo período: #{date}" )
					@LOGGER.info( "" )

					if current_account.current_period.present?
						current_account_period_actual_id = current_account.current_period.id
					else
						current_account_period_actual_id = nil
					end

					if current_account.start_period( date )

						current_account.reload

						if current_account_period_actual_id.present?
							current_account_last_period 	 = CurrentAccountPeriod.find( current_account_period_actual_id )
							@LOGGER.info( "" )
							@LOGGER.info( " El período anterior se cerró correctamente" )
							@LOGGER.info( "" )
							@LOGGER.info( "                Período anterior: N° #{current_account_last_period.order_number }" )
							@LOGGER.info( "                 Fecha de inicio: #{current_account_last_period.initial_date }" )
							@LOGGER.info( "               Crédito de inicio: #{current_account_last_period.initial_credit }" )
							@LOGGER.info( "                Débito de inicio: #{current_account_last_period.initial_debit }" )
							@LOGGER.info( "" )
							@LOGGER.info( "                     Fecha final: #{current_account_last_period.final_date }" )
							@LOGGER.info( "                   Crédito final: #{current_account_last_period.final_credit }" )
							@LOGGER.info( "                    Débito final: #{current_account_last_period.final_debit }" )
							@LOGGER.info( "" )
						end

						current_account_current_period = current_account.current_period

						@LOGGER.info( "" )
						@LOGGER.info( " Se inició un nuevo período:" )
						@LOGGER.info( "" )
						@LOGGER.info( "                  Período actual: N° #{current_account_current_period.order_number }" 	)
						@LOGGER.info( "                 Fecha de inicio: #{current_account_current_period.initial_date }" 		)
						@LOGGER.info( "               Crédito de inicio: #{current_account_current_period.initial_credit }" 	)
						@LOGGER.info( "                Débito de inicio: #{current_account_current_period.initial_debit }" 		)
						@LOGGER.info( "" )

					else

						@LOGGER.info( "" )
						@LOGGER.info( " El período no se cerró correctamente" )
						@LOGGER.info( "" )
						# current_account_current_period.errors.full_messages.each do | message |
						# 	@LOGGER.info( "        <span class=\"text-danger\">#{ message }</span>" )
						# end
						@LOGGER.info( "" )
						@LOGGER.info( "      Motivo:" )
						@LOGGER.info( "" )

					end

				end

			rescue ArgumentError => e
				@LOGGER.info( "Excepción ocurrida:" )
				@LOGGER.info( "                    #{e.to_s}" )
			end

		else
			puts "Por favor indique almenos una fecha ( desde )  [ Formato YYYY-MM-DD ]"
			@LOGGER.info( "Faltan argumento (fecha)" )
		end

		@LOGGER.info( "" )
		@LOGGER.info( " ------------------------------------------------------------------------------ " )

	end

end
