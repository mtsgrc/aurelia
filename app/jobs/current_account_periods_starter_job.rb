class CurrentAccountPeriodsStarterJob < ActiveJob::Base

	queue_as :default
	CONST_MAX_DAYS_RANGE = 3
	CONST_PATH_LOGS	= File.join( Rails.root.join( 'log' , 'job' ) )

	def self.description
		"[ Cuentas Corrientes ] Iniciar nuevos períodos"
	end

	def perform( params )

		@LOGGER = Logger.new( File.join( CONST_PATH_LOGS , "#{self.job_id}.log" ) )

		if ( params[:date] )

			date 										= params[:date]

			begin

				date = Date.strptime( date  , "%d/%m/%Y" )

				arr_data = {}

				current_accounts = CurrentAccount.active

				@LOGGER.info( "*************************************************************************************************" )
				@LOGGER.info( "" )
				@LOGGER.info( "  Los resultados del proceso de inicio de nuevos períodos para todas las cuentas corrientes son: " )
				@LOGGER.info( " ------------------------------------------------------------------------------------------------ " )
				@LOGGER.info( "" )
				@LOGGER.info( "       Fecha de nuevos períodos: #{date}" )
				@LOGGER.info( "" )

				if current_accounts.present?

					arr_data = {}
					arr_data[ :total ] = current_accounts.count
					arr_data[ :new_period_success ] = []
					arr_data[ :new_period_error ] 	= []

					current_accounts.each do |current_account|

						begin
							current_account_data = {}

							current_account_data[ :current_account ] = current_account
							# current_account_data[ :current_account_period_actual_id ] = current_account.current_period.id

							ret_new_period_starting = current_account.start_period( date )

							if ret_new_period_starting
								# current_account_data[ :current_account_last_period 		] = CurrentAccountPeriod.find( current_account_data[ :current_account_period_actual_id ] )
								# current_account_data[ :current_account_current_period ] = current_account.current_period
								arr_data[ :new_period_success ] << current_account_data
							else
								arr_data[ :new_period_error ] << current_account_data
							end
						rescue Exception => exc
							arr_data[ :new_period_error ] << current_account_data
							next
						end

					end

					if arr_data[ :new_period_success ].present?
						@LOGGER.info( "" )
						@LOGGER.info( "       Cuentas corrientes con nuevo período:" )
						@LOGGER.info( "" )
						arr_data[ :new_period_success ].each do |new_period_success_of_current_account|
							@LOGGER.info( "  #{ new_period_success_of_current_account[ :current_account ].detail } " )
						end
						@LOGGER.info( "" )
					end

					if arr_data[ :new_period_error ].present?
						@LOGGER.info( "" )
						@LOGGER.info( "       Cuentas corrientes sin nuevo período:" )
						@LOGGER.info( "" )
						arr_data[ :new_period_error ].each do | new_period_error_of_current_account |
							@LOGGER.info( "  #{ new_period_error_of_current_account[ :current_account ].detail } " )
							# new_period_error_of_current_account[ :current_account ].errors.full_messages.each do | message |
							# 	@LOGGER.info( "        <span class=\"text-danger\">#{ message }</span>" )
							# end
						end
						@LOGGER.info( "" )
					end

				else

					@LOGGER.info( "" )
					@LOGGER.info( " No existen cuentas corrientes activas" )
					@LOGGER.info( "" )

				end
				#
				# if current_account.present?
				#
				# 	if current_account.start_period( date )
				#
				# 		current_account_last_period 	 = CurrentAccountPeriod.find( current_account_period_actual_id )
				# 		current_account_current_period = current_account.current_period
				#
				# 		@LOGGER.info( " El período se cerró correctamente" )
				# 		@LOGGER.info( "" )
				# 		@LOGGER.info( "                Período anterior: N° #{current_account_last_period.order_number }" )
				# 		@LOGGER.info( "                 Fecha de inicio: #{current_account_last_period.initial_date }" )
				# 		@LOGGER.info( "               Crédito de inicio: #{current_account_last_period.initial_credit }" )
				# 		@LOGGER.info( "                Débito de inicio: #{current_account_last_period.initial_debit }" )
				# 		@LOGGER.info( "" )
				# 		@LOGGER.info( "                     Fecha final: #{current_account_last_period.final_date }" )
				# 		@LOGGER.info( "                   Crédito final: #{current_account_last_period.final_credit }" )
				# 		@LOGGER.info( "                    Débito final: #{current_account_last_period.final_debit }" )
				# 		@LOGGER.info( "" )
				# 		@LOGGER.info( "                  Período actual: N° #{current_account_current_period.order_number }" )
				# 		@LOGGER.info( "                 Fecha de inicio: #{current_account_current_period.initial_date }" )
				# 		@LOGGER.info( "               Crédito de inicio: #{current_account_current_period.initial_credit }" )
				# 		@LOGGER.info( "                Débito de inicio: #{current_account_current_period.initial_debit }" )
				# 		@LOGGER.info( "" )
				# end

			rescue ArgumentError => e
				@LOGGER.info( "Excepción ocurrida:" )
				@LOGGER.info( "                    #{e.to_s}" )
			end

		else
			puts "Por favor indique almenos una fecha ( desde )  [ Formato YYYY-MM-DD ]"
			@LOGGER.info( "Faltan argumento (fecha)" )
		end

		@LOGGER.info( "" )
		@LOGGER.info( " ------------------------------------------------------------------------------ " )

	end

end
