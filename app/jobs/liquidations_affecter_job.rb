class LiquidationsAffecterJob < ActiveJob::Base

	queue_as :default
	CONST_MAX_DAYS_RANGE = 3
	CONST_PATH_LOGS	= File.join( Rails.root.join( 'log' , 'job' ) )

	def self.description
		"[ Liquidaciones ] Afectar"
	end

	def perform( params )

		@LOGGER = Logger.new( File.join( CONST_PATH_LOGS , "#{self.job_id}.log" ) )

		if ( params[:date] )

			from_date = params[:date]
			if ( params[:to_date] )
				to_date = params[ :to_date ]
			else
				to_date = params[ :date ]
			end

			begin

				# date_from = Date.strptime( from_date  , "%Y-%m-%d" )
				# date_to 	= Date.strptime( to_date 		, "%Y-%m-%d" )
				date_from = Date.strptime( from_date  , "%d/%m/%Y" )
				date_to 	= Date.strptime( to_date 		, "%d/%m/%Y" )

				if date_from <= date_to
					if (date_from - date_to).to_i <= 3

						datetime_from	= DateTime.new( date_from.year , date_from.month , date_from.day , 00 , 00 , 00 , Time.zone.formatted_offset )
						datetime_to  	= DateTime.new( date_to.year   , date_to.month   , date_to.day   , 23 , 59 , 59 , Time.zone.formatted_offset )

						arr_data = {}

						# @job.message = "Buscando liquidaciones"
						# @job.save
			      if datetime_from.present? and datetime_to.present?
			        instances = Liquidation.between_dates( datetime_from , datetime_to )
			      elsif datetime_from.present? and !datetime_to.present?
			        instances = Liquidation.between_dates( datetime_from , datetime_from )
			      end

						arr_data[ :total ] 								= instances.count

						arr_data[ :affected_before ] 			= instances.select{ | instance | instance.is_movement_created? }
						arr_data[ :not_affected_before ] 	= instances.select{ | instance | !instance.is_movement_created? }

						# Foreach
						# @job.message = "Generando movimientos"
			      instances.each_with_index do | instance , index |
							instance.generate_movement
							# @job.progress_current = ( ( index.to_f / instances.count.to_f ) * 100.00 ).to_i
							# @job.save
						end

						arr_data[ :affected_after ] 		= instances.select{ | instance | instance.is_movement_created? }
						arr_data[ :not_affected_after ] = instances.select{ | instance | !instance.is_movement_created? }

						arr_data[ :new_affected ] 			= arr_data[ :affected_after ] - arr_data[ :affected_before ]


						@LOGGER.info( "*******************************************************************************" )
						@LOGGER.info( "" )
						@LOGGER.info( "  Los resultados del proceso de afectación son los siguientes " )
						@LOGGER.info( " ------------------------------------------------------------------------------ " )
						@LOGGER.info( "" )
						@LOGGER.info( "       Fecha desde (Param): #{from_date}" )
						@LOGGER.info( "       Fecha hasta (Param): #{to_date}" )
						@LOGGER.info( "       Cantidad de liquidaciones: #{arr_data[ :total ].to_s}" )
						@LOGGER.info( "" )
						@LOGGER.info( " ------------------------------------------------------------------------ " )
						@LOGGER.info( "                ANTES DEL PROCESO DE AFECTACIÓN" )
						@LOGGER.info( "" )
						@LOGGER.info( "      Liquidaciones ya afectadas: #{arr_data[ :affected_before ].count}" )
						@LOGGER.info( "       Liquidaciones sin afectar: #{arr_data[ :not_affected_before ].count}" )
						@LOGGER.info( "" )
						@LOGGER.info( " ------------------------------------------------------------------------ " )
						@LOGGER.info( "                DESPUÉS DEL PROCESO DE AFECTACIÓN" )
						@LOGGER.info( "" )
						@LOGGER.info( "         Liquidaciones afectadas: #{arr_data[ :affected_after ].count} ( #{ ( arr_data[ :affected_after ].count - arr_data[ :affected_before ].count ) } nuevas)" )
						@LOGGER.info( "       Liquidaciones sin afectar: #{arr_data[ :not_affected_after ].count}" )
						@LOGGER.info( "" )
						if arr_data[ :not_affected_after ].present?
							@LOGGER.info( " ------------------------------------------------------------------------ " )
							@LOGGER.info( "" )
							@LOGGER.info( "                SIN AFECTAR" )
							@LOGGER.info( "" )
							arr_data[ :not_affected_after ].each do | not_affected_instance |
								@LOGGER.info( "                >>> #{ not_affected_instance.detail }" )
								@LOGGER.info( "                 <span class=\"text-danger\">#{ not_affected_instance.get_problems_to_affect }</span>" )
								@LOGGER.info( "" )
							end
						end

					else
						puts "El rango de fechas no puede superar una diferencia de #{CONST_MAX_DAYS_RANGE} días"
					end
				else
					puts "[Fecha desde] debe ser menor o igual a la [fecha hasta]"
				end

			rescue ArgumentError => e
				@LOGGER.info( "Excepción ocurrida:" )
				@LOGGER.info( "                    #{e.to_s}" )
			end

		else
			puts "Por favor indique almenos una fecha ( desde ) y una fecha hasta(opcional) [ Formato YYYY-MM-DD ]"
			@LOGGER.info( "Faltan argumento (date)" )
		end

		@LOGGER.info( "" )
		@LOGGER.info( " ------------------------------------------------------------------------------ " )

	end

end
