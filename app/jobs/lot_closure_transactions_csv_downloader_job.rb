class LotClosureTransactionsCsvDownloaderJob < ActiveJob::Base

	queue_as :default
	CONST_PATH_LOGS	= File.join( Rails.root.join( 'log' , 'job' ) )

	def self.description
		"[ Transacciones de Cierres de Lote ] Descargar y convertir a CSV"
	end

	def perform( params )

		@LOGGER = Logger.new( File.join( CONST_PATH_LOGS , "#{self.job_id}.log" ) )

		code_system_ref 				= params[ :code_system_ref ]
		table_ref 							= "lot_closure_transaction"

		synchronizer 						= table_ref.camelize.constantize.get_synchronizer_from( code_system_ref )

		if synchronizer

			system_ref 							= System.find_by( :code => code_system_ref )
			params_to_synchronizer 	= params.except( :code_system_ref ).except( :table_ref )

			from_date 							= params_to_synchronizer[ :from_date ]
			to_date 								= params_to_synchronizer[ :to_date ]

			begin

				date_from = Date.strptime( from_date  , "%d/%m/%Y" )
				date_to 	= Date.strptime( to_date 		, "%d/%m/%Y" )

				if date_from <= date_to

					if (date_from - date_to).to_i <= 31

						date_from	= Date.new( date_from.year , date_from.month , date_from.day )
						date_to  	= Date.new( date_to.year   , date_to.month   , date_to.day   )

						params_to_job = { :from_date => date_from , :to_date => date_to }

						ret_sync 								= synchronizer.get_external_data_and_convert_to_csv( params_to_job , nil, self.job_id )

						ret_sync_path_log 			= ret_sync[ :file_path  ]
						ret_sync_ret_method 		= ret_sync[ :ret_method ]
						ret_csv_file 						= ret_sync[ :csv_file ]

						arr_instances = ret_sync_ret_method

						@LOGGER.info( "*******************************************************************************" )
						@LOGGER.info( "" )
						@LOGGER.info( " Los resultados del proceso de exportación a CSV son los siguientes " )
						@LOGGER.info( " ----------------------------------------------------------------------------- " )
						@LOGGER.info( "" )
						@LOGGER.info( "                              Sistema: #{ system_ref.detail }" )
						@LOGGER.info( "                           Parámetros: #{ params_to_synchronizer.to_s }" )
						@LOGGER.info( "                      Path LOG nativo: #{ ret_sync_path_log }" )
						@LOGGER.info( "" )
						@LOGGER.info( " ----------------------------------------------------------------------------- " )
						@LOGGER.info( "" )
						@LOGGER.info( "                          Fichero CSV: <a href='/exports/#{ret_csv_file}'>#{ ret_csv_file }</a>" )
						@LOGGER.info( "" )
						@LOGGER.info( "*******************************************************************************" )
					else
						puts "El rango de fechas no puede superar una diferencia de #{31} días"
					end

				else
					puts "[Fecha desde] debe ser menor o igual a la [fecha hasta]"
				end

			rescue ArgumentError => e
				puts "Por favor indique una fecha desde y una fecha hasta [ Formato YYYY-MM-DD ]"
				@LOGGER.info( "Excepción ocurrida:" )
				@LOGGER.info( "                    #{e.to_s}" )
			end

			@LOGGER.info( "" )
			@LOGGER.info( " ------------------------------------------------------------------------------ " )

		else

			puts "No existe sincronizador, por favor revise los parámetros pasados a la tarea"
			puts "	Parámetros: #{params.to_s}"
			@LOGGER.info( "*******************************************************************************" )
			@LOGGER.info( "" )
			@LOGGER.info( "	 No existe sincronizador, por favor revise los parámetros pasados a la tarea  " )
			@LOGGER.info( "" )
			@LOGGER.info( "          Parámetros: #{params.to_s}" )
			@LOGGER.info( "" )
			@LOGGER.info( "*******************************************************************************" )

		end

	end

end
