class LotClosureTransactionsImporterJob < ActiveJob::Base

	queue_as :default
	CONST_PATH_LOGS	= File.join( Rails.root.join( 'log' , 'job' ) )

	def self.description
		"[ Transacciones de Cierres de Lote ] Descargar"
	end

	def perform( params )

		@LOGGER = Logger.new( File.join( CONST_PATH_LOGS , "#{self.job_id}.log" ) )

		code_system_ref 				= params[ :code_system_ref ]
		table_ref 							= "lot_closure_transaction"

		synchronizer 						= table_ref.camelize.constantize.get_synchronizer_from( code_system_ref )

		if synchronizer

			system_ref 							= System.find_by( :code => code_system_ref )
			params_to_synchronizer 	= params.except( :code_system_ref ).except( :table_ref )

			begin

				params_to_job = { }

				ret_sync 								= synchronizer.get_external_data_and_sync( params_to_job , nil, self.job_id )

				ret_sync_path_log 			= ret_sync[ :file_path  ]
				ret_sync_ret_method 		= ret_sync[ :ret_method ]

				arr_instances = ret_sync_ret_method

				@LOGGER.info( "*******************************************************************************" )
				@LOGGER.info( "" )
				@LOGGER.info( " Los resultados del proceso de importación son los siguientes " )
				@LOGGER.info( " ----------------------------------------------------------------------------- " )
				@LOGGER.info( "" )
				@LOGGER.info( "                              Sistema: #{ system_ref.detail }" )
				@LOGGER.info( "                           Parámetros: #{ params_to_synchronizer.to_s }" )
				@LOGGER.info( "                      Path LOG nativo: #{ ret_sync_path_log }" )
				@LOGGER.info( "" )
				@LOGGER.info( " ----------------------------------------------------------------------------- " )
				@LOGGER.info( "" )
				@LOGGER.info( "       Cantidad de registros externos: #{ ret_sync_ret_method.map{ |k,v| v.count }.sum } registros" )
				@LOGGER.info( "                     Registros nuevos: #{ ret_sync_ret_method[ :news ].count  } registros" )
				@LOGGER.info( "              Registros ya importados: #{ ret_sync_ret_method[ :equal ].count } registros" )
				@LOGGER.info( "                Registros modificados: #{ ret_sync_ret_method[ :mods  ].count } registros" )
				@LOGGER.info( "                  Registros con error: #{ ret_sync_ret_method[ :error ].count } registros" )
				@LOGGER.info( "" )
				@LOGGER.info( " ----------------------------------------------------------------------------- " )
				@LOGGER.info( "               Registros nuevos:" )
				@LOGGER.info( "               -----------------" )
				@LOGGER.info( "" )
				ret_sync_ret_method[ :news ].each_with_index do | instance , index_instance |
					@LOGGER.info( "    #{ index_instance }) #{ instance.detail }" )
				end
				@LOGGER.info( "" )
				@LOGGER.info( "               Registros idénticos:" )
				@LOGGER.info( "               --------------------" )
				@LOGGER.info( "" )
				ret_sync_ret_method[ :equal ].each_with_index do | instance , index_instance |
					@LOGGER.info( "    #{ index_instance }) #{ instance.detail }" )
				end
				@LOGGER.info( "" )
				@LOGGER.info( "               Registros modificados:" )
				@LOGGER.info( "               ----------------------" )
				@LOGGER.info( "" )
				ret_sync_ret_method[ :mods ].each_with_index do | instance , index_instance |
					@LOGGER.info( "    #{ index_instance }) #{ instance.detail }" )
				end
				@LOGGER.info( "" )
				@LOGGER.info( "               Registros con error:" )
				@LOGGER.info( "               --------------------" )
				@LOGGER.info( "" )
				ret_sync_ret_method[ :error ].each_with_index do | instance , index_instance |
					@LOGGER.info( "    #{ index_instance }) #{ instance.to_json } > ERRORS: #{ instance.errors.messages.to_json } " )
				end
				@LOGGER.info( "" )
				@LOGGER.info( "" )
				@LOGGER.info( "*******************************************************************************" )

			rescue ArgumentError => e
				puts "Por favor indique una fecha desde y una fecha hasta [ Formato YYYY-MM-DD ]"
				@LOGGER.info( "Excepción ocurrida:" )
				@LOGGER.info( "                    #{e.to_s}" )
			end

			@LOGGER.info( "" )
			@LOGGER.info( " ------------------------------------------------------------------------------ " )

		else
			puts "No existe sincronizador, por favor revise los parámetros pasados a la tarea"
			puts "	Parámetros: #{params.to_s}"
			@LOGGER.info( "*******************************************************************************" )
			@LOGGER.info( "" )
			@LOGGER.info( "	 No existe sincronizador, por favor revise los parámetros pasados a la tarea  " )
			@LOGGER.info( "" )
			@LOGGER.info( "          Parámetros: #{params.to_s}" )
			@LOGGER.info( "" )
			@LOGGER.info( "*******************************************************************************" )

		end

	end

end
