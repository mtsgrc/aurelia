class PeopleImporterJob < ActiveJob::Base
	queue_as :default
	CONST_PATH_LOGS	= File.join( Rails.root.join( 'log' , 'job' ) )

	def self.description
		"[ Personal ] Descargar y Homologar"
	end

	def perform( params )

		@LOGGER = Logger.new( File.join( CONST_PATH_LOGS , "#{self.job_id}.log" ) )

		code_system_ref 				= 'GBB'
		table_ref 							= "person"

		synchronizer 						= table_ref.camelize.constantize.get_synchronizer_from( code_system_ref )

		if synchronizer

			system_ref 							= System.find_by( :code => code_system_ref )
			params_to_synchronizer 	= params.except( :code_system_ref ).except( :table_ref )
			params_to_job						= {}
			
			begin

				ret_sync 								= synchronizer.get_external_data_and_sync( params_to_job , nil, self.job_id )

				ret_sync_path_log 	= ret_sync[ :file_path  ]
				ret_sync_ret_method = ret_sync[ :ret_method ]

				arr_instances = ret_sync_ret_method

				@LOGGER.info( "*******************************************************************************" )
				@LOGGER.info( "" )
				@LOGGER.info( " Los resultados del proceso de importación son los siguientes " )
				@LOGGER.info( " ----------------------------------------------------------------------------- " )
				@LOGGER.info( "" )
				@LOGGER.info( "                              Sistema: #{ system_ref.detail }" )
				@LOGGER.info( "                           Parámetros: #{ params_to_synchronizer.to_s }" )
				@LOGGER.info( "                      Path LOG nativo: #{ ret_sync_path_log }" )
				@LOGGER.info( "" )
				@LOGGER.info( " ----------------------------------------------------------------------------- " )
				@LOGGER.info( "" )
				@LOGGER.info( "       Cantidad de registros externos: #{ ret_sync_ret_method.map{ |k,v| v.count }.sum } registros" )
				@LOGGER.info( "                     Registros nuevos: #{ ret_sync_ret_method[ :news ].count  } registros" )
				@LOGGER.info( "              Registros ya importados: #{ ret_sync_ret_method[ :equal ].count } registros" )
				@LOGGER.info( "                Registros modificados: #{ ret_sync_ret_method[ :mods  ].count } registros" )
				@LOGGER.info( "                  Registros con error: #{ ret_sync_ret_method[ :error ].count } registros" )
				@LOGGER.info( "" )

				if ret_sync_ret_method[ :news ].present?
					@LOGGER.info( " ----------------------------------------------------------------------------- " )
					@LOGGER.info( "               Registros nuevos:" )
					@LOGGER.info( "               -----------------" )
					@LOGGER.info( "" )
					ret_sync_ret_method[ :news ].each_with_index do | instance , index_instance |
						data_resource = table_ref.camelize.constantize.attribute_names.map{ |attr| "<strong>#{table_ref.camelize.constantize.human_attribute_name( attr.to_sym )}</strong>: #{ ( instance[ attr.to_sym].present? ? instance[ attr.to_sym] : '(VACIO)' )  }" }.join(' | ')
						@LOGGER.info( "    #{( index_instance + 1 )}) #{data_resource}" )
						@LOGGER.info( "" )
					end
					@LOGGER.info( "" )
					@LOGGER.info( " -- Proceso de homologación --" )
					@LOGGER.info( "" )
					ret_generation_or_homologation = Person.generate_or_homologate
					@LOGGER.info( "" )
				end

				if ret_sync_ret_method[ :equal ].present?
					@LOGGER.info( "               Registros idénticos:" )
					@LOGGER.info( "               --------------------" )
					@LOGGER.info( "" )
					ret_sync_ret_method[ :equal ].each_with_index do | instance , index_instance |
						@LOGGER.info( "    #{( index_instance + 1 )}) #{instance.detail}" )
					end
					@LOGGER.info( "" )
				end

				if ret_sync_ret_method[ :mods ].present?
					@LOGGER.info( "               Registros modificados:" )
					@LOGGER.info( "               ----------------------" )
					@LOGGER.info( "" )
					ret_sync_ret_method[ :mods ].each_with_index do | instance , index_instance |
						@LOGGER.info( "    #{( index_instance + 1 )}) #{instance.detail}" )
					end
					@LOGGER.info( "" )
				end

				if ret_sync_ret_method[ :error ].present?
					@LOGGER.info( "               Registros con error:" )
					@LOGGER.info( "               --------------------" )
					@LOGGER.info( "" )
					ret_sync_ret_method[ :error ].each_with_index do | instance , index_instance |
						data_resource = table_ref.camelize.constantize.attribute_names.map{ |attr| "<strong>#{table_ref.camelize.constantize.human_attribute_name( attr.to_sym )}</strong>: #{ ( instance[ attr.to_sym].present? ? instance[ attr.to_sym] : '(VACIO)' )  }" }.join(' | ')
						@LOGGER.info( "    #{( index_instance + 1 )}) #{data_resource}" )
						instance.errors.full_messages.each do | message |
							@LOGGER.info( "        <span class=\"text-danger\">#{ message }</span>" )
						end
						@LOGGER.info( "" )
					end
					@LOGGER.info( "" )
					@LOGGER.info( "" )
				end
				@LOGGER.info( "*******************************************************************************" )

			rescue ArgumentError => e
				puts "Excepción ocurrida: '#{e.to_s}'"
				@LOGGER.info( "Excepción ocurrida:" )
				@LOGGER.info( "                    #{e.to_s}" )
				e.backtrace.each do | trace_line |
					@LOGGER.info( "        <span class=\"text-danger\">#{ trace_line }</span>" )
				end
			end

			@LOGGER.info( "" )
			@LOGGER.info( " ------------------------------------------------------------------------------ " )

		else

			@LOGGER.info( "*******************************************************************************" )
			@LOGGER.info( "" )
			@LOGGER.info( "	 No existe sincronizador, por favor revise los parámetros pasados a la tarea  " )
			@LOGGER.info( "" )
			@LOGGER.info( "          Parámetros: #{params.to_s}" )
			@LOGGER.info( "" )
			@LOGGER.info( "*******************************************************************************" )

		end

	end

end
