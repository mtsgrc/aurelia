class PasswordMailer < ActionMailer::Base
	default from: "admin@aurelia"
	def sample_email(user)
    @user = user
    mail(to: @user.email, subject: 'Sample Email')
  end
end
