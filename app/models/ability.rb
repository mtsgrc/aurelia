class Ability
  include CanCan::Ability

  def initialize(user)

    if user.root?
      can :manage, :all
    else
      can do |action, resource_class|
        if user.permissions
          user.permissions.where( :action => aliases_for_action( action.to_sym ) ).any? do |permission|
            permission.resource_class == resource_class.to_s #&& (subject.nil? || permission.subject_id.nil? || permission.subject_id == subject.id)
          end
        end
      end
    end

    # if user
      #can :manage, User
      # authorize! :manage, User, :message => "Unable to read this article."
      # can :index, User
    # end

  end

  def eval_cancan_action(action)
    case action.to_s
    when "index", "show", "search"
      cancan_action = "read"
      action_desc = I18n.t :read
    when "create", "new"
      cancan_action = "create"
      action_desc = I18n.t :create
    when "edit", "update"
      cancan_action = "update"
      action_desc = I18n.t :edit
    when "delete", "destroy"
      cancan_action = "delete"
      action_desc = I18n.t :delete
    else
      cancan_action = action.to_s
      action_desc = "Other: " << cancan_action
    end
    return action_desc, cancan_action
  end

  def self.write_permission(class_name, cancan_action, force_id_1 = false)

    permission  = Permission.find(:first, :conditions => ["resource_class = ? and action = ?", class_name, cancan_action])
    if not permission
      permission = Permission.new
      permission.id = 1 if force_id_1
      permission.resource_class =  class_name
      permission.action = cancan_action
      permission.save
    else
      permission.save
    end
  end


end
