class AgencyCompany < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # SimplestStatus
    extend SimplestStatus
    statuses :active, :inactive, :deleted

    # Audited
    audited

  # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

    validates :agency_id        , :presence => true
    validates :company_id       , :presence => true
    validates :agency_admin_id  , :presence => true

    belongs_to :agency
    belongs_to :company
    belongs_to :agency_admin , :class_name => 'Agency' , :foreign_key => 'agency_admin_id'

    # has_many  :credit_terminals , :class_name => 'CreditTerminal' , :foreign_key => 'agency_id'

  # ------------------------------ FOR EXTERNAL SYSTEMS DATA ---------------------------------------------

    # Include Model Synchronizer for import data
    extend  SynchronizerMethods

    # Include Model System Methods
    include ModelSystemMethods

    # Always point to a system, if it is external system, will be distinct of 0 ( This system code 'AU+' )
    belongs_to  :system

    # If is an external resource, a synchronizer was used to import him.
    belongs_to  :synchronizer

    # If this resource is the homologation (similar to parent of many instances ) will have many children
    has_many    :children , :class_name => self , :foreign_key => 'parent_id'

    # Get parent (homologation)
    belongs_to  :parent , :class_name => self , :foreign_key => 'parent_id'

    # External module method ( stored in ModelSystemMethods ) that search for the instance of this resource referes
    validate    :parent_existance, :if => :parent_id_changed?

    # This method is for complete resource if any field can't be filled with data from external system before save
    def complete_resource_before_import
      nil
    end

  # ------------------------------ BASIC METHODS ---------------------------------------------

    ACTIONS = [ { :get_edit_credit_maximum_inmodal => "( true )" } ]

    STATUS_TRANSITIONS = {
      :opened => [ :closed ]
    }

    def actions
      super( ACTIONS )
    end

    # Global method for displaying data
    def detail
      #TODO Rollback this
      # "#{ self.system.code } - #{ self.agency.code_nosys } - #{ self.company.code_nosys }"
      "#{ self.agency.code }-#{ self.company.code }".split("-").uniq.join('-')
    end
    #
    # def child_arr_descriptions
    #   children.collect{ |child| "#{child.detail}"}
    # end
    #
    # # Delete system code of instance code, leaving only system agency code if is imported of external system
    # def code_nosys
    #   code.gsub( system.code + "-", "" )
    # end
    #
    # # ------------------------------ CURRENT ACCOUNT MODULE ---------------------------------------------
    #
    # # We must to create a current account, after create an local agency
    # after_create :create_current_account_after_create, :if => :local_system?
    # def create_current_account_after_create
    #   puts "Creando cuenta corriente para #{self.detail}..."
    #   ca = CurrentAccount.new( :kind => CurrentAccount.kinds[ :agency ] , :resource_id => self.id )
    #   if ca.save
    #     puts "CUENTA \"#{ca.detail}\" CREADA!"
    #   else
    #     puts "CUENTA \"#{ca.detail}\" NOCREADA!"
    #   end
    # end
    #
    # def current_account
    #   current_account = CurrentAccount.find_by( :kind => CurrentAccount.kinds[ :agency ] , :resource_id => self.id )
    # end
    #
    # # ------------------------------ BASIC VALIDATIONS ---------------------------------------------
    # #
    # # Code can't be modified
    # validate :code_cant_be_modified, :on => [ :update ]
    # def code_cant_be_modified
    #   self.errors.add( :base , "El código no puede ser modificado" ) if self.changed_attributes.include?(:code)
    # end
    #
    # # ------------------------------ METHODS FOR HOMOLOGATIONS  ---------------------------------------------
    #


end
