class AgencyCompanyPeriod < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # SimplestStatus
    extend SimplestStatus
    statuses :active, :inactive, :deleted

    # Audited
    audited

  # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

    belongs_to  :current_account_period, :class_name => 'CurrentAccountPeriod', :foreign_key => 'current_account_period_id'
    belongs_to  :agency_company, :class_name => 'AgencyCompany', :foreign_key => 'agency_company_id'

    validates :initial_credit , :presence => true
    validates :initial_debit  , :presence => true
    validates :status         , :presence => true

  # ------------------------------ BASIC METHODS ---------------------------------------------


    # Only for get the movements in the current period
    def movements
      self.current_account_period.movements_by( :agency_company , self.agency_company ).values.flatten
      # self.current_account_period.movements_by( :agency_company )[ self.agency_company ]
    end
    # --------------------------------------------------------------------------

    # Sum of movements, call to current_account method setting initial and final dates by period
    # def sum_of_movements kind_of_movement
    #   self.current_account_period.sum_of_movements_by( :agency_company , kind_of_movement , self.agency_company )
    #   # self.current_account_period.sum_of_movements_by( :agency_company , kind_of_movement )[ self.agency_company ]
    # end

    # --------------------------------------------------------------------------

    # Calc credits
    def calc_credits
      self.current_account_period.calc_credits_by( :agency_company , self.agency_company )[ self.agency_company ]
    end

    # --------------------------------------------------------------------------

    # Calc credits
    def calc_debits
      self.current_account_period.calc_debits_by( :agency_company , self.agency_company )[ self.agency_company ]
    end

    # # --------------------------------------------------------------------------

    # Check sum of total movements like :debit , :credit
    def calc_balance
      # self.calc_credits + self.calc_debits
      self.final_credit + self.final_debit
    end

    # # --------------------------------------------------------------------------
    #
    #
    # initial debits + initial credits
    def initial_balance
      self.initial_credit + self.initial_debit
    end

    # final debits + final credits if final date is present
    # calc debits + calc credits if final date is not present
    def final_balance
      # if self.current_account_period.final_date.present?
      #   self.final_credit + self.final_debit
      # else
      #   self.calc_credits + self.calc_debits
      # end
      self.final_credit + self.final_debit
    end

    def detail
      "Período N°#{self.current_account_period.order_number} - #{self.agency_company.detail}"
    end

    # Check sum of total movements like :debit , :credit
    # def total_balance
    #   # if self.current_account_period.final_date.present?
    #   #   self.initial_balance + self.final_balance
    #   # else
    #   #   self.initial_balance + ( self.calc_credits + self.calc_debits )
    #   # end
    #   self.final_balance - self.initial_balance
    # end

    def affect_final_amount movement_or_affectation
      if movement_or_affectation.class == CurrentAccountMovement
        movement = movement_or_affectation
        if movement.kind == :credit.to_s
          self.final_credit = self.final_credit + movement.amount
        elsif movement.kind == :debit.to_s
          self.final_debit = self.final_debit - movement.amount
        end
      elsif movement_or_affectation.class == MovementAffectation
        affectation = movement_or_affectation
        if affectation.movement_from.kind == :credit.to_s
          self.final_credit = self.final_credit + affectation.amount
        elsif affectation.movement_from.kind == :debit.to_s
          self.final_debit = self.final_debit - affectation.amount
        end
      end
      self.save
    end

    # --------------------------------------------------------------------------
    # FIXES
    def fix_final_amounts
      self.final_credit = self.initial_credit + self.calc_credits
      self.final_debit = self.initial_debit + self.calc_debits
      self.save
    end

    def final_date
      self.current_account_period.final_date
    end

    def initial_date
      self.current_account_period.initial_date
    end

    def order_number
      self.current_account_period.order_number
    end

    # def detail
    #   "Período N°#{self.order_number} - Desde #{self.initial_date.strftime('%d/%m/%Y')}#{ (self.final_date.present? ? " - Hasta #{self.final_date.strftime('%d/%m/%Y')} (Cerrado)" : " - HOY (Vigente)") }"
    # end
    #
    # def can_be_modified?
    #   return false
    # end

end
