class BankAccount < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods
    
    # SimplestStatus
    extend SimplestStatus
    statuses :active, :inactive, :deleted

    # Audited
    audited

  # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

    belongs_to :bank

    validates :code   , :presence => true , :uniqueness => true
    validates :name   , :presence => true
    validates :status , :presence => true

    validate :code_cant_be_modified, :on => [ :update ]

  # ------------------------------ FOR EXTERNAL SYSTEMS DATA ---------------------------------------------

    # Include Model Synchronizer for import data
    extend  SynchronizerMethods

    # Include Model System Methods
    include ModelSystemMethods

    # Always point to a system, if it is external system, will be distinct of 0 ( This system code 'AU+' )
    belongs_to  :system

    # If this resource is the homologation (similar to parent of many instances ) will have many children
    has_many    :children , :class_name => self , :foreign_key => 'parent_id'

    # External module method ( stored in ModelSystemMethods ) that search for the instance of this resource referes
    validate    :parent_existance, :if => :parent_id_changed?

    # This method is for complete resource if any field can't be filled with data from external system before save
    def complete_resource_before_import
      nil
    end

  # ------------------------------ BASIC METHODS ---------------------------------------------

    GLOBAL_ACTIONS = [ { :new  => "true" } ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end

    # Code cant be modified
    def code_cant_be_modified
      self.errors.add( :base , "El código no puede ser modificado" ) if self.changed_attributes.include?(:code)
    end

    # Global method for displaying data
    def detail
      "#{self.code} - #{self.name}"
    end

    # This method is only for making relationships through homologations.
    def homologation_identity
      self.code
    end

    # TODO MEJORAR ESTO!!!!
    def number_nosymbols
      str_ret = self.number.gsub('/','').gsub(' ','').gsub('-','')
      if self.name.include?( "NACION" ) and !str_ret.starts_with?("2405")
        return "2405#{str_ret}"
      else
        return "#{str_ret}"
      end
    end

    # def get_homologation
    #   bank = Bank.where( :code => "#{self.code.split('S')[0]}" )
    #   if bank.present?
    #     return bank.first
    #   end
    # end
    #
    # # Associate system_credit_terminal to agency from homologation
    # def homologate force = false
    #   if self.bank_id.nil? or force == true
    #     homologation = get_homologation
    #     if homologation.present?
    #       self.bank_id = homologation.id
    #       self.save
    #     else
    #       puts " No se encuentra homologación para #{self.detail}"
    #     end
    #   else
    #     puts " Recurso ya homologado (No se fuerza el proceso)"
    #   end
    # end

end
