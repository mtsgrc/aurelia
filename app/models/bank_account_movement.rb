class BankAccountMovement < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # SimplestStatus
    extend SimplestStatus
    statuses :active_not_affected, :inactive, :deleted, :affected_totally, :affected_partial

    STATUS_VISIBLES = [ ]
    STATUS_TRANSITIONS = {
			:active_not_affected => [ :deleted , :affected_totally , :affected_parcial ] ,
			:affected_parcial => [ :affected_totally ] ,
      :inactive => [ :deleted ]
		}
    # Audited
    audited

  # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

    belongs_to :bank_account
    belongs_to :bank_account_card

    # types of current_accounts
  	enum kind: [ :credit , :debit ]

  # ------------------------------ FOR EXTERNAL SYSTEMS DATA ---------------------------------------------

    # Include Model Synchronizer for import data
    extend  SynchronizerMethods

    # Include Model System Methods
    include ModelSystemMethods

    # Always point to a system, if it is external system, will be distinct of 0 ( This system code 'AU+' )
    belongs_to  :system

    # If is an external resource, a synchronizer was used to import him.
    belongs_to  :synchronizer

    # If this resource is the homologation (similar to parent of many instances ) will have many children
    has_many    :children , :class_name => self , :foreign_key => 'parent_id'

    # External module method ( stored in ModelSystemMethods ) that search for the instance of this resource referes
    validate    :parent_existance, :if => :parent_id_changed?

    # This method is for complete resource if any field can't be filled with data from external system before save
    def complete_resource_before_import
      nil
    end

  # ------------------------------ BASIC METHODS ---------------------------------------------

  GLOBAL_ACTIONS = [ ]

  def self.actions
    super( GLOBAL_ACTIONS )
  end


  validates     :bank_account_id , :presence => true
  def full_detail
    "#{self.date} - #{self.detail} ( $ #{ ( self.kind == :debit ? ( - self.amount ) : self.amount) } )"
  end

  # ------------------------------ OTHER METHODS ---------------------------------------------

  # Amount never can't be negative... The sign of the amount is given for the kind of the movement => [ :debit , :credit ]
  # before_create :amount_never_can_be_negative
  # def amount_never_can_be_negative
  #   if self.amount < 0
  #     self.errors.add( :base , "El importe no puede ser negativo." )
  #     return false
  #   end
  # end

  # Search bank account card with number equals to self.detail
  def search_bank_account_card_by_detail_and_assign
    bank_account_card = BankAccountCard.find_by( :number => self.detail )
    if bank_account_card.present?
      self.bank_account_card_id = bank_account_card.id
      self.save
    end
  end

  # ------------------------------ CURRENT ACCOUNT & MOVEMENTS  ---------------------------------------------

  ACTIONS = [ { :get_new_current_account_movement_from_bank_account_movement_inmodal => "( is_movement_created? == false ) and ( self.credit? )" } ,
              { :get_new_current_account_movements_from_bank_account_movement_inmodal => "( remaining_amount>0 ) and ( self.credit? )" } ,
             :divider ,
              { :get_new_bank_account_card_from_bank_account_movement_detail_inmodal => "( self.detail.present? ) and ( bank_account_card_id == nil ) and ( self.credit? )" } ,
            ]

  def actions
    super( ACTIONS )
  end

  # Card is assigned but current account movement not created yet
  def current_account_movement_not_created_yet_with_bank_account_card?
    self.bank_account_card_id && !self.is_movement_created?
  end

  # Get current account movement of this resource
  def current_account_movements
    CurrentAccountMovement.where( :resource_type => "#{self.class.model_name}" , :resource_id => self.id )
  end

  # Check if is any movment currentaccount created for this resource
  def is_movement_created?
    return current_account_movements.present?
  end

  # Get current account if movement is created yet
  def current_account
    if self.bank_account_card.present?
      if self.bank_account_card.agency.present?
        self.bank_account_card.agency.current_account
      end
    end
  end

  # Return string
  def get_problems_to_affect
    return "No existe tarjeta ó identificación asociada al movimiento" unless self.bank_account_card.present?
    return "No existe agencia asociada a la tarjeta \"#{ self.bank_account_card.detail }\"" unless self.bank_account_card.agency.present?
    return "No existe cuenta corriente de agencia asociada #{self.bank_account_card.agency.detail}" unless self.bank_account_card.agency.current_account.present?
    if self.bank_account_card.agency.current_account.current_period.present?
      resource_datetime = self.date
      unless ( ( resource_datetime.to_date >= self.bank_account_card.agency.current_account.current_period.initial_date ) and ( resource_datetime.to_date < Date.today ) )
        return "El recurso está fuera del rango de fechas del período activo de la cuenta"
      end
    else
      return "No existe período activo en la cuenta"
    end
    return "Ningun problema para homologar"
  end


  # Generate movement from Agency <> Current Account as soon is created this registry
  def generate_movement
    unless is_movement_created?
      ActiveRecord::Base.transaction do
        if self.current_account.present?
          result_mov = self.current_account.generate_movement(self)
          if result_mov
            self.status = self.class.statuses[ :affected ]
            self.save
          end
        end
      end
    end
  end

  # Affect instances that not have movements
  def self.generate_movements_from_date_to_date from_date, to_date = nil
    if from_date.present? and to_date.present?
      instances = self.between_dates( from_date , to_date )
    elsif from_date.present? and !to_date.present?
      instances = self.between_dates( from_date , from_date )
    end
    instances.each{ | instance | instance.generate_movement }
  end

  def remaining_amount
    if current_account_movements.present?
      ( self.amount - self.current_account_movements.map(&:amount).sum )
    else
      ( self.amount )
    end
  end

  # ------------------------ [END] SPECIALS ------------------------

    # Get Credit Terminal Lot Closures from date to date
    def self.between_dates from_date , to_date

      if from_date.class == Date
        date_from = from_date #.to_datetime.change( :hour => 00 , :min => 00 , :sec => 00 )
      elsif [DateTime,Time,ActiveSupport::TimeWithZone].include?( from_date.class )
        date_from = from_date.to_date
      end

      if to_date.class == Date
        date_to = to_date #.to_datetime.change( :hour => 23 , :min => 59 , :sec => 59 )
      elsif [DateTime,Time,ActiveSupport::TimeWithZone].include?( to_date.class )
        date_to = to_date.to_date
      end

      if date_from <= date_to
        return self.where( "DATE( date ) >= ? AND DATE( date ) <= ?" , date_from.to_s(:db) , date_to.to_s(:db) )
      end

    end

    # Get Credit Terminal Lot Closures from date
    def self.from_date from_date
      return self.between_dates( from_date , from_date )
    end
end
