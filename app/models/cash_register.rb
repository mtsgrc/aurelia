class CashRegister < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # SimplestStatus
    extend SimplestStatus
    statuses :opened, :closed, :deleted

    # Audited
    audited

  # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

    belongs_to :sector
    belongs_to :user

    has_many :cash_register_movements
    has_many :movements, :class_name => 'CashRegisterMovement', :foreign_key => 'cash_register_id'

    GLOBAL_ACTIONS = [ { :new  => "true" } ]

    ACTIONS = [ { :get_new_cash_register_ingress_movement_from_cash_register_inmodal => "( self.opened? )" } ,
                { :get_new_cash_register_egress_movement_from_cash_register_inmodal => "( self.opened? )" } ,
              ]
    STATUS_TRANSITIONS = {
			:opened => [ :closed ]
		}

    def self.actions
      super( GLOBAL_ACTIONS )
    end

    def actions
      super( ACTIONS )
    end

    before_create :assign_next_number_to_valid_user_cash_register
  	def assign_next_number_to_valid_user_cash_register
      last_cash_register_closed = CashRegister.where( :user_id => self.user_id , :status => CashRegister::statuses[ :closed ] ).order( :number => :asc ).last
      if last_cash_register_closed.present?
        self.number = last_cash_register_closed.number + 1
      else
        self.number = 1
      end
  	end

    before_create :check_inexistance_of_another_sector_cash_register_active
  	def check_inexistance_of_another_sector_cash_register_active
      any_sector_cash_register_active = CashRegister.find_by( :user_id => self.user_id , :status => self.status )
      if any_sector_cash_register_active.present?
        self.errors.add( :base, "Ya existe una caja abierta actualmente" )
        return false
      end
  	end

    def detail
      "Caja N° #{self.number} para #{self.user.detail}"
    end

    # Check sum of total movements by kind of movements
    def sum_of_movements kind_of_movement
      eval( "self.movements.#{kind_of_movement}.collect(&:amount).sum" )
    end

    # Calc ingress
    def calc_ingress
      if self.status == CashRegister::statuses[ :opened ]
        self.sum_of_movements( :ingress )
      else
        self.final_ingress
      end
    end

    # Calc egress
    def calc_egress
      if self.status == CashRegister::statuses[ :opened ]
        -( self.sum_of_movements( :egress ) )
      else
        self.final_egress
      end
    end

    # Check sum of total movements like :debit , :credit
    def calc_balance
      if self.status == CashRegister::statuses[ :opened ]
        self.calc_ingress + self.calc_egress
      else
        self.final_ingress + self.final_egress
      end
    end

    def can_be_modified?
      return false
    end

    # Change status of expense and generate movement of current account
		def set_closed
		  if CashRegister.statuses.key( self.status ) == :opened
		    self.final_datetime = DateTime.now
		    self.final_ingress  = self.calc_ingress
		    self.final_egress   = self.calc_egress
		    self.status = CashRegister.statuses[ :closed ]
		    self.save
		  end
		end

end
