class CashRegisterMovement < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # SimplestStatus
    extend SimplestStatus
    statuses :active_not_affected, :inactive, :deleted, :affected_totally, :affected_partial

    # Audited
    audited

  # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

    enum kind: [ :ingress, :egress ]

    belongs_to :cash_register

    has_many :cash_register_movements

    GLOBAL_ACTIONS = [ { :new  => "true" } ]

    before_create :movements_only_can_be_created_to_any_opened_cash_register
    def movements_only_can_be_created_to_any_opened_cash_register
      if cash_register.status != CashRegister::statuses[ :opened ]
        self.errors.add( :base, "El movimiento sólo puede ser cargado a una caja abierta" )
        return false
      end
    end

    def self.actions
      super( GLOBAL_ACTIONS )
    end

    ACTIONS = [ { :get_new_current_account_movement_from_cash_register_movement_inmodal => "( is_movement_created? == false ) and ( self.ingress? )" } ,
                { :get_new_current_account_movements_from_cash_register_movement_inmodal => "( remaining_ingress>0 ) and ( self.ingress? )" } ,
              ]

    def actions
      super( ACTIONS )
    end

    def can_be_modified?
      return false
    end


    # Get current account movement of this resource
    def current_account_movements
      CurrentAccountMovement.where( :resource_type => "#{self.class.model_name}" , :resource_id => self.id )
    end

    # Check if is any movment currentaccount created for this resource
    def is_movement_created?
      return current_account_movements.present?
    end

    # Return string
    # def get_problems_to_affect
    #   return "No existe tarjeta ó identificación asociada al movimiento" unless self.bank_account_card.present?
    #   return "No existe agencia asociada a la tarjeta \"#{ self.bank_account_card.detail }\"" unless self.bank_account_card.agency.present?
    #   return "No existe cuenta corriente de agencia asociada #{self.bank_account_card.agency.detail}" unless self.bank_account_card.agency.current_account.present?
    #   if self.bank_account_card.agency.current_account.current_period.present?
    #     resource_datetime = self.date
    #     unless ( ( resource_datetime.to_date >= self.bank_account_card.agency.current_account.current_period.initial_date ) and ( resource_datetime.to_date < Date.today ) )
    #       return "El recurso está fuera del rango de fechas del período activo de la cuenta"
    #     end
    #   else
    #     return "No existe período activo en la cuenta"
    #   end
    #   return "Ningun problema para homologar"
    # end


    # Generate movement to Current Account as soon is created this registry
    def generate_movement
      unless is_movement_created?
        ActiveRecord::Base.transaction do
          if self.current_account.present?
            result_mov = self.current_account.generate_movement(self)
            if result_mov
              self.status = self.class.statuses[ :affected ]
              self.save
            end
          end
        end
      end
    end

    def signed_amount
      if self.kind == "ingress"
        ( self.amount )
      elsif self.kind == "egress"
        ( - self.amount )
      end
    end


    def remaining_ingress
      if current_account_movements.present?
        ( self.amount - self.current_account_movements.map(&:amount).sum )
      else
        ( self.amount )
      end
    end

end
