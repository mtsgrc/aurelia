class Company < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # SimplestStatus
    extend SimplestStatus
    statuses :active, :inactive, :deleted

    # Audited
    audited

  # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

    # kinds of companies
    enum kind: [ :society, :system ]

    # For logo of company
    has_attached_file :logo, styles: { medium: "300x400>", thumb: "100x75>" } #, default_url: "/images/:style/missing.png"
    validates_attachment_content_type :logo, content_type: /\Aimage\/.*\z/

    belongs_to :system
    has_many :sectors, :class_name => 'Sector', :foreign_key => 'company_id'

    has_many    :children , :class_name => 'Company' , :foreign_key => 'parent_id'
    belongs_to  :parent   , :class_name => 'Company'

    validates :code   , :presence => true , :uniqueness => true
    validates :name   , :presence => true
    validates :kind   , :presence => true
    validates :status , :presence => true

  # ------------------------------ FOR EXTERNAL SYSTEMS DATA ---------------------------------------------

    # Include Model Synchronizer for import data
    extend  SynchronizerMethods

    # Include Model System Methods
    include ModelSystemMethods

    # Always point to a system, if it is external system, will be distinct of 0 ( This system code 'AU+' )
    belongs_to  :system

    # If is an external resource, a synchronizer was used to import him.
    belongs_to  :synchronizer

    # If this resource is the homologation (similar to parent of many instances ) will have many children
    has_many    :children , :class_name => self , :foreign_key => 'parent_id'

    # External module method ( stored in ModelSystemMethods ) that search for the instance of this resource referes
    validate    :parent_existance, :if => :parent_id_changed?

    # This method is for complete resource if any field can't be filled with data from external system before save
    def complete_resource_before_import
      nil
    end

  # ------------------------------ BASIC METHODS ---------------------------------------------

    GLOBAL_ACTIONS = [ { :new  => "true" } ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end

    # For detect loop infinite at parent <=> child
    validate :must_to_not_have_loop_on_dependance
    def must_to_not_have_loop_on_dependance
      arr_children = self.get_children.map{ |p| p.id }
      arr_children << self.id
      if arr_children.include?( self.parent_id )
        self.errors.add(:base, "Se detectó una dependencia cíclica infinita, por favor revise la dependencia del sector") unless self.children.empty?
      end
    end

    # Delete system code of instance code, leaving only system agency code if is imported of external system
    def code_nosys
      code.gsub( system.code + "-", "" )
    end


    # scope: Get the sectors inside this Module
    scope :roots, -> { where( parent_id: nil ) }

    validate :cant_destroy_if_has_children, :on => [ :destroy ]
    # Sector with children cant destroy
    def cant_destroy_if_has_children
      self.errors.add(:base, "Hay sectores que pertenecen al elemento a borrar") unless self.children.empty?
    end

    # Get the parents recursively to top of hierarchy
    def get_parents( arr_data = [] )
      ( self.parent_id? ? [ self.parent ] + self.parent.get_parents( arr_data ) : [] )
    end

    # Get all children of a Sector
    def get_children(arr_data = [])
      ( self.children.any? ? self.children.map{ |child| [ child ] + child.get_children( arr_data ) }  : [] ).flatten
    end

    # Get fullname of a sector depending on sector parents
    def fullname( separator=' > ' )
      arr_nodes = [ self ]
      if self.parent_id?
        arr_nodes += self.get_parents
      end
      arr_nodes.reverse.map{ |s| s.read_attribute( :name ) }.join( separator ).html_safe
    end

    # Global method for displaying name
    def detail
      "#{self.fullname}"
    end

end
