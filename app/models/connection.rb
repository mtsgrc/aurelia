class Connection < ActiveRecord::Base

  # ------------------------------------------------ Global

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods
    
    # SimplestStatus
    extend SimplestStatus
    statuses :active, :inactive, :deleted

    # Audited
    audited

  # ------------------------------------------------ Self

		enum kind: [ :mssql, :mysql , :wsdl, :csv , :nsv , :xls ]
		enum kind_action: [ :read, :write ]

		has_many 		:synchronizers, :class_name => "Synchronizer", :foreign_key => "connection_id", dependent: :destroy
		belongs_to 	:system

		validates :system_id		, :presence => true
		validates :kind					, :presence => true
		validates :kind_action	, :presence => true
		validates :status				, :presence => true

    GLOBAL_ACTIONS = [ { :new  => "true" } ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end

		# Get detail of this Data Accessor
		def detail
			"#{ I18n.t("kind_actions.connection." + self.kind_action) } de #{ I18n.t("kinds.connection.single." + self.kind) }"
		end

		# Get detail about connection
		def connection_detail
			arr_connection_data = []
			arr_connection_data << "#{self.username}@" if self.username?
			arr_connection_data << "#{self.host}" if self.host?
			arr_connection_data << ":#{self.port}" if self.port?
			arr_connection_data << "#{self.uri}" if self.uri?
			arr_connection_data.join
		end

		def connection_detail_no_user
			arr_connection_data = []
			arr_connection_data << "#{self.host}" if self.host?
			arr_connection_data << ":#{self.port}" if self.port?
			arr_connection_data << "#{self.uri}" if self.uri?
			arr_connection_data.join
		end

		# Global method for displaying data
	  def detail
	    "#{self.system.code} - #{self.system.name} | #{self.kind}/#{self.kind_action} #{ !self.connection_detail.empty? ? "(" + self.connection_detail + ")" : ""}"
	  end

end
