class CurrentAccount < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # SimplestStatus
    extend SimplestStatus
    statuses :active, :inactive, :deleted

    # Audited
    audited

  # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

  	RESOURCES_WITH_MOVEMENTS = [ Expense , Liquidation , LotClosure , BankAccountMovement , CashRegisterMovement , CreditNote , DebitNote ]

    has_many :current_account_movements, :class_name => 'CurrentAccountMovement', :foreign_key => 'current_account_id'
    has_many :periods, :class_name => 'CurrentAccountPeriod', :foreign_key => 'current_account_id'

  	# types of current_accounts
  	enum kind: [ :agency , :driver ]

    validates :credit_maximum , :presence => true
    validates :kind           , :presence => true
    validates :status         , :presence => true
    validates :resource_id    , :presence => true

    validates :resource_id , :uniqueness => { scope: :kind }

  # ------------------------------ FOR EXTERNAL SYSTEMS DATA ---------------------------------------------


    # Include Model Synchronizer for import data
    extend  SynchronizerMethods

    # Include Model System Methods
    include ModelSystemMethods

    # Always point to a system, if it is external system, will be distinct of 0 ( This system code 'AU+' )
    belongs_to  :system

    # If is an external resource, a synchronizer was used to import him.
    belongs_to  :synchronizer

    # If this resource is the homologation (similar to parent of many instances ) will have many children
    has_many    :children , :class_name => self , :foreign_key => 'parent_id'

    # External module method ( stored in ModelSystemMethods ) that search for the instance of this resource referes
    validate    :parent_existance, :if => :parent_id_changed?

    # This method is for complete resource if any field can't be filled with data from external system before save
    def complete_resource_before_import
      nil
    end

  # ------------------------------ BASIC METHODS ---------------------------------------------

    GLOBAL_ACTIONS = [ { :get_current_account_periods_starter_job_inmodal  => "true" , :args => { :filter_jobs => :current_account_periods_starter_job } } ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end

    ACTIONS = [ { :get_current_account_period_starter_job_inmodal   => "(periods.present? == false) or ((current_period.present? == true) and (current_period.initial_date<Date.today))" , :args => { :filter_jobs => :current_account_period_starter_job   , :id_current_account_ref => :self } } ,
                { :get_current_account_assign_credits_inmodal => "true" } ,
                # { :current_account_period_finalizer_job_inmodal => "(current_period.present? == true) and (current_period.initial_date<Date.today)" , :args => { :filter_jobs => :current_account_period_finalizer_job , :id_current_account_ref => :self } } ,
              ]

    def actions
      super( ACTIONS )
    end

    # --------------------------------------------------------------------------

    # Get all movements
    def movements initial_date = nil, final_date = nil
      if ( ( initial_date.present? ) and ( final_date.present? ) )
        return self.current_account_movements.where( 'resource_date >= ? AND resource_date <= ?' , initial_date , final_date )
      elsif ( initial_date.present? )
        return self.current_account_movements.where( 'resource_date >= ?' , initial_date )
      else
        return self.current_account_movements
      end
    end

    def group_movements_by_agency_company_from_hash movements_grouped_by

      ret_hash = {}
      movements_grouped_by.each do | agency_company_or_array , these_movements |

        if agency_company_or_array.class == Array

          if agency_company_or_array.count > 0

            affectations = these_movements.map(&:affectations).flatten

            # THis .each_with_object({}){|(k,v),o|(o[v]||=[])<<k}
            # I.E.: converts {"one"=>1,"two"=>2, "1"=>1, "2"=>2}.inverse to {1=>["one", "1"], 2=>["two", "2"]}
            hash_agencies_companies_affectations = affectations.map{ |a| { a => a.agency_company} }.reduce({},:merge).each_with_object({}){ |(k,v),o| (o[v]||=[])<<k }

            remaining = affectations.map(&:movement_from).map(&:remaining_amount)
            hash_agencies_companies_affectations[ :unassigned ] = remaining if remaining.sum > 0.00

            hash_to_append = hash_agencies_companies_affectations

          else
            hash_to_append = { nil => these_movements }
          end

        else
          hash_to_append = { agency_company_or_array => these_movements }
        end

        ret_hash.merge!( hash_to_append ){ | key , oldval , newval | [*oldval].to_a + [*newval].to_a }

      end

      # return ret_hash.map{ |k,v| { k => ( k == :unassigned ? v.sum : v ) } }.reduce({},:merge)
      return ret_hash

    end

    # Get all movements
    def movements_by group_by , initial_date = nil, final_date = nil , instance_ref = nil

      ret_hash = {}

      if ( ( initial_date.present? ) and ( final_date.present? ) )
        movements = self.current_account_movements.where( 'resource_date >= ? AND resource_date <= ?' , initial_date , final_date )
      elsif ( initial_date.present? )
        movements = self.current_account_movements.where( 'resource_date >= ?' , initial_date )
      else
        movements = self.current_account_movements
      end

      # Group by
      if instance_ref.present?
        movements_grouped_by = { instance_ref => [] }
        movements.map do |cam|
          if ( cam.agency_company == instance_ref )
            movements_grouped_by[ instance_ref ] << cam
          else
            if ( ( cam.agency_company.class == Array ) and ( cam.agency_company.include?( instance_ref ) ) )
              movements_grouped_by[ instance_ref ] = movements_grouped_by[ instance_ref ] + cam.affectations.map{ |a| a if a.agency_company == instance_ref }.compact
            end
          end
        end
      else
        movements_grouped_by = movements.group_by(&group_by)
      end

      return group_movements_by_agency_company_from_hash( movements_grouped_by )

    end

    # --------------------------------------------------------------------------

    # Get difference between Credit Maximum and Credit actual
    def remaining_amount
      self.credit_maximum + self.credit_actual
    end

    # Get difference between Credit Maximum and Credit actual
    def existance_of_resource
      begin
        return true if ( Object.const_get self.kind.capitalize).find( self.resource_id )
      rescue ActiveRecord::RecordNotFound
  			self.errors.add( :base , "No existe el recurso indicado" )
  		rescue NameError
  			self.errors.add( :base , "No existe el tipo de recurso" )
      end
    end

    # --------------------------------------------------------------------------

    # Generate movement from
    def generate_movement resource, ref_amount = nil
      begin
        if RESOURCES_WITH_MOVEMENTS.include?( resource.class )

          if self.current_period.present?

            # Setting date and time of resouce
            resource_datetime = resource.liquidated_at  if resource.class == Liquidation
            resource_datetime = resource.closed_at      if resource.class == LotClosure
            resource_datetime = resource.receipt_date   if resource.class == Expense
            resource_datetime = resource.date           if resource.class == BankAccountMovement
            resource_datetime = resource.created_at     if resource.class == CashRegisterMovement
            resource_datetime = resource.created_at     if resource.class == CreditNote
            resource_datetime = resource.created_at     if resource.class == DebitNote

            if ( resource_datetime.to_date >= self.current_period.initial_date and ( resource_datetime <= Date.today or [ CreditNote , DebitNote , CashRegisterMovement ].include?( resource.class ) ) )

              movement = CurrentAccountMovement.new( :current_account_id => self.id )

              movement.resource_type = "#{resource.class.model_name}"
              movement.resource_id   = resource.id

              if ref_amount.present?
                movement.amount           = ref_amount
                movement.remaining_amount = ref_amount
                movement.detail           = "*Parte* " + resource.detail
              end

              if resource_datetime.class == Date
                movement.resource_date = resource_datetime.to_date
                movement.resource_time = nil
              else
                movement.resource_date = resource_datetime.to_date
                movement.resource_time = resource_datetime.to_time
              end

              # Make the same kind of movement => :debit or credit
              movement.kind = CurrentAccountMovement.kinds[ resource.kind.to_sym ] if resource.class == BankAccountMovement
              movement.kind = CurrentAccountMovement.kinds[ :credit ] if resource.class == CashRegisterMovement
              movement.kind = CurrentAccountMovement.kinds[ :credit ] if resource.class == CreditNote
              movement.kind = CurrentAccountMovement.kinds[ :debit  ] if resource.class == DebitNote

              if movement.save
                return true
              else
                movement.errors.full_messages.each do |err_msg|
                  self.errors.add( :base , err_msg )
                end
                return false
              end

            else
              raise MovementResourceOutOfCurrentPeriodDateRange, "Movimiento fuera del período activo de la cuenta"
            end

          else
            raise CurrentAccountHasNotCurrentPeriod, "No hay período activo"
          end

        else
          raise CurrentAccountNotAcceptsTheResource, "No se puede crear movimiento a partir del recurso"
        end
      rescue Exception => exc

        # Agregamos error de los raise al la cuenta corriente actual
        self.errors.add( :base , exc.message )
        return false

      end

    end

    # --------------------------------------------------------------------------

    # Check sum of total movements by kind of movements
    def sum_of_movements kind_of_movement, initial_date = nil, final_date = nil
      if ( ( initial_date.present? ) and ( final_date.present? ) )
        eval( "self.movements( initial_date , final_date ).#{kind_of_movement}.collect(&:amount).sum" )
      elsif ( initial_date.present? )
        eval( "self.movements( initial_date ).#{kind_of_movement}.collect(&:amount).sum" )
      else
        eval( "self.movements.#{kind_of_movement}.collect(&:amount).sum" )
      end
    end

    # Check sum of total movements by kind of movements
    def sum_of_movements_by group_by , kind_of_movement, initial_date = nil, final_date = nil , instance_ref = nil

      if ( ( initial_date.present? ) and ( final_date.present? ) )
        instances = self.movements_by( group_by , initial_date , final_date , instance_ref )
      elsif ( initial_date.present? )
        instances = self.movements_by( group_by , initial_date , nil , instance_ref )
      else
        instances = self.movements_by( group_by , nil , nil , instance_ref )
      end

      instances.map{ | group_by_instance , instances | { group_by_instance => ( instances.map do | instance |
        if instance.class == CurrentAccountMovement
          if instance.kind == "#{kind_of_movement}"
            instance.amount
          end
        elsif instance.class == MovementAffectation
          if instance.movement_from.kind == "#{kind_of_movement}"
            instance.amount
          end
        elsif instance.class == BigDecimal
          instance
        end
      end ).compact.sum } }.reduce( {} , :merge )

    end

    # --------------------------------------------------------------------------

    # Calc credits
    def calc_credits initial_date = nil, final_date = nil
      self.sum_of_movements( :credit , initial_date , final_date )
    end

    # Calc credits by_company
    def calc_credits_by group_by , initial_date = nil , final_date = nil , instance_ref = nil
      self.sum_of_movements_by( group_by , :credit , initial_date , final_date , instance_ref ).map{ |c,t| { c => t } }.reduce( {} , :merge )
    end

    # --------------------------------------------------------------------------

    # Calc credits
    def calc_debits initial_date = nil, final_date = nil
      -( self.sum_of_movements( :debit , initial_date , final_date ) )
    end

    # Calc credits by_company
    def calc_debits_by group_by , initial_date = nil, final_date = nil , instance_ref = nil
      self.sum_of_movements_by( group_by , :debit , initial_date , final_date , instance_ref ).map{ |c,t| { c => -(t) } }.reduce( {} , :merge ).except( :unassigned )
    end

    # --------------------------------------------------------------------------

    # Check sum of total movements like :debit , :credit
    def calc_balance initial_date = nil, final_date = nil
      # self.calc_credits( initial_date , final_date ) + self.calc_debits( initial_date , final_date )
      self.final_credit + self.final_debit
    end

    # Check sum of total movements like :debit , :credit
    # def calc_balance_by group_by , initial_date = nil, final_date = nil , instance_ref = nil
    #   self.calc_debits_by( group_by , initial_date , final_date , instance_ref ).merge( self.calc_credits_by( group_by , initial_date , final_date , instance_ref ) ){ | group_by_instance , total_debits , total_credits | total_debits + total_credits }
    # end

    # --------------------------------------------------------------------------
    # # FIXES
    # def fix_final_amounts
    #   self.current_period.calc_final_credit_and_save
    #   self.current_period.calc_final_debit_and_save
    # end
    #
    # def fix_agency_company_amounts
    #   self.current_period.agency_company_periods.each do | agency_company_period |
    #     agency_company_period.calc_final_credit_and_save
    #     agency_company_period.calc_final_debit_and_save
    #   end
    # end

    # --------------------------------------------------------------------------

    # Method for all models
    def detail
      "#{ ( Object.const_get self.kind.capitalize).find( self.resource_id ).detail }"
    end

    # Affect credit actual
    def affect_credit_actual movement
      self.credit_actual += movement.amount if movement.kind == :credit.to_s
      self.credit_actual -= movement.amount if movement.kind == :debit.to_s
      self.save
    end

    def owner
      ( Object.const_get self.kind.capitalize ).find( self.resource_id )
    end

    # ------------------------------ PERIODS ---------------------------------------------

    def total_amounts

      final_credit  = 0.00
      final_debit   = 0.00

      if self.periods.present?

        if self.current_period.present?

          from_date  = self.current_period.initial_date
          to_date    = Date.today

          credits_in_period = calc_credits( from_date , to_date )
          debits_in_period  = calc_debits( from_date , to_date )

          final_credit  = self.current_period.initial_credit + credits_in_period
          final_debit   = self.current_period.initial_debit  + debits_in_period

        else

          final_credit  = self.last_period.final_credit
          final_debit   = self.last_period.final_debit

        end

      end

      return { :credits => final_credit , :debits => final_debit }

    end

    # Returns last period
    def last_period
      self.periods.order( :id => :desc ).first if self.periods.present?
    end

    # Return current period
    def current_period
      self.periods.find_by( :status => CurrentAccountPeriod::statuses[ :current ] ) if self.periods.present?
    end

    def start_period initial_date = nil , initial_credit = nil , initial_debit = nil

      if self.current_period.present? or !self.periods.present?

        ret_finish_period = false

        if self.current_period.present?

          last_current_period_id = self.current_period.id
          ret_finish_period = self.finish_period( ( initial_date.present? ? ( initial_date - 1.day ) : ( Date.today - 1.day ) ) )

          if ret_finish_period

            last_current_period = CurrentAccountPeriod.find( last_current_period_id )

            initial_date        = ( last_current_period.final_date + 1.day )
            initial_credit      = ( last_current_period.final_credit )
            initial_debit       = ( last_current_period.final_debit )
            order_number        = ( last_current_period.order_number + 1 )

            # Get agency and company periods for last period
            agency_company_periods = last_current_period.agency_company_periods

            # for each agency_company period existance we create a new period with same values except id of the new current account period (relationship)
            if agency_company_periods.present?

              arr_of_new_agency_company_periods = []
              agency_company_periods.each do | agency_company_period |

                attrs_for_new_agency_company_period = {}
                attrs_for_new_agency_company_period[ :initial_credit ]    = agency_company_period.final_credit
                attrs_for_new_agency_company_period[ :initial_debit ]     = agency_company_period.final_debit
                attrs_for_new_agency_company_period[ :agency_company_id ] = agency_company_period.agency_company_id
                attrs_for_new_agency_company_period[ :final_credit ]      = attrs_for_new_agency_company_period[ :initial_credit ]
                attrs_for_new_agency_company_period[ :final_debit ]       = attrs_for_new_agency_company_period[ :initial_debit ]
                new_agency_company_period = AgencyCompanyPeriod.new( attrs_for_new_agency_company_period )

                arr_of_new_agency_company_periods << new_agency_company_period

              end

            end

          else
            return false
          end

        end

        if ( ( ret_finish_period == true ) or ( !self.periods.present? ) )

          current_account_period_params = {}
          current_account_period_params[ :current_account_id ]  = self.id
          current_account_period_params[ :initial_date ]        = ( initial_date.present?   ? initial_date          : Date.today  )
          current_account_period_params[ :initial_credit ]      = ( initial_credit.present? ? initial_credit        : 0.00        )
          current_account_period_params[ :initial_debit ]       = ( initial_debit.present?  ? initial_debit         : 0.00        )
          current_account_period_params[ :order_number ]        = ( order_number.present?   ? order_number          : 1           )
          current_account_period_params[ :final_credit ]        = current_account_period_params[ :initial_credit ]
          current_account_period_params[ :final_debit ]         = current_account_period_params[ :initial_debit ]
          current_account_period_params[ :status ]              = CurrentAccountPeriod::statuses[ :current ]

          period_new = CurrentAccountPeriod.new( current_account_period_params )
          ret_starting_period = period_new.save

          # If perios have been saved
          if ret_starting_period

            # FIX/Calcs if any movement
            period_new.fix_final_amounts

            # for Agency company existance
            if arr_of_new_agency_company_periods.present?

              # arr_ret_of_new_agency_company_periods_saving = []

              # Assign id of current account period to each agency company period and save
              arr_of_new_agency_company_periods.each do | new_agency_company_period |
                new_agency_company_period.current_account_period_id = period_new.id
                new_agency_company_period.save

                # CALCFIX
                new_agency_company_period.fix_final_amounts

              end

              # all agency company periods were been saved
              if ( ( arr_of_new_agency_company_periods.map( &:persisted? ).uniq.count == 1 ) and ( arr_of_new_agency_company_periods.map(&:persisted?).uniq.first == true ) )
                return true
              else
                return arr_of_new_agency_company_periods
              end
            else
              return true
            end

          else
            return false
          end

        else
          return false
        end

      else
        return false
      end

    end

    def delete_last_period_and_childs

      if self.last_period.status == CurrentAccountPeriod::statuses[ :current ]
        last_current_period_id = self.last_period.id
        self.last_period.destroy
        AgencyCompanyPeriod.where( :current_account_period_id => last_current_period_id ).destroy_all
      end

      self.reload

      if self.last_period.present?

        if self.last_period.status == CurrentAccountPeriod::statuses[ :finished ]
          last_finished_period = self.last_period
          last_finished_period.status        = CurrentAccountPeriod::statuses[ :current ]
          last_finished_period.final_date    = nil
          # last_finished_period.final_debit   = 0.00
          # last_finished_period.final_credit  = 0.00
          last_finished_period.save
          AgencyCompanyPeriod.where( :current_account_period_id => last_finished_period.id ).destroy_all
        end

      end

    end

    def finish_period final_date = nil

      final_date = ( Date.today - 1.day ) unless final_date.present?
      current_period = self.current_period

      begin

        raise CurrentAccountWithoutCurrentPeriod, "No existe periodo vigente para la cuenta '#{self.detail}'" unless self.current_period.present?
        raise FinalDatePeriodIsMajorOrEqualThanToday, "La fecha final debe ser menor al día actual '#{final_date}'" unless final_date < Date.today
        raise InitialDatePeriodWasToday, "La fecha inicial del período es Hoy, debe esperar a mañana recién para cerrar el período" if self.current_period.initial_date == Date.today
        raise FinalDatePeriodIsMinorOrEqualThanLastPeriod, "La fecha final debe ser mayor a la fecha inicial del período activo '#{final_date}'. Y la fecha inicial es '#{self.current_period.initial_date}'" unless final_date > self.current_period.initial_date

        ActiveRecord::Base.transaction do

          from_date  = current_period.initial_date
          to_date    = final_date

          ret_finish_period = []

          # --------------- SECTION FOR CLOSE ACTUAL PERIOD ---------------

          #TODO UPGRADE THIS !!! CALC BY AGENCY/COMPANY AND THEN SUMMARIZE THIS VALUES
          credits_in_period = calc_credits( from_date , to_date )
          debits_in_period  = calc_debits( from_date , to_date )

          final_credit  = current_period.initial_credit + credits_in_period
          final_debit   = current_period.initial_debit  + debits_in_period

          current_account_period_params                   = {}
          current_account_period_params[ :final_date ]    = to_date
          current_account_period_params[ :final_credit ]  = final_credit
          current_account_period_params[ :final_debit ]   = final_debit
          current_account_period_params[ :status ]        = CurrentAccountPeriod::statuses[ :finished ]

          ret_finish_period << current_period.update( current_account_period_params )

          # --------------- SECTION FOR UPDATE OR CREATE AGENCY COMPANY PERIODS ---------------

          # credits_in_period_by_agencies_companies  = owner.external_agencies_companies.map{ |eac| { eac => 0.00 } }.reduce({},:merge).merge( calc_credits_by( :agency_company , from_date , to_date ) )
          # debits_in_period_by_agencies_companies   = credits_in_period_by_agencies_companies.clone.merge( calc_debits_by( :agency_company , from_date , to_date ) )
          credits_in_period_by_agencies_companies = calc_credits_by( :agency_company , from_date , to_date ).except( nil ).except( :unassigned )
          debits_in_period_by_agencies_companies  = calc_debits_by( :agency_company , from_date , to_date ).except( nil ).except( :unassigned )

          # Get agency_company_periods present
          agency_company_periods = current_period.agency_company_periods

          ret_agency_company_periods_saving = [ ]

          if agency_company_periods.present?

            agency_company_periods.each do | agency_company_period |

              # Any debit in period
              if credits_in_period_by_agencies_companies[ agency_company_period.agency_company ].present?
                agency_company_period.final_credit = agency_company_period.initial_credit + credits_in_period_by_agencies_companies[ agency_company_period.agency_company ]
              else
                agency_company_period.final_credit = agency_company_period.initial_credit
              end

              # Any debit in period
              if debits_in_period_by_agencies_companies[ agency_company_period.agency_company ].present?
                agency_company_period.final_debit = agency_company_period.initial_debit + debits_in_period_by_agencies_companies[ agency_company_period.agency_company ]
              else
                agency_company_period.final_debit = agency_company_period.initial_debit
              end

              # Update credit and debits
              ret_agency_company_period_saving  = agency_company_period.save!
              ret_finish_period << ret_agency_company_period_saving

              # deleting agency_company from hash calc
              if ret_agency_company_period_saving
                credits_in_period_by_agencies_companies = credits_in_period_by_agencies_companies.except( agency_company_period.agency_company )
                debits_in_period_by_agencies_companies  = debits_in_period_by_agencies_companies.except( agency_company_period.agency_company )
              end

            end

          end

          # Union of agencies / companies
          arr_of_new_agency_company_amounts = ( credits_in_period_by_agencies_companies.keys | debits_in_period_by_agencies_companies.keys )

          # For save all child periods (by agency_company) creations
          arr_of_new_agency_company_period_savings = [ ]

          # For each agency_company present
          arr_of_new_agency_company_amounts.each do | agency_company |

            # Creating attributes for new agency company period
            agency_company_period_attrs = {}
            agency_company_period_attrs[ :current_account_period_id ] = current_period.id
            agency_company_period_attrs[ :agency_company_id ]         = agency_company.id
            agency_company_period_attrs[ :initial_credit ]            = 0.00
            agency_company_period_attrs[ :initial_debit ]             = 0.00
            agency_company_period_attrs[ :final_credit ]              = ( credits_in_period_by_agencies_companies.include?( agency_company ) ? credits_in_period_by_agencies_companies[ agency_company ] : 0.00 )
            agency_company_period_attrs[ :final_debit ]               = ( debits_in_period_by_agencies_companies.include?( agency_company ) ? debits_in_period_by_agencies_companies[ agency_company ] : 0.00 )

            new_agency_company_period = AgencyCompanyPeriod.new( agency_company_period_attrs )
            ret_finish_period << new_agency_company_period.save!

          end

          ret_finish_period << true if ( ret_agency_company_periods_saving.uniq.count == 1 and ret_agency_company_periods_saving.uniq.first == true )

          return ret_finish_period.uniq.first

        end

      rescue => exception
        puts exception
        return false
      end


    end

    # class method for all instances
    def self.start_periods initial_date = nil
      self.all.each do | current_account |
        begin
          if current_account.status == CurrentAccount.statuses[ :active ]
            current_account.start_period( initial_date )
          end
        rescue Exception => exc
          puts "#{exc}"
          next
        end
      end
    end

end

class MovementResourceOutOfCurrentPeriodDateRange < Exception
end
class CurrentAccountWithoutCurrentPeriod < Exception
end
class FinalDatePeriodIsMajorOrEqualThanToday < Exception
end
class FinalDatePeriodIsMinorOrEqualThanLastPeriod < Exception
end
class InitialDatePeriodWasToday < Exception
end
