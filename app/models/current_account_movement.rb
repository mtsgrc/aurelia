class CurrentAccountMovement < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

  	# SimplestStatus
    extend SimplestStatus
    statuses :approved, :deleted, :approved_and_affected_completely

  	# Audited
    audited

    # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

  	belongs_to :current_account, :class_name => 'CurrentAccount', :foreign_key => 'current_account_id'

		belongs_to :resource, polymorphic: true
		has_one :self_ref, :class_name => self, :foreign_key => :id
		has_one :liquidation, :through => :self_ref, :source => :resource, :source_type => Liquidation
		has_one :expense    , :through => :self_ref, :source => :resource, :source_type => Expense
		has_one :lot_closure, :through => :self_ref, :source => :resource, :source_type => LotClosure

    has_one :company        , :through => :liquidation
    has_one :agency         , :through => :liquidation

    has_many :affectations  , :class_name => 'MovementAffectation' ,  :foreign_key => 'movement_from_id'
    has_many :affectors     , :class_name => 'MovementAffectation' ,  :foreign_key => 'movement_to_id'

  	# types of current_accounts
  	enum kind: [ :credit , :debit ]

  	SIGN_KIND_RESOURCE_TYPE = {
  															:Expense 			=> :credit ,
  															:Liquidation  => :debit  ,
                                :LotClosure  	=> :credit
  														}

    validates :resource_type        , :presence => true
    validates :resource_id          , :presence => true
    validates :resource_date		    , :presence => true

    validates :amount 							, :presence => true
    validates :remaining_amount			, :presence => true
    validates :detail           		, :presence => true
    validates :kind         				, :presence => true
    validates :status         			, :presence => true

    validates_uniqueness_of :resource_id, :scope => [ :resource_type , :current_account_id  ]

  # ------------------------------ FOR EXTERNAL SYSTEMS DATA ---------------------------------------------

    GLOBAL_ACTIONS = [ ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end

    ACTIONS = [ { :get_new_movement_affectations_from_current_account_movement_inmodal => "( self.credit? ) and ( self.remaining_amount>0 )" } ]

    def actions
      super( ACTIONS )
    end


    # Include Model Synchronizer for import data
    extend  SynchronizerMethods

    # Include Model System Methods
    include ModelSystemMethods

    # Always point to a system, if it is external system, will be distinct of 0 ( This system code 'AU+' )
    belongs_to  :system

    # If is an external resource, a synchronizer was used to import him.
    belongs_to  :synchronizer

    # If this resource is the homologation (similar to parent of many instances ) will have many children
    has_many    :children , :class_name => self , :foreign_key => 'parent_id'

    # External module method ( stored in ModelSystemMethods ) that search for the instance of this resource referes
    validate    :parent_existance, :if => :parent_id_changed?

    # This method is for complete resource if any field can't be filled with data from external system before save
    def complete_resource_before_import
      nil
    end

  # ------------------------------ BASIC METHODS ---------------------------------------------

	# Amount cant be negative, because the kind of this movement, indicates the sign
	before_create :amount_cant_be_zero_or_nil
	def amount_cant_be_zero_or_nil
    if ( self.amount == nil )
	    self.errors.add( :base, "El monto no puede ser cero" )
      return false
    end
	end

	# Amount cant be negative, because the kind of this movement, indicates the sign
	after_create :affect_current_account_and_periods
	def affect_current_account_and_periods

    # Affect actual_credit of current_account
    self.current_account.affect_credit_actual( self )

    # Affect final amount (final_debit or final_credit) of current_account_period
    self.current_account.current_period.affect_final_amount( self )

    if self.agency and self.company

      agency_company = AgencyCompany.find_by( :agency => self.agency , :company => self.company )

      current_agency_company_period = self.current_account.current_period.agency_company_periods.find_by( :agency_company => agency_company )

      unless current_agency_company_period.present?

        agency_company_period_params = {}
        agency_company_period_params[ :current_account_period_id ] = self.current_account.current_period.id
        agency_company_period_params[ :agency_company_id ]         = agency_company.id

        agency_company_period_params[ :initial_credit ]            = 0.00
        agency_company_period_params[ :initial_debit ]             = 0.00
        agency_company_period_params[ :final_credit ]              = ( self.kind == "credit" ? self.amount : 0.00 )
        agency_company_period_params[ :final_debit ]               = ( self.kind == "debit" ? ( - self.amount ) : 0.00 )

        agency_company_period_params[ :status ]                    = AgencyCompanyPeriod::statuses[ :active ]

        agency_company_period_new = AgencyCompanyPeriod.new( agency_company_period_params )
        ret_agency_company_period_new = agency_company_period_new.save

      else

        # Only will affect if the movement is directly associated with de agency_company and have a current period
        current_agency_company_period.affect_final_amount( self )

      end

    end

	end

	validate :instance_cant_be_modified_after_create_except_status, :on => [ :update ]
	def instance_cant_be_modified_after_create_except_status
		self.errors.add( :base, "No se puede modificar" ) if self.changed.count > 1 or self.changed != ["status"] or self.changed != ["remaining_amount"]
	end

  # Get instance refeered
  def get_instance_refeered
    begin
      return self.resource_type.classify.constantize.find( self.resource_id )
    rescue => exception
      puts "[ERR] #{exception.to_s}"
      return nil
    end
  end

	# Check existence of resource
  before_validation :check_existence_of_resource_and_set_movement_properties
	def check_existence_of_resource_and_set_movement_properties
		begin

			# Set the kind of movement according of resource type passed by user
			resource_class = self.resource_type.classify.constantize

      # Choosing kind if it is not setted previously
			self.kind   = SIGN_KIND_RESOURCE_TYPE[ self.resource_type.to_sym ] unless self.kind

			# Set properties according resource id passed by user, like amount and detail
			resource = resource_class.find( self.resource_id )

			# If it's an Expense...
      if resource_class == Expense
  			self.amount            = resource.receipt_amount unless self.amount
  			self.detail            = "#{resource.detail}" unless self.detail
      end

			# If it's an Liquidation...
      if resource_class == Liquidation
  			self.amount = resource.total_liquidated unless self.amount
  			self.detail = "#{resource.detail}" unless self.detail
      end

			# If it's an Credit Terminal Lot Closure...
      if [ LotClosure , BankAccountMovement , CashRegisterMovement , CreditNote , DebitNote ].include?(resource_class)
  			self.amount = resource.amount unless self.amount
        if ( resource_class == BankAccountMovement and !self.detail.present? and !resource.detail.present? )
          self.detail = "#{resource.date} > Cuenta: #{resource.bank_account.detail}"
        elsif ( resource_class == CashRegisterMovement and !self.detail.present? and !resource.detail.present? )
          self.detail = "#{resource.date} > Cuenta: #{resource.bank_account.detail}"
        else
          self.detail = "#{resource.detail}" unless self.detail
        end
      end

      if self.amount < 0.00
        self.amount = -( self.amount )
        self.kind   = CurrentAccountMovement::kinds[ :debit ]
      end

			self.remaining_amount  = self.amount

		rescue ActiveRecord::RecordNotFound
			self.errors.add( :base , "No existe el recurso indicado" )
		rescue NameError
			self.errors.add( :base , "No existe el tipo de recurso" )
		end
	end

  # For check if attribute is validated or not
  def valid_attribute?(attribute)
    self.errors[attribute].blank?
  end

  def can_be_modified?
    false
  end

  def period
    periods = self.current_account.periods
    if self.current_account.periods.current.first.movements.include?(self)
      self.current_account.periods.current.first
    else
      self.current_account.periods.where( '( initial_date <= ? AND final_date >= ? )' , self.resource_date , self.resource_date ).first
    end
  end

  def signed_amount
    if self.kind == "credit"
      ( self.amount )
    elsif self.kind == "debit"
      ( - self.amount )
    end
  end

  def signed_remaining_amount
    if self.kind == "credit"
      ( self.remaining_amount )
    elsif self.kind == "debit"
      ( - self.remaining_amount )
    end
  end

  def resource_datetime
    DateTime.new( self.resource_date.year , self.resource_date.month , self.resource_date.day , self.resource_time.hour , self.resource_time.min , self.resource_time.sec , self.resource_time.zone )
  end

  def agency_company
    if self.agency and self.company and !self.affectations.present?
      AgencyCompany.find_by( :agency_id => self.agency.id , :company_id => self.company.id )
    else
      self.affectations.map(&:agency_company) #.map{ |m_to| AgencyCompany.find_by( :agency => m_to.agency , :company => m_to.company ) }
    end
  end

  def fix_remaining_amount
    if self.credit?
      if self.affectations.present?
        self.update_attribute( :remaining_amount , ( self.amount - self.affectations.map(&:amount).sum ) )
      end
    else
      if self.affectors.present?
        self.update_attribute( :remaining_amount , ( self.amount - self.affectors.map(&:amount).sum ) )
      end
    end
  end

  def self.fix_remaining_amounts
    self.all.each do | movement |
      movement.fix_remaining_amount
    end
  end

  def get_affectations_remaining
    self.amount - self.affectations.map(&:amount).sum
  end

  def affectations_or_affectors_child
    if self.debit?
      self.affectors
    elsif self.credit?
      self.affectations
    end

  end

end
