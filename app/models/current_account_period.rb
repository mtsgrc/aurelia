class CurrentAccountPeriod < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # SimplestStatus
    extend SimplestStatus
    statuses :current, :finished, :deleted

    # Audited
    audited

  # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

    belongs_to  :current_account, :class_name => 'CurrentAccount', :foreign_key => 'current_account_id'
    has_many    :agency_company_periods, :class_name => 'AgencyCompanyPeriod', :foreign_key => 'current_account_period_id'

    validates :order_number   , :presence => true

    validates :initial_date   , :presence => true
    validates :initial_credit , :presence => true
    validates :initial_debit  , :presence => true

  # ------------------------------ BASIC METHODS ---------------------------------------------
    GLOBAL_ACTIONS = [ { :new  => "true" } ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end

    before_create :existance_of_only_one_current_period_for_one_current_account
    def existance_of_only_one_current_period_for_one_current_account
      if CurrentAccountPeriod.where( :current_account_id => self.current_account_id , :status => CurrentAccountPeriod::statuses[ :current ] ).present?
        self.errors.add( :base , "Ya existe período activo para #{self.current_account.detail}." )
        return false
      end
    end

    # --------------------------------------------------------------------------

    # Only for get the movements in the current period
    def movements
      self.current_account.movements( self.initial_date, ( self.final_date.present? ? self.final_date : Date.today ) )
    end

    # Only for get the movements in the current period
    def movements_by group_by, instance_ref = nil
      self.current_account.movements_by( group_by , self.initial_date, ( self.final_date.present? ? self.final_date : Date.today ) , instance_ref )
    end

    # --------------------------------------------------------------------------

    # Sum of movements, call to current_account method setting initial and final dates by period
    def sum_of_movements kind_of_movement, initial_date = nil, final_date = nil
      self.current_account.sum_of_movements( kind_of_movement , self.initial_date , ( self.final_date.present? ? self.final_date : Date.today ) )
    end

    # Sum of movements by attribute, call to current_account method setting initial and final dates by period
    def sum_of_movements_by group_by , kind_of_movement, initial_date = nil, final_date = nil, instance_ref = nil
      self.current_account.sum_of_movements_by( group_by , kind_of_movement , self.initial_date , ( self.final_date.present? ? self.final_date : Date.today ) , instance_ref )
    end

    # --------------------------------------------------------------------------

    # Calc credits
    def calc_credits
      self.current_account.calc_credits( self.initial_date , self.final_date )
    end

    # Calc credits
    def calc_credits_by group_by, instance_ref = nil
      self.current_account.calc_credits_by( group_by , self.initial_date , self.final_date , instance_ref )
    end

    # --------------------------------------------------------------------------

    # Calc credits
    def calc_debits
      self.current_account.calc_debits( self.initial_date , self.final_date )
    end

    # Calc credits
    def calc_debits_by group_by , instance_ref = nil
      self.current_account.calc_debits_by( group_by , self.initial_date , self.final_date , instance_ref )
    end

    # --------------------------------------------------------------------------

    # Check sum of total movements like :debit , :credit
    def calc_balance
      # self.calc_credits + self.calc_debits
      self.final_credit + self.final_debit
    end

    # Check sum of total movements by attribute like :debit , :credit
    # def calc_balance_by group_by, instance_ref = nil
    #   self.calc_debits_by( group_by , instance_ref ).merge( self.calc_credits_by( group_by , instance_ref ) ){ | group_by_instance , total_debits , total_credits | total_debits + total_credits }
    # end
    before_destroy :destroy_agency_company_periods
    def destroy_agency_company_periods
      self.agency_company_periods.destroy_all
    end

    # --------------------------------------------------------------------------
    # # FIXES
    def fix_final_amounts
      self.final_credit = self.initial_credit + self.calc_credits
      self.final_debit = self.initial_debit + self.calc_debits
      self.save
    end

    def fix_agency_company_amounts
      self.agency_company_periods.each do | agency_company_period |
        agency_company_period.fix_final_amounts
      end
    end

    # --------------------------------------------------------------------------

    def affect_final_amount movement
      if movement.kind == :credit.to_s
        self.final_credit = self.final_credit + movement.amount
      elsif movement.kind == :debit.to_s
        self.final_debit = self.final_debit - movement.amount
      end
      self.save
    end

    # initial debits + initial credits
    def initial_balance
      self.initial_credit + self.initial_debit
    end

    # final debits + final credits if final date is present
    # calc debits + calc credits if final date is not present
    def final_balance
        self.final_credit + self.final_debit
    end

    # Check sum of total movements like :debit , :credit
    # def total_balance
    #   self.final_balance + self.initial_balance
    # end

    def detail
      "Período N°#{self.order_number} - Desde #{self.initial_date.strftime('%d/%m/%Y')}#{ (self.final_date.present? ? " - Hasta #{self.final_date.strftime('%d/%m/%Y')} (Cerrado)" : " - HOY (Vigente)") }"
    end

    def can_be_modified?
      return false
    end

    def not_affected_and_not_instanciated_amounts

      agency_company_periods = self.agency_company_periods

      credits_in_period_by_agencies_companies       = calc_credits_by( :agency_company )
      debits_in_period_by_agencies_companies        = calc_debits_by( :agency_company )

      diff_to_show_debits   = debits_in_period_by_agencies_companies.keys - agency_company_periods.map{ |acp| acp.agency_company }
      diff_to_show_credits  = credits_in_period_by_agencies_companies.keys - agency_company_periods.map{ |acp| acp.agency_company }

      diff_to_show_unique   = ( diff_to_show_credits + diff_to_show_debits ).uniq

      amounts_by_agencies_companies = []
      diff_to_show_unique.each do | instance_to_add |

        if instance_to_add == nil
          detail_of_instance = "Sin identificar"
        elsif instance_to_add == :unassigned
          detail_of_instance = "Sin asignar"
        else
          detail_of_instance = "#{instance_to_add.detail}"
          id = instance_to_add.id
        end

        if id.present?
          amounts_by_agencies_companies << { :id => id , :detail => "#{detail_of_instance}" , :credit => credits_in_period_by_agencies_companies[ instance_to_add ]         , :debit => debits_in_period_by_agencies_companies[ instance_to_add ] , :order_number => self.order_number }
        else
          amounts_by_agencies_companies << { :detail => "#{detail_of_instance}" , :credit => credits_in_period_by_agencies_companies[ instance_to_add ]         , :debit => debits_in_period_by_agencies_companies[ instance_to_add ] , :order_number => self.order_number }
        end
      end

      amounts_by_agencies_companies << { :detail => "Total acumulado" , :credit => credits_in_period_by_agencies_companies.values.sum , :debit => debits_in_period_by_agencies_companies.values.sum }

      return amounts_by_agencies_companies

    end


end
