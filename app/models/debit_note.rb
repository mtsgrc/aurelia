class DebitNote < ActiveRecord::Base

	# ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # SimplestStatus
    extend SimplestStatus
    statuses :waiting, :approved, :rejected, :deleted
		STATUS_TRANSITIONS = {
			:waiting => [ :approved , :rejected , :deleted ]
		}
    # Audited
    audited

  # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

		# Audited
		audited

    validates  :current_account_id  , :presence => true
    validates  :date_at             , :presence => true

		validates  :amount              , :presence => true
		validates  :detail              , :presence => true
		validates  :user_id             , :presence => true

		belongs_to :current_account
		belongs_to :user

  # ------------------------------ FOR EXTERNAL SYSTEMS DATA ---------------------------------------------

    # Include Model Synchronizer for import data
    extend  SynchronizerMethods

    # Include Model System Methods
    include ModelSystemMethods

    # Always point to a system, if it is external system, will be distinct of 0 ( This system code 'AU+' )
    belongs_to  :system

    # If is an external resource, a synchronizer was used to import him.
    belongs_to  :synchronizer

    # If this resource is the homologation (similar to parent of many instances ) will have many children
    has_many    :children , :class_name => self , :foreign_key => 'parent_id'

    # External module method ( stored in ModelSystemMethods ) that search for the instance of this resource referes
    validate    :parent_existance, :if => :parent_id_changed?

    # This method is for complete resource if any field can't be filled with data from external system before save
    def complete_resource_before_import
      nil
    end

  # ------------------------------ BASIC METHODS ---------------------------------------------
  GLOBAL_ACTIONS = [ { :new  => "true" } ]

  def self.actions
    super( GLOBAL_ACTIONS )
  end

    # Detail of this resource
    # def detail
    #   return "#{self.agency.name} #{self.agency.code} - N°##{self.agency_number} - $#{self.total_liquidated}"
    # end

  # ------------------------------ CURRENT ACCOUNT & MOVEMENTS  ---------------------------------------------

    # Amount never can't be negative... The sign of the amount is given for the class
    before_create :amount_never_can_be_negative
    def amount_never_can_be_negative
      if self.amount <= 0
        self.errors.add( :amount , "El importe debe ser positivo." )
        return false
      end
    end

    # Get current account movement of this resource
    def current_account_movement
      CurrentAccountMovement.find_by( :resource_type => "#{self.class.model_name}" , :resource_id => self.id )
    end

    # Check if is any movment currentaccount created for this resource
    def is_movement_created?
      return current_account_movement.present?
    end

    # Generate movement from Agency <> Current Account as soon is created this registry
    def generate_movement
      unless is_movement_created?
        ActiveRecord::Base.transaction do
          if self.current_account.present?
            result_mov = self.current_account.generate_movement(self)
            if result_mov
              self.status = self.class.statuses[ :affected ]
              self.save
            end
          end
        end
      end
    end

		# For check if attribute is validated or not
		def valid_attribute?(attribute)
		  self.errors[attribute].blank?
		end

		# Change status of debit_note and generate movement of current account
		def set_approved
		  if DebitNote.statuses.key( self.status ) == :waiting
		    self.status = DebitNote.statuses[ :approved ]
		    self.save
		    self.generate_movement
		  end
		end

		# Reject debit_note
		def set_rejected
		  if DebitNote.statuses.key( self.status ) == :waiting
		    self.status = DebitNote.statuses[ :rejected ]
		    self.save
		  end
		end

		# Check if the instance can be modified by status != :deteled
		def can_be_modified?
		  self.waiting?
		end

		# Check if instance can be modified to deleted
		def can_be_destroyed?
		  self.can_be_modified and !self.deleted?
		end

end
