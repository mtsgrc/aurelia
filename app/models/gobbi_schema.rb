class GobbiSchema < ActiveRecord::Base

  # ------------------------------------------------ Global

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods
    
    # SimplestStatus
    extend SimplestStatus
    statuses :active, :inactive, :deleted

    # Audited
    audited

  # ------------------------------------------------ Self

    # For logo of company
    belongs_to :company

    validates :name       , :presence => true , :uniqueness => true
    validates :company_id , :presence => true
    validates :status     , :presence => true

    # Global method for displaying data
    def detail
      "#{self.name}"
    end

end
