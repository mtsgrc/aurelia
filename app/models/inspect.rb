class Inspect < ActiveRecord::Base

  # ------------------------------------------------ Global

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # Audited
    audited

  # ------------------------------------------------ Self

    belongs_to :inspector  , :class_name => 'Inspector'

    # Global method for displaying data
    def detail
      "#{ self.run_at } - #{ self.inspector.detail } > #{ self.response_status }"
    end

end
