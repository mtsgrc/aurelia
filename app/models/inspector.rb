class Inspector < ActiveRecord::Base

  # ------------------------------------------------ Global

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods
    # SimplestStatus

    extend SimplestStatus
    statuses :active, :inactive, :deleted
    STATUS_TRANSITIONS = {
      :active => [ :inactive ] ,
      :inactive => [ :active ]
    }
    enum frecuency_unit: [ :minutes , :hours , :days ]

    # Audited
    audited

  # ------------------------------------------------ Self

      # validates :sector_id  , :presence => true
      # validates :name       , :presence => true
      # validates :status     , :presence => true

      has_many :inspects  , :class_name => 'Inspect', :foreign_key => 'inspector_id'

      # Global method for displaying data
      def detail
        # "#{self.symbol_name} [e/#{self.frecuency_amount}#{Inspector.frecuency_units.key(self.frecuency_unit).first}]"
        "#{self.symbol_name} [e/#{ self.frecuency_amount }#{ self.frecuency_unit.first }]"
      end

    GLOBAL_ACTIONS = [ { :new  => "true" } ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end
    #
    ACTIONS = [ { :get_inspector_trigger => "true" } ]

    def actions
      super( ACTIONS )
    end


end
