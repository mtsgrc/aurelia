class Liquidation < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # SimplestStatus
    extend SimplestStatus
    statuses :approved, :rejected, :deleted, :affected

    # Audited
    audited

  # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------


  	validates  :agency_id 			 , :presence => true
  	validates  :company_id			 , :presence => true
  	validates  :agency_number		 , :presence => true
  	validates  :unique_number		 , :presence => true
  	validates  :liquidated_at		 , :presence => true
  	validates  :total_liquidated , :presence => true

  	belongs_to :agency
  	belongs_to :company
  	belongs_to :user , :class_name => 'User', :foreign_key => 'user_id'

  # ------------------------------ FOR EXTERNAL SYSTEMS DATA ---------------------------------------------

    # Include Model Synchronizer for import data
    extend  SynchronizerMethods

    # Include Model System Methods
    include ModelSystemMethods

    # Always point to a system, if it is external system, will be distinct of 0 ( This system code 'AU+' )
    belongs_to  :system

    # If is an external resource, a synchronizer was used to import him.
    belongs_to  :synchronizer

    # If this resource is the homologation (similar to parent of many instances ) will have many children
    has_many    :children , :class_name => self , :foreign_key => 'parent_id'

    # External module method ( stored in ModelSystemMethods ) that search for the instance of this resource referes
    validate    :parent_existance, :if => :parent_id_changed?

    # This method is for complete resource if any field can't be filled with data from external system before save
    def complete_resource_before_import
      nil
    end

  # ------------------------------ BASIC METHODS ---------------------------------------------

    GLOBAL_ACTIONS = [ ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end

    # Detail of this resource
    def detail
      return "#{self.company.code} | #{self.agency.code} | #{self.agency_number}"
    end

    # Detail of this resource
    def full_detail
      return "#{self.company.name} - #{self.agency.name} - N°##{self.agency_number} - $#{self.total_liquidated}"
    end

  # ------------------------------ CURRENT ACCOUNT & MOVEMENTS  ---------------------------------------------

    # Get current account movement of this resource
    def current_account_movement
      CurrentAccountMovement.find_by( :resource_type => "#{self.class.model_name}" , :resource_id => self.id )
    end

    # Check if is any movment currentaccount created for this resource
    def is_movement_created?
      return current_account_movement.present?
    end

    # Get current account if movement is created yet
    def current_account
      if self.agency.present?
        if self.agency.parent.present?
          self.agency.parent.current_account if self.agency.parent.present?
        end
      end
    end

    # Return string
    def get_problems_to_affect
      return "No existe agencia *agency_id* => #{ self.agency_id }" unless self.agency.present?
      return "No existe homologación de agencia #{self.agency.name}" unless self.agency.parent.present?
      return "No existe cuenta corriente de agencia asociada #{self.agency.parent.detail}" unless self.agency.parent.current_account.present?
      if self.agency.parent.current_account.current_period.present?
        resource_datetime = self.liquidated_at
        unless ( ( resource_datetime.to_date >= self.agency.parent.current_account.current_period.initial_date ) and ( resource_datetime.to_date < Date.today ) )
          return "El recurso está fuera del rango de fechas del período activo de la cuenta"
        end
      else
        return "No existe período activo en la cuenta"
      end
      return "Ningun problema para homologar"
    end


    # Generate movement from Agency <> Current Account as soon is created this registry
    def generate_movement
      unless is_movement_created?
        ActiveRecord::Base.transaction do
          if self.current_account.present?
            result_mov = self.current_account.generate_movement(self)
            if result_mov
              self.status = self.class.statuses[ :affected ]
              self.save
            end
          end
        end
      end
    end

  # ------------------------ [END] SPECIALS ------------------------

    # Get Credit Terminal Lot Closures from date to date
    def self.between_dates from_date , to_date

      if from_date.class == Date
        date_from = from_date #.to_datetime.change( :hour => 00 , :min => 00 , :sec => 00 )
      elsif [DateTime,Time,ActiveSupport::TimeWithZone].include?( from_date.class )
        date_from = from_date.to_date
      end

      if to_date.class == Date
        date_to = to_date #.to_datetime.change( :hour => 23 , :min => 59 , :sec => 59 )
      elsif [DateTime,Time,ActiveSupport::TimeWithZone].include?( to_date.class )
        date_to = to_date.to_date
      end

      if date_from <= date_to
        return self.where( "DATE( liquidated_at ) >= ? AND DATE( liquidated_at ) <= ?" , date_from.to_s(:db) , date_to.to_s(:db) )
      end

    end

    # Get Credit Terminal Lot Closures from date
    def self.from_date from_date
      return self.between_dates( from_date , from_date )
    end



end
