class LotClosureTransaction < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # SimplestStatus
    extend SimplestStatus
    statuses :affected, :rejected, :deleted

    # Audited
    audited

  # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

    # EXTERNAL SYSTEM DATA
    belongs_to  :lot_closure

  	# types of current_accounts
  	enum kind: [ :purchase_confirmation , :purchase_return , :purchase_annulment ]

  # ------------------------------ FOR EXTERNAL SYSTEMS DATA ---------------------------------------------

    # Include Model Synchronizer for import data
    extend  SynchronizerMethods

    # Include Model System Methods
    include ModelSystemMethods

    # Always point to a system, if it is external system, will be distinct of 0 ( This system code 'AU+' )
    belongs_to  :system

    # If is an external resource, a synchronizer was used to import him.
    belongs_to  :synchronizer

    # If this resource is the homologation (similar to parent of many instances ) will have many children
    has_many    :children , :class_name => self , :foreign_key => 'parent_id'

    # External module method ( stored in ModelSystemMethods ) that search for the instance of this resource referes
    validate    :parent_existance, :if => :parent_id_changed?

  # ------------------------------ BASIC METHODS ---------------------------------------------

    GLOBAL_ACTIONS = [ ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end

    # Detail of this resource
    def detail
      return "#{self.lot_closure.detail} - Transacción Nro##{self.global_number}"
    end

    # Full Detail of this resource
    # def full_detail
    #   return "#{self.system.name} - #{self.credit_terminal.code} - Lote ##{self.lot_number} - $#{self.amount}"
    # end
    def can_be_modified?
      false
    end

end
