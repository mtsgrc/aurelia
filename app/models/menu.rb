class Menu < ActiveRecord::Base

  # ------------------------------------------------ Global

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods
    
    # SimplestStatus
    extend SimplestStatus
    statuses :visible, :hidden, :testing, :deleted

    # Audited
    audited

  # ------------------------------------------------ Self

    enum kind: [ :module , :menu , :resource ]
    belongs_to :parent, :class_name => 'Menu', :foreign_key => 'parent_id'
    has_many :children, :class_name => 'Menu', :foreign_key => 'parent_id'

    validates :name, :presence => true
    validates :symbol, :presence => true
    validates :kind, :presence => true

    validate :module_type_has_not_parent, :on => [ :create , :update ]
    validate :sub_module_and_resource_types_have_parent, :on => [ :create , :update ]

    validate :cant_destroy_if_has_children, :on => [ :destroy ]

    before_validation :default_icon_before_save
    before_validation :default_status_before_save

    GLOBAL_ACTIONS = [ { :new  => "true" } ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end

    # Module menus type never have parent menu
    def module_type_has_not_parent
      self.errors.add(:base, "Los módulos no pueden pertenecer a otro menú") if self.module? and self.parent != nil
    end

    # Menu with children cant destroy
    def cant_destroy_if_has_children
      self.errors.add(:base, "Hay menús o recursos que pertenecen al elemento a borrar") unless self.children.empty?
    end

    # Sub module and Resource menu types, always have parent menu
    def sub_module_and_resource_types_have_parent
      self.errors.add(:base, "Los menús y recursos deben pertenecer a otro menú") if (self.menu? or self.resource?) and parent.nil?
    end

    # scope: Get the menus inside this Module
    scope :get_modules, -> { where( kind: Menu::kinds[ :module ] ) }

    # Get the menus inside this Module
    def get_menus_and_resources
      Menu.where( parent_id: self.id ).where( kind: [ Menu::kinds[ :menu ] , Menu::kinds[ :resource ] ] )
    end

    # Get the full name
    def get_full attr = :name
      arr_data = [ self.read_attribute(attr).to_s ]
      if self.parent_id?
        arr_data << self.parent.read_attribute(attr).to_s
        if self.parent.parent_id?
          arr_data << self.parent.parent.read_attribute(attr).to_s
        end
      end
      return arr_data.reverse.join(' > ') if attr == :name
      return "/" + arr_data.reverse.join('/') if attr == :symbol
    end

    # Get the resources inside this menu
    def get_resources
      Menu.where( parent_id: self.id ).where( kind: Menu::kinds[ :resource ] )
    end

    # Global method for displaying data
    def detail
      "#{self.get_full}"
    end

    private

      # Setting default icons by menu kind
      def default_icon_before_save
        self.faicon ||= 'fa-circle'      if self.module?
        self.faicon ||= 'fa-caret-right' if self.menu?
        self.faicon ||= 'fa-angle-right' if self.resource?
      end

      # Setting default status
      def default_status_before_save
        self.status ||= Menu::VISIBLE
      end

end
