class MovementAffectation < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

  	# SimplestStatus
    extend SimplestStatus
    statuses :approved, :deleted

  	# Audited
    audited

    # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

  	belongs_to :movement_from  , :class_name => 'CurrentAccountMovement', :foreign_key => 'movement_from_id'
  	belongs_to :movement_to    , :class_name => 'CurrentAccountMovement', :foreign_key => 'movement_to_id'

    # has_many :agency_company , :through => :movement_to
    has_one :agency, :through => :movement_to
    has_one :company, :through => :movement_to
    has_one :current_account, :through => :movement_to

  	# types of current_accounts
  	enum kind: [ :credit , :debit ]

    validates :amount 							, :presence => true
    validates :status         			, :presence => true


  # ------------------------------ BASIC METHODS ---------------------------------------------

	# Amount cant be negative, because the kind of this movement, indicates the sign
	before_create :amount_cant_be_zero_or_nil
	def amount_cant_be_zero_or_nil
    if ( ( self.amount == nil ) or ( self.amount == 0 ) )
	    self.errors.add( :base, "El monto no puede ser cero" )
      return false
    end
	end

	# Amount cant be negative, because the kind of this movement, indicates the sign
	before_create :check_movement_to_amount_can_be_passed
	def check_movement_to_amount_can_be_passed
    movement_affectations = MovementAffectation.where( :movement_to_id => self.movement_to.id )
    if ( ( movement_affectations.map(&:amount).compact.sum + self.amount ) > self.movement_to.amount )
	    self.errors.add( :base, "El monto a afectarse es superado" )
      return false
    end
	end

	# Amount cant be negative, because the kind of this movement, indicates the sign
	after_create :check_movement_to_amount_achieved
	def check_movement_to_amount_achieved

    movement_affectations = MovementAffectation.where( :movement_to_id => self.movement_to.id )
    movement_affectors = MovementAffectation.where( :movement_from_id => self.movement_from.id )

    if ( self.movement_from.amount == self.amount )
      self.movement_from.update_attributes( :status => CurrentAccountMovement.statuses[ :approved_and_affected_completely ] )
    end

    if ( movement_affectations.map(&:amount).compact.sum == self.movement_to.amount )
      self.movement_to.update_attributes( :status => CurrentAccountMovement.statuses[ :approved_and_affected_completely ] )
    end

	end

  after_create :affect_remaining_amount_of_movements
  def affect_remaining_amount_of_movements
    self.movement_from.update_attribute( :remaining_amount , ( self.movement_from.remaining_amount - self.amount ) )
    self.movement_to.update_attribute( :remaining_amount , ( self.movement_to.remaining_amount - self.amount ) )
  end

  after_create :affect_agency_company_period
  def affect_agency_company_period
    current_account_period = current_account.current_period
    current_agency_company_period = current_account_period.agency_company_periods.find_by( :agency_company => agency_company )
    unless current_agency_company_period.present?
      agency_company_period_params = {}
      agency_company_period_params[ :current_account_period_id ] = self.current_account.current_period.id
      agency_company_period_params[ :agency_company_id ]         = agency_company.id

      agency_company_period_params[ :initial_credit ]            = self.amount
      agency_company_period_params[ :initial_debit ]             = 0.00
      agency_company_period_params[ :final_credit ]              = 0.00
      agency_company_period_params[ :final_debit ]               = 0.00

      agency_company_period_params[ :status ]                    = AgencyCompanyPeriod::statuses[ :active ]

      agency_company_period_new = AgencyCompanyPeriod.new( agency_company_period_params )
      ret_agency_company_period_new = agency_company_period_new.save!
      if ret_agency_company_period_new
        current_agency_company_period = agency_company_period_new
      end
    end
    current_agency_company_period.affect_final_amount( self )

  end

  def agency_company
    if self.agency and self.company
      AgencyCompany.find_by( :agency_id => self.agency.id , :company_id => self.company.id )
    end
  end

end
