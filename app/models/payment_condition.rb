class PaymentCondition < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods
    
    # SimplestStatus
    extend SimplestStatus
    statuses :active, :inactive, :deleted

    # Audited
    audited

  # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

    validates  :name, :presence => true
    validates  :code, :uniqueness => true
    validates  :status, :presence => true

  # ------------------------------ FOR EXTERNAL SYSTEMS DATA ---------------------------------------------

    # Include Model Synchronizer for import data
    extend  SynchronizerMethods

    # Include Model System Methods
    include ModelSystemMethods

    # Always point to a system, if it is external system, will be distinct of 0 ( This system code 'AU+' )
    belongs_to  :system

    # If is an external resource, a synchronizer was used to import him.
    belongs_to  :synchronizer

    # If this resource is the homologation (similar to parent of many instances ) will have many children
    has_many    :children , :class_name => self , :foreign_key => 'parent_id'

    # External module method ( stored in ModelSystemMethods ) that search for the instance of this resource referes
    validate    :parent_existance, :if => :parent_id_changed?

    # This method is for complete resource if any field can't be filled with data from external system before save
    def complete_resource_before_import
      nil
    end

  # ------------------------------ BASIC METHODS ---------------------------------------------

    GLOBAL_ACTIONS = [ { :new  => "true" } ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end

    # Global method for displaying data
    def detail
      "(#{self.code}) #{self.name}"
    end

end
