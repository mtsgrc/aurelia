class Permission < ActiveRecord::Base

		# ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

	    # Include Global methods
	    include GlobalMethods
	    extend ClassGlobalMethods

		# Audited
		audited

		belongs_to :role

		validates  :role_id     		, :presence => true
		validates  :resource_class  , :presence => true
		validates  :action        	, :presence => true

	  # ------------------------------ BASIC METHODS ---------------------------------------------
	  GLOBAL_ACTIONS = [ { :new  => "true" } ]

	  def self.actions
	    super( GLOBAL_ACTIONS )
	  end

		def detail

		end

		# Check if the instance can be modified by status != :deteled
		def can_be_modified?
			false
		end

end
