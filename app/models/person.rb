class Person < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # SimplestStatus
    extend SimplestStatus
    statuses :active, :inactive, :deleted

    # Audited
    audited

  # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

    def self.default_scope
      where.not( id_person: '0' )
    end

    belongs_to :role, :class_name => 'Role' , :foreign_key => 'role_id'

    has_one :user, :class_name => 'User' , :foreign_key => 'person_id'

    validates_uniqueness_of :dni, :scope => [ :id_person, :gobbi_schema_id ]
    validates :first_name, :presence => true
    validates :last_name, :presence => true
    validates :status, :presence => true

    # validate :cannot_change_id_person

    #after_create :create_user_of_person

  # ------------------------------ FOR EXTERNAL SYSTEMS DATA ---------------------------------------------

    GLOBAL_ACTIONS = [ { :new  => "true" } ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end

    # Include Model Synchronizer for import data
    extend  SynchronizerMethods

    # Include Model System Methods
    include ModelSystemMethods

    # Always point to a system, if it is external system, will be distinct of 0 ( This system code 'AU+' )
    belongs_to  :system

    # If is an external resource, a synchronizer was used to import him.
    belongs_to  :synchronizer

    # If this resource is the homologation (similar to parent of many instances ) will have many children
    has_many    :children , :class_name => self , :foreign_key => 'parent_id'

    # External module method ( stored in ModelSystemMethods ) that search for the instance of this resource referes
    validate    :parent_existance, :if => :parent_id_changed?

    # This method is for complete resource if any field can't be filled with data from external system before save
    def complete_resource_before_import
      nil
    end

  # ------------------------------ BASIC METHODS ---------------------------------------------

    ACTIONS = [ { :get_new_user_from_native_person_inmodal => "( can_create_user? == true )" } ]
    def actions
      super( ACTIONS )
    end

    # Global method for displaying data
    def detail
      "#{self.id_person} - #{self.first_name} #{self.last_name}"
    end

    # Global method for displaying data
    def full_name
      "#{self.last_name}, #{self.first_name}"
    end

    # Global method for displaying data
    def username_as_it_should_be
      arr_names = []
      arr_names << self.last_name.split.first
      arr_names << self.first_name.split.first
      str_names = arr_names.join
      str_names = ActiveSupport::Inflector.transliterate( str_names )
      str_names = str_names.underscore
    end

    def create_user_of_person_from_names
      user = User.new( :username => self.username_as_it_should_be , :password => self.username_as_it_should_be , :password_confirmation => self.username_as_it_should_be, :email => nil , :person_id => self.id , :status => User::INACTIVE )
      user.save
    end

    def self.generate_or_homologate force_all = false
      require 'string/similarity'

      if force_all
        persons = self.external.order( :gobbi_schema_id => :asc , :status => :asc )
      else
        persons = self.external.where( :parent_id => nil ).order( :gobbi_schema_id => :asc , :status => :asc )
      end

      persons.each do | person |

        person_parent = self.local.find_by( :dni => person.dni )

        if person_parent.present?

          person.parent_id = person_parent.id
          person.save!

        else

          people = [] # Para los casos que son varios distintos
          similar_people = self.external.where( :dni => person.dni )

          if similar_people.count == 1

            people << similar_people.first

          elsif similar_people.count >= 2

            arr_full_names  = similar_people.map{ |p| p.full_name }
            arr_id_persons  = similar_people.map{ |p| p.id_person }
            arr_statuses    = similar_people.map{ |p| p.status }

            if arr_full_names.uniq.count == 1
              if arr_statuses.uniq.count == 1
                if arr_id_persons.uniq.count == 1
                  people << similar_people.sample
                else
                  people << similar_people.order( :gobbi_schema_id => :asc ).first
                end
              else
                if arr_id_persons.uniq.count == 1
                  people << similar_people.order( :status => :asc ).first
                else
                  people << similar_people.order( :status => :asc , :gobbi_schema_id => :asc ).first
                end
              end

            elsif arr_full_names.uniq.count == 2

              person_1 = similar_people.map{ |p| p if p.full_name == arr_full_names.uniq.first }.compact.first
              person_2 = similar_people.map{ |p| p if p.full_name == arr_full_names.uniq.last }.compact.first

              if String::Similarity.cosine( person_1.full_name , person_2.full_name ) > 0.5
                people << person_1 if person_1.full_name.size > person_2.full_name.size
                people << person_2 if person_2.full_name.size >= person_1.full_name.size
              end

            elsif arr_full_names.uniq.count > 2

              similar_people.each do | similar_person |

                unless people.present?
                  people << similar_person
                else
                  people.each do | p |
                    if ( String::Similarity.cosine( similar_person.full_name , p.full_name ) < 0.5 ) and
                      people << similar_person
                    end
                  end
                end

              end

            end

          end

          #Create local person
          if people.present?

            people.each do | new_person |

              new_obj = Person.new()
              new_obj.dni        = new_person.dni
              new_obj.id_person  = new_person.id_person
              new_obj.first_name = new_person.first_name
              new_obj.last_name  = new_person.last_name
              new_obj.status     = new_person.status
              new_obj.role_id    = Role.find_by( :name => "Ingresante" ).id

              return_new_person = new_obj.save

            end

            if people.count == 1
              person.parent_id = people.first.id
              person.save!
            end

          end

        end
      end

      # native_people   = []
      # persons_grouped = self.all.group_by(&:dni)
      #
      # # (
      # persons_grouped.each do | dni , persons |
      #
      #   people = []can_create_user?
      #       people << { :dni => person.dni , :id_person => person.id_person , :first_name => person.first_name , :last_name => person.last_name , :status => persons.status }
      #     else
      #       people.each do | peopl |
      #         if String::Similarity.cosine( person.full_name , peopl.full_name ) < 0.5
      #       end
      #     end
      #
      #   end
      #
      #
      #
      #   if persons.count == 1
      #
      #     person = persons.first
      #     people << { :dni => person.dni , :id_person => person.id_person , :first_name => person.first_name , :last_name => person.last_name , :status => persons.status }
      #
      #   elsif persons.count > 1
      #
      #     arr_full_names  = persons.map{ |p| "#{p.last_name}, #{p.first_name}" }
      #     arr_id_persons  = persons.map{ |p| p.id_person }
      #     arr_statuses    = persons.map{ |p| p.status }
      #
      #     hash_names = { :last_name => "" , :first_name => "" }
      #     if arr_full_names.uniq.count == 1
      #
      #       person = persons.sample
      #
      #     elsif arr_full_names.uniq.count == 2
      #
      #       person_1 = persons.map{ |p| p if p.full_name == arr_full_names.uniq.first }.compact.first
      #       person_2 = persons.map{ |p| p if p.full_name == arr_full_names.uniq.last }.compact.first
      #
      #       if String::Similarity.cosine( person_1.full_name , person_2.full_name ) > 0.5
      #         person = person_1 if person_1.full_name.size > person_2.full_name.size
      #         person = person_2 if person_2.full_name.size >= person_1.full_name.size
      #       end

      #     end
      #
      #     hash_names[ :first_name ] = person.first_name
      #     hash_names[ :last_name ] = person.last_name





          # if arr_id_persons.uniq.count == 1
          #   if arr_id_persons.uniq.count == 1
          #     if arr_id_persons.uniq.count == 1
          #       person
          #       people << { :dni => person.dni , :id_person => person.id_person , :first_name => person.first_name , :last_name => person.last_name }
          #     else
          #     end
          #   else
          #   end
          # else
          # end

          # people << { :dni => person.dni , :id_person => person.id_person , :first_name => person.first_name , :last_name => person.last_name }

          # if persons.map(&:status)
          # if persons.map(&:id_person).uniq.count == 1
          # else
          # end
        # end

        # persons.each do | person |

          # unless people.present?
          #   # people << { :id_person => person.id_person_digits , :first_name => person.first_name , :last_name => person.last_name }
          #   people << { :dni => person.dni , :id_person => person.id_person , :first_name => person.first_name , :last_name => person.last_name }
          # else
          #   if people.count == 1
          #     if String::Similarity.cosine( person.full_name , "#{people.first[ :last_name ]}, #{people.first[ :first_name ]}" ) < 0.5
          #       people << name
          #     end
          #   elsif people.count > 1
          #   end
          # end

        # end

      #   if people.count == 1
      #     native_people << people.first
      #   else
      #   end
      #
      # end
      # ).reduce({},:merge).count

      #cata_persons = Person.where( 'id_person LIKE "CATA%" ').count
      # string_difference_percent("asd","dd")

    end

    def can_create_user?
      if native?
        if user.present?
          return false
        else
          return true
        end
      else
        return false
      end
    end

    private
    # Validation for not permit changing :id_person attribute of person, making it uniqueness
    # def cannot_change_id_person
    #   errors.add( :id_person, "no puede ser modificado" ) if self.id_person_changed? && self.persisted?
    # end

end
