class PersonCheck < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # SimplestStatus
    extend SimplestStatus
    statuses :active, :inactive, :deleted

    enum kind: [  :input, :output ]

    # Audited
    audited

  # ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------

    belongs_to :person
    belongs_to :biometric

  # ------------------------------ FOR EXTERNAL SYSTEMS DATA ---------------------------------------------

    # Include Model Synchronizer for import data
    extend  SynchronizerMethods

    # Include Model System Methods
    include ModelSystemMethods

    # Always point to a system, if it is external system, will be distinct of 0 ( This system code 'AU+' )
    belongs_to  :system

    # If is an external resource, a synchronizer was used to import him.
    belongs_to  :synchronizer

    # If this resource is the homologation (similar to parent of many instances ) will have many children
    has_many    :children , :class_name => self , :foreign_key => 'parent_id'

    # External module method ( stored in ModelSystemMethods ) that search for the instance of this resource referes
    validate    :parent_existance, :if => :parent_id_changed?

    # This method is for complete resource if any field can't be filled with data from external system before save
    def complete_resource_before_import
      nil
    end

  # ------------------------------ BASIC METHODS ---------------------------------------------

    GLOBAL_ACTIONS = [ ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end


    # Global method for displaying data
    def detail
      "#{self.biometric.detail} ( #{self.check_at} ) > #{self.person.full_name}"
    end

    def check_at_turn
      if ( ( self.check_at >= self.check_at.change(hour: 0) ) and ( self.check_at < self.check_at.change(hour: 6) ) )
        "Madrugada"
      elsif ( ( self.check_at >= self.check_at.change(hour: 6) ) and ( self.check_at < self.check_at.change(hour:12) ) )
        "Mañana"
      elsif ( ( self.check_at >= self.check_at.change(hour:12) ) and ( self.check_at < self.check_at.change(hour:18) ) )
        "Tarde"
      elsif ( ( self.check_at >= self.check_at.change(hour:18) ) and ( self.check_at < self.check_at.change(hour:24) ) )
        "Noche"
      end
    end

    def last_check
      PersonCheck.where( "person_id = ? AND biometric_id = ? AND id < ?" , self.person , self.biometric , self.id  ).order('id DESC').limit(1).first
    end

    def input_or_output
      # byebug
      if ! self.kind.present?
        last_check = self.last_check
        if ( last_check.present? ) and ( ( ( self.check_at - last_check.check_at) / 3600 ) < 12 ) and ( last_check.input_or_output == PersonCheck.kinds[ :input ] )
          self.kind = PersonCheck.kinds[ :output ]
        else
          self.kind = PersonCheck.kinds[ :input ]
        end
        self.save!
      end
      # return PersonCheck.kinds[ self.kind.to_sym ]
      self.kind
    end


end
