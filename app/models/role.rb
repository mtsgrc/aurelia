class Role < ActiveRecord::Base

  def self.default_scope
    where.not(name: '-- God --')
  end

  # ------------------------------------------------ Global

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # SimplestStatus
    extend SimplestStatus
    statuses :active, :inactive, :deleted

    # Audited
    audited

  # ------------------------------------------------ Self

      belongs_to :sector, :class_name => 'Sector', :foreign_key => 'sector_id'
      has_one :company, :through => :sector
      # has_many :menus, :class_name => 'Menu', :foreign_key => 'parent_id'
      has_many :permissions, :class_name => 'Permission', :foreign_key => 'role_id'

      validates :sector_id  , :presence => true
      validates :name       , :presence => true
      validates :status     , :presence => true

      # Global method for displaying data
      def detail
        "#{self.name} en #{self.sector.name}"
      end

    GLOBAL_ACTIONS = [ { :new  => "true" } ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end

    ACTIONS = [ { :get_assign_permissions => "true" } ]

    def actions
      super( ACTIONS )
    end


end
