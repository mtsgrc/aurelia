class Synchronizer < ActiveRecord::Base

  # ------------------------------------------------ Global

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods
    
    # SimplestStatus
    extend SimplestStatus
    statuses :active, :inactive, :deleted

    # Audited
    audited

  # ------------------------------------------------ Self

		include ExternalSystems::DataGetter

		enum kind: [ :get, :set ]

		belongs_to :connection, :class_name => "Connection", :foreign_key => "connection_id"
		has_one :system, :through => :connection

		validates :connection_id , :presence => true
		validates :kind									, :presence => true
		validates :table_name						, :presence => true
		validates :status								, :presence => true

    GLOBAL_ACTIONS = [ { :new  => "true" } ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end


		# Get detail of this Data Accessor
		def detail
			"#{ I18n.t("kinds.synchronizer.single." + self.kind) } de #{self.table_name.classify.constantize.model_name.human(:count => 2) }"
		end

		# Global method for displaying resource
		def detail
			"#{self.system.name} - #{self.kind} (#{self.table_name})"
		end

end
