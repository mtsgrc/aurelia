class System < ActiveRecord::Base

  # ------------------------------ BASIC GLOBAL HEADER ---------------------------------------------

		# Include Global methods
		include GlobalMethods
		extend ClassGlobalMethods

		# SimplestStatus
		extend SimplestStatus
		statuses :active, :inactive, :deleted

		# Audited
		audited

	# ------------------------------ OWN VALIDATIONS AND REFERENCES ---------------------------------------------
	
		has_many :connections		, dependent: :destroy
		has_many :synchronizers	, through: :connections, dependent: :destroy

		validates :code   , :presence => true
		validates :name   , :presence => true
		validates :status , :presence => true

		GLOBAL_ACTIONS = [ { :new  => "true" } ]

		def self.actions
			super( GLOBAL_ACTIONS )
		end

		def self.this
			System.find(0)
		end

		# All external systems
		scope :external, -> { where.not(id: 0) }

		# Global method for displaying data
	  def detail
	    "[#{self.code}] #{self.name}"
	  end



end
