class User < ActiveRecord::Base

  # ------------------------------------------------ Global

    # Include Global methods
    include GlobalMethods
    extend ClassGlobalMethods

    # SimplestStatus
    extend SimplestStatus
    statuses :active, :inactive, :deleted

    # Audited
    audited except: [ :last_sign_in_at, :current_sign_in_at, :last_sign_in_ip, :current_sign_in_ip, :last_seen ]

  # ------------------------------------------------ Self

    belongs_to :person, :class_name => 'Person' , :foreign_key => 'person_id'
    has_one :role, :class_name => 'Role', :through => :person
    has_many :permissions, :class_name => 'Permission', :through => :role

    # Devise
    devise  :database_authenticatable, :recoverable, :rememberable, :trackable, :lastseenable

    # Own of User Model
    validates :username, :presence => true , uniqueness: true
    # validates :email , uniqueness: true

    # validates :password, :presence => true, :on => :create , :confirmation => true
    # validates :password, :presence => true, :on => :update, :allow_blank => true , :confirmation => true , :if => :password_changed?
    #
    # validates :password_confirmation, :presence => true, :on => :create
    # validates :password_confirmation, :presence => true, :on => :update, :if => :password_changed?

    validates :password , :on => :create , :confirmation => true
    validates :password , :on => :update, :allow_blank => true , :confirmation => true
    # validates :password , :on => :update, :allow_blank => true , :confirmation => true , :if => :password_changed?

    # validates :password_confirmation, :presence => true, :on => :create
    # validates :password_confirmation, :presence => true, :on => :update
    # validates :password_confirmation, :presence => true, :on => :update, :if => :password_changed?

    validate :cannot_change_username
    validate :cannot_change_if_inactive

    before_destroy :cant_delete_root_user

    GLOBAL_ACTIONS = [ { :new  => "true" } ]

    def self.actions
      super( GLOBAL_ACTIONS )
    end

    ACTIONS = [ { :user_get_reset_password_inmodal => "( self.can_reset_password? )" } ]
    def actions
      super( ACTIONS )
    end

    def online
  		( last_seen ? last_seen > 5.minutes.ago : false )
    end

    def history
      Audited.audit_class.where(user_id: self.id).order(created_at: :desc)
    end

    def can_reset_password?
      !self.root? && self.active?
    end

    def password_changed?
    end


    # For check :#{status} of user before authentication
    def active_for_authentication?
      super && ( self.status == User::ACTIVE )
    end

    # Message for user inactive on login
    def inactive_message
      "Lo lamento, pero el acceso al sistema ha sido deshabilitado."
    end

    def root?
      self.username == 'root'
    end

    # Global method for displaying data
    def detail
      unless self.root?
        if self.person
          "#{person.full_name}"
        else
          "(@#{self.username})"
        end
      else
        "ROOT"
      end
    end

    # Global method for displaying data
    def person_full_name
      unless self.root?
        if self.person
          "(@#{self.username}) #{self.person.full_name}"
        else
          "(@#{self.username}) * SIN PERSONA ASIGNADA *"
        end
      else
        "ROOT"
      end
    end

    # Global method for displaying data
    def display_role
      unless self.root?
        if self.person
          "#{self.person.role.name}"
        else
          " * SIN PERSONA/ROL ASIGNADO * "
        end
      end
    end



    def set_reset_password_token
      raw, enc = Devise.token_generator.generate(self.class, :reset_password_token)
      update_columns(reset_password_token: enc, reset_password_sent_at: Time.now.utc)
      raw
    end

# PRIVATEEEEEEEEEE

    private

    # Validation for not permit changing :username attribute of user, making it uniqueness
    def cannot_change_username
      errors.add( :username, "no puede ser modificado") if username_changed? && self.persisted?
    end

    # cant_delete_root_user
    def cant_delete_root_user
      return false if self.root?
    end

    # Validation for not permit changing entire model if instance is :inactive
    def cannot_change_if_inactive
      errors.add( :username, "no puede ser modificado") if username_changed? && self.persisted?
    end


# FROM EXTERNAL SYSTEM

  #
  # # ------------------------------------------------ Global
  #
  #   # Include Global methods
  #   include GlobalMethods
  #
  #   # Include Global methods
  #   include ModelSystemMethods
  #
  #   # SimplestStatus
  #   extend SimplestStatus
  #   statuses :active, :inactive, :deleted
  #
  #   # Audited
  #   audited
  #
  # # ------------------------------------------------ Self
  #
  #   validates   :system_id          , :presence => true
  #
  #   validates   :external_id        , :presence => true
  #   validates   :external_jsondata  , :presence => true
  #
  #   validates   :status             , :presence => true
  #
  #   validates   :external_id        , :uniqueness => { :scope => :system_id }
  #
  #   belongs_to  :system , :class_name => 'System', :foreign_key => 'system_id'
  #   belongs_to  :user , :class_name => "#{model_name.name}", :foreign_key => 'resource_id'
  #
  #   validate    :resource_id_exists


end
