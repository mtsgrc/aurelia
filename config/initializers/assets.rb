
Rails.application.config.assets.precompile += %w( welcome.css )
Rails.application.config.assets.precompile += %w( welcome.js )


Rails.application.config.assets.precompile += %w( payment_conditions.css )
Rails.application.config.assets.precompile += %w( payment_conditions.js )

Rails.application.config.assets.precompile += %w( currencies.css )
Rails.application.config.assets.precompile += %w( currencies.js )

Rails.application.config.assets.precompile += %w( cost_centers.css )
Rails.application.config.assets.precompile += %w( cost_centers.js )

Rails.application.config.assets.precompile += %w( work_orders.css )
Rails.application.config.assets.precompile += %w( work_orders.js )

Rails.application.config.assets.precompile += %w( current_accounts.css )
Rails.application.config.assets.precompile += %w( current_accounts.js )

Rails.application.config.assets.precompile += %w( current_account_movements.css )
Rails.application.config.assets.precompile += %w( current_account_movements.js )

Rails.application.config.assets.precompile += %w( expenses.css )
Rails.application.config.assets.precompile += %w( expenses.js )

Rails.application.config.assets.precompile += %w( liquidations.css )
Rails.application.config.assets.precompile += %w( liquidations.js )

Rails.application.config.assets.precompile += %w( permissions.css )
Rails.application.config.assets.precompile += %w( permissions.js )

Rails.application.config.assets.precompile += %w( roles.css )
Rails.application.config.assets.precompile += %w( roles.js )



Rails.application.config.assets.precompile += %w( registration.css )
Rails.application.config.assets.precompile += %w( registration.js )

Rails.application.config.assets.precompile += %w( sessions.css )
Rails.application.config.assets.precompile += %w( sessions.js )

Rails.application.config.assets.precompile += %w( users.css )
Rails.application.config.assets.precompile += %w( users.js )

Rails.application.config.assets.precompile += %w( menus.css )
Rails.application.config.assets.precompile += %w( menus.js )



Rails.application.config.assets.precompile += %w( layout_for_jobs.css )
Rails.application.config.assets.precompile += %w( layout_for_jobs.js )
