# In config/initializers
Kaminari.configure do |config|
  config.default_per_page = 100
  config.param_name = 'q[page]'
end
