AureliaPlus::Application.routes.draw do

  default_url_options :host => "aurelia"

  #resources :work_orders, path: 'administration/constants/payment_conditions'

  devise_for :users, controllers: { sessions: 'users/sessions' , passwords: 'users/passwords' }

  # administration
  scope :administration do

    # accounts
    scope :accounts do

      resources :expenses do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end

      resources :credit_notes do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end

      resources :debit_notes do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end

      resources :liquidations do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end

      resources :current_accounts do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end

      get   '/current_accounts/:current_account_id/main'              , :controller => :current_accounts  , :action => :get_main_view       , :as => :get_current_account_main_view

      get   '/current_accounts/:current_account_id/assign_credits'    , :controller => :current_accounts  , :action => :get_assign_credits  , :as => :get_current_account_assign_credits
      post  '/current_accounts/:current_account_id/assign_credits'    , :controller => :current_accounts  , :action => :post_assign_credits , :as => :post_current_account_assign_credits

      get   '/current_accounts/:current_account_id/movements/:current_account_movement_id/generate_movement_affectations', :controller => :current_account_movements, :action => :get_new_movement_affectations_from_current_account_movement  , :as => :get_new_movement_affectations_from_current_account_movement
      post  '/current_accounts/:current_account_id/movements/:current_account_movement_id/generate_movement_affectations', :controller => :current_account_movements, :action => :post_new_movement_affectations_from_current_account_movement , :as => :post_new_movement_affectations_from_current_account_movement

      resources :current_account_movements, path: :movements do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end

      resources :current_account_periods do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end

    end

    # constants
    scope :constants do
      resources :agencies do
        member do
          get 'set/:status'   , action: :set_status, as: :set_status
        end
      end
      scope :agencies do
        get  '/:agency_id/homologate', :controller => :application , :action => :get_homologate  , :as => :get_homologate_agency
        post '/:agency_id/homologate', :controller => :application , :action => :post_homologate , :as => :post_homologate_agency
      end

      resources :agency_companies do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
      scope :agency_companies do
        get  '/:agency_company_id/edit_credit_maximum'  , :controller => :agency_companies , :action => :get_edit_credit_maximum , :as => :get_edit_credit_maximum
        post  '/:agency_company_id/edit_credit_maximum' , :controller => :agency_companies , :action => :post_edit_credit_maximum , :as => :post_edit_credit_maximum
      end
      resources :cost_centers do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
      resources :currencies do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
      resources :payment_conditions do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
    end

  end

  # systems
  scope :systems do
    scope :accesses do
      resources :users do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
        get  '/reset_password', :controller => :users, :action => :get_reset_password   , :as => :get_reset_password
        post '/reset_password', :controller => :users, :action => :post_reset_password  , :as => :post_reset_password
      end
      resources :menus do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
      resources :permissions
      # get   '/permissions'                 , :controller => :permissions  , :action => :index               , :as => :get_permissions_index
      # get   '/permissions/:role_id/main'              , :controller => :permissions  , :action => :get_main_view               , :as => :get_permissions_main_view
      # post  '/permissions/:role_id/assign_permissions'              , :controller => :permissions  , :action => :post_assign_permissions              , :as => :post_assign_permissions
      # post  '/permissions'              , :controller => :permissions  , :action => :post_assign_permissions              , :as => :post_assign_permissions

      # get  '/permissions/:role_id/assign_permissions'   , :controller => :permissions  , :action => :get_assign_permissions   , :as => :get_assign_permissions
      # post  '/permissions/:role_id/assign_permissions'  , :controller => :permissions  , :action => :post_assign_permissions  , :as => :post_assign_permissions

      # get  '/permissions/assign'   , :controller => :permissions  , :action => :get_assign_permissions   , :as => :get_assign_permissions
      # post  '/permissions/assign'  , :controller => :permissions  , :action => :post_assign_permissions  , :as => :post_assign_permissions
    end
    scope :constants do
      resources :systems do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
    end
    scope :external_systems do
      resources :connections, path: :connections do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
      resources :synchronizers, path: :synchronizers do
        member do
          get 'set/:status', action: :set_status, as: :set_status
          # get 'execute', action: :execute, as: :execute
          # get 'execute', action: :execute, as: :execute
        end
      end
      resources :gobbi_schemas, path: :gobbi_schemas do
        member do
          get 'set/:status', action: :set_status, as: :set_status
          # get 'execute', action: :execute, as: :execute
        end
      end
      resources :homologations, path: :homologations do
        member do
          get 'set/:status', action: :set_status, as: :set_status
          # get 'execute', action: :execute, as: :execute
        end
      end
    end
    scope :jobs do
      get  '/' => 'layout_for_jobs#index'
      get  '/:job_id/log', :controller => :layout_for_jobs, :action => :get_view_log, :as => :get_view_job_log
      get  '/:job_id/retry', :controller => :layout_for_jobs, :action => :get_retry_job, :as => :get_retry_job
      get  '/log/:job_id', :controller => :layout_for_jobs, :action => :get_raw_log, :as => :get_raw_job_log
      get  '/new', :controller => :layout_for_jobs, :action => :get_new   , :as => :get_new_job
      post '/new', :controller => :layout_for_jobs, :action => :post_new  , :as => :post_new_job
    end
    scope :aurelia do
      resources :feedback, path: :feedback do
        member do
          get 'set/:status', action: :set_status, as: :set_status
          # get 'execute', action: :execute, as: :execute
        end
      end
      get  '/inspinia' => 'layout_for_inspinia#index'
      get  '/about' => 'layout_for_aurelia_about#index'
    end
    resources :inspectors do
      member do
        get 'set/:status', action: :set_status, as: :set_status
      end
    end
    get  'inspectors/:inspector_id/trigger', :controller => :inspectors, :action => :get_inspector_trigger, :as => :get_inspector_trigger
  end

  scope :treasury do
    scope :lot_closures do
      resources :lot_closures do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
      resources :lot_closure_transactions do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
    end
    resources :bank_account_movements do
      member do
        get 'set/:status', action: :set_status, as: :set_status
      end
    end
    scope :bank_account_movements do
      get  '/:bank_account_movement_id/generate_current_account_movement' , :controller => :bank_account_movements, :action => :get_new_current_account_movement_from_bank_account_movement   , :as => :get_new_current_account_movement_from_bank_account_movement
      post '/:bank_account_movement_id/generate_current_account_movement' , :controller => :bank_account_movements, :action => :post_new_current_account_movement_from_bank_account_movement  , :as => :post_new_current_account_movement_from_bank_account_movement
      get  '/:bank_account_movement_id/generate_current_account_movements', :controller => :bank_account_movements, :action => :get_new_current_account_movements_from_bank_account_movement  , :as => :get_new_current_account_movements_from_bank_account_movement
      post '/:bank_account_movement_id/generate_current_account_movements', :controller => :bank_account_movements, :action => :post_new_current_account_movements_from_bank_account_movement , :as => :post_new_current_account_movements_from_bank_account_movement
      get  '/:bank_account_movement_id/create_card'                       , :controller => :bank_account_movements, :action => :get_new_bank_account_card_from_bank_account_movement_detail   , :as => :get_new_bank_account_card_from_bank_account_movement_detail
      post '/:bank_account_movement_id/create_card'                       , :controller => :bank_account_movements, :action => :post_new_bank_account_card_from_bank_account_movement_detail  , :as => :post_new_bank_account_card_from_bank_account_movement_detail
    end

    resources :cash_registers do
      member do
        get 'set/:status', action: :set_status, as: :set_status
      end
      resources :movements , controller: 'cash_register_movements' do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
    end
    scope :cash_registers do
      get  '/:cash_register_id/new_ingress' , :controller => :cash_registers, :action => :get_new_cash_register_ingress_movement_from_cash_register   , :as => :get_new_cash_register_ingress_movement_from_cash_register
      post '/:cash_register_id/new_ingress' , :controller => :cash_registers, :action => :post_new_cash_register_ingress_movement_from_cash_register  , :as => :post_new_cash_register_ingress_movement_from_cash_register
      get  '/:cash_register_id/new_egress' , :controller => :cash_registers, :action => :get_new_cash_register_egress_movement_from_cash_register     , :as => :get_new_cash_register_egress_movement_from_cash_register
      post '/:cash_register_id/new_egress' , :controller => :cash_registers, :action => :post_new_cash_register_egress_movement_from_cash_register    , :as => :post_new_cash_register_egress_movement_from_cash_register

      get  '/:cash_register_id/movements/:cash_register_movement_id/generate_current_account_movement' , :controller => :cash_register_movements, :action => :get_new_current_account_movement_from_cash_register_movement   , :as => :get_new_current_account_movement_from_cash_register_movement
      post '/:cash_register_id/movements/:cash_register_movement_id/generate_current_account_movement' , :controller => :cash_register_movements, :action => :post_new_current_account_movement_from_cash_register_movement  , :as => :post_new_current_account_movement_from_cash_register_movement
      get  '/:cash_register_id/movements/:cash_register_movement_id/generate_current_account_movements', :controller => :cash_register_movements, :action => :get_new_current_account_movements_from_cash_register_movement  , :as => :get_new_current_account_movements_from_cash_register_movement
      post '/:cash_register_id/movements/:cash_register_movement_id/generate_current_account_movements', :controller => :cash_register_movements, :action => :post_new_current_account_movements_from_cash_register_movement , :as => :post_new_current_account_movements_from_cash_register_movement
    end

    scope :constants do
      resources :bank_accounts do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
      resources :bank_account_cards do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
      resources :credit_terminals do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
      scope :credit_terminals do
        get  '/:credit_terminal_id/homologate', :controller => :application , :action => :get_homologate  , :as => :get_homologate_credit_terminal
        post '/:credit_terminal_id/homologate', :controller => :application , :action => :post_homologate , :as => :post_homologate_credit_terminal
      end
    end
  end

  # direction
  scope :direction do
    scope :constants do
      resources :companies do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
      resources :sectors do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
      resources :roles do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
      get  '/roles/:role_id/assign_permissions'   , :controller => :roles , :action => :get_assign_permissions  , :as => :get_assign_permissions
      post  '/roles/:role_id/assign_permissions'  , :controller => :roles , :action => :post_assign_permissions , :as => :post_assign_permissions
    end
  end


  # direction
  scope :personal do
    scope :constants do
      resources :people do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
      scope :people do
        get  '/:person_id/generate_user' , :controller => :people, :action => :get_new_user_from_native_person  , :as => :get_new_user_from_native_person
        post '/:person_id/generate_user' , :controller => :people, :action => :post_new_user_from_native_person , :as => :post_new_user_from_native_person
      end
    end
    scope :control do
      resources :person_checks do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
        member do
          get 'swap_kind', action: :swap_kind, as: :swap_kind
        end
      end
      resources :biometrics do
        member do
          get 'set/:status', action: :set_status, as: :set_status
        end
      end
    end
  end

  # match  '/*url/homologate' , :via => [ :get ] , :controller => :application , :action => :get_homologate_external_to_native_instance , :as => :get_homologate_external_to_native_instance
  # match  '/homologate(/:field_name)' , :via => [ :get ] , :controller => :application , :action => :get_homologate_external_to_native_instance , :as => :get_homologate_external_to_native_instance
  # post  '/:agency_id/homologate' , :controller => :application , :action => :post_homologate_external_to_native_instance , :as => :post_homologate_external_to_native_instance


    # get  'homologate' , :controller => :application , :action => :get_homologate_external_to_native_instance , :as => :get_homologate_external_to_native_instance
    # post 'homologate' , :controller => :application , :action => :post_homologate_external_to_native_instance , :as => :post_homologate_external_to_native_instance


  get  '/filter_search' , :controller => :application, :action => :filter_search  , :as => :filter_search
  get 'welcome/index'

  root to: 'layout_for_welcome#index'

end
