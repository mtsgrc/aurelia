# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:

# set :output, File.join( Rails.root, "tmp" , "cron_log.log" )
set :output, File.expand_path( File.dirname( File.dirname(__FILE__) ) + "/tmp/cron_log.log")
# set :environment, 'production'
set :environment, 'development'
job_type :cmd,  "cd :path && :task :output"
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# My Inspector trigger
# every 1.minute do
#   runner "Inspectors::Walker.trigger"
# end

# Learn more: http://github.com/javan/whenever

yesterday = Date.today.prev_day

# Update crontab every night
every 1.day, :at => '0:00 am' do
	cmd "whenever --update-crontab"
end

# Liquidations
every 1.day, :at => '0:30 am' do
	runner "LiquidationsImporterJob.set( wait_until: Time.now + 5.second ).perform_later( { :code_system_ref => 'PRC' , :date => '#{yesterday.strftime('%d/%m/%Y')}' } )"
	runner "LiquidationsImporterJob.set( wait_until: Time.now + 5.second ).perform_later( { :code_system_ref => 'PRP' , :date => '#{yesterday.strftime('%d/%m/%Y')}' } )"
end

# Lot Closures, Lot Closure Transactions && Bank Account Movements
every 1.day, :at => '1:00 am' do
	runner "LotClosuresImporterJob.set( wait_until: Time.now + 5.second ).perform_later( { :code_system_ref => 'LGC' , :date => '#{yesterday.strftime('%d/%m/%Y')}' } )"
	runner "LotClosureTransactionsImporterJob.set( wait_until: Time.now + 5.second ).perform_later( { :code_system_ref => 'NPS' } )"
	runner "BankAccountMovementsImporterJob.set( wait_until: Time.now + 5.second ).perform_later( { :code_system_ref => 'IBK' } )"
	runner "BankAccountMovementsImporterJob.set( wait_until: Time.now + 5.second ).perform_later( { :code_system_ref => 'HBN' } )"
	runner "BankAccountMovementsImporterJob.set( wait_until: Time.now + 5.second ).perform_later( { :code_system_ref => 'OGL' } )"
	runner "BankAccountMovementsImporterJob.set( wait_until: Time.now + 5.second ).perform_later( { :code_system_ref => 'RBN' } )"
	runner "BankAccountMovementsImporterJob.set( wait_until: Time.now + 5.second ).perform_later( { :code_system_ref => 'NCF' } )"
end

# Affecter
every 1.day, :at => '2:00 am' do
	runner "LiquidationsAffecterJob.set( wait_until: Time.now + 5.second ).perform_later( { :date => '#{yesterday.strftime('%d/%m/%Y')}' } )"
	runner "LotClosuresAffecterJob.set( wait_until: Time.now + 5.second ).perform_later( { :date => '#{yesterday.strftime('%d/%m/%Y')}' } )"
	runner "BankAccountMovementsAffecterJob.set( wait_until: Time.now + 5.second ).perform_later( { :date => '#{yesterday.strftime('%d/%m/%Y')}' } )"
end

# Person checks
every 1.day, :at => '3:00 am' do
	runner "PersonChecksImporterJob.set( wait_until: Time.now + 5.second ).perform_later( { :date => '#{yesterday.strftime('%d/%m/%Y')}' } )"
end
