class CreateWorkOrders < ActiveRecord::Migration
  def change

    create_table :work_orders do |t|

      t.integer     :number , :null => false , :index => true
      t.string      :detail , :null => false

      t.references  :user   , :index => true , :foreign_key => true , :null => false
      t.integer     :status , :null => false , :default => 0

      t.timestamps  :null => false

    end

    add_index :work_orders, [ :number ] , unique: true, name: 'index_unique_for_work_orders'

  end
end
