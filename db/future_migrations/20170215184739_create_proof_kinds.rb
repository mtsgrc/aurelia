class CreateProofKinds < ActiveRecord::Migration
  def change

    create_table :proof_kinds do |t|

      t.string      :symbol , :index => true, :null => false
      t.string      :name   , :index => true, :null => false
      t.string      :detail

      t.timestamps  :null => false

    end

    add_index :proof_kinds, [ :symbol ] , unique: true, name: 'index_unique_for_proof_kinds'

  end
end
