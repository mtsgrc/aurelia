class CreateProofs < ActiveRecord::Migration
  def change
    create_table :proofs do |t|

      t.string      :number     , :index => true , :null => false

      t.references  :proof_kind , index: true , foreign_key: true

      t.timestamps null: false

    end
  end
end
