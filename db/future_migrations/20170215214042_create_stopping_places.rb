class CreateStoppingPlaces < ActiveRecord::Migration
  def change

    create_table :stopping_places do |t|

      t.string     :symbol , :index => true , :null => false
      t.string     :name , :index => true , :null => false
      
      t.references :city , :index => true , :foreign_key => true

      t.timestamps :null => false

    end
    add_index :stopping_places, [ :symbol ] , unique: true, name: 'index_unique_for_stopping_places'
  end
end
