class CreateCountries < ActiveRecord::Migration
  def change

    create_table :countries do |t|

      t.string :alpha2  , :index => true , :null => false
      t.string :name    , :index => true , :null => false

      t.timestamps :null => false

    end

    add_index :countries, [ :alpha2 ] , unique: true, name: 'index_unique_for_countries'

  end
end
