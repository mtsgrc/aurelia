class CreateStates < ActiveRecord::Migration
  def change

    create_table :states do |t|

      t.string      :alpha2   , :index => true , :false => true
      t.string      :name     , :index => true , :false => true

      t.references  :country  , :index => true , :foreign_key => true

      t.timestamps  :null => false

    end

    add_index :states, [ :alpha2 ] , unique: true, name: 'index_unique_for_states'

  end
end
