class CreateCities < ActiveRecord::Migration
  def change

    create_table :cities do |t|

      t.string      :symbol , :index => true , :null => false
      t.string      :name   , :index => true , :null => false

      t.references  :state  , :index => true , :foreign_key => true

      t.timestamps  :null => false
    end

    add_index :cities, [ :symbol ] , unique: true, name: 'index_unique_for_cities'

  end
end
