class CreatePassengers < ActiveRecord::Migration
  def change

    create_table :passengers do |t|

      t.string :kind_identification , :null => false , :default => 0

      t.string :identification      , :index => true , :null => false
      t.string :ruc                 , :index => true
      
      t.string :names               , :index => true , :null => false

      t.timestamps :null => false

    end
    add_index :passengers, [ :kind_identification , :identification ] , unique: true, name: 'index_unique_for_passengers'
  end
end
