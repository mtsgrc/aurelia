class CreateInvoiceTransactions < ActiveRecord::Migration
  def change
    create_table :invoice_transactions do |t|

      t.references :system , :index => true , :foreign_key => true
      t.integer    :external_id , :index => true , :null => false

      t.string     :company_code , :index => true, :null => false
      t.string     :receipt_number , :index => true
      t.datetime   :receipt_issued_at , :index => true, :null => false

      t.string     :agency_origin_code , :index => true , :null => false
      t.string     :agency_origin_name
      t.string     :agency_origin_business_name
      t.string     :agency_origin_location_name

      t.string     :agency_destiny_code , :index => true
      t.string     :agency_destiny_name
      t.string     :agency_destiny_business_name
      t.string     :agency_destiny_location_name

      t.decimal    :amount_total_cot
      t.decimal    :amount_total_net_taxed
      t.decimal    :amount_total_iva
      t.decimal    :amount_total_gross

      t.string     :client_business_name
      t.string     :client_cuit
      t.string     :client_type

      t.boolean    :void

      t.string     :receipt_letter
      t.string     :user_username
      t.timestamps :null => false

    end

    add_index :invoice_transactions, [:system_id, :external_id] , unique: true, name: 'index_unique_for_invoice_transactions'

  end
end
