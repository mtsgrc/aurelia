class CreateBusTicketTransactions < ActiveRecord::Migration
  def change
    create_table :bus_ticket_transactions do |t|

      #################### SITT PROSYS ####################

      # SYSTEM REF
      t.references  :system , index: true , foreign_key: true, null: false

      # COMPANY
      t.string      :company_code , index: true , limit: 10 , null: false       # PROSYS     - CSV INDEX(000) - AgenciaVentaCodigo ==> VARCHAR(5)
                                                                                # MICRONAUTA -                - EmpresaCodigo
      # TICKET NUMBER
      t.integer     :ticket_number                                              # PROSYS     - CSV INDEX(001) - TicketNumber ==> INTEGER

      # AGENCY
      t.string      :agency_seller_code , limit: 10 , index: true , null: false # PROSYS     - CSV INDEX(002) - AgenciaVentaCodigo ==> VARCHAR(5)
      t.string      :agency_seller_description , limit: 50                      # PROSYS     - CSV INDEX(003) - AgenciaVentaDescripcion ==> VARCHAR(50)
      t.string      :agency_seller_province_code , limit: 10                    # PROSYS     - CSV INDEX(004) - AgenciaVentaProvinciaCodigo ==> VARCHAR(5)
      t.string      :agency_seller_province_description , limit: 50             # PROSYS     - CSV INDEX(005) - AgenciaVentaProvinciaDescripcion ==> VARCHAR(50)
      t.string      :agency_seller_country , limit: 20                          # PROSYS     - CSV INDEX(078) - AgenciaPais ==> VARCHAR(50)

      # AGENCY INTERVENER
      t.string      :agency_intervener_code , limit: 10                         # PROSYS     - CSV INDEX(006) - AgenciaIntervinienteCodigo ==> VARCHAR(5)
      t.string      :agency_intervener_description , limit: 50                  # PROSYS     - CSV INDEX(007) - AgenciaIntervinienteDescripcion ==> VARCHAR(50)
      t.string      :agency_intervener_province_code , limit: 10                # PROSYS     - CSV INDEX(008) - AgenciaIntervinienteProvinciaCodigo ==> VARCHAR(5)
      t.string      :agency_intervener_province_description , limit: 50         # PROSYS     - CSV INDEX(009) - AgenciaIntervinienteProvinciaDescripcion ==> VARCHAR(50)

      # SALE
      t.datetime    :sale_at , index: true                                      # PROSYS     - CSV INDEX() - VentaFechaHora ==> DATETIME
                                                                                # MICRONAUTA -             - VentaFechaHora
      t.string      :user_username, limit: 30                                   # PROSYS     - CSV INDEX() - VentaFechaHora ==> VARCHAR

      # SERVICE | LINE | SEAT
      t.datetime    :service_at                                                 # PROSYS     - CSV INDEX(014) - ServicioFechaHora ==> DATETIME
                                                                                # PROSYS     -                - ServicioFechaHora
      t.string      :service_code , limit: 10                                   # PROSYS     - CSV INDEX(012) - ServicioCodigo ==> VARCHAR(10)
      t.string      :service_type , limit: 20                                   # PROSYS     - CSV INDEX(064) - ServicioTipo ==> VARCHAR(5)
      t.string      :line_code , limit: 10                                      # PROSYS     - CSV INDEX(070) - LineaCodigo ==> VARCHAR(10)
      t.string      :line_description , limit: 50                               # PROSYS     - CSV INDEX(013) - LineaDescripcion ==> VARCHAR(100)
      t.integer     :seat_number                                                # PROSYS     - CSV INDEX(015) - ButacaNumero==> INT(11)

      # ORIGIN
      t.string      :origin_code , limit: 10                                    # PROSYS     - CSV INDEX(016) - OrigenCodigo ==> VARCHAR(5)
      t.string      :origin_description , limit: 50                             # PROSYS     - CSV INDEX(017) - OrigenDescripcion ==> VARCHAR(50)
      t.string      :origin_province_code , limit: 10                           # PROSYS     - CSV INDEX(018) - OrigenProvinciaCodigo ==> VARCHAR(5)
                                                                                # MICRONAUTA -                - OrigenProvinciaCodigo
      t.string      :origin_province_description , limit: 50                    # PROSYS     - CSV INDEX(019) - OrigenProvinciaDescripcion ==> VARCHAR(50)
                                                                                # MICRONAUTA -                - OrigenProvinciaDescripcion
      t.string      :origin_location_code                                       # MICRONAUTA -                - OrigenLocalidadCodigo ==> VARCHAR(11)
      t.string      :origin_location_description                                # MICRONAUTA -                - OrigenLocalidadDescripcion ==> VARCHAR(50)

      # DESTINY
      t.string      :destiny_code , limit: 10                                   # PROSYS     - CSV INDEX(020) - DestinoCodigo ==> VARCHAR(5)
      t.string      :destiny_description , limit: 50                            # PROSYS     - CSV INDEX(021) - DestinoDescripcion ==> VARCHAR(50)
      t.string      :destiny_province_code , limit: 10                          # PROSYS     - CSV INDEX(022) - DestinoProvinciaCodigo ==> VARCHAR(5)
                                                                                # MICRONAUTA -                - DestinoProvinciaCodigo
      t.string      :destiny_province_description , limit: 50                   # PROSYS     - CSV INDEX(023) - DestinoProvinciaDescripcion ==> VARCHAR(50)
                                                                                # MICRONAUTA -                - DestinoProvinciaDescripcion
      t.string      :destiny_location_code                                      # MICRONAUTA -                - DestinoLocalidadCodigo ==> VARCHAR(11)
      t.string      :destiny_location_description                               # MICRONAUTA -                - DestinoLocalidadDescripcion ==> VARCHAR(50)

      # DISCOUNTS
      t.string      :discount_code                                              # PROSYS     - CSV INDEX(026) - DescuentoCodigo ==> VARCHAR(5)
      t.decimal     :discount_round_trip                                        # PROSYS     - CSV INDEX(024) - DescuentoIdaVuelta ==> DECIMAL(12,2)
      t.decimal     :discount_special                                           # PROSYS     - CSV INDEX(025) - DescuentoEspecial ==> DECIMAL(12,2)
      t.decimal     :discount_amount                                            # PROSYS     - CSV INDEX(027) - DescuentoImporte ==> DECIMAL(12,2)

      # CARD | CHEQUE
      t.integer     :card_fees                                                  # PROSYS     - CSV INDEX(049) - TarjetaCuotas ==> INTEGER(11)
      t.decimal     :card_fees_interest                                         # PROSYS     - CSV INDEX(028) - TarjetaInteresCuota ==> DECIMAL(12,2)
      t.decimal     :card_interest_amount                                       # PROSYS     - CSV INDEX(051) - TarjetaInteresImporte ==> DECIMAL(12,2)
      t.string      :card_entity_code                                           # PROSYS     - CSV INDEX(040) - EntidadEmisoraCodigo ==> VARCHAR(10)
      t.string      :card_ticket_number                                         # PROSYS     - CSV INDEX(041) - EntidadEmisoraComprobanteNumero ==> VARCHAR(30)
      t.string      :card_holder_last_name                                      # PROSYS     - CSV INDEX(042) - EntidadEmisoraTitularApellido ==> VARCHAR(50)
      t.string      :card_holder_first_name                                     # PROSYS     - CSV INDEX(043) - EntidadEmisoraTitularNombre ==> VARCHAR(50)
      t.string      :card_trader                                                # PROSYS     - CSV INDEX(044) - TarjetaComercio ==> VARCHAR(45)
      t.string      :card_terminal                                              # PROSYS     - CSV INDEX(045) - TarjetaTerminal ==> VARCHAR(45)
      t.string      :card_allotment                                             # PROSYS     - CSV INDEX(046) - TarjetaLote ==> VARCHAR(45)
      t.string      :card_coupon                                                # PROSYS     - CSV INDEX(047) - TarjetaCupon ==> VARCHAR(45)
      t.string      :card_authorization                                         # PROSYS     - CSV INDEX(048) - TarjetaAutorizacion ==> VARCHAR(45)

      t.datetime    :cheque_check_at                                            # PROSYS     - CSV INDEX(050) - ChequeFechaCobro ==> DATETIME

      # STATUS
      t.string      :ticket_status                                              # PROSYS     - CSV INDEX(080) - Estado ==> VARCHAR(5)
      t.boolean     :ticket_returned                                            # PROSYS     - CSV INDEX(031) - Devuelto ==> BIT(1)
      t.boolean     :ticket_opened                                              # PROSYS     - CSV INDEX(032) - Abierto ==> BIT(1)
      t.boolean     :ticket_void                                                # PROSYS     - CSV INDEX(033) - Anulado ==> BIT(1)
      t.boolean     :ticket_dispatched                                          # PROSYS     - CSV INDEX(067) - Remitido ==> BIT(1)
      t.boolean     :ivm_joined                                                 # PROSYS     - CSV INDEX(036) - IngresadoIVM ==> BIT(1)
      t.boolean     :destiny_retired                                            # PROSYS     - CSV INDEX(068) - RetiradoEnDestino ==> BIT(1)

      # AMOUNTS | CURRENCY | PAYMENT METHOD
      t.decimal     :amount_base_tariff                                         # PROSYS     - CSV INDEX(071) - TarifaBase ==> DECIMAL(12,2)
      t.decimal     :amount_base_tariff_iva                                     # PROSYS     - CSV INDEX(072) - TarifaBaseIVA ==> DECIMAL(12,2)
      t.decimal     :amount                                                     # PROSYS     - CSV INDEX(030) - Importe ==> DECIMAL(12,2)
      t.decimal     :amount_iva                                                 # PROSYS     - CSV INDEX(029) - IVA ==> DECIMAL(12,2)
      t.decimal     :amount_perception                                          # PROSYS     - CSV INDEX(079) - ImportePercepcion ==> DECIMAL(12,2)
      t.decimal     :amount_total                                               # PROSYS     - CSV INDEX(081) - ImporteTotal ==> DECIMAL(12,2)
      t.decimal     :amount_total                                               # MICRONAUTA -                - ImporteTotal

      t.string      :currency_symbol                                            # PROSYS     - CSV INDEX(065) - Moneda ==> VARCHAR(5)
      t.string      :payment_method_code                                        # PROSYS - CSV INDEX(039) - FormaPagoCodigo ==> VARCHAR(10)

      # TRANSACTION
      t.integer     :number                                                     # PROSYS     - CSV INDEX() - TransaccionNumero ==> INTEGER()
                                                                                # MICRONAUTA -             - TransaccionNumero

      t.integer     :serie                                                      # MICRONAUTA - CSV INDEX(021) - TransaccionSerie ==> INTEGER(11)

      t.integer     :ticket_swapped_number                                      # PROSYS     - CSV INDEX(035) - BoletoCanjeadoNumero ==> INTEGER(11)

      # RETURNED | AMOUNTS
      t.datetime    :returned_at , index: true                                  # PROSYS     - CSV INDEX(054) - DevolucionFechaHora ==> DATETIME
      t.integer     :returned_ticket_number_signed                              # PROSYS     - CSV INDEX(069) - DevolucionBoletoFirmadoNumero ==> INTEGER(11)
      t.string      :returned_currency_symbol                                   # PROSYS     - CSV INDEX(082) - MonedaDevolucion ==> VARCHAR(5)
      t.integer     :returned_ticket_number                                     # PROSYS     - CSV INDEX(057) - DevolucionComprobanteNumero ==> INTEGER(11)
      t.string      :returned_code , index: true                                # PROSYS     - CSV INDEX(058) - DevolucionAgenciaCodigo ==> VARCHAR(5)
      t.decimal     :returned_amount                                            # PROSYS     - CSV INDEX(055) - DevolucionImporte ==> DECIMAL(12,2)
      t.decimal     :returned_amount_iva                                        # PROSYS     - CSV INDEX(056) - DevolucionIVA ==> DECIMAL(12,2)
      t.string      :returned_description                                       # PROSYS     - CSV INDEX(059) - DevolucionAgenciaDescripcion ==> VARCHAR(50)
      t.string      :returned_intervener_code                                   # PROSYS     - CSV INDEX(060) - DevolucionAgenciaIntervinienteCodigo ==> VARCHAR(5)
      t.string      :returned_intervener_description                            # PROSYS     - CSV INDEX(061) - DevolucionAgenciaIntervinienteDescripcion ==> VARCHAR(50)

      # LIQUIDATION | CACHIER | SURRENDER
      t.integer     :liquidation_number                                         # PROSYS     - CSV INDEX(052) - LiquidacionNumero ==> INTEGER(11)
      t.datetime    :liquidation_at                                             # PROSYS     - CSV INDEX(062) - LiquidacionFechaHora ==> DATETIME
      t.datetime    :liquidation_ending_at                                      # PROSYS     - CSV INDEX(066) - LiquidacionFechaHoraCierre ==> DATETIME
      t.integer     :liquidation_agency_number                                  # PROSYS     - CSV INDEX(063) - LiquidacionAgenciaNumero ==> INTEGER(11)
      t.integer     :cachier_closing_at                                         # PROSYS     - CSV INDEX(037) - CierreCajaNumero ==> INTEGER(11)
      t.integer     :surrender_number                                           # PROSYS     - CSV INDEX(038) - RendicionNumero ==> INTEGER(11)
      t.datetime    :surrender_date_up                                          # PROSYS     - CSV INDEX(053) - RendicionFechaAlta ==> DATETIME

      # PASSENGER
      t.string      :passenger_dni                                              # PROSYS     - CSV INDEX(074) - PasajeroDNI ==> VARCHAR(45)
      t.string      :passenger_names                                            # PROSYS     - CSV INDEX(075) - PasajeroNombreApellido ==> VARCHAR(60)
      t.string      :passenger_ruc                                              # PROSYS     - CSV INDEX(076) - PasajeroRUC ==> VARCHAR(50)
      t.string      :passenger_business_name                                    # PROSYS     - CSV INDEX(077) - PasajeroEmpresaAsociadaRazonSocial ==> VARCHAR(50)

      t.string      :agency_admin_description , index: true                     # PROSYS     - CSV INDEX(073) - AgenciaAdministradoraDescripcion ==> VARCHAR(50)


      #################### MICRONAUTA ####################
      t.string      :receipt_number                                             # MICRONAUTA - ComprobanteNumero ==> INTEGER(11)
      t.integer     :external_id , index: true                                  # MICRONAUTA - VentaExternalID ==> INTEGER(20)
      t.string      :driver_code                                                # MICRONAUTA - ChoferCodigo ==> VARCHAR(11)
      t.string      :bus_code                                                   # MICRONAUTA - CocheCodigo ==> VARCHAR(11)
      t.integer     :device_id                                                  # MICRONAUTA - EquipoId ==> INTEGER(11)
      t.integer     :sale_point                                                 # MICRONAUTA - VentaPunto ==> INTEGER(11)
      t.integer     :receipt_type_number                                        # MICRONAUTA - ComprobanteTipoNumero ==> INTEGER(11)
      t.string      :receipt_type_code                                          # MICRONAUTA - ComprobanteTipoCodigo ==> VARCHAR(11)
      t.string      :receipt_owner_code                                         # MICRONAUTA - ComprobanteResponsableCodigo ==> VARCHAR(30)
      t.integer     :receipt_zeta_number                                        # MICRONAUTA - ComprobanteZetaNumero ==> INTEGER(11)


      t.timestamps null: false

    end
    add_index :bus_ticket_transactions, [:system_id, :ticket_number, :number] , unique: true, name: 'index_unique_for_bus_ticket_transactions'
  end
end
