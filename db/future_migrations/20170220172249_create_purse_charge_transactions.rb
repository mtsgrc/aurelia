class CreatePurseChargeTransactions < ActiveRecord::Migration
  def change
    create_table :purse_charge_transactions do |t|

      t.references  :system , index: true , foreign_key: true
      t.string      :company_code , index: true , limit: 10 , null: false       # MICRONAUTA - EmpresaCodigo ==> VARCHAR(11)
      t.datetime    :charged_at , index: true                                   # MICRONAUTA - CargaFechaHora ==> DATETIME
      t.string      :origin_province_description , limit: 50                    # MICRONAUTA - OrigenProvinciaDescripcion ==> VARCHAR(50)
      t.decimal     :amount_total                                               # MICRONAUTA - ImporteTotal
      t.string      :user_username, limit: 30                                   # MICRONAUTA - CargaUsuario ==> VARCHAR
      t.integer     :external_id , index: true                                  # MICRONAUTA - VentaExternalID ==> INTEGER(20)
      t.string      :card_serie , limit: 50                                     # MICRONAUTA - TArjetaSerie ==> VARCHAR(50)

      t.timestamps null: false
    end
  end
end
