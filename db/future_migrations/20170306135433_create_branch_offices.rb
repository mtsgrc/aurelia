class CreateBranchOffices < ActiveRecord::Migration
  def change
    create_table :branch_offices do |t|

      t.references  :system       , index: true , foreign_key: true , null: false
      t.string      :company_code , index: true , limit: 10         , null: false
      t.integer     :external_id  , index: true
      t.string      :name         , index: true , limit: 100        , null: false
      t.string      :company_name , index: true , limit: 100
      t.string      :address                    , limit: 100
      t.string      :province_external_id       , limit: 100
      t.string      :postal_code                , limit: 20
      t.string      :telephone_number           , limit: 30
      t.string      :fax_number                 , limit: 30

      t.timestamps null: false

    end

    add_index :branch_offices , [:system_id, :company_code, :external_id] , unique: true, name: 'index_unique_for_branch_offices'

  end
end
