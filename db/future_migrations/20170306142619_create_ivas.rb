class CreateIvas < ActiveRecord::Migration
  def change
    create_table :ivas do |t|

      t.references  :system       , index: true , foreign_key: true , null: false
      t.string      :company_code , index: true , limit: 10         , null: false
      t.integer     :external_id  , index: true
      t.string      :name         , index: true , limit: 100        , null: false
      t.string      :letter_vta                 , limit: 10
      t.string      :letter_cpa                 , limit: 10


      t.timestamps null: false

    end

    add_index :ivas , [:system_id, :company_code, :external_id] , unique: true, name: 'index_unique_for_ivas'

  end
end
