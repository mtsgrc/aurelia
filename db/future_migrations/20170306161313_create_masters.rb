class CreateMasters < ActiveRecord::Migration
  def change
    create_table :masters do |t|

      t.references  :system       , index: true , foreign_key: true , null: false
      t.string      :company_code , index: true , limit: 10         , null: false

      t.integer     :branch_office_external_id  , index: true , null: false
      t.integer     :class_master_external_id   , index: true , null: false
      t.integer     :external_id                , index: true , null: false

      t.string      :first_name                 , index: true , limit: 50
      t.string      :last_name                  , index: true , limit: 50 , null: false
      t.string      :fantasy_name               , index: true , limit: 50 , null: false

      t.integer     :type_company_external_id   , index: true , null: false

      t.string      :address                                  , limit: 30
      t.string      :state_name                               , limit: 30
      t.string      :telephone_number                         , limit: 40
      t.string      :fax_number                               , limit: 40
      t.string      :email_address                            , limit: 40

      t.integer     :iva_external_id                          , null: false


      t.timestamps null: false

    end
    add_index :masters , [:system_id, :company_code, :branch_office_external_id, :class_master_external_id, :external_id] , unique: true, name: 'index_unique_for_masters'
  end
end
