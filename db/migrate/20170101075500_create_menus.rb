class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|

      t.string      :symbol     , :index => true, :null => false
      t.integer     :kind       , :null => false, :default => 0

      t.references  :parent     , :index => true 

      t.string      :name       , :null => false
      t.string      :faicon

      t.integer :status         , :null => false, :default => 0

      t.timestamps null: false
    end
  end
end
