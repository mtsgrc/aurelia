class CreateSystems < ActiveRecord::Migration
  def change
    create_table :systems do |t|

      t.string  :code     , :index => true , :null => false
      t.string  :name     , :index => true , :null => false
      t.string  :company  , :null => false

      t.integer :status   , :null => false , :default => 0

      t.timestamps :null => false

    end
    add_index :systems, [ :code ] , unique: true, name: 'index_unique_for_systems'
    execute "ALTER TABLE systems AUTO_INCREMENT = 0;"

  end
end
