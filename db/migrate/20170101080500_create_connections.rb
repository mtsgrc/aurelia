class CreateConnections < ActiveRecord::Migration
  def change

    create_table :connections do |t|

      t.references  :system , :index => true , :foreign_key => true, :null => false

      t.integer     :kind   , :null => false
      t.integer     :status , :null => false , :default => 0

      t.string      :host
      t.integer     :port
      t.string      :username
      t.string      :password
      t.string      :uri

      t.integer     :kind_action      , :null => false

      t.timestamps  :null => false

    end

  end
end
