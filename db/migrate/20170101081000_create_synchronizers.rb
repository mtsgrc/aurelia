class CreateSynchronizers < ActiveRecord::Migration
  def change
    create_table :synchronizers do |t|

      t.references  :connection      , :index => true , :foreign_key => true, :null => false

      t.string      :table_name             , :null => false
      t.integer     :kind                   , :null => false

      t.integer     :job_frequency_seconds
      t.time        :job_start_hour

      t.text        :conditions_default
      t.text        :schemas_default

      t.integer     :status, :null => false, :default => 0

      t.timestamps :null => false

    end

    add_index :synchronizers, [ :connection_id , :table_name , :kind ] , unique: true, name: 'index_unique_for_synchronizers'

  end
end
