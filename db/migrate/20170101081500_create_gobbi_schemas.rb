class CreateGobbiSchemas < ActiveRecord::Migration
  def change

    create_table :gobbi_schemas do |t|

      t.string      :name     , :index => true , :null => false
      t.references  :company  , :index => true , :null => false

      t.integer     :status   , :null => false , :default => 0

      t.timestamps :null => false
    end

    add_index :gobbi_schemas, [ :name ] , unique: true, name: 'index_unique_for_gobbi_schemas'

  end
end
