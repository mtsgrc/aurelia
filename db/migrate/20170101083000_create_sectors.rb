class CreateSectors < ActiveRecord::Migration
  def change
    create_table :sectors do |t|

      t.references  :company  , :foreign_key => true , :index => true , :null => false
      t.string      :name     , :index => true , :null => false

      t.references  :parent   , :index => true

      t.integer :status       , :null => false, :default => 0

      # -------------------- [ INIT ] This fields are for external data importations --------------------
      t.references  :system             , :index => true , :null => false , :default => 0 , :foreign_key => true
      t.references  :synchronizer       , :foreign_key => true
      t.integer     :external_id        , :index => true , :limit => 8
      t.text        :external_jsondata
      t.references  :gobbi_schema       , :index => true                  , :foreign_key => true
      t.references  :parent             , :index => true
      # -------------------- [ END ] This fields are for external data importations --------------------

      t.timestamps  :null => false

    end

    add_index :sectors, [ :company_id, :name, :parent_id ] , unique: true, name: 'index_unique_for_sectors'

  end
end
