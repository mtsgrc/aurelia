class CreateRoleMenus < ActiveRecord::Migration
  def change
    create_table :role_menus, id: false do |t|
      t.belongs_to :role, index: true
      t.belongs_to :menu, index: true
    end
  end
end
