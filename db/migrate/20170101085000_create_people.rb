class CreatePeople < ActiveRecord::Migration
  def change

    create_table :people do |t|

      t.integer     :dni           , :index => true , :null => false

      t.string      :first_name    , :null => false
      t.string      :last_name     , :null => false

      t.integer     :id_person     , :index => true , :null => false
      t.string      :image_base64

      t.references  :role          , :index => true , :foreign_key => true , :null => false

      t.integer     :status        , :null => false , :default => 0

      # -------------------- [ INIT ] This fields are for external data importations --------------------
      t.references  :system             , :index => true , :null => false , :default => 0 , :foreign_key => true
      t.references  :synchronizer       , :foreign_key => true
      t.integer     :external_id        , :index => true , :limit => 8
      t.text        :external_jsondata
      t.references  :gobbi_schema       , :index => true                  , :foreign_key => true
      t.references  :parent             , :index => true
      # -------------------- [ END ] This fields are for external data importations --------------------

      t.timestamps  :null => false

    end

    add_index :people, [ :dni , :id_person , :gobbi_schema_id ] , unique: true, name: 'index_unique_for_people'

  end
end
