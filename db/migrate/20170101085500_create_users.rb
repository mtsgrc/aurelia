class CreateUsers < ActiveRecord::Migration
  def change

    create_table :users do |t|

      t.string      :username, :index => true , limit: 32

      t.references  :person , :index => true , :foreign_key => true

      t.integer     :status , :null => false, :default => 0

      t.datetime    :last_seen

      # -------------------- [ INIT ] This fields are for external data importations --------------------
      t.references  :system             , :index => true , :null => false , :default => 0 , :foreign_key => true
      t.references  :synchronizer       , :foreign_key => true
      t.integer     :external_id        , :index => true , :limit => 8
      t.text        :external_jsondata
      t.references  :gobbi_schema       , :index => true                  , :foreign_key => true
      t.references  :parent             , :index => true
      # -------------------- [ END ] This fields are for external data importations --------------------

      t.timestamps  :null => false

    end

    add_index :users, [ :username ] , unique: true, name: 'index_unique_for_users'

  end
end
