class CreateExpenses < ActiveRecord::Migration
  def change

    create_table :expenses do |t|

      t.integer     :commerce_number        , :null => false
      t.integer     :receipt_number         , :null => false
      t.date        :receipt_date           , :null => false , :default => Time.now

      t.string      :detail
      t.decimal     :receipt_amount         , :null => false

      t.references  :agency                 , :index => true , :foreign_key => true

      t.references  :user                   , :index => true , :foreign_key => true

      t.integer     :status                 , :null => false , :default => 0

      # -------------------- [ INIT ] This fields are for external data importations --------------------
      t.references  :system             , :index => true , :null => false , :default => 0 , :foreign_key => true
      t.references  :synchronizer       , :foreign_key => true
      t.integer     :external_id        , :index => true , :limit => 8
      t.text        :external_jsondata
      t.references  :gobbi_schema       , :index => true                  , :foreign_key => true
      t.references  :parent             , :index => true
      # -------------------- [ END ] This fields are for external data importations --------------------

      t.timestamps  :null => false

    end

    add_index :expenses, [ :commerce_number , :receipt_number ] , unique: true, name: 'index_unique_for_expenses'

  end
end
