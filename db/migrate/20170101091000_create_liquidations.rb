class CreateLiquidations < ActiveRecord::Migration
  def change
    create_table :liquidations do |t|

      t.references  :company          , :index => true , :foreign_key => true , :null => false
      t.references  :agency           , :index => true , :foreign_key => true , :null => false

      t.integer     :agency_number    , :index => true , :null => false
      t.integer     :unique_number    , :index => true , :null => false

      t.datetime    :liquidated_at    , :null => false

      t.decimal     :total_liquidated , :null => false , :default => 0

      t.references  :user             , :index => true , :foreign_key => true

      t.integer     :status           , :null => false , :default => 0

      # -------------------- [ INIT ] This fields are for external data importations --------------------
      t.references  :system             , :index => true , :null => false , :default => 0 , :foreign_key => true
      t.references  :synchronizer       , :foreign_key => true
      t.integer     :external_id        , :index => true , :limit => 8
      t.text        :external_jsondata
      t.references  :gobbi_schema       , :index => true                  , :foreign_key => true
      t.references  :parent             , :index => true
      # -------------------- [ END ] This fields are for external data importations --------------------

      t.timestamps  :null => false

    end

    add_index :liquidations, [ :company_id , :agency_id , :agency_number ] , unique: true, name: 'index_unique_for_liquidations'

  end
end
