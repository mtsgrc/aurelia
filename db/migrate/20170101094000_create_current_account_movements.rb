class CreateCurrentAccountMovements < ActiveRecord::Migration
  def change
    create_table :current_account_movements do |t|

      t.references  :current_account          , index: true , foreign_key: true , null: false

      t.string      :resource_type            , :null => false
      t.integer     :resource_id              , :null => false
      t.date        :resource_date            , :null => false
      t.time        :resource_time            , :null => true

      t.decimal     :amount                   , :null => false
      t.decimal     :remaining_amount         , :null => false
      t.string      :detail                   , :null => false

      t.integer     :kind                     , :null => false
      t.integer     :status                   , :null => false , :default => 0

      # -------------------- [ INIT ] This fields are for external data importations --------------------
      t.references  :system             , :index => true , :null => false , :default => 0 , :foreign_key => true
      t.references  :synchronizer       , :foreign_key => true
      t.integer     :external_id        , :index => true , :limit => 8
      t.text        :external_jsondata
      t.references  :gobbi_schema       , :index => true                  , :foreign_key => true
      t.references  :parent             , :index => true
      # -------------------- [ END ] This fields are for external data importations --------------------

      t.timestamps  :null => false

    end
    add_index :current_account_movements, [ :current_account_id , :resource_type , :resource_id ] , unique: true, name: 'index_unique_for_current_account_movements'
  end
end
