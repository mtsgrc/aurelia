class CreateHomologations < ActiveRecord::Migration
  def change
    create_table :homologations do |t|

      t.string      :child_table_name               , :null => false , :index => true
      t.string      :child_value                    , :null => false , :index => true

      t.string      :parent_table_name               , :null => false , :index => true
      t.string      :parent_value                    , :null => false , :index => true

      t.integer     :status, :null => false, :default => 0

      # -------------------- [ INIT ] This fields are for external data importations --------------------
      t.references  :system             , :index => true , :null => false , :default => 0 , :foreign_key => true
      t.references  :synchronizer       , :foreign_key => true
      t.integer     :external_id        , :index => true , :limit => 8
      t.text        :external_jsondata
      t.references  :gobbi_schema       , :index => true                  , :foreign_key => true
      t.references  :parent             , :index => true
      # -------------------- [ END ] This fields are for external data importations --------------------

      t.timestamps :null => false

    end

    add_index :homologations, [ :child_table_name , :child_value , :parent_table_name , :parent_value ] , unique: true, name: 'index_unique_for_homologations'

  end
end
