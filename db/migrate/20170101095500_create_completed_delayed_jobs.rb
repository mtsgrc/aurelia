class CreateCompletedDelayedJobs < ActiveRecord::Migration

  def self.up
    create_table :completed_delayed_jobs do |t|
      t.integer :priority, default: 0, null: false
      t.integer :attempts, default: 0, null: false
      t.text :handler,                 null: false
      t.text :last_error
      t.datetime :run_at
      t.datetime :locked_at
      t.datetime :failed_at
      t.datetime :completed_at                        # This is added to store the datetime when job is completed
      t.string :locked_by
      t.string :queue
      t.timestamps null: true
    end
    add_index :completed_delayed_jobs, [:priority, :run_at], name: "unique_for_completed_delayed_jobs"
  end

  def self.down
    drop_table :completed_delayed_jobs
  end

end
