class CreateLotClosures < ActiveRecord::Migration
  def change
    create_table :lot_closures do |t|

      t.references  :credit_terminal    , :index => true , :null => false , :foreign_key => true

      t.integer     :global_number      , :index => true , :null => false , :limit => 8
      t.integer     :lot_number         , :index => true , :null => false

      t.integer     :host_number        , :index => true , :null => false , :limit => 8
      t.string      :host_name          , :index => true , :null => false

      t.date        :closed_at          , :null => false
      t.decimal     :amount             , :null => false

      t.integer     :status                              , :null => false , :default => 0

      # -------------------- [ INIT ] This fields are for external data importations --------------------
      t.references  :system             , :index => true , :null => false , :default => 0 , :foreign_key => true
      t.references  :synchronizer       , :foreign_key => true
      t.integer     :external_id        , :index => true , :limit => 8
      t.text        :external_jsondata
      t.references  :gobbi_schema       , :index => true                  , :foreign_key => true
      t.references  :parent             , :index => true
      # -------------------- [ END ] This fields are for external data importations --------------------

      t.timestamps                                         :null => false

    end

    add_index :lot_closures, [ :system_id, :global_number ] , unique: true, name: 'index_unique_for_lot_closures'

  end
end
