class CreateBankAccountCards < ActiveRecord::Migration
  def change

    create_table :bank_account_cards do |t|

      t.string      :code         , :index => true , :null => false
      t.string      :name         , :index => true , :null => false

      t.string      :number       , :index => true , :null => false
      t.string      :id_number    , :index => true , :null => false

      t.references  :bank_account , :index => true , :foreign_key => true
      t.references  :agency       , :index => true , :foreign_key => true

      t.integer     :status       , :null => false , :default => 0

      # -------------------- [ INIT ] This fields are for external data importations --------------------
      t.references  :system             , :index => true , :null => false , :default => 0 , :foreign_key => true
      t.references  :synchronizer       , :foreign_key => true
      t.integer     :external_id        , :index => true , :limit => 8
      t.text        :external_jsondata
      t.references  :gobbi_schema       , :index => true                  , :foreign_key => true
      t.references  :parent             , :index => true
      # -------------------- [ END ] This fields are for external data importations --------------------

      t.timestamps  :null => false

    end

    add_index :bank_account_cards, [ :code ] , unique: true, name: 'index_unique_for_bank_account_cards'

  end
end
