class CreateBankAccountMovements < ActiveRecord::Migration
  def change

    create_table :bank_account_movements do |t|

      t.references  :bank_account       , :index => true    , :null => false , :foreign_key => true
      t.references  :bank_account_card  , :index => true    , :foreign_key => true
      t.date        :date               , :index => true    , :null => false
      t.integer     :number             , :index => true    , :null => false , :limit => 8
      t.decimal     :amount             , :null => false
      t.string      :detail             , :null => false
      t.integer     :number_row         , :null => false , :default => 0

      t.integer     :branch_number
      t.integer     :terminal_number
      t.string      :origin
      t.string      :other_detail
      t.references  :currency           , :index => true , :foreign_key => true

      t.integer     :kind               , :null => false , :default => 0
      t.integer     :status             , :null => false , :default => 0

      # -------------------- [ INIT ] This fields are for external data importations --------------------
      t.references  :system             , :index => true , :null => false , :default => 0 , :foreign_key => true
      t.references  :synchronizer       , :foreign_key => true
      t.integer     :external_id        , :index => true , :limit => 8
      t.text        :external_jsondata
      t.references  :gobbi_schema       , :index => true                  , :foreign_key => true
      t.references  :parent             , :index => true
      # -------------------- [ END ] This fields are for external data importations --------------------

      t.timestamps  :null => false

    end

    add_index :bank_account_movements, [ :bank_account_id , :date , :number , :amount , :detail , :number_row, :branch_number , :terminal_number , :origin, :other_detail , :currency_id ] , unique: true, name: 'index_unique_for_bank_account_movements'

  end
end
