class CreateLotClosureTransactions < ActiveRecord::Migration
  def change
    create_table :lot_closure_transactions do |t|

      t.references  :lot_closure        , :index => true , :null => false , :foreign_key => true

      t.integer     :global_number      , :index => true , :null => false , :limit => 8
      t.datetime    :datetime_at        , :null => false

      t.integer     :kind               , :null => false  , :default => 0
      t.string      :transmission_mode  , :null => false

      t.string      :card_class         , :null => false
      t.string      :card_entity        , :null => false
      t.string      :commerce           , :null => false

      t.string      :currency           , :null => false

      t.decimal     :amount             , :null => false

      t.integer     :status                              , :null => false , :default => 0

      # -------------------- [ INIT ] This fields are for external data importations --------------------
      t.references  :system             , :index => true , :null => false , :default => 0 , :foreign_key => true
      t.references  :synchronizer       , :foreign_key => true
      t.integer     :external_id        , :index => true , :limit => 8
      t.text        :external_jsondata
      t.references  :gobbi_schema       , :index => true                  , :foreign_key => true
      t.references  :parent             , :index => true
      # -------------------- [ END ] This fields are for external data importations --------------------

      t.timestamps                                         :null => false

    end

    add_index :lot_closure_transactions, [ :system_id, :lot_closure_id , :global_number ] , unique: true, name: 'index_unique_for_lot_closure_transactions'

  end
end
