class CreateCurrentAccountPeriods < ActiveRecord::Migration
  def change
    create_table :current_account_periods do |t|

      t.references  :current_account  , :index => true , :null => false , :foreign_key => true

      t.integer     :order_number     , :index => true , :null => false

      t.date        :initial_date     , :null => false
      t.decimal     :initial_credit   , :null => false
      t.decimal     :initial_debit    , :null => false

      t.date        :final_date       , :null => true
      t.decimal     :final_credit     , :null => true
      t.decimal     :final_debit      , :null => true

      t.integer     :status           , :null => false , :default => 0

      t.timestamps  :null => false

    end

    add_index :current_account_periods, [ :current_account_id, :order_number ] , unique: true, name: 'index_unique_for_current_account_periods'

  end
end
