class CreateCreditNotes < ActiveRecord::Migration
  def change

    create_table :credit_notes do |t|

      t.references  :current_account    , :index => true , :foreign_key => true
			t.date        :date_at           	, :null => false , :default => Date.today
      t.string      :detail             , :null => false
      t.decimal     :amount             , :null => false

      t.references  :user               , :index => true , :foreign_key => true

      t.integer     :status             , :null => false , :default => 0

      # -------------------- [ INIT ] This fields are for external data importations --------------------
      t.references  :system             , :index => true , :null => false , :default => 0 , :foreign_key => true
      t.references  :synchronizer       , :foreign_key => true
      t.integer     :external_id        , :index => true , :limit => 8
      t.text        :external_jsondata
      t.references  :gobbi_schema       , :index => true                  , :foreign_key => true
      t.references  :parent             , :index => true
      # -------------------- [ END ] This fields are for external data importations --------------------

      t.timestamps  :null => false

    end

  end
end
