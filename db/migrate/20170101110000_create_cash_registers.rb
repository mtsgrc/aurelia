class CreateCashRegisters < ActiveRecord::Migration
  def change

    create_table :cash_registers do |t|

      t.datetime    :initial_datetime , :index => true , :null => false

      t.integer     :number           , :index => true , :null => false

      t.datetime    :final_datetime   , :index => true
      t.decimal     :final_ingress    , :null => true
      t.decimal     :final_egress     , :null => true

      t.references  :user             , :index => true , :null => false , :foreign_key => true
      t.integer     :status           , :null => false , :default => 0

      t.timestamps                    :null => false

    end

  end
end
