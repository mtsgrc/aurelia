class CreateCashRegisterMovements < ActiveRecord::Migration
  def change

    create_table :cash_register_movements do |t|

      t.references  :cash_register  , :index => true    , :null => false , :foreign_key => true

      t.decimal     :amount         , :null => false
      t.string      :detail         , :null => false

      t.integer     :kind           , :null => false , :default => 0
      t.integer     :status         , :null => false , :default => 0

      t.timestamps  :null => false

    end

  end
end
