class CreateAgencyCompanies < ActiveRecord::Migration
  def change

    create_table :agency_companies do |t|

      t.references  :agency           , :index => true                  , :foreign_key => true
      t.references  :company          , :index => true                  , :foreign_key => true

      t.references  :agency_admin     , :index => true , :references => :agencies

      t.string      :own              , :index => true , :null => false
      t.string      :liquidation_type , :index => true , :null => false

      t.decimal     :credit_maximum , :precision => 20 , :scale => 4 , :default => 0
      # t.decimal     :credit_actual  , :null => false , :precision => 20 , :scale => 4 , :default => 0

      t.integer     :status   , :null => false , :default => 0

      # -------------------- [ INIT ] This fields are for external data importations --------------------
      t.references  :system             , :index => true , :null => false , :default => 0 , :foreign_key => true
      t.references  :synchronizer       , :foreign_key => true
      t.integer     :external_id        , :index => true , :limit => 8
      t.text        :external_jsondata
      t.references  :gobbi_schema       , :index => true                  , :foreign_key => true
      t.references  :parent             , :index => true
      # -------------------- [ END ] This fields are for external data importations --------------------

      t.timestamps  :null => false

    end

    add_index :agency_companies, [ :agency_id , :company_id , :agency_admin_id ] , unique: true, name: 'index_unique_for_agency_companies'

  end
end
