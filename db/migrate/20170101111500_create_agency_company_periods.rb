class CreateAgencyCompanyPeriods < ActiveRecord::Migration
  def change
    create_table :agency_company_periods do |t|

      t.references  :current_account_period , :index => true , :null => false , :foreign_key => true
      t.references  :agency_company         , :index => true , :null => false , :foreign_key => true

      t.decimal     :initial_credit   , :null => false
      t.decimal     :initial_debit    , :null => false

      t.decimal     :final_credit     , :null => true
      t.decimal     :final_debit      , :null => true

      t.integer     :status   , :null => false , :default => 0

      t.timestamps  :null => false

    end

    add_index :agency_company_periods, [ :current_account_period_id, :agency_company_id ] , unique: true, name: 'index_unique_for_agency_company_periods'

  end
end
