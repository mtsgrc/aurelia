class CreateMovementAffectations < ActiveRecord::Migration
  def change
    create_table :movement_affectations do |t|

      t.references  :movement_from  , :references => :current_account_movement , index: true , null: false
      t.references  :movement_to    , :references => :current_account_movement , index: true , null: false

      t.decimal     :amount                   , :null => false

      t.integer     :status                   , :null => false , :default => 0

      t.timestamps  :null => false

    end
    add_index :movement_affectations, [ :movement_from_id , :movement_to_id , :status ] , unique: true, name: 'index_unique_for_movement_affectations'
  end
end
