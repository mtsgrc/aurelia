class CreateBiometrics < ActiveRecord::Migration
  def change
    create_table :biometrics do |t|

			t.string      :name   , :index => true , :foreign_key => true , :null => false
      t.integer     :kind   , :null => false , :default => 0

      t.references  :sector , :index => true , :foreign_key => true

      # -------------------- [ INIT ] This fields are for external data importations --------------------
      t.references  :system             , :index => true , :null => false , :default => 0 , :foreign_key => true
      t.references  :synchronizer       , :foreign_key => true
      t.integer     :external_id        , :index => true , :limit => 8
      t.text        :external_jsondata
      t.references  :gobbi_schema       , :index => true                  , :foreign_key => true
      t.references  :parent             , :index => true
      # -------------------- [ END ] This fields are for external data importations --------------------

			t.integer     :status                              , :null => false , :default => 0
      t.timestamps                                         :null => false

    end

    add_index :biometrics, [ :name ] , unique: true, name: 'index_unique_for_biometrics'

  end
end
