class CreatePersonChecks < ActiveRecord::Migration
  def change
    create_table :person_checks do |t|

      t.references  :biometric  , :index => true , :foreign_key => true , :null => false
      t.datetime    :check_at   , :index => true , :null => false
      t.references  :person     , :index => true , :foreign_key => true , :null => false

      # -------------------- [ INIT ] This fields are for external data importations --------------------
      t.references  :system             , :index => true , :null => false , :default => 0 , :foreign_key => true
      t.references  :synchronizer       , :foreign_key => true
      t.integer     :external_id        , :index => true , :limit => 8
      t.text        :external_jsondata
      t.references  :gobbi_schema       , :index => true                  , :foreign_key => true
      t.references  :parent             , :index => true
      # -------------------- [ END ] This fields are for external data importations --------------------

			t.integer     :status                              , :null => false , :default => 0
      t.timestamps                                         :null => false

    end

    add_index :person_checks, [ :person_id, :biometric_id, :check_at ] , unique: true, name: 'index_unique_for_person_checks'

  end
end
