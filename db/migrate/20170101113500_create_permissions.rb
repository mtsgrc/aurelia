class CreatePermissions < ActiveRecord::Migration
  def change
    create_table :permissions do |t|
      t.integer :role_id     , :index => true , :foreign_key => true , :null => false
      t.string :resource_class  , :index => true , :null => false
      t.string :action          , :index => true , :null => false
      t.timestamps null: false
    end
    add_index :permissions, [ :role_id, :resource_class, :action ] , unique: true, name: 'index_unique_for_permissions'
  end
end
