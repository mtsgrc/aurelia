class CreateInspectors < ActiveRecord::Migration
  def change
    create_table :inspectors, force: true do |t|
      t.string      :symbol_name      , :null => false  , limit: 128
      t.integer     :frecuency_unit   , :default => 0   , :null => false
      t.integer     :frecuency_amount , :default => 1   , :null => false
      t.integer     :status           , :default => 0   , :null => false
      t.text        :on_error
      t.timestamps  :null => true
    end
    add_index :inspectors, [ :symbol_name ], name: "index_unique_for_inspectors"
  end
end
