class CreateInspects < ActiveRecord::Migration
  def change
    create_table :inspects, force: true do |t|
      t.references  :inspector          , :index => true                  , :foreign_key => true
      t.datetime    :run_at
      t.boolean     :response_status
      t.text        :response_message , :limit => 102400
      t.references  :user             , :index => true                  , :foreign_key => true
      t.timestamps  :null => true
    end
    add_index :inspects, [ :inspector_id, :run_at ], name: "index_unique_for_inspects"
  end
end
