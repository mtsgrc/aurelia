class AddKindToPersonChecks < ActiveRecord::Migration
  def change
    add_column :person_checks, :kind, :integer
  end
end
