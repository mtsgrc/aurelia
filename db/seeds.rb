# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or find_or_create_byd alongside the db with db:setup).
#
# Examples:
#
#   cities = City.find_or_create_by([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.find_or_create_by(:name => 'Emanuel', city: cities.first)

encrypter = ActiveSupport::MessageEncryptor.new( Rails.application.secrets.secret_key_base )

def log_find_or_create_by( model, options , attributes , &block)

	model_object 							= model.unscoped.find_or_initialize_by( attributes )
	model_object_description 	= model_object.detail

	if model_object.persisted?
		puts " #{"\u2714".force_encoding('UTF-8').white} #{model_object_description}"
	else
		if options[ :with_encrypted_passwords ]
			model_object.attributes = options[ :with_encrypted_passwords ]
		end
		if model_object.save
			puts " #{"\u2714".force_encoding('UTF-8').green} #{model_object_description}"
		else
			puts " #{"\2718".force_encoding('UTF-8').red} #{model_object_description}"
		end
	end
	return model_object
end

# ====================================================================================================================================
puts "\n=============================================================================== CREATION OF > Systems... \n"

# CREATING System	s
ActiveRecord::Base.connection.execute("SET SESSION sql_mode='NO_AUTO_VALUE_ON_ZERO'")
sys_aurel									= log_find_or_create_by( System , {} ,:name => 'Aurelia+'		 									, :code => "AU+" 	, :company => "sunCodex" 					 					, :id => 0 )
sys_aurel_old							= log_find_or_create_by( System , {} ,:name => 'Aurelia(OLD)'									, :code => "AUR" 	, :company => "sunCodex" 											)
sys_sitt_prc							= log_find_or_create_by( System , {} ,:name => 'SITT Prosys (Servidor CATA)' 	, :code => "PRC" 	, :company => "Prosys" 												)
sys_sitt_prp							= log_find_or_create_by( System , {} ,:name => 'SITT Prosys (Servidor PDLA)' 	, :code => "PRP" 	, :company => "Prosys" 												)
sys_encom 								= log_find_or_create_by( System , {} ,:name => 'Agilis PXP'	 									, :code => "PXP" 	, :company => "Sisorg S.R.L." 								)
sys_mcrnt									= log_find_or_create_by( System , {} ,:name => 'Micronauta'	 									, :code => "MCR" 	, :company => "Global Visum/Siscadat S.A." 		)
sys_biome 								= log_find_or_create_by( System , {} ,:name => 'Biométricos' 									, :code => "BIO" 	, :company => "*Varios*" 											)
sys_gobbi 								= log_find_or_create_by( System , {} ,:name => 'Gobbi'			 									, :code => "GBB" 	, :company => "Chainsoft" 										)
sys_fics 									= log_find_or_create_by( System , {} ,:name => 'Fics'			 	 									, :code => "FCS" 	, :company => "" 															)

sys_logiciel							= log_find_or_create_by( System , {} ,:name => 'Logiciel'	 	 									, :code => "LGC" 	, :company => "" 															)
sys_nps  									= log_find_or_create_by( System , {} ,:name => 'NPS'			 	 									, :code => "NPS" 	, :company => "" 															)
sys_interbanking					= log_find_or_create_by( System , {} ,:name => 'Interbanking'									, :code => "IBK" 	, :company => "Interbanking"									)
sys_homebanking_bna				= log_find_or_create_by( System , {} ,:name => 'Home Banking B.N.A.'					, :code => "HBN" 	, :company => "Banco de la Nacion Argentina" 	)
sys_rss_bna								= log_find_or_create_by( System , {} ,:name => 'RSS B.N.A.'										, :code => "RBN" 	, :company => "Banco de la Nacion Argentina" 	)
sys_officebanking_galicia	= log_find_or_create_by( System , {} ,:name => 'Office Banking Galicia'				, :code => "OGL" 	, :company => "Banco Galicia" 								)
sys_netcash_frances				= log_find_or_create_by( System , {} ,:name => 'NET Cash Francés'							, :code => "NCF" 	, :company => "Banco Francés"									)

# ====================================================================================================================================
puts "\n=============================================================================== CREATION OF > Companies... \n"

# CREATING Companies
company_cata 				= log_find_or_create_by( Company , {} , :name => 'C.A.T.A. Internacional Ltda.' 				, :code => "CAT" 	, :kind => Company::kinds[:society] , :parent_id => nil	)
company_mitre   		= log_find_or_create_by( Company , {} , :name => 'Transp. Gral. B. Mitre S.R.L.'				, :code => "TBM" 	, :kind => Company::kinds[:society] , :parent_id => nil	)
company_radiomovil 	= log_find_or_create_by( Company , {} , :name => 'Radiomóvil' 													, :code => "RDM" 	, :kind => Company::kinds[:society] , :parent_id => nil )
company_elmendocino	= log_find_or_create_by( Company , {} , :name => 'El Mendocino' 												, :code => "MND" 	, :kind => Company::kinds[:society] , :parent_id => nil	)
company_alvear 			= log_find_or_create_by( Company , {} , :name => 'Presidente Alvear'										, :code => "ALV" 	, :kind => Company::kinds[:society] , :parent_id => nil )
company_julmar 			= log_find_or_create_by( Company , {} , :name => 'Julmar' 															, :code => "JUL" 	, :kind => Company::kinds[:society] , :parent_id => nil	)
company_turismo 		= log_find_or_create_by( Company , {} , :name => 'CATA Turismo' 												, :code => "TUR"	, :kind => Company::kinds[:society] , :parent_id => nil	)
company_tupungato   = log_find_or_create_by( Company , {} , :name => 'Tupungato S.A.' 											, :code => "TUP" 	, :kind => Company::kinds[:society] , :parent_id => nil	)

company_g800 				= log_find_or_create_by( Company , {} , :name => 'Grupo 800' 														, :code => "800"	, :kind => Company::kinds[:system]  , :parent_id => company_cata.id	)
company_g650 				= log_find_or_create_by( Company , {} , :name => 'Grupo 650'														, :code => "650"	, :kind => Company::kinds[:system]  , :parent_id => company_cata.id	)
company_g580 				= log_find_or_create_by( Company , {} , :name => 'Grupo 580 - Viento Sur' 							, :code => "580"	, :kind => Company::kinds[:system]  , :parent_id => company_cata.id	)
company_g850 				= log_find_or_create_by( Company , {} , :name => 'Grupo 850' 														, :code => "850"	, :kind => Company::kinds[:system]  , :parent_id => company_mitre.id )
company_ute 				= log_find_or_create_by( Company , {} , :name => 'Grupo UTE Sur (CATA - Buttini)' 			, :code => "UTE"	, :kind => Company::kinds[:system]  , :parent_id => company_cata.id	)
company_abo 				= log_find_or_create_by( Company , {} , :name => '(ABONOS)' 														, :code => "ABO" 	, :kind => Company::kinds[:system]  , :parent_id => company_cata.id	)

company_psj 				= log_find_or_create_by( Company , {} , :name => 'Pool de San Juan'											, :code => "PSJ" 	, :kind => Company::kinds[:system]  , :parent_id => company_cata.id	)
company_pdc 				= log_find_or_create_by( Company , {} , :name => 'Pool de Cuyo' 												, :code => "PDC" 	, :kind => Company::kinds[:system]  , :parent_id => company_cata.id	)
company_pco 				= log_find_or_create_by( Company , {} , :name => 'Pool de Córdoba' 											, :code => "PCO" 	, :kind => Company::kinds[:system]  , :parent_id => company_cata.id	)
company_psr 				= log_find_or_create_by( Company , {} , :name => 'Pool de San Rafael' 									, :code => "PSR" 	, :kind => Company::kinds[:system]  , :parent_id => company_cata.id	)
company_pmr 				= log_find_or_create_by( Company , {} , :name => 'Pool de Mendoza-Rosario' 							, :code => "PMR" 	, :kind => Company::kinds[:system]  , :parent_id => company_cata.id	)

company_test 				= log_find_or_create_by( Company , {} , :name => 'PRUEBAS' 															, :code => "TST" 	, :kind => Company::kinds[:system]  , :parent_id => company_cata.id	)
company_unknown			= log_find_or_create_by( Company , {} , :name => 'Desconocidas' 												, :code => "???" 	, :kind => Company::kinds[:system]  , :parent_id => company_cata.id	)


# ====================================================================================================================================
puts "\n=============================================================================== CREATION OF > Gobbi Schemas... \n"

# CREATING Gobbi Schemas
gobbi_schema_cata 				= log_find_or_create_by( GobbiSchema , {} , :name => 'CATA' 				, :status => 0 , :company_id => company_cata.id 				)
gobbi_schema_mitre 				= log_find_or_create_by( GobbiSchema , {} , :name => 'MITRE' 				, :status => 0 , :company_id => company_mitre.id 				)
gobbi_schema_alvear 			= log_find_or_create_by( GobbiSchema , {} , :name => 'ALVEAR' 			, :status => 1 , :company_id => company_alvear.id 			)
gobbi_schema_julmar 			= log_find_or_create_by( GobbiSchema , {} , :name => 'JULMAR' 			, :status => 1 , :company_id => company_julmar.id 			)
gobbi_schema_elmendocino 	= log_find_or_create_by( GobbiSchema , {} , :name => 'ELMENDOCINO' 	, :status => 1 , :company_id => company_elmendocino.id 	)
gobbi_schema_radiomovil 	= log_find_or_create_by( GobbiSchema , {} , :name => 'RADIOMOVIL' 	, :status => 1 , :company_id => company_radiomovil.id 	)
gobbi_schema_tupungato 		= log_find_or_create_by( GobbiSchema , {} , :name => 'TUPUNGATO' 		, :status => 1 , :company_id => company_tupungato.id 		)

# ====================================================================================================================================
puts "\n=============================================================================== CREATION OF > Sectors... \n"

# CREATING Sectors

sector_direction = log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Dirección' )

sector_gerency = log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Gerencia General' )

	sector_department_commercial = log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Departamento Comercial' , :parent_id => sector_gerency.id )
		sector_sale_non_face_to_face 	= log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Venta No Presencial' , :parent_id => sector_department_commercial.id )
		sector_sale_face_to_face 			= log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Venta Presencial' , :parent_id => sector_department_commercial.id )
		sector_sale_corporative 			= log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Venta Corporativa' , :parent_id => sector_department_commercial.id )

	sector_gerency_administrative = log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Gerencia Administrativa' , :parent_id => sector_gerency.id )
		sector_accountant 					= log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Contable' , :parent_id => sector_gerency_administrative.id )
		sector_agency_control	 			= log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Control de Agencias' , :parent_id => sector_gerency_administrative.id )
		sector_treasury	 						= log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Tesorería' , :parent_id => sector_gerency_administrative.id )
		sector_pymes 								= log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Pymes' , :parent_id => sector_gerency_administrative.id )
		sector_purchases 						= log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Compras' , :parent_id => sector_gerency_administrative.id )
		sector_statistics 					= log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Estadísticas' , :parent_id => sector_gerency_administrative.id )
		sector_collectors	 					= log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Cobranzas' , :parent_id => sector_gerency_administrative.id )
		sector_reception 						= log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Recepción' , :parent_id => sector_gerency_administrative.id )

	sector_gerency_operative = log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Gerencia Operativa' , :parent_id => sector_gerency.id )
		sector_trafic 							= log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Tráfico' , :parent_id => sector_gerency_operative.id )
		sector_maintenance					= log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Mantenimiento' , :parent_id => sector_gerency_operative.id )
		sector_operations_auditory	= log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Auditoría de Operaciones' , :parent_id => sector_gerency_operative.id )
		sector_service_quality			= log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Calidad de Servicio' , :parent_id => sector_gerency_operative.id )

	sector_human_development = log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Desarrollo Humano' , :parent_id => sector_gerency.id )
		sector_human_resources = log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Recursos Humanos' , :parent_id => sector_human_development.id )

	sector_systems = log_find_or_create_by( Sector , {} , :company_id => company_cata.id , :name => 'Sistemas' , :parent_id => sector_gerency.id )

# ====================================================================================================================================
puts "\n=============================================================================== CREATION OF > Roles... \n"

role_god = log_find_or_create_by( Role , {} , :sector_id => sector_direction.id , :name => '-- God --' )
log_find_or_create_by( Role , {} , :sector_id => sector_direction.id , :name => 'Propietario' )
log_find_or_create_by( Role , {} , :sector_id => sector_direction.id , :name => 'Director' )
log_find_or_create_by( Role , {} , :sector_id => sector_gerency.id , :name => 'Gerente General' )

	log_find_or_create_by( Role , {} , :sector_id => sector_sale_face_to_face.id , :name => 'Jefe de Sector' )
		log_find_or_create_by( Role , {} , :sector_id => sector_sale_face_to_face.id , :name => 'Responsable' )
		log_find_or_create_by( Role , {} , :sector_id => sector_sale_face_to_face.id , :name => 'Boletero' )

	log_find_or_create_by( Role , {} , :sector_id => sector_sale_non_face_to_face.id , :name => 'Jefe de Sector' )
		log_find_or_create_by( Role , {} , :sector_id => sector_sale_non_face_to_face.id , :name => 'Responsable' )
		log_find_or_create_by( Role , {} , :sector_id => sector_sale_non_face_to_face.id , :name => 'Operario' )

	log_find_or_create_by( Role , {} , :sector_id => sector_gerency_administrative.id , :name => 'Gerente' )

		log_find_or_create_by( Role , {} , :sector_id => sector_accountant.id , :name => 'Jefe de Sector' )
		log_find_or_create_by( Role , {} , :sector_id => sector_accountant.id , :name => 'Operario' )
	log_find_or_create_by( Role , {} , :sector_id => sector_agency_control.id , :name => 'Jefe de Sector' )
		log_find_or_create_by( Role , {} , :sector_id => sector_agency_control.id , :name => 'Responsable' )
		log_find_or_create_by( Role , {} , :sector_id => sector_agency_control.id , :name => 'Operario' )
	log_find_or_create_by( Role , {} , :sector_id => sector_treasury.id , :name => 'Jefe de Sector' )
		log_find_or_create_by( Role , {} , :sector_id => sector_treasury.id , :name => 'Operario' )

log_find_or_create_by( Role , {} , :sector_id => sector_human_development.id , :name => 'Jefe de Sector' )
log_find_or_create_by( Role , {} , :sector_id => sector_human_development.id , :name => 'Operario' )

log_find_or_create_by( Role , {} , :sector_id => sector_systems.id , :name => 'Jefe de Sector' )
log_find_or_create_by( Role , {} , :sector_id => sector_systems.id , :name => 'Encargado' )
log_find_or_create_by( Role , {} , :sector_id => sector_systems.id , :name => 'Operario' )

log_find_or_create_by( Role , {} , :sector_id => sector_human_resources.id , :name => 'Ingresante' )

# ====================================================================================================================================
puts "\n=============================================================================== CREATION OF > ROOT... \n"

pwd_for_user_root = "Cata1933_admin$"
user_root = log_find_or_create_by( User , { :with_encrypted_passwords => { :password => pwd_for_user_root , :password_confirmation => pwd_for_user_root } } , :username => 'root' , :email => "maati.garcia@gmail.com" )


# ====================================================================================================================================
puts "\n=============================================================================== CREATION OF > Connections... \n"

# CREATING Connections
sys_aur_plus_conn_fa_csv 	= log_find_or_create_by( Connection , {} , :system_id => sys_aurel.id, :kind => Connection.kinds[:csv]   , :kind_action => Connection.kind_actions[:read] )

sys_sitt_prc_conn_ws 			= log_find_or_create_by( Connection , {} , :system_id => sys_sitt_prc.id, :kind => Connection.kinds[:wsdl]  , :host => "192.168.0.254", :port => 888, :uri => "/WSAdmin/WSGenericoAdm.asmx?WSDL", :kind_action => Connection.kind_actions[:read] )
sys_sitt_prc_conn_fa_csv 	= log_find_or_create_by( Connection , {} , :system_id => sys_sitt_prc.id, :kind => Connection.kinds[:csv]   , :kind_action => Connection.kind_actions[:read] )

sys_sitt_prp_conn_ws 			= log_find_or_create_by( Connection , {} , :system_id => sys_sitt_prp.id, :kind => Connection.kinds[:wsdl]  , :host => "wsclientes.sittnet.com", :port => 8047, :uri => "/PDAWSGenericoAdmPool/WSGenericoAdmPool.asmx?WSDL", :kind_action => Connection.kind_actions[:read] )
sys_sitt_prp_conn_fa_csv 	= log_find_or_create_by( Connection , {} , :system_id => sys_sitt_prp.id, :kind => Connection.kinds[:csv]   , :kind_action => Connection.kind_actions[:read] )

sys_logiciel_conn_ws = log_find_or_create_by( Connection , {} , :system_id => sys_logiciel.id, :kind => Connection.kinds[:wsdl]  , :host => "logicielsrl.com.ar", :port => 85, :uri => "/LoteIp?wsdl", :kind_action => Connection.kind_actions[:read] , :username => "cata" )
sys_logiciel_conn_ws.password = encrypter.encrypt_and_sign('zp16kkl9')
sys_logiciel_conn_ws.save

sys_aurel_old_conn_db_mysql = log_find_or_create_by( Connection , {} , :system_id => sys_aurel_old.id, :kind => Connection.kinds[:mysql] , :host => "192.168.0.249", :port => 3306, :username => "aurelio", :kind_action => Connection.kind_actions[:read] )
sys_aurel_old_conn_db_mysql.password = encrypter.encrypt_and_sign('itamto05')
sys_aurel_old_conn_db_mysql.save

sys_mcrnt_conn_db_mysql = log_find_or_create_by( Connection , {} , :system_id => sys_mcrnt.id, :kind => Connection.kinds[:mysql] , :host => "190.220.132.132", :port => 3306, :username => "cata2015", :kind_action => Connection.kind_actions[:read] )
sys_mcrnt_conn_db_mysql.password = encrypter.encrypt_and_sign('horaciorus')
sys_mcrnt_conn_db_mysql.save

sys_encom_conn_db_mssql = log_find_or_create_by( Connection , {} , :system_id => sys_encom.id, :kind => Connection.kinds[:mssql] , :host => "192.168.0.249", :port => nil, :username => "aurelio", :kind_action => Connection.kind_actions[:read] )
sys_encom_conn_db_mssql.password = encrypter.encrypt_and_sign('aurelia')
sys_encom_conn_db_mssql.save

sys_biome_conn_db_mssql = log_find_or_create_by( Connection , {} , :system_id => sys_biome.id, :kind => Connection.kinds[:mssql] , :host => "10.1.10.97", :port => 51406, :username => "bio", :kind_action => Connection.kind_actions[:read] )
sys_biome_conn_db_mssql.password = encrypter.encrypt_and_sign('Gobbi_2016')
sys_biome_conn_db_mssql.save

sys_gobbi_conn_db_mssql_writer = log_find_or_create_by( Connection , {} , :system_id => sys_gobbi.id, :kind => Connection.kinds[:mssql], :host => "192.168.0.249", :port => 49501, :username => "AurelioGN", :kind_action => Connection.kind_actions[:write] )
sys_gobbi_conn_db_mssql_writer.password = encrypter.encrypt_and_sign('GN*Au2016')
sys_gobbi_conn_db_mssql_writer.save

sys_gobbi_conn_db_mssql_reader = log_find_or_create_by( Connection , {} , :system_id => sys_gobbi.id, :kind => Connection.kinds[:mssql], :host => "192.168.0.249", :port => 49501, :username => "AurelioGBB", :kind_action => Connection.kind_actions[:read] )
sys_gobbi_conn_db_mssql_reader.password = encrypter.encrypt_and_sign('Gbb*Au2016')
sys_gobbi_conn_db_mssql_reader.save

sys_nps_conn_fa_csv = log_find_or_create_by( Connection , {} , :system_id => sys_nps.id					, :kind => Connection.kinds[ :csv ]   , :kind_action => Connection.kind_actions[ :read ] )

sys_interbanking_conn_fa_csv 					= log_find_or_create_by( Connection , {} , :system_id => sys_interbanking.id					, :kind => Connection.kinds[ :csv ]   , :kind_action => Connection.kind_actions[ :read ] )
sys_homebanking_bna_conn_fa_xls 			= log_find_or_create_by( Connection , {} , :system_id => sys_homebanking_bna.id				, :kind => Connection.kinds[ :xls ]   , :kind_action => Connection.kind_actions[ :read ] )
sys_rss_bna_conn_fa_txt 							= log_find_or_create_by( Connection , {} , :system_id => sys_rss_bna.id								, :kind => Connection.kinds[ :nsv ]   , :kind_action => Connection.kind_actions[ :read ] )
sys_officebanking_galicia_conn_fa_csv	= log_find_or_create_by( Connection , {} , :system_id => sys_officebanking_galicia.id	, :kind => Connection.kinds[ :csv ]   , :kind_action => Connection.kind_actions[ :read ] )
sys_netcash_frances_conn_fa_xls				= log_find_or_create_by( Connection , {} , :system_id => sys_netcash_frances.id				, :kind => Connection.kinds[ :xls ]   , :kind_action => Connection.kind_actions[ :read ] )

# ====================================================================================================================================
puts "\n=============================================================================== CREATION OF > Synchronizers... \n"

# Aurelia +
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_aur_plus_conn_fa_csv.id 				, :kind => Synchronizer.kinds[ :get ] , :table_name => AgencyCompany.table_name )

# Micronauta
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_mcrnt_conn_db_mysql.id 				, :kind => Synchronizer.kinds[ :get ] , :table_name => Agency.table_name )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_mcrnt_conn_db_mysql.id 				, :kind => Synchronizer.kinds[ :get ] , :table_name => Company.table_name )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_mcrnt_conn_db_mysql.id 				, :kind => Synchronizer.kinds[ :get ] , :table_name => Liquidation.table_name , :conditions_default => "{from_date: Time.now - 1.hour, to_date: Time.now }" )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_mcrnt_conn_db_mysql.id 				, :kind => Synchronizer.kinds[ :get ] , :table_name => BusTicketTransaction.table_name , :conditions_default => "{from_date: Time.now - 1.hour, to_date: Time.now }" )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_mcrnt_conn_db_mysql.id 				, :kind => Synchronizer.kinds[ :get ] , :table_name => PurseChargeTransaction.table_name , :conditions_default => "{from_date: Time.now - 1.hour, to_date: Time.now }" )

# -------------------------------------------------------- PXP daemons
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_encom_conn_db_mssql.id 				, :kind => Synchronizer.kinds[ :get ] , :table_name => Agency.table_name )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_encom_conn_db_mssql.id 				, :kind => Synchronizer.kinds[ :get ] , :table_name => Company.table_name )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_encom_conn_db_mssql.id 				, :kind => Synchronizer.kinds[ :get ] , :table_name => InvoiceTransaction.table_name , :conditions_default => "{from_date: Time.now - 1.hour, to_date: Time.now }" )

# -------------------------------------------------------- GBB daemons
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => Agency.table_name , :schemas_default => "[ 'CATA' ]" )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => Homologation.table_name , :schemas_default => "[ 'CATA' ]" )
# log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => Bank.table_name , :schemas_default => "[ 'CATA' ]" )
#log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => BankBranch.table_name , :schemas_default => "[ 'CATA' ]" )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => BankAccount.table_name , :schemas_default => "[ 'CATA' ]" )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => BankAccountCard.table_name , :schemas_default => "[ 'CATA' ]" )

log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => Company.table_name , :schemas_default => "[ 'CATA' ]" )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => Currency.table_name , :schemas_default => "[ 'CATA' ]" )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => ClassMaster.table_name , :schemas_default => "[ 'CATA' ]" )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => CostCenter.table_name , :schemas_default => "[ 'CATA' ]" )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => PaymentCondition.table_name , :schemas_default => "[ 'CATA' ]" )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => Master.table_name , :schemas_default => "[ 'CATA' ]" )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => Sector.table_name , :schemas_default => "[ 'CATA' ]" )

log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => Person.table_name , :schemas_default => "[ 'CATA' , 'MITRE' , 'ELMENDOCINO' , 'RADIOMOVIL' ]" )

# -------------------------------------------------------- IBK daemons
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_interbanking_conn_fa_csv.id , :kind => Synchronizer.kinds[ :get ] , :table_name => BankAccountMovement.table_name , :schemas_default => "" )


log_find_or_create_by( Synchronizer , {} , :connection_id => sys_homebanking_bna_conn_fa_xls.id 			, :kind => Synchronizer.kinds[ :get ] , :table_name => BankAccountMovement.table_name , :schemas_default => "" )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_rss_bna_conn_fa_txt.id 							, :kind => Synchronizer.kinds[ :get ] , :table_name => BankAccountMovement.table_name , :schemas_default => "" )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_officebanking_galicia_conn_fa_csv.id , :kind => Synchronizer.kinds[ :get ] , :table_name => BankAccountMovement.table_name , :schemas_default => "" )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_netcash_frances_conn_fa_xls.id 			, :kind => Synchronizer.kinds[ :get ] , :table_name => BankAccountMovement.table_name , :schemas_default => "" )


# log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => BranchOffice.table_name , :schemas_default => "[ 'CATA' ]" )
# log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => TypeCompany.table_name , :schemas_default => "[ 'CATA' ]" )
# log_find_or_create_by( Synchronizer , {} , :connection_id => sys_gobbi_conn_db_mssql_reader.id , :kind => Synchronizer.kinds[ :get ] , :table_name => Iva.table_name , :schemas_default => "[ 'CATA' ]" )

# -------------------------------------------------------- PRC
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_sitt_prc_conn_fa_csv.id 	, :kind => Synchronizer.kinds[ :get ] , :table_name => Agency.table_name )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_sitt_prc_conn_ws.id 			, :kind => Synchronizer.kinds[ :get ] , :table_name => Company.table_name )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_sitt_prc_conn_ws.id 			, :kind => Synchronizer.kinds[ :get ] , :table_name => AgencyCompany.table_name )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_sitt_prc_conn_ws.id 			, :kind => Synchronizer.kinds[ :get ] , :table_name => Liquidation.table_name , :conditions_default => "{ 'FechaDesde': Date.today - 1.day, 'FechaHasta': Date.today - 1.day }" )

# -------------------------------------------------------- PRP
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_sitt_prp_conn_fa_csv.id 	, :kind => Synchronizer.kinds[ :get ] , :table_name => Agency.table_name )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_sitt_prp_conn_ws.id 			, :kind => Synchronizer.kinds[ :get ] , :table_name => Company.table_name )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_sitt_prp_conn_ws.id 			, :kind => Synchronizer.kinds[ :get ] , :table_name => AgencyCompany.table_name )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_sitt_prp_conn_ws.id 			, :kind => Synchronizer.kinds[ :get ] , :table_name => Liquidation.table_name , :conditions_default => "{ 'FechaDesde': Date.today - 1.day, 'FechaHasta': Date.today - 1.day }" )

# -------------------------------------------------------- LGC
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_logiciel_conn_ws.id 		, :kind => Synchronizer.kinds[ :get ] , :table_name => LotClosure.table_name , :conditions_default => "{from_date: ( Date.today - 1.day ), to_date: ( Date.today - 1.day ) }" )
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_logiciel_conn_ws.id 		, :kind => Synchronizer.kinds[ :get ] , :table_name => LotClosureTransaction.table_name , :conditions_default => "{from_date: ( Date.today - 1.day ), to_date: ( Date.today - 1.day ) }" )
# -------------------------------------------------------- NPS
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_nps_conn_fa_csv.id 			, :kind => Synchronizer.kinds[ :get ] , :table_name => LotClosureTransaction.table_name , :schemas_default => "" )
# -------------------------------------------------------- BIO daemons
log_find_or_create_by( Synchronizer , {} , :connection_id => sys_biome_conn_db_mssql.id 				, :kind => Synchronizer.kinds[ :get ] , :table_name => PersonCheck.table_name )

puts "\n=============================================================================== CREATION OF > Banks... \n"
# -------------------------------------------------------- Creating Banks
log_find_or_create_by( Bank , {} , :code => 'BBVA' 			, :name => "Banco Francés" )
log_find_or_create_by( Bank , {} , :code => 'BNA' 			, :name => "Banco de la Nación Argentina" )
log_find_or_create_by( Bank , {} , :code => 'ICBC' 			, :name => "ICBC | Ex-Standard Bank" )
log_find_or_create_by( Bank , {} , :code => 'GALICIA' 	, :name => "Banco Galicia" )
log_find_or_create_by( Bank , {} , :code => 'SANTANDER' , :name => "Banco Santander" )
log_find_or_create_by( Bank , {} , :code => 'CDADBSAS'	, :name => "Banco de la Cdad. de Bs.AS." )

puts "\n=============================================================================== CREATION OF > Inspectors... \n"
# -------------------------------------------------------- Inspectors
log_find_or_create_by( Inspector , {} , :symbol_name => 'check_sitt_services'	, :frecuency_amount => 0 , :frecuency_unit => 0 , :status => 0 , :on_error => "send_mail_to_root;send_mail_to_managers")
