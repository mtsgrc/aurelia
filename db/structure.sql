CREATE TABLE "schema_migrations" ("version" varchar NOT NULL);
CREATE UNIQUE INDEX "unique_schema_migrations" ON "schema_migrations" ("version");
CREATE TABLE "expenses" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "commerce_number" integer NOT NULL, "receipt_number" integer NOT NULL, "receipt_date" date DEFAULT '2017-05-11 15:42:20.925102' NOT NULL, "detail" varchar, "receipt_amount" decimal NOT NULL, "currency_id" integer, "payment_condition_id" integer, "cost_center_id" integer, "work_order_id" integer, "user_id" integer, "status" integer DEFAULT 0 NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_expenses_on_currency_id" ON "expenses" ("currency_id");
CREATE INDEX "index_expenses_on_payment_condition_id" ON "expenses" ("payment_condition_id");
CREATE INDEX "index_expenses_on_cost_center_id" ON "expenses" ("cost_center_id");
CREATE INDEX "index_expenses_on_work_order_id" ON "expenses" ("work_order_id");
CREATE INDEX "index_expenses_on_user_id" ON "expenses" ("user_id");
CREATE UNIQUE INDEX "index_unique_for_expenses" ON "expenses" ("commerce_number", "receipt_number");
CREATE TABLE "systems" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "code" varchar NOT NULL, "name" varchar NOT NULL, "company" varchar NOT NULL, "status" integer DEFAULT 0 NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_systems_on_code" ON "systems" ("code");
CREATE INDEX "index_systems_on_name" ON "systems" ("name");
CREATE UNIQUE INDEX "index_unique_for_systems" ON "systems" ("code");
CREATE TABLE "menus" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "symbol" varchar NOT NULL, "kind" integer DEFAULT 0 NOT NULL, "parent_id" integer, "name" varchar NOT NULL, "faicon" varchar, "status" integer DEFAULT 0 NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_menus_on_symbol" ON "menus" ("symbol");
CREATE INDEX "index_menus_on_parent_id" ON "menus" ("parent_id");
CREATE TABLE "companies" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "code" varchar NOT NULL, "name" varchar NOT NULL, "status" integer DEFAULT 0 NOT NULL, "logo_file_name" varchar, "logo_content_type" varchar, "logo_file_size" integer, "logo_updated_at" datetime, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_companies_on_code" ON "companies" ("code");
CREATE INDEX "index_companies_on_name" ON "companies" ("name");
CREATE UNIQUE INDEX "index_unique_for_companies" ON "companies" ("code");
CREATE TABLE "system_companies" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "system_id" integer NOT NULL, "company_id" integer, "code" varchar NOT NULL, "name" varchar NOT NULL, "gobbi_schema" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_system_companies_on_system_id" ON "system_companies" ("system_id");
CREATE INDEX "index_system_companies_on_company_id" ON "system_companies" ("company_id");
CREATE INDEX "index_system_companies_on_code" ON "system_companies" ("code");
CREATE INDEX "index_system_companies_on_name" ON "system_companies" ("name");
CREATE UNIQUE INDEX "index_unique_for_system_companies" ON "system_companies" ("system_id", "code");
CREATE TABLE "sectors" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "company_id" integer NOT NULL, "name" varchar NOT NULL, "parent_id" integer, "status" integer DEFAULT 0 NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_sectors_on_company_id" ON "sectors" ("company_id");
CREATE INDEX "index_sectors_on_name" ON "sectors" ("name");
CREATE INDEX "index_sectors_on_parent_id" ON "sectors" ("parent_id");
CREATE UNIQUE INDEX "index_unique_for_sectors" ON "sectors" ("company_id", "name", "parent_id");
CREATE TABLE "roles" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "sector_id" integer NOT NULL, "name" varchar NOT NULL, "status" integer DEFAULT 0 NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_roles_on_sector_id" ON "roles" ("sector_id");
CREATE INDEX "index_roles_on_name" ON "roles" ("name");
CREATE UNIQUE INDEX "index_unique_for_roles" ON "roles" ("sector_id", "name");
CREATE TABLE "role_menus" ("role_id" integer, "menu_id" integer);
CREATE INDEX "index_role_menus_on_role_id" ON "role_menus" ("role_id");
CREATE INDEX "index_role_menus_on_menu_id" ON "role_menus" ("menu_id");
CREATE TABLE "agencies" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "code" varchar NOT NULL, "name" varchar NOT NULL, "status" integer DEFAULT 0 NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_agencies_on_code" ON "agencies" ("code");
CREATE INDEX "index_agencies_on_name" ON "agencies" ("name");
CREATE UNIQUE INDEX "index_unique_for_agencies" ON "agencies" ("code");
CREATE TABLE "system_agencies" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "system_id" integer NOT NULL, "code" varchar NOT NULL, "agency_id" integer, "name" varchar NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_system_agencies_on_system_id" ON "system_agencies" ("system_id");
CREATE INDEX "index_system_agencies_on_code" ON "system_agencies" ("code");
CREATE INDEX "index_system_agencies_on_agency_id" ON "system_agencies" ("agency_id");
CREATE INDEX "index_system_agencies_on_name" ON "system_agencies" ("name");
CREATE UNIQUE INDEX "index_unique_for_system_agencies" ON "system_agencies" ("system_id", "code");
CREATE TABLE "system_users" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "system_id" integer NOT NULL, "username" varchar NOT NULL, "user_id" integer, "name" varchar NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_system_users_on_system_id" ON "system_users" ("system_id");
CREATE INDEX "index_system_users_on_username" ON "system_users" ("username");
CREATE INDEX "index_system_users_on_user_id" ON "system_users" ("user_id");
CREATE INDEX "index_system_users_on_name" ON "system_users" ("name");
CREATE UNIQUE INDEX "index_unique_for_system_users" ON "system_users" ("system_id", "username");
CREATE TABLE "audits" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "auditable_id" integer, "auditable_type" varchar, "associated_id" integer, "associated_type" varchar, "user_id" integer, "user_type" varchar, "username" varchar, "action" varchar, "audited_changes" text, "version" integer DEFAULT 0, "comment" varchar, "remote_address" varchar, "request_uuid" varchar, "created_at" datetime);
CREATE INDEX "auditable_index" ON "audits" ("auditable_id", "auditable_type");
CREATE INDEX "associated_index" ON "audits" ("associated_id", "associated_type");
CREATE INDEX "user_index" ON "audits" ("user_id", "user_type");
CREATE INDEX "index_audits_on_request_uuid" ON "audits" ("request_uuid");
CREATE INDEX "index_audits_on_created_at" ON "audits" ("created_at");
CREATE TABLE "system_connections" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "system_id" integer NOT NULL, "kind" integer NOT NULL, "status" integer DEFAULT 0 NOT NULL, "host" varchar, "port" integer, "username" varchar, "password" varchar, "uri" varchar, "kind_action" integer NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_system_connections_on_system_id" ON "system_connections" ("system_id");
CREATE TABLE "system_data_accessors" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "system_connection_id" integer NOT NULL, "table_name" varchar NOT NULL, "kind" integer NOT NULL, "job_frequency_seconds" integer, "job_start_hour" time, "conditions_default" text, "schemas_default" text, "status" integer DEFAULT 0 NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_system_data_accessors_on_system_connection_id" ON "system_data_accessors" ("system_connection_id");
CREATE UNIQUE INDEX "index_unique_for_system_data_accessors" ON "system_data_accessors" ("system_connection_id", "table_name", "kind");
CREATE TABLE "liquidations" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "company_id" integer NOT NULL, "agency_id" integer NOT NULL, "agency_number" integer NOT NULL, "unique_number" integer NOT NULL, "liquidated_at" datetime NOT NULL, "total_liquidated" decimal DEFAULT 0 NOT NULL, "total_effective" decimal DEFAULT 0 NOT NULL, "total_trade" decimal DEFAULT 0 NOT NULL, "total_pass" decimal DEFAULT 0 NOT NULL, "total_voucher" decimal DEFAULT 0 NOT NULL, "total_purse" decimal DEFAULT 0 NOT NULL, "user_id" integer NOT NULL, "status" integer DEFAULT 0 NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_liquidations_on_company_id" ON "liquidations" ("company_id");
CREATE INDEX "index_liquidations_on_agency_id" ON "liquidations" ("agency_id");
CREATE INDEX "index_liquidations_on_agency_number" ON "liquidations" ("agency_number");
CREATE INDEX "index_liquidations_on_unique_number" ON "liquidations" ("unique_number");
CREATE INDEX "index_liquidations_on_user_id" ON "liquidations" ("user_id");
CREATE UNIQUE INDEX "index_unique_for_liquidations" ON "liquidations" ("company_id", "agency_id", "agency_number");
CREATE TABLE "system_liquidations" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "system_id" integer NOT NULL, "liquidation_id" integer, "external_id" integer NOT NULL, "unique_number" integer NOT NULL, "agency_number" integer NOT NULL, "liquidated_at" datetime NOT NULL, "company_code" varchar(10) NOT NULL, "agency_code" varchar(10) NOT NULL, "total_liquidated" decimal NOT NULL, "total_effective" decimal NOT NULL, "total_trade" decimal NOT NULL, "total_pass" decimal NOT NULL, "total_voucher" decimal NOT NULL, "total_purse" decimal NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_system_liquidations_on_system_id" ON "system_liquidations" ("system_id");
CREATE INDEX "index_system_liquidations_on_liquidation_id" ON "system_liquidations" ("liquidation_id");
CREATE INDEX "index_system_liquidations_on_external_id" ON "system_liquidations" ("external_id");
CREATE INDEX "index_system_liquidations_on_unique_number" ON "system_liquidations" ("unique_number");
CREATE INDEX "index_system_liquidations_on_agency_number" ON "system_liquidations" ("agency_number");
CREATE INDEX "index_system_liquidations_on_company_code" ON "system_liquidations" ("company_code");
CREATE INDEX "index_system_liquidations_on_agency_code" ON "system_liquidations" ("agency_code");
CREATE UNIQUE INDEX "index_unique_for_system_liquidations" ON "system_liquidations" ("system_id", "external_id");
CREATE TABLE "cost_centers" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar NOT NULL, "status" integer DEFAULT 0 NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "system_cost_centers" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "system_id" integer NOT NULL, "company_code" varchar NOT NULL, "external_id" integer NOT NULL, "cost_center_id" integer, "name" varchar(50) NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_system_cost_centers_on_system_id" ON "system_cost_centers" ("system_id");
CREATE INDEX "index_system_cost_centers_on_company_code" ON "system_cost_centers" ("company_code");
CREATE INDEX "index_system_cost_centers_on_external_id" ON "system_cost_centers" ("external_id");
CREATE INDEX "index_system_cost_centers_on_cost_center_id" ON "system_cost_centers" ("cost_center_id");
CREATE UNIQUE INDEX "index_unique_for_system_cost_centers" ON "system_cost_centers" ("system_id", "company_code", "external_id");
CREATE TABLE "currencies" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar NOT NULL, "symbol" varchar NOT NULL, "status" integer DEFAULT 0 NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE UNIQUE INDEX "index_unique_for_currencies" ON "currencies" ("symbol");
CREATE TABLE "system_currencies" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "system_id" integer NOT NULL, "company_code" varchar NOT NULL, "external_id" integer NOT NULL, "name" varchar NOT NULL, "symbol" varchar NOT NULL, "afip_code" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_system_currencies_on_system_id" ON "system_currencies" ("system_id");
CREATE INDEX "index_system_currencies_on_company_code" ON "system_currencies" ("company_code");
CREATE INDEX "index_system_currencies_on_external_id" ON "system_currencies" ("external_id");
CREATE UNIQUE INDEX "index_unique_for_system_currencies" ON "system_currencies" ("system_id", "company_code", "external_id");
CREATE TABLE "payment_conditions" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar NOT NULL, "code" varchar(10) NOT NULL, "status" integer DEFAULT 0 NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE UNIQUE INDEX "index_unique_for_payment_conditions" ON "payment_conditions" ("code");
CREATE TABLE "system_payment_conditions" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "system_id" integer NOT NULL, "company_code" varchar(10) NOT NULL, "external_id" integer NOT NULL, "code" varchar(10) NOT NULL, "name" varchar(50) NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_system_payment_conditions_on_system_id" ON "system_payment_conditions" ("system_id");
CREATE INDEX "index_system_payment_conditions_on_company_code" ON "system_payment_conditions" ("company_code");
CREATE INDEX "index_system_payment_conditions_on_external_id" ON "system_payment_conditions" ("external_id");
CREATE UNIQUE INDEX "index_unique_for_system_payment_conditions" ON "system_payment_conditions" ("system_id", "company_code", "external_id");
CREATE TABLE "system_sectors" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "system_id" integer NOT NULL, "external_id" integer NOT NULL, "sector_id" integer NOT NULL, "company_code" varchar NOT NULL, "name" varchar NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_system_sectors_on_system_id" ON "system_sectors" ("system_id");
CREATE INDEX "index_system_sectors_on_external_id" ON "system_sectors" ("external_id");
CREATE INDEX "index_system_sectors_on_sector_id" ON "system_sectors" ("sector_id");
CREATE INDEX "index_system_sectors_on_company_code" ON "system_sectors" ("company_code");
CREATE UNIQUE INDEX "index_unique_for_system_sectors" ON "system_sectors" ("system_id", "external_id");
CREATE TABLE "current_accounts" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "kind" integer NOT NULL, "resource_id" integer NOT NULL, "credit_maximum" decimal(4) DEFAULT 0 NOT NULL, "credit_actual" decimal(4) DEFAULT 0 NOT NULL, "status" integer DEFAULT 0 NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "system_personal_checks" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "system_id" integer NOT NULL, "personal_id" integer NOT NULL, "checktime" datetime NOT NULL, "device_name" varchar NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_system_personal_checks_on_system_id" ON "system_personal_checks" ("system_id");
CREATE INDEX "index_system_personal_checks_on_personal_id" ON "system_personal_checks" ("personal_id");
CREATE TABLE "current_account_movements" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "current_account_id" integer NOT NULL, "resource_type" varchar NOT NULL, "resource_id" integer NOT NULL, "amount" decimal NOT NULL, "detail" varchar NOT NULL, "kind" integer NOT NULL, "status" integer DEFAULT 0 NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_current_account_movements_on_current_account_id" ON "current_account_movements" ("current_account_id");
INSERT INTO schema_migrations (version) VALUES ('20170207160915');

INSERT INTO schema_migrations (version) VALUES ('20170213192359');

INSERT INTO schema_migrations (version) VALUES ('20170215182210');

INSERT INTO schema_migrations (version) VALUES ('20170215182215');

INSERT INTO schema_migrations (version) VALUES ('20170215182216');

INSERT INTO schema_migrations (version) VALUES ('20170215182220');

INSERT INTO schema_migrations (version) VALUES ('20170215182300');

INSERT INTO schema_migrations (version) VALUES ('20170215182330');

INSERT INTO schema_migrations (version) VALUES ('20170215200332');

INSERT INTO schema_migrations (version) VALUES ('20170215200333');

INSERT INTO schema_migrations (version) VALUES ('20170215200934');

INSERT INTO schema_migrations (version) VALUES ('20170215200935');

INSERT INTO schema_migrations (version) VALUES ('20170220194441');

INSERT INTO schema_migrations (version) VALUES ('20170222204111');

INSERT INTO schema_migrations (version) VALUES ('20170224130222');

INSERT INTO schema_migrations (version) VALUES ('20170302123300');

INSERT INTO schema_migrations (version) VALUES ('20170302123301');

INSERT INTO schema_migrations (version) VALUES ('20170302234333');

INSERT INTO schema_migrations (version) VALUES ('20170302234334');

INSERT INTO schema_migrations (version) VALUES ('20170303183202');

INSERT INTO schema_migrations (version) VALUES ('20170303183203');

INSERT INTO schema_migrations (version) VALUES ('20170303193810');

INSERT INTO schema_migrations (version) VALUES ('20170303193811');

INSERT INTO schema_migrations (version) VALUES ('20170308215443');

INSERT INTO schema_migrations (version) VALUES ('20170328140805');

INSERT INTO schema_migrations (version) VALUES ('20170404053627');

INSERT INTO schema_migrations (version) VALUES ('20170404164910');

INSERT INTO schema_migrations (version) VALUES ('20170418215729');

INSERT INTO schema_migrations (version) VALUES ('20170419195855');

