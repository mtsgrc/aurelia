#!/bin/bash
ACTUALDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"		# AureliaPlus dir
PARENTDIR="$(dirname "$ACTUALDIR")"															# SYS dir
TEMPLATEDIR="$PARENTDIR/Inspinia"

echo "=-=-= Initializing all applications =-=-="

# Init template app
echo -n "   > Running template app..."
cd $TEMPLATEDIR
bundle exec rails s -b 0.0.0.0 -p 3001 &
PID_TEMPLATE_APP=$!
echo " (PID:$PID_TEMPLATE_APP)"

# Init main app
echo -n "   > Running main app..."
cd $ACTUALDIR
bundle exec rails s -b 0.0.0.0 -p 3000
PID_MAIN_APP=$!
echo " (PID:$PID_MAIN_APP)"

trap ctrl_c INT

function ctrl_c() {
	echo "Finishing applications..."
	kill -9 $PID_TEMPLATE_APP
	echo " > APP TEMPLATE => PID: $PID_TEMPLATE_APP"
	kill -9 $PID_MAIN_APP
	echo " > APP MAIN => PID: $PID_MAIN_APP"
	exit

	echo "=-=-=-=-=-=-=-=-=- END =-=-=-=-=-=-=-=-="

}
