module ClassGlobalMethods

  # Get array of all actions
  def actions( arr_actions = [] )
    arr_ret = []
		arr_ret += ( arr_actions.map do | action_validation |
      action_validation.keys.first if eval( action_validation.values.first )
    end ).compact
    return arr_ret
  end
end
