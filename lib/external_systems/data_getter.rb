# /lib/external_systems/data_getter.rb

module ExternalSystems::DataGetter
	include ExternalSystems::Constants
	require 'csv'

	# Keep in mind that this module is imported from any System Instance.
	# Then you can do this
	#
	# => Synchronizer.find(X).get_external_data 										( ONLY FOR READ )
	# => Synchronizer.find(X).get_external_data_and_sync 						( READ AND IMPORT )
	# => Synchronizer.find(X).get_external_data_and_convert_to_csv 	( READ AND IMPORT )
	#
	# run_now method call get_external_data method

	# Global Instances
	CONST_PATH_LOGS	= File.join( Rails.root.join('log') , Date.today.strftime("%Y/%m/%d") )	# string path like '#{rails_path}/log/YYYY/mm/dd/SYS' if not exists

	FileUtils::mkdir_p( CONST_PATH_LOGS ) unless File.exists?( CONST_PATH_LOGS ) 						# Create logs dir if not exists

	CONST_ARR_SYS_CODE_ID = System.all.map{ |s| [s.code,s.id] }.to_h

	# To search relation of external column name starts with "_"
	def replace_fk_columns model, columns_and_values

		ret_columns_replaced = columns_and_values.map do |col,val|
			if col.starts_with?("_")
				model.get_relation( col , val )
			else
				{ col => val }
			end
		end

		return ret_columns_replaced.reduce( :merge )

	end

	# Create directory if not exist
	def get_external_data param_conditions = nil, param_schemas = nil, job_id = nil, special_action = nil

		@crypter									= ActiveSupport::MessageEncryptor.new(Rails.application.secrets.secret_key_base)
		@action 									= self
		@action_name							= @action.kind.to_s + "_" + @action.table_name
		@connection 							= @action.connection
		@system 									= @connection.system
		@path_system_files 				= File.join( File.dirname( __FILE__ ) , @system.code.upcase )																															  						# string like '#{current_dir}/MCR'
		@path_system_files_tmp 		= File.join( @path_system_files , 'tmp' )
		@model										= @action.table_name.classify.safe_constantize
		@LOGGER 									= Logger.new( File.join( CONST_PATH_LOGS , @system.code.upcase + ".log" ) )

		method_params_and_values	= method( __method__ ).parameters.map { |arg| arg[1] }.map { |arg| "#{arg} = #{eval arg.to_s}" }	# hash   like { arg: arg_value , arg: arg_value, ... }


		job_id_post = ( job_id.present? ? "JOB_ID##{job_id} | " : "" )

		@LOGGER.info( "#{job_id_post}******************************* [ INIT ] *******************************" )
		@LOGGER.info( "#{job_id_post}" )
		@LOGGER.info( "#{job_id_post}" + [ __method__.to_s , @model.to_s ].join('|') + ( method_params_and_values ? " | PARAMS => " + method_params_and_values.join(', ') : " | NO PARAMS" ) )
		@LOGGER.info( "#{job_id_post}------------------------------------------------------------------------" )
		@LOGGER.info( "#{job_id_post}" )

		# SETTINGS VARS - Pre set
		ret_all_regs							= []																																																																		# array empty for append rets
		arr_columns_unique_as_sym = {}
		actions_to_do_after 			= []
		arr_model_unique_indexes	= ActiveRecord::Base.connection.indexes( @action.table_name ).map{ |i| [i] if i.unique == true }.compact

		unless arr_model_unique_indexes.blank?

			arr_columns_unique_as_sym = arr_model_unique_indexes.first.first.columns.map{ |col| col.parameterize.underscore.to_sym }																				# Array of [ :column_unique, :column_unique , ... ]

			hash_column_types 				= Hash[ @model.columns_hash.map{ |ct| [ ct[0].to_s , ct[1].type.to_s ] } ]																																# Array of [ :column_name => 'integer' , :column_name => 'string' , ... ]

			unless param_conditions
				if @action.conditions_default
					@conditions = eval( @action.conditions_default )
				else
					@conditions = {}
				end
			else
				if param_conditions.class == Hash
					@conditions = param_conditions
				elsif param_conditions.class == String
					@conditions = eval( param_conditions )
				else
					@conditions = {}
				end
			end

			if @system == System.find_by( :code => "GBB" )
				unless param_schemas
					if @action.schemas_default
						@schemas = GobbiSchema.where( name: eval( @action.schemas_default ) ).map{ |schema| schema.name }
					else
						@schemas = GobbiSchema.where( status: GobbiSchema::ACTIVE ).map{ |schema| schema.name }
					end
				else
					if param_schemas.class == Array
						@schemas = GobbiSchema.where( name: param_schemas ).map{ |schema| schema.name }
					elsif param_schemas.class == String
						@schemas = GobbiSchema.where( name: eval( param_schemas ) ).map{ |schema| schema.name }
					else
						@schemas = []
					end
				end
			end

			if @conditions

				if ( ( @conditions[:from_date] && @conditions[:to_date] ) && ( @conditions[:from_date] <= @conditions[:to_date] ) )																									# Set variables of range of time
					@fechahora_inicio  	= "#{ @conditions[:from_date] }"
					@fechahora_fin 			= "#{ @conditions[:to_date] }"
				else
					@fechahora_inicio  	= Time.now.localtime - ExternalSystems::Constants.const_get(:CONST_TIMEAGO_AMOUNT_MEDIUMLEVEL)
					@fechahora_fin 			= Time.now.localtime
				end

				if ExternalSystems::Constants.const_get( :CONST_CONNECTION_TYPES )[:db_connection].include? @connection.kind

					path_system_file_action		= File.join( @path_system_files , @action_name + ( special_action.present? ? "_#{special_action}" : "" ) +".sql.erb" )																			# string like '#{current_dir}/MCR/get_model_name.sql.erb'
					path_system_file_perform 	= File.join( @path_system_files , 'perform_' + @action_name + ( special_action.present? ? "_#{special_action}" : "" ) + ".yml.erb" )												# string like '#{current_dir}/MCR/perform_get_model_name.yml.erb'

					if File.exists?( path_system_file_action )

						connection_params							= {}																																					# We set al vars for driver database connection

						connection_params[:host]			= @connection.host																														# set :host
						connection_params[:port]			= @connection.port																														# set :port
						connection_params[:username]	= @connection.username																												# set :username
						connection_params[:password]	= @crypter.decrypt_and_verify( @connection.password )													# set :password

						str_query_template 	= ERB.new( File.read( path_system_file_action ) )																							# For editing template with seted variables
						str_query 					= str_query_template.result( binding )																												# set Query

						File.open( path_system_file_action + ".tmp" , "w" ) { |f| f.write str_query}																				# Write last query executed

						if @connection.kind == ExternalSystems::Constants.const_get( :CONST_CONNECTION_TYPE_DB_MYSQL )
							conn = Mysql2::Client.new( connection_params )		 																															# Get connection if mysql
							all_rows = conn.query( str_query )																																							# Get all rows of query if mysql
						else
							conn = TinyTds::Client.new( connection_params ) 																																# Get connection if mssql
							all_rows 				= conn.execute( str_query ) 																																						# Get all rows of query if sql_server
							all_rows 				= all_rows.to_a																																												# Extra convert to array
						end

						conn.close																																																				# Close connection established

					end

					if ( File.exists?( path_system_file_perform ) )

						hash_data = []
						perform_system_file_perform		= File.read( path_system_file_perform )
						all_rows.each do |instance|
							@self = self
							@instance = instance.deep_symbolize_keys

							perform_file_erb_result	= ERB.new( perform_system_file_perform ).result( binding )
							instance								= YAML.load( perform_file_erb_result )

							if !hash_data.map{ |r| r.except("number_row") }.include?( instance.except( "number_row" ) )
								instance[ "number_row" ] = 0
								hash_data << instance
							else
								instance[ "number_row" ] = hash_data.map{ |r| r.except("number_row") }.count( instance.except( "number_row" ) )
								hash_data << instance
							end

						end

						all_rows = hash_data
					else
						#TODO CHECK IF THIS WORKS... IF perform FILE NOT EXISTS WE CLONE INSTANCE TO INSTANCE
						all_rows.each do |instance|
							@self = self
#							@instance = instance.deep_symbolize_keys

							if !hash_data.map{ |r| r.except("number_row") }.include?( instance.except( "number_row" ) )
								instance[ "number_row" ] = 0
								hash_data << instance
							else
								instance[ "number_row" ] = hash_data.map{ |r| r.except("number_row") }.count( instance.except( "number_row" ) )
								hash_data << instance
							end

						end

						all_rows = hash_data

					end



				end

				# Web Services
				if ExternalSystems::Constants.const_get( :CONST_CONNECTION_TYPES )[:web_service].include? @connection.kind

					# Client object
					if @connection.username.present? and @connection.password.present?
						connection_username	= @connection.username
						connection_password	= @crypter.decrypt_and_verify( @connection.password )
						wsdl_client = Savon::Client.new( :wsdl => "https://#{@connection.connection_detail_no_user}" , :basic_auth => [ connection_username , connection_password ] , :ssl_verify_mode => :none )
					else
						wsdl_client = Savon::Client.new( :wsdl => "http://#{@connection.connection_detail}" )
					end
					if ( @conditions.include?( :from_date ) and @conditions.include?( :to_date ) ) and ( ( @conditions[ :from_date ].class == Date ) and ( @conditions[ :to_date ].class == Date ) ) and !( @conditions[ :from_date ] == @conditions[ :to_date ] )

						all_rows = []

						( ( @conditions[ :from_date ] )..( @conditions[ :to_date ] ) ).to_a.each do | x_date |

							@conditions = @conditions.except(*[:from_date, :to_date])
							@conditions[ :date ] =  x_date

							ret_wsdl = detect_wsdl_data( wsdl_client , @system , @action.table_name , @conditions , special_action )

							if ret_wsdl.class == Array
								# through perform and structure, detect data
								all_rows = all_rows + ret_wsdl
							elsif ret_wsdl.class == Hash

								if !all_rows.present?
									# through perform and structure, detect data
									all_rows = ret_wsdl
								else
									# through perform and structure, detect data
									all_rows = all_rows.keep_merge( ret_wsdl )
								end

							end

						end

					else

						# through perform and structure, detect data
						all_rows = detect_wsdl_data( wsdl_client , @system , @action.table_name , @conditions , special_action )

					end

				end

				# Access to data throught file accessing
				if ExternalSystems::Constants.const_get( :CONST_CONNECTION_TYPES )[:file_access].include? @connection.kind

					path_system_file_data	= File.join( @path_system_files_tmp , @action.table_name + ".#{@connection.kind}" )																			# string like '#{current_dir}/MCR/get_model_name.sql.erb'

					if File.exists?( path_system_file_data )

						encoding = `file -b --mime-encoding #{path_system_file_data} `[0..-2]

						# PATCH FOR PRP and agencies and CSV-unknown-8bit file encoding

						if ( ( @action.table_name == 'agencies' ) and ( @system == System.find_by( :code => 'PRP' ) ) and ( encoding == "unknown-8bit" ) )
							data_system_file = File.open( path_system_file_data , "r:CP1252").read.gsub( /\",\"/ , "||" ).gsub(/\"/,'').gsub(/\r/,'')
						elsif ( ( @action.table_name == 'lot_closure_transactions' ) and ( @system == System.find_by( :code => 'NPS' ) ) and ( encoding == "utf-8" ) )
							data_system_file = File.open( path_system_file_data , "r:CP1252").read.gsub( /\",\"/ , ',' ).gsub( /\"/ , '' )
						elsif ( encoding == "us-ascii" )
							data_system_file = File.open( path_system_file_data )
						else
							data_system_file = File.open( path_system_file_data , "r:#{encoding.upcase}")
						end

						if @connection.kind == "csv"
							separator = ACSV::Detect.separator( data_system_file )
							if data_system_file
								all_rows = detect_csvfile_data( @system , @action.table_name , data_system_file , separator )
							end
						elsif @connection.kind == "xls"
							if data_system_file
								all_rows = detect_xlsfile_data( @system , @action.table_name , data_system_file )
							end
						elsif @connection.kind == "nsv"
							if data_system_file
								all_rows = detect_nsvfile_data( @system , @action.table_name , data_system_file )
							end
						end

					else

						puts "No existe fichero ==> #{path_system_file_data}"

					end

				end

				# Any row?
				if all_rows

					# If is a Hash is because we must o do actions after insert index data
					if all_rows.class == Hash
						if all_rows[ :actions_after_all ].present?
							actions_to_do_after = all_rows[ :actions_after_all ]
							all_rows = all_rows[ :hash_data ]
						end
					end

					if special_action.present? and special_action == :to_csv
						ret_all_regs = all_rows
					else
						all_rows_count	= all_rows.count
						all_rows.each_with_index do | x_row , n_row |																																			# For each row, we create a new model instance with values formatted

							MyProgressBar::draw( n_row , all_rows_count ) 																																		# Draw

							x_row_replaced	 									  = replace_fk_columns( @model , x_row )

							tmp_unique_key_values 							= Hash[ arr_columns_unique_as_sym.map{ |k| [ k , x_row_replaced[k.to_s] ] } ]					# set hash with the unique key and the value, for then use it in search of model instances

							# if @system.code != "AUR"				# MIGRATION !!! FROM OLD AURELIA
							# 	tmp_unique_key_values[ :system_id ] = @system.id
							# end

							# tmp_unique_key_values									= replace_fk_columns( @model , x_row )
							ext_reg 															= @model.where( tmp_unique_key_values.compact ).first_or_initialize 																	# Find or initialize instance of model
							x_row_final														= {}

							@model.column_names.each do | column_name |
								x_row_final[ column_name.to_sym ] = x_row_replaced[ column_name ] if x_row_replaced.include?( column_name )
							end

							x_row_final[ :external_jsondata ]			= x_row_replaced.deep_symbolize_keys.except( *x_row_final.keys ).to_json unless x_row_replaced.deep_symbolize_keys.except( *x_row_final.keys ).empty?
							x_row_final[ :synchronizer_id ]				= self.id
							x_row_final[ :system_id ]							= @system.id

							x_row_final.each do | column_key , column_val |																																				# For each column , we format them according on the type of the column
								ext_reg.send("#{column_key}=", format_value( column_val, hash_column_types[ "#{column_key}" ] ))											# According to name of column, we set the value formatted in there
							end

							# ext_reg.complete_resource_before_import

							ret_all_regs << ext_reg																																													# Append actual object to array of objects

						end
					end

				end

			else

				@LOGGER.info( "#{job_id_post}This model has no unique indexes" )

			end

		else

			@LOGGER.info( "#{job_id_post}No conditions specified" )

		end

		# If we have actions to do after insert central data we return a hash with both information
		if actions_to_do_after.present?
			return { :all_rows => ret_all_regs , :actions_to_do_after => actions_to_do_after }
		else
			return ret_all_regs
		end

	end

	def get_external_data_and_convert_to_csv param_conditions = nil, schemas = nil, job_id = nil

		process_time_start 	= Time.now																																												# mark init time of import ONE row

		# SET VARIABLES
		rows_status				 				= { news: [] , mods: [] , equal: [] , error: [] }																										# hash   with empty [ news | mods | equal | errors ] arrays
		action_rows_status 				= { news: [] , mods: [] , equal: [] , error: [] }																										# hash   with empty [ news | mods | equal | errors ] arrays
		arr_columns_unique_as_sym = {}
		@action 									= self
		@action_name							= @action.kind.to_s + "_" + @action.table_name
		@connection 							= @action.connection
		@system 									= @connection.system
		@LOGGER 									= Logger.new( File.join( CONST_PATH_LOGS , @system.code.upcase + ".log" ) )

		arr_model_unique_indexes	= ActiveRecord::Base.connection.indexes( @action.table_name ).map{ |i| [i] if i.unique == true }.compact

		job_id_post 							= ( job_id.present? ? "JOB_ID##{job_id} | " : "" )
		actions_to_do_after 			= []

		unless arr_model_unique_indexes.empty?

			arr_columns_unique_as_sym = arr_model_unique_indexes.first.first.columns.map{ |col| col.parameterize.underscore.to_sym }																				# Array of [ :column_unique, :column_unique , ... ]

			all_obj							= get_external_data( param_conditions, schemas , job_id , :to_csv )																				# Get all_rows

			csv_content = ""
			if all_obj.present?
				attributes = all_obj.sample.keys
				csv_content = CSV.generate(headers: true) do |csv|
					csv << attributes
					all_obj.each do | obj |
						csv << attributes.map{ | attr | obj[ attr ] }
					end
				end
			end

			# Make temporal file
			tmpcsv = Tempfile.new( [ 'export' , '.csv' ], Rails.root.join('tmp')  )
			tmpcsv.write( csv_content )
			tmpcsv.close

			tempfile_name = tmpcsv.path.gsub( Rails.root.join('tmp/').to_s , "" )

			# Move file to public for accessing web
			FileUtils.mv( tmpcsv.path , Rails.root.join('public/exports/') + tempfile_name )

			n_all 							= all_obj.count																																									# Total of all_rows
			#byebug
		end

		process_time_end = Time.now																																														# mark init time of import ONE row
		process_time_elapsed = ( process_time_end - process_time_start ).to_f.round(3)																				# Calc the difference of time marks

		if action_rows_status.present?
			return { :file_path => File.join( CONST_PATH_LOGS , @system.code.upcase + ".log" ).gsub( Rails.root.to_s , '' ) , :ret_method => rows_status , :extra => action_rows_status , :csv_file => ( tempfile_name.present? ? tempfile_name : "" )	}
		else
			return { :file_path => File.join( CONST_PATH_LOGS , @system.code.upcase + ".log" ).gsub( Rails.root.to_s , '' ) , :ret_method => rows_status , :csv_file => ( tempfile_name.present? ? tempfile_name : "" ) }
		end
	end

	def get_external_data_and_sync param_conditions = nil, schemas = nil, job_id = nil

		process_time_start 	= Time.now																																												# mark init time of import ONE row

		# SET VARIABLES
		rows_status				 				= { news: [] , mods: [] , equal: [] , error: [] }																										# hash   with empty [ news | mods | equal | errors ] arrays
		action_rows_status 				= { news: [] , mods: [] , equal: [] , error: [] }																										# hash   with empty [ news | mods | equal | errors ] arrays
		arr_columns_unique_as_sym = {}
		@action 									= self
		@action_name							= @action.kind.to_s + "_" + @action.table_name
		@connection 							= @action.connection
		@system 									= @connection.system
		@LOGGER 									= Logger.new( File.join( CONST_PATH_LOGS , @system.code.upcase + ".log" ) )

		arr_model_unique_indexes	= ActiveRecord::Base.connection.indexes( @action.table_name ).map{ |i| [i] if i.unique == true }.compact

		job_id_post 							= ( job_id.present? ? "JOB_ID##{job_id} | " : "" )
		actions_to_do_after 			= []

		unless arr_model_unique_indexes.empty?

			arr_columns_unique_as_sym = arr_model_unique_indexes.first.first.columns.map{ |col| col.parameterize.underscore.to_sym }																				# Array of [ :column_unique, :column_unique , ... ]

			all_obj							= get_external_data( param_conditions, schemas , job_id )																				# Get all_rows

			# If is a Hash is because we must o do actions after insert index data
			if all_obj.class == Hash
				if all_obj[ :actions_to_do_after ].present?
					actions_to_do_after = all_obj[ :actions_to_do_after ]
					all_obj = all_obj[ :all_rows ]
				end
			end

			n_all 							= all_obj.count																																									# Total of all_rows

			# Iterate for each row
			all_obj.each.with_index(1) do | x_obj , n_obj |

				n_status 						= "#{n_obj}/#{n_all}"																																						# String like 1/233
				n_status_percentaje	= ( n_obj.to_f / n_all.to_f * 100.00 ).to_i																											# String like 1/233


				tmp_unique_key_values = Hash[ arr_columns_unique_as_sym.map{ |k| [ k , x_obj[k.to_s] ] } ]											# set hash with the unique key and the value, for then use it in search of model instances
				puts "#{n_status} > Checking to save... #{tmp_unique_key_values.to_s}"

				# Logging actions did it
				if x_obj.persisted? and !x_obj.changed?
					@LOGGER.info( "#{job_id_post}(#{n_status_percentaje}%) Checking #{n_status} #{tmp_unique_key_values.to_s} > [=]" )
					rows_status[:equal] += [ x_obj ]
				else
					if x_obj.persisted? and x_obj.changed
						@LOGGER.info( "#{job_id_post}(#{n_status_percentaje}%) Checking #{n_status} #{tmp_unique_key_values.to_s} > [M] => #{x_obj.changes.to_s}" )
						rows_status[:mods] += [ x_obj ]
					else
						@LOGGER.info( "#{job_id_post}(#{n_status_percentaje}%) Checking #{n_status} #{tmp_unique_key_values.to_s} > [+] => #{ x_obj.attributes.map{ |f,v| v }.compact.join('|') }" )
						rows_status[:news] += [ x_obj ]
					end

					# try to save instance
					begin
						rows_status[:error] += [ x_obj ] if !x_obj.save # We save, updated or insert
					rescue => e
						rows_status[:error] += [ x_obj ]
						puts "Ha ocurrido la excepción \"#{e.message}\", con la instancia: \n #{ x_obj.to_json }".red
					end

				end

			end

			# IF we have to do actions after insert central data
			if actions_to_do_after.present?

				# For each action
				actions_to_do_after.each do |action|

					# Must to be a creation of instance at the end of code
					if action[ :action ] == "create_instance_model_at_end"

						action_model 					= action[ :model ]
						action_file_path 			= action[ :action_file_path ]
						action_data 					= action[ :data ]

						action_model_constant = action_model.pluralize.camelize.classify.safe_constantize
						hash_column_types = Hash[ action_model_constant.columns_hash.map{ |ct| [ ct[0].to_s , ct[1].type.to_s ] } ]
						arr_x_model_unique_indexes = ActiveRecord::Base.connection.indexes( action_model.pluralize ).map{ |i| [i] if i.unique == true }.compact

						if File.exists?( action_file_path )

							perform_action_file_object = File.read( action_file_path )

							action_data_count = action_data.count

							action_data.each_with_index do | x_row , index |

								n_action_status 						= "#{index}/#{action_data_count}"																																						# String like 1/233
								n_action_status_percentaje	= ( index.to_f / action_data_count.to_f * 100.00 ).to_i

								@x_row = x_row

								perform_file_erb_result	= ERB.new( perform_action_file_object ).result( binding )
								instance								= YAML.load( perform_file_erb_result )

								x_row_replaced = replace_fk_columns( action_model_constant , instance )

								arr_x_columns_unique_as_sym = arr_model_unique_indexes.first.first.columns.map{ |col| col.parameterize.underscore.to_sym }																				# Array of [ :column_unique, :column_unique , ... ]
								tmp_unique_key_values = Hash[ arr_x_columns_unique_as_sym.map{ |k| [ k , x_row_replaced[k.to_s] ] } ]					# set hash with the unique key and the value, for then use it in search of model instances

								ext_reg = action_model_constant.where( tmp_unique_key_values.compact ).first_or_initialize 																	# Find or initialize instance of model
								x_row_final = {}

								action_model_constant.column_names.each do | column_name |
									x_row_final[ column_name.to_sym ] = x_row_replaced[ column_name ] if x_row_replaced.include?( column_name )
								end

								x_row_final[ :external_jsondata ]	= x_row_replaced.deep_symbolize_keys.except( *x_row_final.keys ).to_json unless x_row_replaced.deep_symbolize_keys.except( *x_row_final.keys ).empty?
								x_row_final[ :synchronizer_id ]	= self.id

								x_row_final.each do | column_key , column_val |																																				# For each column , we format them according on the type of the column
									ext_reg.send("#{column_key}=", format_value( column_val, hash_column_types[ "#{column_key}" ] ))											# According to name of column, we set the value formatted in there
								end

								puts " --------------------------------------------------------------------------- "
								puts "#{n_action_status} > Checking to save... #{tmp_unique_key_values.to_s}"

								# Logging actions did it
								if ext_reg.persisted? and !ext_reg.changed?
									@LOGGER.info( "#{job_id_post}(#{n_action_status_percentaje}%) Checking #{n_action_status} #{tmp_unique_key_values.to_s} > [=]" )
									action_rows_status[:equal] += [ ext_reg ]
								else
									if ext_reg.persisted? and ext_reg.changed
										@LOGGER.info( "#{job_id_post}(#{n_action_status_percentaje}%) Checking #{n_action_status} #{tmp_unique_key_values.to_s} > [M] => #{ext_reg.changes.to_s}" )
										action_rows_status[:mods] += [ ext_reg ]
									else
										@LOGGER.info( "#{job_id_post}(#{n_action_status_percentaje}%) Checking #{n_action_status} #{tmp_unique_key_values.to_s} > [+] => #{ ext_reg.attributes.map{ |f,v| v }.compact.join('|') }" )
										action_rows_status[:news] += [ ext_reg ]
									end

									# try to save instance
									begin
										action_rows_status[:error] += [ ext_reg ] if !ext_reg.save # We save, updated or insert
									rescue => e
										action_rows_status[:error] += [ ext_reg ]
										puts "Ha ocurrido la excepción \"#{e.message}\", con la instancia: \n #{ ext_reg.to_json }".red
									end
								end

							end

						end

					end

				end

			end

		end

		process_time_end = Time.now																																														# mark init time of import ONE row
		process_time_elapsed = ( process_time_end - process_time_start ).to_f.round(3)																				# Calc the difference of time marks

		@LOGGER.info( "#{job_id_post}" )
		@LOGGER.info( "#{job_id_post}------------------------------------------------------------------------" )
		@LOGGER.info( "#{job_id_post}" )
		@LOGGER.info( "#{job_id_post}                  " + "System".bold + ": (#{@system.id}) #{@system.code} - #{@system.name}" )
		@LOGGER.info( "#{job_id_post}            " + "Process name".bold + ": #{@action_name}" )
		@LOGGER.info( "#{job_id_post}  " + "The process started at".bold + ": #{process_time_start}" )
		@LOGGER.info( "#{job_id_post}  " + "The process finised at".bold + ": #{process_time_end}" )
		@LOGGER.info( "#{job_id_post}            " + "Time elapsed".bold + ": #{process_time_elapsed} seconds" )
		@LOGGER.info( "#{job_id_post}" )
		@LOGGER.info( "#{job_id_post}              " + "Total rows".bold + ": #{rows_status.map{ |k,v| v.count }.sum} rows" )
		@LOGGER.info( "#{job_id_post}               " + "Rows news".bold + ": #{rows_status[:news].count} rows" )
		@LOGGER.info( "#{job_id_post}             " + "Rows equals".bold + ": #{rows_status[:equal].count} rows" )
		@LOGGER.info( "#{job_id_post}               " + "Rows mods".bold + ": #{rows_status[:mods].count} rows" )
		@LOGGER.info( "#{job_id_post}              " + "Rows error".bold + ": #{rows_status[:error].count} rows" )

		if action_rows_status.present?
			@LOGGER.info( "#{job_id_post}		EXTRA DATA IMPORTED " )
			@LOGGER.info( "#{job_id_post}              " + "Total rows".bold + ": #{action_rows_status.map{ |k,v| v.count }.sum} rows" )
			@LOGGER.info( "#{job_id_post}               " + "Rows news".bold + ": #{action_rows_status[:news].count} rows" )
			@LOGGER.info( "#{job_id_post}             " + "Rows equals".bold + ": #{action_rows_status[:equal].count} rows" )
			@LOGGER.info( "#{job_id_post}               " + "Rows mods".bold + ": #{action_rows_status[:mods].count} rows" )
			@LOGGER.info( "#{job_id_post}              " + "Rows error".bold + ": #{action_rows_status[:error].count} rows" )
		end
		@LOGGER.info( "#{job_id_post}" )
		@LOGGER.info( "#{job_id_post}******************************* [ ENDS ] *******************************" )

		if action_rows_status.present?
			return { :file_path => File.join( CONST_PATH_LOGS , @system.code.upcase + ".log" ).gsub( Rails.root.to_s , '' ) , :ret_method => rows_status , :extra => action_rows_status }
		else
			return { :file_path => File.join( CONST_PATH_LOGS , @system.code.upcase + ".log" ).gsub( Rails.root.to_s , '' ) , :ret_method => rows_status }
		end
	end


	private

	# ------------------------------------------------------------------------
	# MIGRATION | FOR IDENTIFY SAME REGISTRY BETWEEN OLD AND NEW AURELIA
	def get_identical_from_old model , unique_value
		CONST_ARR_SYS_CODE_ID[ unique_value ] if model == :system
	end

	# ------------------------------------------------------------------------
	# FORMATING VALUES ACCORDING TO TYPE PASSED
	def format_value arg_value, arg_type
		return nil unless arg_value
		case arg_type
			when 'string'
				ret_value = arg_value.to_s.strip[0..255]
			when 'text'
				ret_value = arg_value.to_s.strip
			when 'integer'
				ret_value = arg_value.to_s.to_i
			when 'float'
				ret_value = arg_value.to_s.to_f.round(4)
			when 'decimal'
				ret_value = arg_value.to_s.to_f.round(4)
			when 'datetime'
				# If data contain only DATE TIME, we append ZONETIME
				if arg_value && arg_value.to_s.split(" ").count == 2
					ret_value = DateTime.strptime( [ arg_value.to_s , DateTime.now.strftime("%z") ].join(" ") , ExternalSystems::Constants.const_get(:CONST_MYSQL_FORMATOF_DATETIMEZ) )
				else
					ret_value = DateTime.strptime( arg_value.to_s , ExternalSystems::Constants.const_get(:CONST_MYSQL_FORMATOF_DATETIMEZ) )
				end
			when 'time'
				ret_value = Time.strptime( arg_value.to_s , ExternalSystems::Constants.const_get(:CONST_MYSQL_FORMATOF_TIME) ) if arg_value
			when 'date'
				if arg_value.to_s =~ /^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/
					ret_value = Date.strptime( arg_value.to_s , ExternalSystems::Constants.const_get( :CONST_MYSQL_FORMATOF_DATE_YYYY_MM_DD ) ) if arg_value
				elsif arg_value.to_s =~ /^[0-9]{2}\-[0-9]{2}\-[0-9]{2}$/
					ret_value = Date.strptime( arg_value.to_s , ExternalSystems::Constants.const_get( :CONST_MYSQL_FORMATOF_DATE_YY_MM_DD ) ) if arg_value
				elsif arg_value.to_s =~ /^[0-9]{4}[0-9]{2}[0-9]{2}$/
					ret_value = Date.strptime( arg_value.to_s , ExternalSystems::Constants.const_get( :CONST_MYSQL_FORMATOF_DATE_YYYYMMDD ) ) if arg_value
				elsif arg_value.to_s =~ /^[0-9]{2}[0-9]{2}[0-9]{2}$/
					ret_value = Date.strptime( arg_value.to_s , ExternalSystems::Constants.const_get( :CONST_MYSQL_FORMATOF_DATE_YYMMDD ) ) if arg_value
				elsif arg_value.to_s =~ /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/
					ret_value = Date.strptime( arg_value.to_s , ExternalSystems::Constants.const_get( :CONST_MYSQL_FORMATOF_DATE_DD_MM_YYYY ) ) if arg_value
				elsif arg_value.to_s =~ /^[0-9]{2}\-[0-9]{2}\-[0-9]{4}$/
					ret_value = Date.strptime( arg_value.to_s , ExternalSystems::Constants.const_get( :CONST_MYSQL_FORMATOF_DATE_DD_MM_YYYY_DASH ) ) if arg_value
				elsif arg_value.to_s =~ /^[0-9]{2}\/[0-9]{2}\/[0-9]{2}$/
					ret_value = Date.strptime( arg_value.to_s , ExternalSystems::Constants.const_get( :CONST_MYSQL_FORMATOF_DATE_DD_MM_YY ) ) if arg_value
				elsif arg_value.to_s =~ /^[0-9]{2}\-[0-9]{2}\-[0-9]{2}$/
					ret_value = Date.strptime( arg_value.to_s , ExternalSystems::Constants.const_get( :CONST_MYSQL_FORMATOF_DATE_DD_MM_YY_DASH ) ) if arg_value
				elsif arg_value.to_s =~ /^[0-9]{2}[0-9]{2}[0-9]{4}$/
					ret_value = Date.strptime( arg_value.to_s , ExternalSystems::Constants.const_get( :CONST_MYSQL_FORMATOF_DATE_DDMMYYY ) ) if arg_value
				elsif arg_value.to_s =~ /^[0-9]{2}[0-9]{2}[0-9]{2}$/
					ret_value = Date.strptime( arg_value.to_s , ExternalSystems::Constants.const_get( :CONST_MYSQL_FORMATOF_DATE_DDMMYY ) ) if arg_value
				else
					ret_value = arg_value
				end
			when 'binary'
				ret_value = arg_value.to_s.strip
			when 'boolean'
				ret_value = ( ( arg_value.to_s == '1' || arg_value.to_s == 1 ) ? true : false )
			when nil
				ret_value = arg_value.to_s.strip
		end
	end

	# -------------------------------------------------------------------------------------------------------------------------------------
	# -------------------------------------------------------------------------------------------------------------------------------------
	# 	 																	METHODS BY DATA ACCESSING TYPE ( WSDL, CSV, NSV, XLS )
	# -------------------------------------------------------------------------------------------------------------------------------------
	# -------------------------------------------------------------------------------------------------------------------------------------

	# Parsing wsdl data
	def detect_wsdl_data wsdl_client, system_ref, table_name, conditions, special_action = nil

		hash_column_types 	= Hash[ @model.columns_hash.map{ |ct| [ ct[0].to_s , ct[1].type.to_s ] } ]

		class_reference			= table_name.classify.safe_constantize
		hash_data 					= [ ]

		# For structure ( FILE OBJECT )
		if special_action.present?
			structure_file_path = Rails.root.to_s + "/lib/external_systems/#{system_ref.code.upcase}/structure_#{table_name}_#{special_action}.yml"
		else
			structure_file_path = Rails.root.to_s + "/lib/external_systems/#{system_ref.code.upcase}/structure_#{table_name}.yml"
		end
		unless ( File.exists?(structure_file_path) )
			puts "No existe fichero de estructura '#{structure_file_path}'"
			return []
		end
		structure_file_load_obj	= File.read( structure_file_path )
		struct_file = HashWithIndifferentAccess.new( YAML.load( structure_file_load_obj ) )

		steps							= struct_file[ "steps" ]

		# for the next service calling
		arr_collects = []

		# To do actions after import this data => Array of hashes with :action and :data
		actions_after_all = []

		if steps.present?

			steps.each_with_index do | x_step, n_step |

				service_name 							= x_step[ "service_name" ]
				service_responses					= x_step[ "service_responses" ]

				static_conditions 				= x_step[ "static_conditions" ]
				params_translations				= x_step[ "params_translations" ]
				params_input 							= x_step[ "params_input" ]
				collect_for_next_step			= x_step[ "collect_for_next_step" ]
				params_output 						= x_step[ "params_output" ]

				service_response_actions	= x_step[ "service_response_actions" ]

				# Here we store all responses of service calls
				arr_of_wsdl_service_call_responses = []

				# Depending of conditions we realize several calls to the service
				# IF we dont have collection of attributes grouped in the previous calling
				# We do it a mix of conditions between default conditions ( from yml and params passed )
				unless arr_collects.present?

					# Merging all arg => values* of static_conditions in file
					if static_conditions.present?
						static_conditions			= static_conditions.map{ | sc_key , sc_values | ( ( sc_values.class == String and sc_key.starts_with?("eval_") ) ? { sc_key.gsub("eval_","") => eval( sc_values ) } : { sc_key => sc_values } ) }.reduce({},:merge)
						all_conditions 				= static_conditions.merge( conditions )
					else
						all_conditions 				= conditions
					end

					# We make a product of all conditions if any attribute is an Array of values
					# If any attribute is an array of values, we create several group of conditions realizing a product of all conditions
					array_of_conditions = []
					if all_conditions.present?
						static_conditions_mixed = all_conditions.map{|k,v| ( ( v.class == Array ) ? [k].product( v ) : [k].product( [v] ) ) }
						array_of_conditions 	= static_conditions_mixed.shift.product( *static_conditions_mixed ).map {|a| Hash[a]}
					end

					# We change the attribute names according the services
					array_of_conditions = array_of_conditions.map do | conditions , n_cond |
						( conditions.map do | cond_key , cond_value |
							if params_translations.present?
								if params_translations.include?( cond_key )
									{ params_translations[cond_key] => cond_value }
								else
									{ cond_key => cond_value }
								end
							else
								{ cond_key => cond_value }
							end
						end ).reduce({},:merge)
					end

				else

					# If arr_collects have any hash of attributes, we have passed for a service call yet

					# If we have params_output ( and arr_collects ) it significates that we must to perform these attributes for this service calling though collects
					if params_output.present?

						array_of_conditions = []
						arr_collects.each do | x_collect |
							# If collect_param is a hash with eval for evaluate an expression
							# Else is a plain value
							x_condition = ( params_output.map do |key,value|
								if value.class == ActiveSupport::HashWithIndifferentAccess
									{ key => eval( value[ "eval" ] ) }
								elsif value.class == String
									{ key => value }
								end
							end )

							# Perform params to pass to the next service calling
							x_condition = x_condition.reduce({},:merge)

							# We add the x_collect of attributes and values, from origin
							x_condition[ :x_collect ] = x_collect

							# Adding to the array of conditions
							array_of_conditions << x_condition

						end

					else

						array_of_conditions = arr_collects

					end

				end

				# IF any condition exists in array of conditions
				if array_of_conditions.present?

					array_of_conditions_count = array_of_conditions.count

					# Format every value attribute according to service action and params_input
					array_of_conditions.each_with_index do | group_of_conditions , index_condition |

						group_of_conditions_with_stringified_keys = ( group_of_conditions.map do | k , v |

							if params_input

								if params_input[ k.to_s ].present?

									unless k.to_s.starts_with?("*")
										if v.class == params_input[ k.to_s ][ "type" ].constantize

											if v.class == Date
												{ k.to_s => v.strftime( params_input[ k.to_s ][ "format" ] ) }
											else
												{ k.to_s => v }
											end

										end
									end

								else
									{ k.to_s => v }
								end

							else
								{ k.to_s => v }
							end

						end ).reduce( {},:merge )

						puts "#{(index_condition+1).to_s.rjust(5, ' ')}/#{array_of_conditions_count}) Consultando \"#{service_name}\" para => #{ group_of_conditions_with_stringified_keys.to_json }"
						ret_service_call = wsdl_client.call( service_name.to_sym , :message => group_of_conditions_with_stringified_keys )
						ret_service_call.to_hash[ :x_collect ] = group_of_conditions_with_stringified_keys[ "x_collect" ]

						arr_of_wsdl_service_call_responses << ret_service_call

					end

				else
					# IF none conditions, we call to the service plainly

					puts "Consultando \"#{service_name}\""
					ret_service_call = wsdl_client.call( service_name.to_sym )

					arr_of_wsdl_service_call_responses << ret_service_call

				end

				# For each response
				arr_of_wsdl_service_call_responses.each do | wsdl_service_call_response |

					# Get data from previous service calling
					@collection = wsdl_service_call_response.to_hash[ :x_collect ]

					# For each item in service_responses, we get service_response.body[ item1 ][ item2 ][ item3 ][ ... ]
					wsdl_service_call_response_data = service_responses.map{ |n| n.to_sym}.inject( wsdl_service_call_response.body ){|h, k| h.to_h[k]}

					# If the last instance is a Hash, indicates that the response is an unique element, we insert it in an array
					if wsdl_service_call_response_data.class == Hash
						wsdl_service_call_response_data = [ wsdl_service_call_response_data ]
					end

					arr_actions_perform_files = {}
					array_service_group_data = {}

					if service_response_actions.present?

						if service_response_actions[ "all_instances" ].present?
							# tasks to do with all instances, before mapping every one
							arr_actions_perform_files[ "all_instances" ] = []
							service_response_actions[ "all_instances" ].map do |action|
								if ( action["action_name"] == "group_all_values_by_key" )
									if wsdl_service_call_response_data.present?
										# hash_service_group_data = wsdl_service_call_response_data.sample.keys.map{ |key| { key => wsdl_service_call_response_data.map{ | instance | instance[ key ] } } }.reduce({},:merge)
										array_service_group_data = wsdl_service_call_response_data
									end
								end
							end
						end

						# We load one time the ERB file to be performed en every_instance for avoid multiples disk accesses
						if service_response_actions[ "every_instance" ].present?

							arr_actions_perform_files[ "every_instance" ] = []
							service_response_actions[ "every_instance" ].map do |action|

								# Create instantly an instance
								if ( action["action_name"] == "create_instance_model" )
									action_file_path = Rails.root.to_s + "/lib/external_systems/#{system_ref.code.upcase}/perform_#{action["action_params"]["model"].pluralize}_from_#{table_name}.yml.erb"
									if ( File.exists?( action_file_path ) )
										arr_actions_perform_files[ "every_instance" ] << { action["action_params"]["model"] => File.read( action_file_path ) }
									end
								end

								# Create instance of any model before next step
								if ( action["action_name"] == "create_instance_model_at_end" )

									# Hash with all data to do and action
									action_todo_after = {}

									# Add data to the array for actions to do after import this service data
									action_todo_after[ :action ] 						= action["action_name"]
									action_todo_after[ :model ] 						= action["action_params"][ "model" ]
									action_todo_after[ :action_file_path ] 	= Rails.root.to_s + "/lib/external_systems/#{system_ref.code.upcase}/perform_#{action["action_params"]["model"].pluralize}_from_#{table_name}.yml.erb"
									action_todo_after[ :data ] 							= wsdl_service_call_response_data

									# Add action to do after all
									action_todo_after_found = false

									# searching for action existance yet ?
									the_action = actions_after_all.select{ |action| ( ( action[:action] == action_todo_after[ :action ] ) and ( action[ :model ] == action_todo_after[ :model ] ) and ( action[ :action_file_path ] == action_todo_after[ :action_file_path ] ) ) }

									# IF action exists yet we only add data to the array, else we add action complete
									if the_action.present?
										the_action.first[ :data ] = the_action.first[ :data ] + action_todo_after[ :data ]
									else
										actions_after_all << action_todo_after
									end

								end

								# Create instantly an instance
								if ( action["action_name"] == "not_create_instance_model" )
									if special_action.present?
										action_file_path 	= Rails.root.to_s + "/lib/external_systems/#{system_ref.code.upcase}/perform_#{table_name}_#{special_action}.yml.erb"
									else
										action_file_path 	= Rails.root.to_s + "/lib/external_systems/#{system_ref.code.upcase}/perform_#{table_name}.yml.erb"
									end
									if ( File.exists?( action_file_path ) )
										arr_actions_perform_files[ "every_instance" ] << { action["action_params"]["model"] => File.read( action_file_path ) }
									end
								end
							end

						end

					end

					# For perform ( FILE OBJECT )
					if special_action.present?
						perform_file_path 	= Rails.root.to_s + "/lib/external_systems/#{system_ref.code.upcase}/perform_#{table_name}_#{special_action}.yml.erb"
					else
						perform_file_path 	= Rails.root.to_s + "/lib/external_systems/#{system_ref.code.upcase}/perform_#{table_name}.yml.erb"
					end
					unless ( File.exists?(perform_file_path) )
						puts "No existe fichero de perform '#{perform_file_path}'"
						return []
					end
					perform_file_load_obj		= File.read( perform_file_path )

					# This indicates that we ahve array of rows
					if array_service_group_data.present?

						# @instance_grouped = hash_service_group_data.map{ |key,values| { key => values } }.reduce({},:merge)
						@array_service_group_data = array_service_group_data

						# Add resource identificated throught headers
						@system_ref_id 	= system_ref.id
						@system_ref_code = system_ref.code

						perform_file_erb_result	= ERB.new( perform_file_load_obj ).result( binding )
						instance								= YAML.load( perform_file_erb_result )

						if !hash_data.map{ |r| r.except("number_row") }.include?( instance.except( "number_row" ) )
							instance[ "number_row" ] = 0
							hash_data << instance
						else
							instance[ "number_row" ] = hash_data.map{ |r| r.except("number_row") }.count( instance.except( "number_row" ) )
							hash_data << instance
						end

					else

						if wsdl_service_call_response_data.present?

							# for each element in csv we detect all data
							wsdl_service_call_response_data.each_with_index do | x_row , n_row |

								if service_response_actions.present?

									if service_response_actions[ "every_instance" ].present?

										service_response_actions[ "every_instance" ].each do | x_action |

											# Recognize the action to do
											x_action_name 	= x_action[ "action_name" ]
											x_action_params = x_action[ "action_params" ]

											# Create instance of any model before next step
											if ( x_action_name == "create_instance_model" )

												# Model and params for perform new instance model
												x_action_param_model = x_action_params[ "model" ]

												# File perform loading
												arr_actions_perform_files[ "every_instance" ].each do | x_perform |

													# If is the key we are searching
													if x_perform.keys.first == x_action_param_model

														# Prepare certain values to pass to the erb
														@x_row = x_row
														@system_ref = system_ref
														@self = self

														# Perform file and replace values getting attributes
														action_perform_file_erb_result 	= ERB.new( x_perform.values.first ).result( binding )
														action_instance_attributes			= YAML.load( action_perform_file_erb_result )

														# Find or initialize instance
														action_instance = x_action_param_model.camelize.constantize.find_or_initialize_by( action_instance_attributes.except( "synchronizer_id" ) )
														action_instance.save unless action_instance.persisted?

													end

												end

											end

											# Create instance of any model before next step
											if ( x_action_name == "not_create_instance_model" )

												# Model and params for perform new instance model
												if x_action[ "action_params" ].present?
													x_action_param_model = x_action_params[ "model" ]
												end

												# File perform loading
												arr_actions_perform_files[ "every_instance" ].each do | x_perform |

													# If is the key we are searching
													if x_perform.keys.first == x_action_param_model

														# Prepare certain values to pass to the erb
														@x_row = x_row
														@system_ref = system_ref
														@self = self

														# Perform file and replace values getting attributes
														action_perform_file_erb_result 	= ERB.new( x_perform.values.first ).result( binding )
														action_instance_attributes			= YAML.load( action_perform_file_erb_result )

														# Find or initialize instance
														#action_instance = x_action_param_model.camelize.constantize.find_or_initialize_by( action_instance_attributes.except( "synchronizer_id" ) )
														# action_instance.save unless action_instance.persisted?
														hash_data << action_instance_attributes

													end

												end

											end

										end

									end

								end

								# CSV ?
								if special_action.present? and x_step == steps.last

									# Push this collect of { attr => value , ... } to collect array for to be used in the next step
									arr_collects << x_row

								else

									# This indicates that we must to collect certaing values for pass in the next service
									unless collect_for_next_step.present?

										# Add resource identificated throught headers
										@system_ref_id 	= system_ref.id
										@system_ref_code = system_ref.code
										@self = self
										@instance = ( x_row.map do | key , value |
											unless value.class == Hash and value == { :"@xml:space"=>"preserve" }
												unless key.to_s.starts_with?( "empty" )
													# format each string
													{ key.to_sym => format_value( value, hash_column_types[ "#{key}" ] ) }
												end
											else
												{ key.to_sym => "" }
											end
										end ).compact.reduce( {} , :merge )

										perform_file_erb_result	= ERB.new( perform_file_load_obj ).result( binding )
										instance								= YAML.load( perform_file_erb_result )

										if !hash_data.map{ |r| r.except("number_row") }.include?( instance.except( "number_row" ) )
											instance[ "number_row" ] = 0
											hash_data << instance
										else
											instance[ "number_row" ] = hash_data.map{ |r| r.except("number_row") }.count( instance.except( "number_row" ) )
											hash_data << instance
										end

									else

										# Get only attributes and values how collect file indicates
										x_row_collect_data = x_row.slice( *collect_for_next_step.map{ |attr| attr.to_sym } )

										# Push this collect of { attr => value , ... } to collect array for to be used in the next step
										arr_collects << x_row_collect_data

									end

								end

							end

						else

							return []

						end

					end

				end # Each response service call

			end

		end

		# If we have actions to do after insert central data we return a hash with both information
		if actions_after_all.present?
			return { :hash_data => hash_data , :actions_after_all => actions_after_all }
		else
			return hash_data
		end

	end

	# Parsing CSV file data
	def detect_csvfile_data system_ref, table_name , file_ref, separator

		action_rows_status 	= { news: [] , mods: [] , equal: [] , error: [] }																										# hash   with empty [ news | mods | equal | errors ] arrays
		hash_column_types 	= Hash[ @model.columns_hash.map{ |ct| [ ct[0].to_s , ct[1].type.to_s ] } ]
		# :col_sep => "," , :quote_char => "'"

		if ( ( file_ref.class == String ) and ( @model == Agency ) and ( system_ref == System.find_by( :code => 'PRP' ) ) )
			parsed_data = CSV.parse( file_ref , :col_sep => "||" , :quote_char => '"' )
		elsif ( ( file_ref.class == String ) and ( @model == LotClosureTransaction ) and ( system_ref == System.find_by( :code => 'NPS' ) ) )
			parsed_data = CSV.parse( file_ref , :col_sep => "," , :quote_char => "'" )
		else
			parsed_data	= CSV.parse( file_ref , col_sep: separator )
		end

		class_reference			= table_name.classify.safe_constantize
		hash_data 					= [ ]

		if parsed_data.count > 0
			arr_field_counts_data = [ ]
			parsed_data.map do |l|
				unless arr_field_counts_data.empty?
					if arr_field_counts_data.last.keys.first == l.count
						arr_field_counts_data.last[l.count] = arr_field_counts_data.last[l.count]+1
					else
						arr_field_counts_data << { l.count => 1 }
					end
				else
					arr_field_counts_data << { l.count => 1 }
				end
			end

			# For any structure of CSV, we must to define a file with the structure of the CSV like the next structure, under ExternalSystems::{CODE_SYS}::structure_{table_name}.yml :
			#
			# FILE STRUCTURE:
			# -----------------------------------------------------------------
			# 	conditions: STRING TO EVALUATE CONDITION
			#   fields:
			#   	- # COLS WITH N COLUMNS
			#       - col1
			#       - col2
			#       - col3
			#       - ...
			#       - coln
			#   	- # COLS WITH N COLUMNS
			#       - col1
			#       - col2
			#       - ...
			#       - coln
			# ...

			# For perform instance ( FILE OBJECT )
			structure_file_path = Rails.root.to_s + "/lib/external_systems/#{system_ref.code.upcase}/structure_#{table_name}.yml"
			perform_file_path 	= Rails.root.to_s + "/lib/external_systems/#{system_ref.code.upcase}/perform_#{table_name}.yml.erb"

			unless ( File.exists?(structure_file_path) )
				puts "No existe fichero de estructura '#{structure_file_path}'"
				return []
			end
			unless ( File.exists?(perform_file_path) )
				puts "No existe fichero de perform '#{perform_file_path}'"
				return []
			end

			structure_file_load_obj	= File.read( structure_file_path )
			perform_file_load_obj		= File.read( perform_file_path )

			# Reading YML file
			struct_file = HashWithIndifferentAccess.new( YAML.load( structure_file_load_obj ) )
			structure_file = struct_file[ "structure" ]
			validation_file = struct_file[ "validation" ]

			actions_file = struct_file[ "actions" ]

			# Trying to validate
			begin
				# hash like { "validation_1" => eval( "validation_1" ) , "validation_2" => eval( "validation_2" ) , ... }
				validations = validation_file.map{ |validation| { validation => eval(validation) }}.reduce({}, :merge)
			rescue => except
				puts "Una excepción ocurrió al intentar validar el fichero"
				puts "#{except.to_s}"
				validations = { "" => false }
			end

			# Validate all validations in structure file
			if ( ( validations.values.uniq.count == 1 ) and ( validations.values.uniq.first == true ) )

				# hash like { cant_fields => [ col1 , col2 , col3, ... , coln ] , 18 =>  }
				hash_fields_by_cols = structure_file.map{ | key , fields | { fields.count => key } }.reduce( {} , :merge )

				# We take the first file structure of yml file
				last_file_structure_n = nil
				last_rows_count				= nil
				last_rows_count_n			= 0

				# for each element in csv we detect all data
				parsed_data.each_with_index do | x_row , n_row |

					if last_rows_count != x_row.count
						last_rows_count = x_row.count
						last_rows_count_n	= 0
					else
						last_rows_count_n	= last_rows_count_n + 1
					end

					# Go to the next row if it is not included in the structure
					unless hash_fields_by_cols.key?( last_rows_count )
						next
					end

					# Get key and name fields of structure by quantity of row fields
					last_structure_key 			= hash_fields_by_cols[ last_rows_count ]
					last_structure_fields		= structure_file[ last_structure_key ]

					if last_structure_key.split('_').include?("coredata")

						# Indexes
						arr_coredata_indexes = last_structure_key.split('_').index( "coredata" )

						# we detect if this row is valid row ( data valing )
						row_valid = last_rows_count_n >= arr_coredata_indexes

						# Until coredata is present
						if row_valid

							# Add resource identificated throught headers
							@system_ref_id = system_ref.id
							@system_ref_code = system_ref.code
							@self = self
							@instance = ( last_structure_fields.zip(x_row).map do | key , value |
								unless key.starts_with?( "empty" )

									# transform decimal string like "1.523.000,00" to 1523000.00, else normal way
									if hash_column_types[ "#{key}" ] == "decimal" and value =~ /^(?=.)(\d{1,3}(.\d{3})*)?(\,\d+)?$/
										{ key.to_sym => format_value( value.gsub(/[^\d^\,]/, '').to_f , hash_column_types[ "#{key}" ] ) }
									else
										{ key.to_sym => format_value( value, hash_column_types[ "#{key}" ] ) }
									end
								end
							end ).compact.reduce({},:merge)

							perform_file_erb_result	= ERB.new( perform_file_load_obj ).result( binding )
							begin
								instance								= YAML.load( perform_file_erb_result )
							rescue => exc
								puts "EXCEPTION OCCURRED >>>>#{exc}".red
							end

							if !hash_data.map{ |r| r.except("number_row") }.include?( instance.except( "number_row" ) )
								instance[ "number_row" ] = 0
								hash_data << instance
							else
								instance[ "number_row" ] = hash_data.map{ |r| r.except("number_row") }.count( instance.except( "number_row" ) )
								hash_data << instance
							end

						else

							# Pass to the next x_row
							next

						end

					end

					# puts "#{(n_row+1).to_s.rjust(5, ' ')}/#{last_rows_count_n}) parsed"
					puts "#{(n_row+1).to_s.rjust(5, ' ')}/#{parsed_data.count}) parsed"

				end

				# If we have a an action to do with all instances
				if actions_file
					if actions_file[ "all_instances" ].present?

						actions = actions_file[ "all_instances" ]

						actions.each do | action |

							action_name 	= action[ "action_name" ]
							action_params = action[ "action_params" ]
							collect = nil
							puts " >>> TASK: #{action_name} (#{action_params}) "

							# Creation of model
							if action_name == "create_instance_model_before"

								# Model to create
								action_model 			= action_params[ "model" ]

								# Prepare
								action_model_constant = action_model.pluralize.camelize.classify.safe_constantize
								hash_column_types = Hash[ action_model_constant.columns_hash.map{ |ct| [ ct[0].to_s , ct[1].type.to_s ] } ]
								arr_x_model_unique_indexes = ActiveRecord::Base.connection.indexes( action_model.pluralize ).map{ |i| [i] if i.unique == true }.compact

								action_file_path = Rails.root.to_s + "/lib/external_systems/#{system_ref.code.upcase}/perform_#{action_model.pluralize}_from_#{table_name}.yml.erb"

								if File.exists?( action_file_path )

									# Indexes of model to create
									arr_model_unique_indexes	= ActiveRecord::Base.connection.indexes( action_model.pluralize ).map{ |i| [i] if i.unique == true }.compact

									# Collect exists? AND NOT group by, ONLY collect
									if ( ( action_params[ "collect" ].present? ) and !( action_params[ "group_by" ].present? ) )

										# assign collection
										collect = action_params[ "collect" ]

										arr_collect = collect.split(',')
										collection_of_action = hash_data.map{ |d| d.slice( *arr_collect ) }
										instances_to_create = collection_of_action.uniq

									end

									# Grouping exists?
									# This create an hash of array of hash of array ... depends count of elements to group...
									# array of elements will be converted to this example
									#
									# { g_key_1 =>
									# 						g_key_2 => {
									# 													g_key_3 => Array ,
									# 													g_key_3 => Array
									#  						} ,
									#   g_key_1 =>
									# 						g_key_2 => {
									# 													g_key_3 => Array ,
									# 													g_key_3 => Array
									#  						} ,
									# 						g_key_2 => {
									# 													g_key_3 => Array ,
									# 													g_key_3 => Array
									# 						}
									# }
									#
									if action_params[ "group_by" ].present?

										# assign group_by
										group_by = action_params[ "group_by" ]
										arr_group_by = group_by.split(',').map{ |x| x.strip }
										arr_grouping_command = []
										arr_group_by.each_with_index do |e,depth|
											str_main_group = ".group_by{ |p_#{depth}| p_#{depth}[ '#{e}' ] }"
											if ( depth == 0 )
												arr_grouping_command << str_main_group
											else
												str_group_n = ""
												( 0..(depth-1) ).each do | n_depth |
													str_group_n += ".each_with_object({}) { |(k_#{depth},v_#{depth}), h_#{depth}| h_#{depth}[k_#{depth}] = v_#{depth}"
												end
												str_group_n += str_main_group
												( 0..(depth-1) ).each do | n_depth |
													str_group_n += "}"
												end
												arr_grouping_command << str_group_n
											end

										end

										str_grouping_command 	= "hash_data#{arr_grouping_command.join}"
										instances_to_create 	= eval( str_grouping_command )

										# Collect exists? WITH GROUP BY is only for filter rows
										if ( action_params[ "collect" ].present? )
											# assign collection
											collect = action_params[ "collect" ]

											arr_collect = collect.split(',')

											collect = action_params[ "collect" ]
											arr_collect = collect.split(',').map{ |x| x.strip }

											str_collect 		= ""

											( 0..(arr_collect.count-1) ).each do | n_depth |
												# str_collect += ".each_with_object({}) { |(k_#{n_depth},v_#{depth}), h_#{depth}| h_#{depth}[k_#{depth}] = v_#{depth}"
												str_collect += ".map{ |key_#{n_depth},values_#{n_depth}| { key_#{n_depth} => values_#{n_depth}"
											end

											str_collect += ".map{ |arr_hash| arr_hash.slice( *arr_collect ) } "

											( 0..(arr_collect.count-1) ).each do | n_depth |
												str_collect += "} }.reduce({},:merge)"
											end

											str_grouping_command 	= "instances_to_create#{str_collect}"
											instances_to_create 	= eval( str_grouping_command )

										end

									end


									perform_action_file_object = File.read( action_file_path )

									action_data_count = instances_to_create.count

									# This indicates that the instances are an array of elements for insert
									if instances_to_create.class == Array

										instances_to_create.each_with_index do | x_row , index |

											puts " >>> TASK: #{action_name} (#{action_params}) - Instance #{ ( index+1 ) } of #{action_data_count}"

											n_action_status 						= "#{index}/#{action_data_count}"																																						# String like 1/233
											n_action_status_percentaje	= ( index.to_f / action_data_count.to_f * 100.00 ).to_i

											@x_row = x_row.deep_symbolize_keys

											perform_file_erb_result	= ERB.new( perform_action_file_object ).result( binding )
											instance								= YAML.load( perform_file_erb_result )

											x_row_replaced = replace_fk_columns( action_model_constant , instance )

											arr_x_columns_unique_as_sym = arr_model_unique_indexes.first.first.columns.map{ |col| col.parameterize.underscore.to_sym }																				# Array of [ :column_unique, :column_unique , ... ]
											tmp_unique_key_values = Hash[ arr_x_columns_unique_as_sym.map{ |k| [ k , x_row_replaced[k.to_s] ] } ]					# set hash with the unique key and the value, for then use it in search of model instances

											ext_reg = action_model_constant.where( tmp_unique_key_values.compact ).first_or_initialize 																	# Find or initialize instance of model
											x_row_final = {}

											action_model_constant.column_names.each do | column_name |
												x_row_final[ column_name.to_sym ] = x_row_replaced[ column_name ] if x_row_replaced.include?( column_name )
											end

											x_row_final[ :external_jsondata ]	= x_row_replaced.deep_symbolize_keys.except( *x_row_final.keys ).to_json unless x_row_replaced.deep_symbolize_keys.except( *x_row_final.keys ).empty?
											x_row_final[ :synchronizer_id ]	= self.id

											x_row_final.each do | column_key , column_val |																																				# For each column , we format them according on the type of the column
												ext_reg.send("#{column_key}=", format_value( column_val, hash_column_types[ "#{column_key}" ] ))											# According to name of column, we set the value formatted in there
											end

											puts " --------------------------------------------------------------------------- "
											puts "#{n_action_status} > Checking to save... #{tmp_unique_key_values.to_s}"

											# Logging actions did it
											if ext_reg.persisted? and !ext_reg.changed?
												@LOGGER.info( "(#{n_action_status_percentaje}%) Checking #{n_action_status} #{tmp_unique_key_values.to_s} > [=]" )
												action_rows_status[:equal] += [ ext_reg ]
											else
												if ext_reg.persisted? and ext_reg.changed
													@LOGGER.info( "(#{n_action_status_percentaje}%) Checking #{n_action_status} #{tmp_unique_key_values.to_s} > [M] => #{ext_reg.changes.to_s}" )
													action_rows_status[:mods] += [ ext_reg ]
												else
													@LOGGER.info( "(#{n_action_status_percentaje}%) Checking #{n_action_status} #{tmp_unique_key_values.to_s} > [+] => #{ ext_reg.attributes.map{ |f,v| v }.compact.join('|') }" )
													action_rows_status[:news] += [ ext_reg ]
												end

												# try to save instance
												begin
													action_rows_status[:error] += [ ext_reg ] if !ext_reg.save # We save, updated or insert
												rescue => e
													action_rows_status[:error] += [ ext_reg ]
													puts "Ha ocurrido la excepción \"#{e.message}\", con la instancia: \n #{ ext_reg.to_json }".red
												end

											end

										end

									end

									# This indicates that the instances are a group of elements for insert
									if instances_to_create.class == Hash

										@group_of_elements 		= []
										arr_grouping_command 	= []

										# Hash with keys and nil values from group of collect
										arr_group_by.map{ |e| { e => nil }}.reduce( {} , :merge )

										str_collect 		= ""
										arr_instances = []
										( 0..(arr_group_by.count-1) ).each do | n_depth |
											if n_depth == ( arr_group_by.count-1 )
												str_x = arr_group_by.each_with_index.map{ | arr_elem , arr_depth | "ret_hash[ '#{arr_elem}' ] = eval('key_#{arr_depth}')" }.join( ' ; ' )
												str_collect += ".each{ |key_#{n_depth},values_#{n_depth}| ret_hash = { } \; #{str_x} \; ret_hash[ 'group' ] = values_#{n_depth} \; arr_instances << ret_hash } "
											else
												str_collect += ".each{ |key_#{n_depth},values_#{n_depth}| { key_#{n_depth} => values_#{n_depth}"
											end
										end

										( 0..(arr_group_by.count-1) ).each do | n_depth |
											str_collect += "}"
										end
										eval( "instances_to_create#{str_collect}" )

										instances_to_create = arr_instances

										instances_to_create.each_with_index do | x_row , index |

											puts " >>> TASK: #{action_name} (#{action_params}) - Instance #{ ( index+1 ) } of #{action_data_count}"


											n_action_status 						= "#{index}/#{action_data_count}"																																						# String like 1/233
											n_action_status_percentaje	= ( index.to_f / action_data_count.to_f * 100.00 ).to_i

											@x_row_grouped_and_collect = x_row.deep_symbolize_keys

											perform_file_erb_result	= ERB.new( perform_action_file_object ).result( binding )
											instance								= YAML.load( perform_file_erb_result )

											x_row_replaced = replace_fk_columns( action_model_constant , instance )

											arr_x_columns_unique_as_sym = arr_model_unique_indexes.first.first.columns.map{ |col| col.parameterize.underscore.to_sym }																				# Array of [ :column_unique, :column_unique , ... ]
											tmp_unique_key_values = Hash[ arr_x_columns_unique_as_sym.map{ |k| [ k , x_row_replaced[k.to_s] ] } ]					# set hash with the unique key and the value, for then use it in search of model instances

											ext_reg = action_model_constant.where( tmp_unique_key_values.compact ).first_or_initialize 																	# Find or initialize instance of model
											x_row_final = {}

											action_model_constant.column_names.each do | column_name |
												x_row_final[ column_name.to_sym ] = x_row_replaced[ column_name ] if x_row_replaced.include?( column_name )
											end

											x_row_final[ :external_jsondata ]	= x_row_replaced.deep_symbolize_keys.except( *x_row_final.keys ).to_json unless x_row_replaced.deep_symbolize_keys.except( *x_row_final.keys ).empty?
											x_row_final[ :synchronizer_id ]	= self.id

											x_row_final.each do | column_key , column_val |																																				# For each column , we format them according on the type of the column
												ext_reg.send("#{column_key}=", format_value( column_val, hash_column_types[ "#{column_key}" ] ))											# According to name of column, we set the value formatted in there
											end

											puts " --------------------------------------------------------------------------- "
											puts "#{n_action_status} > Checking to save... #{tmp_unique_key_values.to_s}"

											# Logging actions did it
											if ext_reg.persisted? and !ext_reg.changed?
												@LOGGER.info( "(#{n_action_status_percentaje}%) Checking #{n_action_status} #{tmp_unique_key_values.to_s} > [=]" )
												action_rows_status[:equal] += [ ext_reg ]
											else
												if ext_reg.persisted? and ext_reg.changed
													@LOGGER.info( "(#{n_action_status_percentaje}%) Checking #{n_action_status} #{tmp_unique_key_values.to_s} > [M] => #{ext_reg.changes.to_s}" )
													action_rows_status[:mods] += [ ext_reg ]
												else
													@LOGGER.info( "(#{n_action_status_percentaje}%) Checking #{n_action_status} #{tmp_unique_key_values.to_s} > [+] => #{ ext_reg.attributes.map{ |f,v| v }.compact.join('|') }" )
													action_rows_status[:news] += [ ext_reg ]
												end

												# try to save instance
												begin
													action_rows_status[:error] += [ ext_reg ] if !ext_reg.save # We save, updated or insert
												rescue => e
													action_rows_status[:error] += [ ext_reg ]
													puts "Ha ocurrido la excepción \"#{e.message}\", con la instancia: \n #{ ext_reg.to_json }".red
												end

											end

										end

									end

								end

							end

						end

					end

				end

			else

				puts "El fichero no cumple la validación"
				puts "Validaciones: #{validations.to_s}"

			end

		else

			puts "No hay registros"

		end

		return hash_data

	end

	# Parsing and detect XLS data
	def detect_xlsfile_data system_ref, table_name , file_ref

		require 'rubygems'

		hash_column_types 	= Hash[ @model.columns_hash.map{ |ct| [ ct[0].to_s , ct[1].type.to_s ] } ]
		xls_file						= ::Spreadsheet::open( file_ref )

		class_reference			= table_name.classify.safe_constantize
		hash_data 					= [ ]

		#
		if xls_file

			# For perform instance ( FILE OBJECT )
			structure_file_path = Rails.root.to_s + "/lib/external_systems/#{system_ref.code.upcase}/structure_#{table_name}.yml"
			perform_file_path 	= Rails.root.to_s + "/lib/external_systems/#{system_ref.code.upcase}/perform_#{table_name}.yml.erb"

			unless ( File.exists?(structure_file_path) )
				puts "No existe fichero de estructura '#{structure_file_path}'"
				return []
			end
			unless ( File.exists?(perform_file_path) )
				puts "No existe fichero de perform '#{perform_file_path}'"
				return []
			end

			structure_file_load_obj	= File.read( structure_file_path )
			perform_file_load_obj		= File.read( perform_file_path )

			# Reading YML file
			struct_file = HashWithIndifferentAccess.new( YAML.load( structure_file_load_obj ) )
			structure_file = struct_file[ "structure" ]
			validation_file = struct_file[ "validation" ]

			# Trying to validate
			begin
				# hash like { "validation_1" => eval( "validation_1" ) , "validation_2" => eval( "validation_2" ) , ... }
				validations = validation_file.map{ |validation| { validation => eval(validation) }}.reduce({}, :merge)
			rescue => except
				puts "Una excepción ocurrió al intentar validar el fichero"
				puts "#{except.to_s}"
				validations = { "" => false }
			end

			# Validate all validations in structure file
			if ( ( validations.values.uniq.count == 1 ) and ( validations.values.uniq.first == true ) )

				# key exists ?
				if structure_file[ "spreadsheet" ].present?

					# the spreadsheet where we will find the data
					parsed_data = xls_file.worksheet( structure_file[ "spreadsheet" ] )

				end

				# We take the first file structure of yml file
				row_valid							= false

				# for each element in csv we detect all data
				parsed_data.each_with_index do | x_row , n_row |

					# Go to the next row if it is not included in the structure
					unless x_row.join.strip == "#{ structure_file[ "title_coredata" ] }"
						unless row_valid
							next
						end
					else
						unless row_valid
							row_valid = true
							next
						end
					end

					# Get key and name fields of structure by quantity of row fields
					last_structure_fields = structure_file[ "coredata" ]

					# Until coredata is present
					if row_valid

						# Add resource identificated throught headers
						@system_ref_id = system_ref.id
						@system_ref_code = system_ref.code
						@instance = ( last_structure_fields.zip(x_row).map do | key , value |
							unless key.starts_with?( "empty" )
								# transform decimal string like "1.523.000,00" to 1523000.00, else normal way
								if hash_column_types[ "#{key}" ] == "decimal" and value =~ /^(?=.)(\d{1,3}(.\d{3})*)?(\,\d+)?$/
									{ key.to_sym => format_value( value.gsub(/[^\d^\,]/, '').to_f , hash_column_types[ "#{key}" ] ) }
								elsif hash_column_types[ "#{key}" ] == "date" and value.class == Float
									{ key.to_sym => x_row.date( last_structure_fields.index("#{key}") ) }
								elsif hash_column_types[ "#{key}" ] == "datetime" and value.class == Float
									{ key.to_sym => x_row.datetime( last_structure_fields.index("#{key}") ) }
								else
									{ key.to_sym => format_value( value, hash_column_types[ "#{key}" ] ) }
								end
							end
						end ).compact.reduce({},:merge)

						perform_file_erb_result	= ERB.new( perform_file_load_obj ).result( binding )
						instance								= YAML.load( perform_file_erb_result )


						if !hash_data.map{ |r| r.except("number_row") }.include?( instance.except( "number_row" ) )
							instance[ "number_row" ] = 0
							hash_data << instance
						else
							instance[ "number_row" ] = hash_data.map{ |r| r.except("number_row") }.count( instance.except( "number_row" ) )
							hash_data << instance
						end

					else
						# Pass to the next x_row
						next

					end

				end

			else

				puts "El fichero no cumple la validación"
				puts "Validaciones: #{validations.to_s}"

			end

		else

			puts "No hay registros"

		end

		return hash_data

	end

	# Parsing and detect NSV ( Nothing separated values - fixed chars ) data
	def detect_nsvfile_data system_ref, table_name , file_ref

		hash_column_types 	= Hash[ @model.columns_hash.map{ |ct| [ ct[0].to_s , ct[1].type.to_s ] } ]

		class_reference			= table_name.classify.safe_constantize
		hash_data 					= [ ]

		file_ref_dup = file_ref.dup

		# the split for each lines
		# lines_data = file_ref.each_line
		lines_data = file_ref.each_line.map{ |line| line }
		lines_data_chars_per_line = lines_data.map{ |line| line.size }

		# For perform instance ( FILE OBJECT )
		structure_file_path = Rails.root.to_s + "/lib/external_systems/#{system_ref.code.upcase}/structure_#{table_name}.yml"
		perform_file_path 	= Rails.root.to_s + "/lib/external_systems/#{system_ref.code.upcase}/perform_#{table_name}.yml.erb"

		unless ( File.exists?(structure_file_path) )
			puts "No existe fichero de estructura '#{structure_file_path}'"
			return []
		end
		unless ( File.exists?(perform_file_path) )
			puts "No existe fichero de perform '#{perform_file_path}'"
			return []
		end

		structure_file_load_obj	= File.read( structure_file_path )
		perform_file_load_obj		= File.read( perform_file_path )
		# Reading YML file
		struct_file = HashWithIndifferentAccess.new( YAML.load( structure_file_load_obj ) )
		structure_file = struct_file[ "structure" ]
		validation_file = struct_file[ "validation" ]

		# Trying to validate
		begin
			# hash like { "validation_1" => eval( "validation_1" ) , "validation_2" => eval( "validation_2" ) , ... }
			validations = validation_file.map{ |validation| { validation => eval(validation) }}.reduce({}, :merge)
		rescue => except
			puts "Una excepción ocurrió al intentar validar el fichero"
			puts "#{except.to_s}"
			validations = { "" => false }
		end

		# Validate all validations in structure file
		if ( ( validations.values.uniq.count == 1 ) and ( validations.values.uniq.first == true ) )

			# The maximum of chars in line
			chars_per_line = structure_file["coredata"].values.map{ |string_len| ( string_len.class == Fixnum ? string_len : string_len.size )}.sum

			# for each element in csv we detect all data
			lines_data.each_with_index do | x_row , n_row |

				# Delete the record separator
				line_string = x_row.chomp

				# Go to the next row if it is not included in the structure
				unless chars_per_line == line_string.size
					next
				end

				# To start cutting the string
				last_position_of_string = 0

				# Get key and name fields of structure by quantity of row fields
				last_structure_fields = structure_file[ "coredata" ]

				this_valid_reg = true
				this_reg = {}

				last_structure_fields.each do |key,value_len|

					if ( value_len.class == Fixnum )
						string_len = value_len
					else
						string_len = value_len.size
					end

					init_pos = last_position_of_string
					end_pos = ( last_position_of_string + string_len )
					substr =line_string[init_pos..(end_pos-1)]
					last_position_of_string = end_pos

					if key.starts_with?("zeros_")
						if !substr.gsub('0','').empty?
							this_valid_reg = false
						end
					elsif key.starts_with?("spaces_")
						if !substr.gsub(' ','').empty?
							this_valid_reg = false
						end
					else

						if ( value_len.class == String )
							if substr == value_len
								this_reg[key] = substr
							else
								this_valid_reg = false
							end
						else
							this_reg[key] = substr
						end

					end
				end

				# Add resource identificated throught headers
				@system_ref_id = system_ref.id
				@system_ref_code = system_ref.code
				@instance = ( this_reg.map do | key , value |
					unless key.starts_with?( "empty" )
						# format each string
						{ key.to_sym => format_value( value, hash_column_types[ "#{key}" ] ) }
					end
				end ).compact.reduce( {} , :merge )

				perform_file_erb_result	= ERB.new( perform_file_load_obj ).result( binding )
				instance								= YAML.load( perform_file_erb_result )

				if !hash_data.map{ |r| r.except("number_row") }.include?( instance.except( "number_row" ) )
					instance[ "number_row" ] = 0
					hash_data << instance
				else
					instance[ "number_row" ] = hash_data.map{ |r| r.except("number_row") }.count( instance.except( "number_row" ) )
					hash_data << instance
				end

			end

		else

			puts "El fichero no cumple la validación"
			puts "Validaciones: #{validations.to_s}"

		end

		return hash_data

	end

end


class Hash
	def keep_merge(hash)
		 target = dup
		 hash.keys.each do |key|
				if hash[key].is_a? Hash and self[key].is_a? Hash
					 target[key] = target[key].keep_merge(hash[key])
					 next
				end
				#target[key] = hash[key]
				target.update(hash) { |key, *values| values.flatten.uniq }
		 end
		 target
	end
end
