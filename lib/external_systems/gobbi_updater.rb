# Download
@SYS_GBB = System.find_by_code('GBB')

# BASIC
@SYS_GBB.synchronizer.find_by_action_and_table_name('get', 'companies').get_external_data_and

# EXPENSES EXCLUSIVELY
@SYS_GBB.synchronizer.find_by_action_and_table_name('get', 'currencies').get_external_data_and
@SYS_GBB.synchronizer.find_by_action_and_table_name('get', 'cost_centers').get_external_data_and
@SYS_GBB.synchronizer.find_by_action_and_table_name('get', 'payment_conditions').get_external_data_and

# MASTER ASSOCIATIONS
@SYS_GBB.synchronizer.find_by_action_and_table_name('get', 'class_masters').get_external_data_and
@SYS_GBB.synchronizer.find_by_action_and_table_name('get', 'masters').get_external_data_and
