class ModelizedGenerator < Rails::Generators::Base
  source_root File.expand_path('../templates', __FILE__)
  argument :my_model_name, :type => :string, :default => "application"

  def generate_my_model
    copy_file "new.html.erb", "app/views/#{constant_name_underscored_plural}/new.html.erb"
    copy_file "edit.html.erb", "app/views/#{constant_name_underscored_plural}/edit.html.erb"
    template "_form.html.erb", "app/views/#{constant_name_underscored_plural}/_form.html.erb"
    template "index.html.erb", "app/views/#{constant_name_underscored_plural}/index.html.erb"

    template "controllers.rb.erb", "app/controllers/#{constant_name_underscored_plural}_controller.rb"
    template "models.rb.erb", "app/models/#{constant_name_underscored_singular}.rb"

  end

  private

  def detect_model_name_kind
    return "undercased"   if my_model_name.include?("_") && !(my_model_name =~ /[A-Z]/)
    return "constantized" if my_model_name =~ /[A-Z]/ && !(my_model_name.include?("_"))
    return nil
  end


  def constant_name_singular
    if detect_model_name_kind == "undercased"
      return my_model_name.camelize.singularize
    elsif detect_model_name_kind == "constantized"
      return my_model_name.singularize
    end
  end

  def constant_name_plural
    if detect_model_name_kind == "undercased"
      return my_model_name.camelize.pluralize
    elsif detect_model_name_kind == "constantized"
      return my_model_name.pluralize
    end
  end

  def constant_name_underscored_plural
    if detect_model_name_kind == "undercased"
      return my_model_name.pluralize
    elsif detect_model_name_kind == "constantized"
      return my_model_name.underscore.pluralize
    end
  end

  def constant_name_underscored_singular
    if detect_model_name_kind == "undercased"
      return my_model_name.singularize
    elsif detect_model_name_kind == "constantized"
      return my_model_name.underscore.singularize
    end
  end

end
