module GlobalMethods

	# For check if attribute is validated or not
  def valid_attribute?(attribute)
    self.errors[attribute].blank?
  end

  # Get array of all actions
  def actions( arr_actions = [] )
		if ( !self.class.attribute_names.include?("status") ) or ( self.class.attribute_names.include?("status") and self.class.statuses.key( self.status ) != :deleted )
			arr_ret = [ :view ]
			if self.can_be_modified?
				arr_ret << :edit
			end
      arr_specials_actions = []
			if self.class.attribute_names.include?("status") and self.next_statuses
				self.next_statuses.each do |status|
					if self.class.statuses.key( self.status ) != status.symbol && status.symbol != :deleted
           arr_specials_actions << "set/#{status}"
					end
				end
				arr_ret << :divider if arr_specials_actions.present?
				arr_ret = arr_ret + arr_specials_actions if arr_specials_actions.present?
        arr_ret << :divider if arr_specials_actions.present?
			end
			arr_ret += ( arr_actions.map do | action_validation |
        if action_validation == :divider
          :divider
        else
          action_validation.keys.first if eval( action_validation.values.first )
        end
      end ).compact

      if self.external? and self.class.constants.include?( :HOMO_FIELDS ) #and self.class.const_get( :HOMO_FIELDS ).class == Array #self.class.const_get( :HOMO_MODEL ) == true
        arr_ret << :divider
        # ------------
        # Next line add a link like get_homologate_external_agency_to_native_instance_inmodal
        #                      like get_homologate_external_company_to_native_instance_inmodal
        #                      like get_homologate_external_credit_terminal_to_native_instance_inmodal

        # arr_ret << "get_homologate_#{self.class.table_name.singularize}_path"
        arr_ret << "get_homologate_#{self.class.table_name.singularize}_inmodal"
        # ---- mmmmm ----
        # arr_ret << "get_homologate_external_to_native_instance_inmodal"
      end
      if self.class.attribute_names.include?("status") and self.next_statuses
        if self.next_statuses.keys.include?( :deleted )
          arr_ret << :divider
          arr_ret << :delete
        end
      end
		else
			arr_ret = [ :view ]
		end
    arr_ret = arr_ret.chunk{ |n| n }.map(&:first)
    arr_ret.pop if arr_ret.last == :divider
    return arr_ret
  end

	# Change status of expense and generate movement of current account
  def set_status( status )
		if self.can_be_modified? or self.next_statuses.keys.include?(status.to_sym)
	    unless [ status , :deleted ].include?( self.class.statuses.key( self.status ) )
        eval( "self.set_#{status}" )
	    end
		end
  end

  def next_statuses
    if defined? self.class::STATUS_TRANSITIONS
      self.class.statuses.slice( *self.class::STATUS_TRANSITIONS[ self.class.statuses.key( self.status ) ] )
    end
  end

  # Check if the instance can be modified by status != :null
  def can_be_modified?
    if ( self.attribute_names.include?("status") )
      if ( self.attribute_names.include?("system_id") )
        if ( self.system_id != 0 )
          return false
        else
          return !self.deleted?
        end
      else
        return !self.deleted?
      end
    else
      return true
    end
  end

  # Check if instance can be modified to null
  def can_be_deleted?
    !self.deleted?
  end

  def native?
    if self.system.present?
      if self.system == System.this
        return true
      else
        return false
      end
    else
      return true
    end
  end

  def external?
    if self.attribute_names.include?("system")
      if self.system.present?
        unless self.system == System.this
          return true
        else
          return false
        end
      else
        return false
      end
    else
      return false
    end
  end


end
