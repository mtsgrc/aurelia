module Inspectors::CheckSittServices

	def trigger

		return_data = {}
		return_data[ :messages ] = []

		wsdl_uri_endpoint = "http://www.sittnet.net:8090/WS_MC_INTERFACES/prosys.integra.e.cataCH.088E77674E46.asmx?WSDL"

		arr_of_services_results = []

		# (STEP 1) Service: start_session
		params_for = {}
		params_for[ :start_session ] = {}
		params_for[ :start_session ]["CodigoWebId"] = "137"
		params_for[ :start_session ]["Username"] 		= "wcc001"
		params_for[ :start_session ]["Password"] 		= "wcc001"
		params_for[ :start_session ]["Key"] 				= "410E5313-25AC-442E-AA3E-C7B067CE26E9"
		connection_id = nil

		arr_of_dates = ( ( Date.today )..( Date.today+7.day ) ).map{ |d| d.strftime( '%Y-%m-%d' ) }
		params_for[ :get_by_fecha_origen_destino ] = {}
		params_for[ :get_by_fecha_origen_destino ]["CodigoWebId"] 		= params_for[ :start_session ]["CodigoWebId"]
		params_for[ :get_by_fecha_origen_destino ]["Username"] 				= params_for[ :start_session ]["Username"]
		params_for[ :get_by_fecha_origen_destino ]["Password"] 				= params_for[ :start_session ]["Password"]
		params_for[ :get_by_fecha_origen_destino ]["Key"] 						= params_for[ :start_session ]["Key"]
		params_for[ :get_by_fecha_origen_destino ]["Conexion"]				= nil
		params_for[ :get_by_fecha_origen_destino ]["WebAgenciaId"] 		= "6021" # (Agencia “Web CATA Chile”)
		params_for[ :get_by_fecha_origen_destino ]["IdParadaOrigen"] 	= "936"	# Mendoza (TERMINAL)
		params_for[ :get_by_fecha_origen_destino ]["IdParadaDestino"] = "460" # Bariloche
		params_for[ :get_by_fecha_origen_destino ]["FechaBusqueda"] 	= "" # Bariloche

		params_for[ :end_session ] = {}
		params_for[ :end_session ]["CodigoWebId"] = params_for[ :start_session ]["CodigoWebId"]
		params_for[ :end_session ]["Username"] 		= params_for[ :start_session ]["Username"]
		params_for[ :end_session ]["Password"] 		= params_for[ :start_session ]["Password"]
		params_for[ :end_session ]["Key"] 				= params_for[ :start_session ]["Key"]
		params_for[ :end_session ]["Conexion"]		= params_for[ :get_by_fecha_origen_destino ]["Conexion"]

		wsdl_client = Savon::Client.new( :wsdl => wsdl_uri_endpoint )

		# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		# (STEP 1) Service: start_session
		user_session = wsdl_client.call( :start_session , :message => params_for[ :start_session ] ).to_hash

		# All fucking checks for existance of keys in hash for getting connection_id
		if user_session[ :start_session_response ].present?
			if user_session[ :start_session_response ][ :start_session_result ].present?
				if user_session[ :start_session_response ][ :start_session_result ][ :new_data_set ].present?
					if user_session[ :start_session_response ][ :start_session_result ][ :new_data_set ][ :result ].present?
						if user_session[ :start_session_response ][ :start_session_result ][ :new_data_set ][ :result ][ :is_ok ].present?
							if user_session[ :start_session_response ][ :start_session_result ][ :new_data_set ][ :result ][ :is_ok ] == true
								if user_session[ :start_session_response ][ :start_session_result ][ :new_data_set ][ :session_information ].present?
									if user_session[ :start_session_response ][ :start_session_result ][ :new_data_set ][ :session_information ][ :connection_id ].present?
										connection_id = user_session[ :start_session_response ][ :start_session_result ][ :new_data_set ][ :session_information ][ :connection_id ]
										return_data[ :messages ] << "<< OK >> La sesión se ha iniciado correctamente\n"
									else
										return_data[ :messages ] << "<< ERROR >> user_session[ :start_session_response ][ :start_session_result ][ :new_data_set ][ :session_information ][ :connection_id ].present? FALSE\n"
									end
								else
									return_data[ :messages ] << "<< ERROR >> user_session[ :start_session_response ][ :start_session_result ][ :new_data_set ][ :session_information ].present? FALSE\n"
								end
							else
								return_data[ :messages ] << "<< ERROR >> user_session[ :start_session_response ][ :start_session_result ][ :new_data_set ][ :result ][ :is_ok ] => FALSE\n"
							end
						else
							return_data[ :messages ] << "<< ERROR >> user_session[ :start_session_response ][ :start_session_result ][ :new_data_set ][ :result ][ :is_ok ].present? FALSE\n"
						end
					else
						return_data[ :messages ] << "<< ERROR >> user_session[ :start_session_response ][ :start_session_result ][ :new_data_set ][ :result ].present? FALSE\n"
					end
				else
					return_data[ :messages ] << "<< ERROR >> user_session[ :start_session_response ][ :start_session_result ][ :new_data_set ].present? FALSE\n"
				end
			else
				return_data[ :messages ] << "<< ERROR >> user_session[ :start_session_response ][ :start_session_result ].present? FALSE\n"
			end
		else
			return_data[ :messages ] << "<< ERROR >> user_session[ :start_session_response ].present? FALSE\n"
		end

		# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		# (STEP 2) Service: start_session
		if connection_id.present?

			params_for[ :get_by_fecha_origen_destino ]["Conexion"] = connection_id
			return_data[ :messages ] << "Consultando servicios...\n"

			arr_of_dates.each do | date_ref |
				return_data[ :messages ] << " - Fecha: #{date_ref}\n"
				params_for[ :get_by_fecha_origen_destino ]["FechaBusqueda"] = date_ref

				user_session = wsdl_client.call( :get_by_fecha_origen_destino , :message => params_for[ :get_by_fecha_origen_destino ] ).to_hash

				# All fucking checks for existance of keys in hash for getting connection_id
				if user_session[ :get_by_fecha_origen_destino_response ].present?
					if user_session[ :get_by_fecha_origen_destino_response ][ :get_by_fecha_origen_destino_result ].present?
						if user_session[ :get_by_fecha_origen_destino_response ][ :get_by_fecha_origen_destino_result ][ :resultado ].present?
							if user_session[ :get_by_fecha_origen_destino_response ][ :get_by_fecha_origen_destino_result ][ :resultado ][ :servicio ].present?
								arr_of_services_results << user_session[ :get_by_fecha_origen_destino_response ][ :get_by_fecha_origen_destino_result ][ :resultado ][ :servicio ]
							else
								return_data[ :messages ] << "<< ERROR >> user_session[ :get_by_fecha_origen_destino_response ][ :get_by_fecha_origen_destino_result ][ :resultado ][ :servicio ].present? FALSE\n"
							end
						else
							return_data[ :messages ] << "<< ERROR >> user_session[ :get_by_fecha_origen_destino_response ][ :get_by_fecha_origen_destino_result ][ :resultado ].present? FALSE\n"
						end
					else
						return_data[ :messages ] << "<< ERROR >> user_session[ :get_by_fecha_origen_destino_response ][ :get_by_fecha_origen_destino_result ].present? FALSE\n"
					end
				else
					return_data[ :messages ] << "<< ERROR >> user_session[ :get_by_fecha_origen_destino_response ].present? FALSE\n"
				end

			end

			# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			# (STEP 3) Service: end_session
			user_session = wsdl_client.call( :end_session , :message => params_for[ :end_session ] ).to_hash

			# All fucking checks for existance of keys in hash for getting connection_id
			if user_session[ :end_session_response ].present?
				if user_session[ :end_session_response ][ :end_session_result ].present?
					if user_session[ :end_session_response ][ :end_session_result ][ :new_data_set ].present?
						if user_session[ :end_session_response ][ :end_session_result ][ :new_data_set ][ :result ].present?
							if user_session[ :end_session_response ][ :end_session_result ][ :new_data_set ][ :result ][ :is_ok ].present?
								if user_session[ :end_session_response ][ :end_session_result ][ :new_data_set ][ :result ][ :is_ok ] == true
									return_data[ :messages ] << "<< OK >> La sesión ha finalizado correctamente\n"
								else
									return_data[ :messages ] << "<< ERROR >> user_session[ :end_session_response ][ :end_session_result ][ :new_data_set ][ :result ][ :is_ok ] => FALSE\n"
								end
							else
								return_data[ :messages ] << "<< ERROR >> user_session[ :end_session_response ][ :end_session_result ][ :new_data_set ][ :result ][ :is_ok ].present? FALSE\n"
							end
						else
							return_data[ :messages ] << "<< ERROR >> user_session[ :end_session_response ][ :end_session_result ][ :new_data_set ][ :result ].present? FALSE\n"
						end
					else
						return_data[ :messages ] << "<< ERROR >> user_session[ :end_session_response ][ :end_session_result ][ :new_data_set ].present? FALSE\n"
					end
				else
					return_data[ :messages ] << "<< ERROR >> user_session[ :end_session_response ][ :end_session_result ].present? FALSE\n"
				end
			else
				return_data[ :messages ] << "<< ERROR >> user_session[ :end_session_response ].present? FALSE\n"
			end
		end

		return_data[ :messages ] << "================================================================================\n"
		return_data[ :messages ] << "  Se encontraron #{arr_of_services_results.count} servicio(s)\n"
		return_data[ :messages ] << "#{arr_of_services_results.to_json}"

		return_data[ :response_data ] = arr_of_services_results.to_json
		if arr_of_services_results.count > 0
			return_data[ :response_status ] = true
		else
			return_data[ :response_status ] = false
		end

		return return_data

	end

	module_function :trigger

end
