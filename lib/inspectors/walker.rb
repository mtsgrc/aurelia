module Inspectors::Walker

	def trigger

		Inspector.active.each do | inspector |

			every_time = "#{inspector.frecuency_amount}.#{inspector.frecuency_unit}"

			if ( inspector.inspects.present? and ( Time.now - inspector.inspects.last.run_at ) > eval( every_time ) ) or (!inspector.inspects.present?)

				require 'inspectors/' + inspector.symbol_name
				inspector_module = "Inspectors::#{inspector.symbol_name.camelize}".constantize

				return_triggering = inspector_module.trigger

				inspect = Inspect.new

				inspect.inspector_id      = inspector.id
				inspect.run_at            = Time.now
				inspect.response_status   = return_triggering[ :response_status ]
				inspect.response_message  = return_triggering[ :messages ].join('\n')
				inspect.user_id           = 1

				inspect.save!

				if inspect.response_status == false
					byebug
				end
				byebug

			end

		end

	end

	module_function :trigger

end
