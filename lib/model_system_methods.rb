module ModelSystemMethods

  # Get external data from json
  def get_from_external_jsondata attribute_name
    JSON.parse( self.external_jsondata )[ attribute_name.to_s ]
  end

  # Get external data from json
  def external_data
    return JSON.parse( self.external_jsondata )
  end

  # VALIDATION BEFORE SAVE
  def parent_existance
    if self.parent_id
      self.errors.add( :base , "No existe recurso padre" ) if model_name.name.constantize.find_by_id(self.parent_id).nil?
    end
  end

  # Indicates if resource is of an external system, distinct of this [AU+] ==> id#0
  def external_system?
    self.system_id != 0
  end

  # Indicates if resource is locally created
  def local_system?
    self.system_id == 0
  end

end
