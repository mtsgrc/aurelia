# /lib/my_progress_bar.rb

module MyProgressBar

	# Print progress bar according to n_actual and n_total
  def self.draw n_actual, n_total
    pb = ProgressBar.create( :format => "%a | %b\u{15E7}%i| %J%% %t (%c/%C)", :progress_mark  => ' ', :remainder_mark => "\u{FF65}" , :total => n_total )
    n_actual.times { pb.increment }
    print pb
  end

end
