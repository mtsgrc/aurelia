module Perms

	CONST_FORMAT = "(.:format)"
	CONST_HIDDEN_ROUTES = []
	CONST_FORALL_ROUTES = []

# HIDDEN ROUTES FOR LISTING
	CONST_HIDDEN_ROUTES << "/assets"
	CONST_HIDDEN_ROUTES << "/rails/info/properties(.:format)"
	CONST_HIDDEN_ROUTES << "/rails/info/routes(.:format)"
	CONST_HIDDEN_ROUTES << "/rails/info(.:format)"
	CONST_HIDDEN_ROUTES << "/rails/mailers(.:format)"
	CONST_HIDDEN_ROUTES << "/rails/mailers/*path(.:format)"

# ROUTES FOR ALL USERS
	CONST_FORALL_ROUTES << "/"
	CONST_FORALL_ROUTES << "/users/sign_in(.:format)"
	CONST_FORALL_ROUTES << "/users/sign_out(.:format)"
	CONST_FORALL_ROUTES << "/users/password(.:format)"
	CONST_FORALL_ROUTES << "/users/password/new(.:format)"
	CONST_FORALL_ROUTES << "/users/password/edit(.:format)"
	CONST_FORALL_ROUTES << "/welcome/index(.:format)"


	def self.routes
		routes = Rails.application.routes.routes.map{ |route| route.path.spec.to_s }.uniq
		routes = routes - CONST_HIDDEN_ROUTES
	end
end
