module SynchronizerMethods

  # For getting from external system
  def get_synchronizer_from ref_system, connection_conditions = {}, synchronizer_conditions = {}

    obj_system        = nil
    obj_connection    = nil
    obj_synchronizer  = nil

    if ref_system.class == Class
      obj_system = ref_system
    elsif ref_system.class == String
      tmp_system = System.where( :code => ref_system )
      obj_system = tmp_system.first if tmp_system.count == 1
    end

    if obj_system
      tmp_connection = obj_system.connections.where( :kind_action => Connection.kind_actions[ :read ] ).where( connection_conditions )
      obj_connection = tmp_connection.first if tmp_connection.count == 1
    end

    arr_synchronizers = []
    if tmp_connection.count == 1
      obj_connection = tmp_connection.first
      tmp_synchronizer = obj_connection.synchronizers.where( :kind => Synchronizer.kinds[ :get ] ).where( :table_name => model_name.to_s.underscore.pluralize ).where( synchronizer_conditions )
      obj_synchronizer = tmp_synchronizer.first if tmp_synchronizer.count == 1
    elsif tmp_connection.count > 1
      obj_connections = tmp_connection
      obj_connections.each do | connection |
        tmp_synchronizer = connection.synchronizers.where( :kind => Synchronizer.kinds[ :get ] ).where( :table_name => model_name.to_s.underscore.pluralize ).where( synchronizer_conditions )
        if tmp_synchronizer.present?
          if tmp_synchronizer.count == 1
            arr_synchronizers << tmp_synchronizer.first
          elsif tmp_synchronizer.count > 1
            tmp_synchronizer.each do | synchronizer |
              arr_synchronizers << synchronizer
            end
          end
        end
      end
    end

    if arr_synchronizers.count == 1
      obj_synchronizer = arr_synchronizers.first
    end

    return obj_synchronizer

  end

	def local
		self.where( :system_id => 0 )
	end

	def external
		self.where.not( :system_id => 0 )
	end

  # To found relationship of local column respect external column
  def get_relation column_name, column_value

    ret_hash = {}

    fk_columns  = self.columns.map{ |column| column.name.gsub( "_id", "" ) if column.name.ends_with?("_id") }.compact

    fk_columns.each do |fk_column|

      _fk_column = "_" + fk_column

      if column_name.starts_with?( _fk_column )

        model_ref   = fk_column.camelize.constantize
        column_ref  = column_name.gsub( _fk_column + "_" , "" )

        instance    = model_ref.find_by( column_ref.to_sym => column_value  )

        if instance.present?
          ret_hash = { "#{fk_column}_id" => instance.id  }
        # else
        #   ret_hash = { "#{fk_column}_id" => nil  }
        end

        break
        
      end

    end

    ret_hash = { column_name => column_value } if ret_hash.nil?

    return ret_hash
  end


end
