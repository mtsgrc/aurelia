# /lib/systems/aurelia.rb


module Systems::Aurelia

		# BASIC CONSTANTS
		FILE_BASENAME = File.basename( __FILE__ )
		FILE_EXTENSION = File.extname( __FILE__ )
		FILE_NAME = File.basename( __FILE__ , FILE_EXTENSION )
		FILE_PATH = File.dirname( __FILE__ )
		SYS_NAME=FILE_NAME
		DIR_FILES=File.join(FILE_PATH,SYS_NAME)

		# CONSTANT FOR CONNECTION DATABASE
	  CONST_HOST="192.168.0.249"
	  CONST_USER="aurelio"
	  CONST_PASS="itamto05"

	  def self.get_ventas from_date = nil, to_date = nil

			from_date = Date.today.to_s unless from_date
			to_date 	= Date.today.to_s unless to_date

			if from_date <= to_date

				conn = self.get_connection
				result_set_from_date = conn.query( "SET @FechaHoraInicio := '#{from_date} 00:00:00' ;" )
				result_set_to_date = conn.query( "SET @FechaHoraFin    := '#{to_date} 23:59:59' ;" )

				str_query   = File.read( File.join( DIR_FILES , "get_ventas.sql" ) )
				result_main = conn.query( str_query )

			else
				nil
			end

	  end

		def self.get_connection
			Mysql2::Client.new(:host => CONST_HOST, :username => CONST_USER, :password => CONST_PASS )
		end

end
