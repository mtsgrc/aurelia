# /lib/systems/micronauta.rb


module Systems::Micronauta
	# BASIC CONSTANTS
	FILE_BASENAME 	= File.basename( __FILE__ )
	FILE_EXTENSION 	= File.extname( __FILE__ )
	FILE_NAME 			= File.basename( __FILE__ , FILE_EXTENSION )
	FILE_PATH 			= File.dirname( __FILE__ )
	SYS_NAME				= FILE_NAME
	SYS	 						= System.where( 'name LIKE ?' , "%#{FILE_NAME.capitalize}%" ).first

	# ESPECIAL FILES FOR SYSTEM
	DIR_FILES 			= File.join( FILE_PATH , SYS_NAME )

	# LOG FILES
	DIR_LOG   			= File.join( Rails.root.join('log') , Date.today.strftime("%Y/%m/%d") , SYS.code )				# LIKE log/YYYY/mm/dd/SYS
	FileUtils::mkdir_p( DIR_LOG ) if !File.exists?( DIR_LOG )

	# Global Instances
	LOGGER    			= Logger.new( File.join( DIR_LOG , "main.log" ) )

	# OTHERS VARIABLES
	CONST_TIMEAGO_AMOUNT = 12.hours
	CONST_FORMATOF_DATETIME = '%Y-%m-%d %H:%M:%S'
	CONST_FORMATOF_DATETIMEZ = '%Y-%m-%d %H:%M:%S %z'
	CONST_FORMATOF_DATE = '%Y-%m-%d'
	CONST_FORMATOF_TIME = '%H:%M:%S'
	CONST_COLUMNS_INDEX_UNIQUE=[:external_id]

	# CONSTANT FOR CONNECTION DATABASE
	CONST_HOST="190.220.132.132"
	CONST_USER="cata2015"
	CONST_PASS="horaciorus"

	def self.get_external_data model , arr_unique_columns , conditions = nil , import = true
		# mark init time of import ONE row
		process_time_start = Time.now
		# Format of args:
		# => model 										= Model
		# => arr_unique_columns 			= [:external_id]

		# This make an 'get_bus_ticket_transactions' string
		action_getting = [ "get" , "_" , model.table_name ].join

		LOGGER_print_starting_method( action_getting )
		LOGGER_print_header_method( method( __method__ ).parameters.map { |arg| arg[1] }.map { |arg| "#{arg} = #{eval arg.to_s}" }.join(', ') )

		hash_column_types = Hash[ model.columns_hash.map{ |ct| [ ct[0].to_s , ct[1].type.to_s ] } ]														# According to model and rows, we create an array with their formats
		rows_status = { news: [] , mods: [] , equal: [] , error: [] }																													# Save in hash all rows by state

		# Set variables of range of time
		if ( ( conditions[:from_date] && conditions[:to_date] ) && ( conditions[:from_date] <= conditions[:to_date] ) )
			@fechahora_inicio  	= "#{conditions[:from_date]}"
			@fechahora_fin 			= "#{conditions[:to_date]}"
		else
			@fechahora_inicio  	= Time.now.localtime - CONST_TIMEAGO_AMOUNT
			@fechahora_fin 			= Time.now.localtime
		end

		str_query_template = ERB.new( File.read( File.join( DIR_FILES , ( action_getting + ".sql.erb" ) ) ) )									# For editing template with seted variables
		str_query = str_query_template.result( binding )																																			# set Query

		conn = self.get_connection																																														# Make connection
		all_rows = conn.query( str_query )																																										# Get all rows of query

		# Iterate for each row
		all_rows.each_with_index do | x_row , n_row |

			row_time_start = Time.now																																														# mark init time of import ONE row

			tmp_n_actual_total 		= { n_act: n_row + 1 , n_all: all_rows.count }																								# n° of rows
			tmp_unique_key_values = Hash[ arr_unique_columns.map{ |k| [ k , x_row[k.to_s] ] } ]																	# set hash with the unique key and the value, for then use it in search of model instances
			ext_reg 							= model.where( system_id: SYS.id ).where( tmp_unique_key_values ).first_or_initialize 				# Find or initialize instance of model

			# For each column , we format them according on the type of the column
			x_row.each do | column_key , column_val |
				ext_reg.send("#{column_key}=", format_value( column_val, hash_column_types[column_key] ))													# According to name of column, we set the value formatted in there
			end

			row_time_end 			= Time.now																																												# Mark ending time of importr ONE row
			row_time_elapsed 	= ( row_time_end - row_time_start ).to_f.round(3)																									# Calc the difference of time marks

			# Logging actions did it
			if ext_reg.persisted? and !ext_reg.changed?
				LOGGER_print_x_import_method tmp_n_actual_total , tmp_unique_key_values , "> IDENTICALS > #{row_time_elapsed}s"
				rows_status[:equal] += [ext_reg]
			else
				if ext_reg.persisted? and ext_reg.changed
					LOGGER_print_x_import_method tmp_n_actual_total , tmp_unique_key_values , "> UPDATED => #{ext_reg.changes.to_s} > #{time_elapsed}s"
					rows_status[:mods] += [ext_reg]
				else
					LOGGER_print_x_import_method tmp_n_actual_total , tmp_unique_key_values , "> INSERTED => #{ ext_reg.attributes.map{ |f,v| v if x_row.key? f }.compact.join('|') } > #{time_elapsed}s"
					rows_status[:news] += [ext_reg]
				end

				ext_reg.save!																																																			# We save, updated or insert

			end

		end

		conn.close																																																						# Closing connection

		process_time_end = Time.now																																														# mark init time of import ONE row
		process_time_elapsed = ( process_time_end - process_time_start ).to_f.round(3)																				# Calc the difference of time marks

		LOGGER_print_footer_method( action_getting , { started_at: process_time_start , finished_at: process_time_end , time_elapsed: process_time_elapsed , system: SYS , rows_status: rows_status } )
		LOGGER_print_ending_method( action_getting )

	end


	private

	# ------------------------------------------------------------------------
	# FORMATING VALUES ACCORDING TO TYPE PASSED
	def self.format_value arg_value, arg_type
		case arg_type
		when 'string'
			ret_value = arg_value.to_s.strip[0..255]
		when 'text'
			ret_value = arg_value.to_s.strip
		when 'integer'
			ret_value = arg_value.to_s.to_i
		when 'float'
			ret_value = arg_value.to_s.to_f.round(4)
		when 'decimal'
			ret_value = arg_value.to_s.to_f.round(4)
		when 'datetime'
			# If data contain only DATE TIME, we append ZONETIME
			if arg_value && arg_value.to_s.split(" ").count == 2
				ret_value = DateTime.strptime( [ arg_value.to_s , DateTime.now.strftime("%z") ].join(" ") , CONST_FORMATOF_DATETIMEZ )
			else
				ret_value = DateTime.strptime( arg_value.to_s , CONST_FORMATOF_DATETIMEZ )
			end
		when 'time'
			ret_value = Date.strptime( arg_value.to_s , CONST_FORMATOF_TIME ) if arg_value
		when 'date'
			ret_value = Time.strptime( arg_value.to_s , CONST_FORMATOF_DATE ) if arg_value
		when 'binary'
			ret_value = arg_value.to_s.strip
		when 'boolean'
			ret_value = ( ( arg_value.to_s == '1' || arg_value.to_s == 1 ) ? true : false )
		end
	end

	# ------------------------------------------------------------------------
	# LOGGER FUNCTIONS

	# STARTING
	def self.LOGGER_print_starting_method action_getting
		LOGGER.info( "*--------------- Starting process of *#{action_getting}* ---------------*" )
	end

	# HEADER
	def self.LOGGER_print_header_method arr_arguments
		LOGGER.info( " PARAMS => " + arr_arguments + "" )
	end

	# BODY
	def self.LOGGER_print_method str_text
		LOGGER.info( "#{str_text}" )
	end

	# IMPORTING
	def self.LOGGER_print_x_import_method arr_n_totals , arr_unique_kvs , str_text
		n_act = arr_n_totals[:n_act]
		n_all = arr_n_totals[:n_all]
		n_all_strlenght = n_all.to_s.size
		LOGGER.info( "#{n_act.to_s.rjust(n_all.to_s.size,' ')}/#{n_all} (" + ( "%0.2f" % ( n_act.to_f / n_all.to_f * 100.00 ) ).rjust(6, ' ') + "%) Checking #{arr_unique_kvs.to_s} #{str_text}" )
	end

	# FOOTER
	def self.LOGGER_print_footer_method action_getting, arr_footer_data
		LOGGER.info( "" )
		LOGGER.info( "-----------------------------------------------------------------------" )
		LOGGER.info( "" )
		LOGGER.info( "                  " + "System".bold + ": (#{arr_footer_data[:system].id}) #{arr_footer_data[:system].code} - #{arr_footer_data[:system].name}" ) if arr_footer_data[:system]
		LOGGER.info( "            " + "Process name".bold + ": #{action_getting}" ) if action_getting
		LOGGER.info( "  " + "The process started at".bold + ": " + arr_footer_data[:started_at].to_s ) if arr_footer_data[:started_at]
		LOGGER.info( "  " + "The process finised at".bold + ": " + arr_footer_data[:finished_at].to_s ) if arr_footer_data[:finished_at]
		LOGGER.info( "            " + "Time elapsed".bold + ": " + arr_footer_data[:time_elapsed].to_s + " seconds" ) if arr_footer_data[:time_elapsed]
		LOGGER.info( "" )
		LOGGER.info( "              " + "Total rows".bold + ": " + arr_footer_data[:rows_status].map{ |k,v| v.count }.sum.to_s + " rows" ) if arr_footer_data[:rows_status]
		LOGGER.info( "               " + "Rows news".bold + ": " + arr_footer_data[:rows_status][:news].count.to_s + " rows" ) if arr_footer_data[:rows_status]
		LOGGER.info( "             " + "Rows equals".bold + ": " + arr_footer_data[:rows_status][:equal].count.to_s + " rows" ) if arr_footer_data[:rows_status]
		LOGGER.info( "               " + "Rows mods".bold + ": " + arr_footer_data[:rows_status][:mods].count.to_s + " rows" ) if arr_footer_data[:rows_status]
		LOGGER.info( "              " + "Rows error".bold + ": " + arr_footer_data[:rows_status][:error].count.to_s + " rows" ) if arr_footer_data[:rows_status]
		LOGGER.info( "" )
	end

	# ENDING
	def self.LOGGER_print_ending_method action_getting
		LOGGER.info( "*************** Finishing process of *#{action_getting}* ***************\n" )
	end


	def self.get_connection
		Mysql2::Client.new(:host => CONST_HOST, :username => CONST_USER, :password => CONST_PASS )
	end

end
