# /lib/systems/pxp.rb

module Systems::Pxp

		# BASIC CONSTANTS
		FILE_BASENAME 	= File.basename( __FILE__ )
		FILE_EXTENSION 	= File.extname( __FILE__ )
		FILE_NAME 			= File.basename( __FILE__ , FILE_EXTENSION )
		FILE_PATH 			= File.dirname( __FILE__ )
		SYS_NAME				= FILE_NAME
		SYS	 						= System.where( 'name LIKE ?' , "%#{FILE_NAME.capitalize}%" ).first

		# ESPECIAL FILES FOR SYSTEM
		DIR_FILES 			= File.join( FILE_PATH , SYS_NAME )

		# LOG FILES
		DIR_LOG   			= File.join( Rails.root.join('log') , Date.today.strftime("%Y/%m/%d") , SYS.code )				# LIKE log/YYYY/mm/dd/SYS
		FileUtils::mkdir_p( DIR_LOG ) if !File.exists?( DIR_LOG )

		# Global Instances
		LOGGER    			= Logger.new( File.join( DIR_LOG , "main.log" ) )

		def initialize

		end

		# CONSTANT FOR CONNECTION DATABASE
	  CONST_HOST="192.168.0.249"
	  CONST_USER="aurelio"
	  CONST_PASS="aurelia"

	  def self.get_invoices from_date = Date.today.to_s, to_date = Date.today.to_s
			LOGGER_print_header_method( __method__ , method(__method__).parameters.map { |arg| arg[1] }.map { |arg| "#{arg} = #{eval arg.to_s}" }.join(', ') )

			if from_date <= to_date

				conn = self.get_connection

				@fechahora_inicio  	= "#{from_date} 00:00:00"
				@fechahora_fin 			= "#{to_date} 23:59:59"

				str_query_template	= ERB.new( File.read( File.join( DIR_FILES , "get_ventas.sql.erb" ) ) )
				str_query 					= str_query_template.result( binding )

				# puts str_query
				all_rows = conn.execute( str_query )

				all_rows.each_with_index(:as => :array) do | x_row , n_row |

					transaction_number								=	x_row["Id"]

					ext_reg = InvoiceTransaction.where( external_id: transaction_number , system_id: SYS.id ).first_or_initialize

		      ext_reg.company_code 									= x_row["EmpresaCodigo"].to_s.strip
		      ext_reg.receipt_number 								=	x_row["ComprobanteNumero"].to_s.strip
		      ext_reg.receipt_issued_at   					= DateTime.strptime( x_row["EmisionFechaHora"].to_s , '%Y-%m-%d %H:%M:%S' )
		      ext_reg.agency_origin_code 						= x_row["AgenciaOrigenCodigo"].to_s.strip
		      ext_reg.agency_origin_name 						= x_row["AgenciaOrigenNombre"].to_s.strip
		      ext_reg.agency_origin_business_name 	= x_row["AgenciaOrigenRazonSocial"].to_s.strip
		      ext_reg.agency_origin_location_name 	= x_row["AgenciaOrigenLocalidadNombre"].to_s.strip
		      ext_reg.agency_destiny_code 					= x_row["AgenciaDestinoCodigo"].to_s.strip
		      ext_reg.agency_destiny_name 					= x_row["AgenciaDestinoNombre"].to_s.strip
		      ext_reg.agency_destiny_business_name 	= x_row["AgenciaDestinoRazonSocial"].to_s.strip
		      ext_reg.agency_destiny_location_name 	= x_row["AgenciaDestinoLocalidadNombre"].to_s.strip
		      ext_reg.amount_total_cot							= x_row["TotalCOT"].to_s.to_f.round(4)
		      ext_reg.amount_total_net_taxed				= x_row["TotalNetoGravado"].to_s.to_f.round(4)
		      ext_reg.amount_total_iva							= x_row["TotalIVA"].to_s.to_f.round(4)
		      ext_reg.amount_total_gross						= x_row["TotalBruto"].to_s.to_f.round(4)
		      ext_reg.client_business_name 					= x_row["ClienteRazonSocial"].to_s.strip
		      ext_reg.client_cuit									  = x_row["ClienteCUIT"].to_s.strip
		      ext_reg.client_type 									= x_row["ClienteTipo"].to_s.strip
					ext_reg.user_username									= x_row["VentaUsuario"].to_s.strip
		      ext_reg.void													= ( x_row["Anulado"].to_s.strip == '1' ? true : false )
		      ext_reg.receipt_letter 								= x_row["ComprobanteLetra"].to_s.strip

					if ext_reg.persisted? and !ext_reg.changed?
						LOGGER_print_x_import_method __method__ , n_row + 1 , all_rows.count ,"IDENTICAL [##{transaction_number}] #{ext_reg.company_code}|#{ext_reg.receipt_number}"
					else
						if ext_reg.persisted? and ext_reg.changed
							LOGGER_print_x_import_method __method__ , n_row + 1 , all_rows.count , "UPDATING [##{transaction_number}] #{ext_reg.company_code}|#{ext_reg.receipt_number}"
						else
							LOGGER_print_x_import_method __method__ , n_row + 1 , all_rows.count , "INSERTING [##{transaction_number}] #{ext_reg.company_code}|#{ext_reg.receipt_number}"
						end
						ext_reg.save!
					end
				end

				conn.close

			else
				nil
			end

			LOGGER_print_footer_method( __method__ )

	  end

		private

		# LOGGER FUNCTIONS
		def self.LOGGER_print_header_method str_method, arr_arguments
			LOGGER.info( "\n*--------------- Starting process of *#{str_method}* ---------------*" )
			LOGGER.info( " #{str_method} | PARAMS => " + arr_arguments + "\n" )
		end
		def self.LOGGER_print_x_import_method str_method , int_n_row , int_total_rows , str_text
			LOGGER.info( " #{str_method} | #{(int_n_row).to_s.rjust(int_total_rows.to_s.size,' ')}/#{int_total_rows} # #{str_text}" )
		end
		def self.LOGGER_print_footer_method str_method
			LOGGER.info( "\n*************** Finishing process of *#{str_method}* ***************" )
		end

		def self.get_connection
			TinyTds::Client.new(:host => CONST_HOST, :username => CONST_USER, :password => CONST_PASS )
		end

end
