namespace :accesses do
  desc "TODO"
  task refresh_menu: :environment do

    def log_find_or_create_by( model, options , attributes , &block)

    	model_object 							= model.find_or_initialize_by( attributes )
      model_object_description 	= model_object.detail

    	if model_object.persisted?
    		puts " #{"\u2714".force_encoding('UTF-8').white} #{model_object_description}"
    	else
    		if options[ :with_encrypted_passwords ]
    			model_object.attributes = options[ :with_encrypted_passwords ]
    		end
    		if model_object.save
    			puts " #{"\u2714".force_encoding('UTF-8').green} #{model_object_description}"
    		else
    			puts " #{"\2718".force_encoding('UTF-8').red} #{model_object_description}"
    		end
    	end
    	return model_object
    end


    # ====================================================================================================================================
    puts "\n=============================================================================== CREATION OF > Menus... \n"

    menu_index = log_find_or_create_by( Menu , {} , :name => "Inicio" , :symbol => "root" , :parent_id => nil, :kind => Menu::kinds[ :module ] , :faicon => 'fa-home' )

    menu_administration = log_find_or_create_by( Menu , {} , :name => "Administración" , :symbol => "administration" , :parent_id => nil, :kind => Menu::kinds[ :module ], :faicon => 'fa-sitemap' )
      menu_administration_accounts = log_find_or_create_by( Menu , {} , :name => "Cuentas" , :symbol => "accounts" , :parent_id => menu_administration.id, :kind => Menu::kinds[ :menu ] )
        menu_administration_accounts_current_accounts = log_find_or_create_by( Menu , {} , :name => "Cuentas corrientes" , :symbol => "current_accounts" , :parent_id => menu_administration_accounts.id, :kind => Menu::kinds[ :resource ] )
        # menu_administration_accounts_movements = log_find_or_create_by( Menu , {} , :name => "Movimientos" , :symbol => "movements" , :parent_id => menu_administration_accounts.id, :kind => Menu::kinds[ :resource ] )
        menu_administration_accounts_liquidations = log_find_or_create_by( Menu , {} , :name => "Liquidaciones" , :symbol => "liquidations" , :parent_id => menu_administration_accounts.id, :kind => Menu::kinds[ :resource ] )
        menu_administration_accounts_expenses = log_find_or_create_by( Menu , {} , :name => "Gastos" , :symbol => "expenses" , :parent_id => menu_administration_accounts.id, :kind => Menu::kinds[ :resource ] )
        menu_administration_accounts_credit_notes = log_find_or_create_by( Menu , {} , :name => "Notas de crédito" , :symbol => "credit_notes" , :parent_id => menu_administration_accounts.id, :kind => Menu::kinds[ :resource ] )
        menu_administration_accounts_debit_notes = log_find_or_create_by( Menu , {} , :name => "Notas de débito" , :symbol => "debit_notes" , :parent_id => menu_administration_accounts.id, :kind => Menu::kinds[ :resource ] )
      menu_administration_constants = log_find_or_create_by( Menu , {} , :name => "Constantes" , :symbol => "constants" , :parent_id => menu_administration.id, :kind => Menu::kinds[ :menu ] )
        menu_administration_constants_agencies = log_find_or_create_by( Menu , {} , :name => "Agencias" , :symbol => "agencies" , :parent_id => menu_administration_constants.id, :kind => Menu::kinds[ :resource ] )
        menu_administration_constants_agency_companies = log_find_or_create_by( Menu , {} , :name => "Agencias y Empresas" , :symbol => "agency_companies" , :parent_id => menu_administration_constants.id, :kind => Menu::kinds[ :resource ] )
        menu_administration_constants_cost_centers = log_find_or_create_by( Menu , {} , :name => "Centros de costo" , :symbol => "cost_centers" , :parent_id => menu_administration_constants.id, :kind => Menu::kinds[ :resource ] )
        menu_administration_constants_payment_conditions = log_find_or_create_by( Menu , {} , :name => "Formas de pago" , :symbol => "payment_conditions" , :parent_id => menu_administration_constants.id, :kind => Menu::kinds[ :resource ] )
        menu_administration_constants_payment_conditions = log_find_or_create_by( Menu , {} , :name => "Monedas" , :symbol => "currencies" , :parent_id => menu_administration_constants.id, :kind => Menu::kinds[ :resource ] )


    menu_systems = log_find_or_create_by( Menu , {} , :name => "Sistemas" , :symbol => "systems" , :parent_id => nil, :kind => Menu::kinds[ :module ], :faicon => 'fa-terminal' )
      menu_systems_accesses = log_find_or_create_by( Menu , {} , :name => "Accesos" , :symbol => "accesses" , :parent_id => menu_systems.id, :kind => Menu::kinds[ :menu ] )
        menu_systems_accesses_users = log_find_or_create_by( Menu , {} , :name => "Usuarios" , :symbol => "users" , :parent_id => menu_systems_accesses.id, :kind => Menu::kinds[ :resource ] )
        menu_systems_accesses_menus = log_find_or_create_by( Menu , {} , :name => "Menúes" , :symbol => "menus" , :parent_id => menu_systems_accesses.id, :kind => Menu::kinds[ :resource ] )
        menu_systems_accesses_permissions = log_find_or_create_by( Menu , {} , :name => "Permisos" , :symbol => "permissions" , :parent_id => menu_systems_accesses.id, :kind => Menu::kinds[ :resource ] )
      menu_systems_constants = log_find_or_create_by( Menu , {} , :name => "Constantes" , :symbol => "constants" , :parent_id => menu_systems.id, :kind => Menu::kinds[ :menu ] )
        menu_systems_constants_systems = log_find_or_create_by( Menu , {} , :name => "Sistemas" , :symbol => "systems" , :parent_id => menu_systems_constants.id, :kind => Menu::kinds[ :resource ] )
      menu_systems_external_systems = log_find_or_create_by( Menu , {} , :name => "Sistemas externos" , :symbol => "external_systems" , :parent_id => menu_systems.id, :kind => Menu::kinds[ :menu ] )
        menu_systems_external_systems_connections = log_find_or_create_by( Menu , {} , :name => "Conexiones" , :symbol => "connections" , :parent_id => menu_systems_external_systems.id, :kind => Menu::kinds[ :resource ] )
        menu_systems_external_systems_synchronizers = log_find_or_create_by( Menu , {} , :name => "Sincronizadores" , :symbol => "synchronizers" , :parent_id => menu_systems_external_systems.id, :kind => Menu::kinds[ :resource ] )
        menu_systems_external_systems_gobbi_schemas = log_find_or_create_by( Menu , {} , :name => "Gobbi's DBs" , :symbol => "gobbi_schemas" , :parent_id => menu_systems_external_systems.id, :kind => Menu::kinds[ :resource ] )
        menu_systems_external_systems_homologations = log_find_or_create_by( Menu , {} , :name => "Homologación" , :symbol => "homologations" , :parent_id => menu_systems_external_systems.id, :kind => Menu::kinds[ :resource ] )
      menu_systems_aurelia = log_find_or_create_by( Menu , {} , :name => "Aurelia" , :symbol => "aurelia" , :parent_id => menu_systems.id, :kind => Menu::kinds[ :menu ] )
        menu_systems_aurelia_feedback = log_find_or_create_by( Menu , {} , :name => "Feedback" , :symbol => "feedback" , :parent_id => menu_systems_aurelia.id, :kind => Menu::kinds[ :resource ] )
        menu_systems_aurelia_inspinia = log_find_or_create_by( Menu , {} , :name => "Inspinia" , :symbol => "inspinia" , :parent_id => menu_systems_aurelia.id, :kind => Menu::kinds[ :resource ] )
        menu_systems_aurelia_about = log_find_or_create_by( Menu , {} , :name => "Acerca" , :symbol => "about" , :parent_id => menu_systems_aurelia.id, :kind => Menu::kinds[ :resource ] )
      menu_systems_jobs = log_find_or_create_by( Menu , {} , :name => "Inspectores" , :symbol => "inspectors" , :parent_id => menu_systems.id, :kind => Menu::kinds[ :resource ] )
      menu_systems_jobs = log_find_or_create_by( Menu , {} , :name => "Tareas" , :symbol => "jobs" , :parent_id => menu_systems.id, :kind => Menu::kinds[ :resource ] )

    menu_personal = log_find_or_create_by( Menu , {} , :name => "Personal" , :symbol => "personal" , :parent_id => nil, :kind => Menu::kinds[ :module ], :faicon => 'fa-users'  )
      menu_personal_constants = log_find_or_create_by( Menu , {} , :name => "Constantes" , :symbol => "constants" , :parent_id => menu_personal.id , :kind => Menu::kinds[ :menu ] )
        menu_personal_constants_persons = log_find_or_create_by( Menu , {} , :name => "Personas" , :symbol => "people" , :parent_id => menu_personal_constants.id, :kind => Menu::kinds[ :resource ] )
      menu_personal_control = log_find_or_create_by( Menu , {} , :name => "Control" , :symbol => "control" , :parent_id => menu_personal.id , :kind => Menu::kinds[ :menu ] )
        menu_systems_control_person_checks = log_find_or_create_by( Menu , {} , :name => "Marcaciones" , :symbol => "person_checks" , :parent_id => menu_personal_control.id, :kind => Menu::kinds[ :resource ] )
        menu_systems_control_terminals = log_find_or_create_by( Menu , {} , :name => "Biométricos" , :symbol => "biometrics" , :parent_id => menu_personal_control.id, :kind => Menu::kinds[ :resource ] )

    menu_treasury = log_find_or_create_by( Menu , {} , :name => "Tesorería" , :symbol => "treasury" , :parent_id => nil, :kind => Menu::kinds[ :module ], :faicon => 'fa-money'  )
      menu_treasury_lot_closures_and_transactions = log_find_or_create_by( Menu , {} , :name => "Cierres de Lote y transacciones" , :symbol => "lot_closures" , :parent_id => menu_treasury.id, :kind => Menu::kinds[ :menu ] )
        menu_treasury_lot_closures = log_find_or_create_by( Menu , {} , :name => "Cierres de Lote" , :symbol => "lot_closures" , :parent_id => menu_treasury_lot_closures_and_transactions.id, :kind => Menu::kinds[ :resource ] )
        menu_treasury_lot_closure_transactions = log_find_or_create_by( Menu , {} , :name => "Transacciones" , :symbol => "lot_closure_transactions" , :parent_id => menu_treasury_lot_closures_and_transactions.id, :kind => Menu::kinds[ :resource ] )
      menu_treasury_bank_account_movements = log_find_or_create_by( Menu , {} , :name => "Movimientos bancarios" , :symbol => "bank_account_movements" , :parent_id => menu_treasury.id, :kind => Menu::kinds[ :resource ] )
      menu_treasury_cash_registers = log_find_or_create_by( Menu , {} , :name => "Cajas y movimientos" , :symbol => "cash_registers" , :parent_id => menu_treasury.id , :kind => Menu::kinds[ :resource ] )
      menu_treasury_constants = log_find_or_create_by( Menu , {} , :name => "Constantes" , :symbol => "constants" , :parent_id => menu_treasury.id , :kind => Menu::kinds[ :menu ] )
        menu_treasury_constants_bank_accounts = log_find_or_create_by( Menu , {} , :name => "Cuentas bancarias" , :symbol => "bank_accounts" , :parent_id => menu_treasury_constants.id, :kind => Menu::kinds[ :resource ] )
        menu_treasury_constants_bank_account_cards = log_find_or_create_by( Menu , {} , :name => "Tarjetas bancarias" , :symbol => "bank_account_cards" , :parent_id => menu_treasury_constants.id, :kind => Menu::kinds[ :resource ] )
        menu_treasury_constants_credit_terminals = log_find_or_create_by( Menu , {} , :name => "Terminales de crédito" , :symbol => "credit_terminals" , :parent_id => menu_treasury_constants.id, :kind => Menu::kinds[ :resource ] )

    menu_direction = log_find_or_create_by( Menu , {} , :name => "Dirección" , :symbol => "direction" , :parent_id => nil, :kind => Menu::kinds[ :module ], :faicon => 'fa-location-arrow'  )
      menu_direction_constants = log_find_or_create_by( Menu , {} , :name => "Constantes" , :symbol => "constants" , :parent_id => menu_direction.id , :kind => Menu::kinds[ :menu ] )
        menu_direction_constants_companies = log_find_or_create_by( Menu , {} , :name => "Empresas" , :symbol => "companies" , :parent_id => menu_direction_constants.id, :kind => Menu::kinds[ :resource ] )
        menu_direction_constants_sectors = log_find_or_create_by( Menu , {} , :name => "Sectores" , :symbol => "sectors" , :parent_id => menu_direction_constants.id, :kind => Menu::kinds[ :resource ] )
        menu_direction_constants_roles = log_find_or_create_by( Menu , {} , :name => "Roles" , :symbol => "roles" , :parent_id => menu_direction_constants.id, :kind => Menu::kinds[ :resource ] )

  end

end
