namespace :db do

  desc 'Truncate all tables, except schema_migrations (customizable)'
  task :truncate, [ :tables ] => 'db:load_config' do |t, args|
    args.with_defaults(tables: 'schema_migrations')

    tables = args[:tables].split(' ')
    config  = ActiveRecord::Base.configurations[::Rails.env]

    ActiveRecord::Base.establish_connection
    ActiveRecord::Base.connection.tables.each do |table|
			if Rails.env == "development"
      	ActiveRecord::Base.connection.execute("DELETE FROM #{table}") if tables.include?(table)
      	ActiveRecord::Base.connection.execute("VACUUM") if tables.include?(table)
      	ActiveRecord::Base.connection.execute("UPDATE sqlite_sequence SET seq = 0 WHERE name=  '#{table}'") if tables.include?(table)
			else
				ActiveRecord::Base.connection.execute("TRUNCATE #{table} RESTART IDENTITY;") if tables.include?(table)
			end
    end
  end

end
