namespace :specials do

# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# MAIN

	desc "Importar todo"
	task :sync_all => [ :sync_homologations_from_gbb ,
											:import_and_homologate_agencies ,
											:import_and_homologate_credit_terminals ,
											:import_cards_and_bank_accounts_and_homologate
										]

# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# GLOBAL

	desc "Importar [ homologations ] de GOBBI (Locales de maestros, tarjetas, etc...)"
	task :sync_homologations_from_gbb => :environment do
		Homologation.get_synchronizer_from( 'GBB' ).get_external_data_and_sync
	end

# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Importar agencias de sistemas, agencias  y homologar

	desc "Importar [ agencies ] de GOBBI (Maestros clase 04)"
	task :sync_agencies_from_gbb => :environment do
		Agency.get_synchronizer_from( 'GBB' ).get_external_data_and_sync
	end

	desc "Homologar [ agencies ] to [ agency ]"
	task :homologate_agencies => :environment do

			homologations = Homologation.where( :child_table_name => 'agency' )

			homologations.each do |homologation|

				agency_child = Agency.find_by( :code => homologation.child_value )

				if agency_child
					agency_parent = Agency.find_by( :code => homologation.parent_value )
					if agency_parent
						agency_child.parent_id = agency_parent.id
						agency_child.save
					else
						puts "No se puede encontrar recurso padre para homologar la agencia #{agency_child.detail}"
					end
				end
			end

	end

	desc "Importar [ homologations ] de GOBBI (Locales de maestros, tarjetas, etc...) > Importar [ agencies ] de GOBBI (Maestros clase 04) > Homologar [ agencies ] to [ agencies ] "
	task :import_and_homologate_agencies => [ :sync_homologations_from_gbb , :sync_agencies_from_gbb , :homologate_agencies ]

# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Importar terminales de crédito de sistemas, terminales de crédito y homologar

	desc "Homologar [ credit_terminals ] to [ agencies ]"
	task :homologate_credit_terminal_to_agencies => :environment do

		homologations = Homologation.where( :child_table_name => 'credit_terminal' )

		homologations.each do |homologation|

			credit_terminal_child = CreditTerminal.find_by( :code => homologation.child_value )

			if credit_terminal_child
				agency_parent = Agency.find_by( :code => homologation.parent_value )
				if agency_parent.present?
					credit_terminal_child.agency_id = agency_parent.id
					credit_terminal_child.save
				else
					puts "No se puede encontrar recurso padre (agencia) para homologar la terminal de crédito #{credit_terminal_child.detail}"
				end
			end
		end

	end

	desc "Importar [ homologations ] de GOBBI (Locales de maestros, tarjetas, etc...) >>> Homologar [ credit_terminals ] to [ credit_terminals ] >>> Asociar [ credit_terminals ] to [ agencies ]"
	task :import_and_homologate_credit_terminals => [ :sync_homologations_from_gbb , :homologate_credit_terminal_to_agencies ]

# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Import bank_accounts
	desc "Importar [ bank_accounts ] de Gobbi (GBB)"
	task :sync_bank_accounts_from_gbb => :environment do
		BankAccount.get_synchronizer_from( 'GBB' ).get_external_data_and_sync
	end

# Import bank_account_cards
	desc "Importar [ bank_account_cards ] de Gobbi (GBB)"
	task :sync_bank_account_cards_from_gbb => :environment do
		BankAccountCard.get_synchronizer_from( 'GBB' ).get_external_data_and_sync
	end

	# Homologar [ bank_account_cards ] to [ bank_accounts ]
	desc "Homologar [ bank_account_cards ] to [ agencies ]"
	task :homologate_bank_account_cards => :environment do

		homologations = Homologation.where( :child_table_name => 'bank_account_card' )
		homologations.each do |homologation|
			bank_account_card_child = BankAccountCard.find_by( :code => homologation.child_value )
			if bank_account_card_child
				agency_parent = Agency.find_by( :code => homologation.parent_value )
				if agency_parent.present?
					bank_account_card_child.agency_id = agency_parent.id
					bank_account_card_child.save
				else
					puts "No se puede encontrar recurso padre (agencia) para homologar la tarjeta de cuenta bancaria #{bank_account_card_child.detail}"
				end
			end
		end

	end

	desc "Importar [ bank_accounts ] de Gobbi (GBB) >>> Importar [ bank_account_cards ] de Gobbi (GBB) >>> Homologar [ bank_account_cards ] to [ agencies ] >>> Homologar [ bank_account_cards ] to [ bank_accounts ]"
	task :import_cards_and_bank_accounts_and_homologate => [ :sync_bank_accounts_from_gbb , :sync_bank_account_cards_from_gbb , :homologate_bank_account_cards ]

end
