#!/bin/bash

# ------------------------------------------------------------------------------------------------------------
args=$(getopt -l "all,only:" -o ":AO:" -- "$@")
font_bold=$(tput bold)
font_normal=$(tput sgr0)
eval set -- "$args"

function show_help() {
  echo -e "\n-------------------------------------------------------------------------\n"

  echo -e "Opciones válidas:\n"
  echo "  ${font_bold}-A, --all${font_normal}"
  echo "      Todas las tablas"
  echo "  ${font_bold}-O, --only${font_normal}"
  echo "      Sólo la tabla indicada"

  echo -e "\n-------------------------------------------------------------------------\n"
}

OPT_SYNC="ALL"

while [ $# -ge 1 ]; do
  case "$1" in

    --)
      shift
      break
     ;;

    -A|--all)
      OPT_PASSED=1
      ;;

    -O|--only)
      OPT_PASSED=1
      OPT_SYNC="$2"
      ;;

    -h|--help)
      show_help
      exit 0;
      ;;

  esac
  shift
done

if [ ! $OPT_PASSED ]; then
  echo "No se pasaron argumentos válidos."
  show_help
  exit 0;
fi

if [ "$OPT_SYNC" == "ALL" ] ; then
    RAILS_ENV=production bundle exec rake db:migrate:reset
else
    RAILS_ENV=production bundle exec rails runner "ActiveRecord::Migration.drop_table(:${OPT_SYNC}) if ActiveRecord::Base.connection.table_exists? '${OPT_SYNC}'"
    #rake db:schema:load
    RAILS_ENV=production bundle exec rake db:migrate
fi
RAILS_ENV=production bundle exec rake db:seed
RAILS_ENV=production bundle exec rake accesses:refresh_menu
