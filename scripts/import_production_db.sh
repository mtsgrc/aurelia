#!/bin/bash
# ssh aurelio@aurelia 'cd /home/aurelio/aurelia ; '
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")
echo " - Dumpeando base de datos MYSQL en produccion './tmp/db_${TIMESTAMP}_production.sql'"
mysqldump -h 192.168.0.249 -u aurelio -p aureliaplus > ./tmp/db_${TIMESTAMP}_production.sql
rake db:drop
rake db:create
echo " - Importando base de datos MYSQL de produccion './tmp/db_${TIMESTAMP}_production.sql'"
mysql -u root -p aureliaplus < ./tmp/db_${TIMESTAMP}_production.sql
rake db:migrate
