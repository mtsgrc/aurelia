#!/bin/bash
ACTUALDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PARENTDIR="$(dirname "$ACTUALDIR")" 
cd $PARENTDIR && RAILS_ENV=production ./bin/delayed_job start
cd $PARENTDIR && RAILS_ENV=production bundle exec passenger start
