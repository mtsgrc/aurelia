#!/bin/bash
ACTUALDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PARENTDIR="$(dirname "$ACTUALDIR")" 
kill -9 `cat $PARENTDIR/tmp/pids/delayed_job.pid`
kill -9 `cat $PARENTDIR/tmp/pids/passenger.3000.pid`
