require 'test_helper'

class ExpensesControllerTest < ActionController::TestCase
  setup do
    @expense = expenses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:expenses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create expense" do
    assert_difference('Expense.count') do
      post :create, expense: { commerce_number: @expense.commerce_number, cost_center_id: @expense.cost_center_id, currency_id: @expense.currency_id, description: @expense.description, payment_condition_id: @expense.payment_condition_id, receipt_date: @expense.receipt_date, receipt_number: @expense.receipt_number, receipt_number: @expense.receipt_number, work_order_id: @expense.work_order_id }
    end

    assert_redirected_to expense_path(assigns(:expense))
  end

  test "should show expense" do
    get :show, id: @expense
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @expense
    assert_response :success
  end

  test "should update expense" do
    patch :update, id: @expense, expense: { commerce_number: @expense.commerce_number, cost_center_id: @expense.cost_center_id, currency_id: @expense.currency_id, description: @expense.description, payment_condition_id: @expense.payment_condition_id, receipt_date: @expense.receipt_date, receipt_number: @expense.receipt_number, receipt_number: @expense.receipt_number, work_order_id: @expense.work_order_id }
    assert_redirected_to expense_path(assigns(:expense))
  end

  test "should destroy expense" do
    assert_difference('Expense.count', -1) do
      delete :destroy, id: @expense
    end

    assert_redirected_to expenses_path
  end
end
